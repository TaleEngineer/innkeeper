﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Order
{
	public Humanoid waiter;
	public FurnishingLibrary.HoldableID[] orderedHoldables;
	public Dictionary<FurnishingLibrary.HoldableID, int> ammountNeeded = new Dictionary<FurnishingLibrary.HoldableID, int>();
	public Table orderedFrom;
	public Party orderers;

	public bool hasBeenCalled;
	public bool isDrink;
	public bool hasBeenDelievered;

	public Holdable[] holdableToRetrieve = new Holdable[4];

	/// <summary>
	/// Set the specified orderer, waiter, orders and orderedFrom.
	/// </summary>
	/// <param name="orderer">Orderer.</param>
	/// <param name="waiter">Waiter.</param>
	/// <param name="orders">Orders.</param>
	/// <param name="orderedFrom">Ordered from.</param>
	public void Set(Humanoid _waiter, FurnishingLibrary.HoldableID[] orders, Table _orderedFrom, bool isDrinkOrder, Party _orderers)
	{
		waiter = _waiter;
		orderedFrom = _orderedFrom;
		orderedHoldables = orders;
		ammountNeeded = new Dictionary<FurnishingLibrary.HoldableID, int>();
		for(int i = 0; i < 4; i++)
		{
			if(ammountNeeded.ContainsKey(orders[i]))
			{
				ammountNeeded[orders[i]]++;
			}
			else
			{
				ammountNeeded.Add(orders[i], 1);
			}
		}
		isDrink = isDrinkOrder;
		orderers = _orderers;
	}

	/// <summary>
	/// Adds retrievable items from Kitchen Window.
	/// </summary>
	/// <param name="toRetrieve">To retrieve.</param>
	public void addRetrievable(Holdable toRetrieve, int slot)
	{
		if(!hasBeenCalled)
		{
			holdableToRetrieve[slot] = toRetrieve;
		}
	}

	/// <summary>
	/// Checks if retrievable items are left. True = items still remaint
	/// </summary>
	/// <returns><c>true</c>, if items left was checked, <c>false</c> otherwise.</returns>
	public bool checkItemsLeft()
	{
		for(int i = 0; i < 4; i++)
		{
			if(holdableToRetrieve[i] != null)
			{
				return true;
			}
		}
		return false;
	}

	public bool needSatisfied(int whoCheck)
	{

		if(isDrink)
		{
			if(orderers.Members[whoCheck].wantedDrink == FurnishingLibrary.HoldableID.Error)
			{
				return true;
			}
		}
		else
		{
			if(orderers.Members[whoCheck].wantedFood == FurnishingLibrary.HoldableID.Error)
			{
				return true;
			}
		}
		return false;
	}
}
