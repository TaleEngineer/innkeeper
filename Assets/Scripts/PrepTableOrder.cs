﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrepTableOrder
{
    public Recipe recipeToMake;
    public Recipe recipeClone;
    public List<FurnishingLibrary.HoldableID> completedBakeables = new List<FurnishingLibrary.HoldableID>();

    public void Set(Recipe toMake)
    {
        recipeToMake = toMake;
        recipeClone = new Recipe();
        recipeClone.Set(toMake.holdableID);
    }

    public void Set(FurnishingLibrary.HoldableID holdableID)
    {
        Recipe toAdd = new Recipe();
        toAdd.Set(holdableID);
        Set(toAdd);
    }
}
