﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wall : MonoBehaviour 
{
	public Transform mainTransform;

	public bool buildComplete = false;
	public float percentBuilt = 0;
	SpriteRenderer spriteRenderer;
	
	public List<Sprite> buildSprites = new List<Sprite>();

	public void buildObject(float buildPercentIncrease)
	{
		percentBuilt += buildPercentIncrease;
	}

	void Awake()
	{
		mainTransform = transform;
	}

	void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	/*void OnGUI()
	{
		if(percentBuilt > 0 && percentBuilt < 1)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);
			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
			GUI.color = new Color(0,0,0,0.75f);
			GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			
			Rect progressRect = new Rect(drawRect.x+1, drawRect.y+1, percentBuilt*(drawRect.width-2), drawRect.height-2);
			
			GUI.color = Color.white;
			
			GUI.DrawTexture(progressRect, MainMechanics.mechanics.progressMarker);
		}
	}*/
	
	// Update is called once per frame
	void Update () 
	{
		if(!buildComplete)
		{
			if(percentBuilt >= 1)
			{
				spriteRenderer.sprite = buildSprites[4];
			}
			else if(percentBuilt >= 0.75f)
			{
				spriteRenderer.sprite = buildSprites[3];
			}
			else if(percentBuilt >= 0.5f)
			{
				spriteRenderer.sprite = buildSprites[2];
			}
			else if(percentBuilt >= 0.25f)
			{
				spriteRenderer.sprite = buildSprites[1];
			}
			else
			{
				spriteRenderer.sprite = buildSprites[0];
			}
		}
		if(!buildComplete && percentBuilt >= 1)
		{
			GetComponent<BoxCollider2D>().enabled = true;
			buildComplete = true;
			MainMechanics.mechanics.wallsToBuild.Remove(this);
            MainMechanics.mechanics.changeMoney(-MainMechanics.mechanics.wallCost, transform.position);
            MainMechanics.mechanics.setGridPathing((Vector2) transform.position-new Vector2(1,1),(Vector2) transform.position+new Vector2(1,1));
		}
	}
}
