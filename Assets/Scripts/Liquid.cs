﻿using UnityEngine;
using System.Collections;

public class Liquid
{
	public string liquidName;
	public Color liquidColor;
	public float thirstQuench;
    public float tasteQuality;

	public FurnishingLibrary.HoldableID liquidID;

	public virtual void Set(FurnishingLibrary.HoldableID ID)
	{
		liquidID = ID;

		liquidName = FurnishingLibrary.Library.getItemName(ID, true);

		liquidColor = FurnishingLibrary.Library.getLiquidColor(ID);

		if(ID == FurnishingLibrary.HoldableID.LiqWater)
		{
			thirstQuench = 20f;
		}
		else if(ID == FurnishingLibrary.HoldableID.LiqYBeer || ID == FurnishingLibrary.HoldableID.LiqBlAle || ID == FurnishingLibrary.HoldableID.LiqWine)
		{
			thirstQuench = 30f;
            tasteQuality = 1;
		}
	}

	public virtual void ConsumptionEffect(Humanoid Consumer)
	{
		if(Consumer.wantedDrink == liquidID)
		{
			Consumer.thirst -= thirstQuench;
			Consumer.wantedDrink = FurnishingLibrary.HoldableID.Error;
            if((Consumer.thisNPCType == Humanoid.NPCType.Civilian || Consumer.thisNPCType == Humanoid.NPCType.Adventurer) && Consumer.tableSittingAt != null)
            {
                Consumer.tableSittingAt.moneyToGive += FurnishingLibrary.Library.getHoldablePrefab(liquidID).cost;
                Consumer.tableSittingAt.fameToGive += (int)tasteQuality;
                Consumer.timeSinceOrderedDrinkMultiplier = 0;
                //MainMechanics.mechanics.moneyChange(FurnishingLibrary.Library.getHoldablePrefab(liquidID).cost, Consumer.mainTransform.position);
            }
		}
	}
}
