﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinder
{
	bool whoCares; //Pointless but needed
	Dictionary<PathNode, bool> destinations;
	List<Node> openList;
	List<Node> closedList;
	Node currentNode;
	Node startNode;
	Node destinatioNode;
	//List<Node> multiDestinationNode;
	
	//int movementCost = 5;
	
	bool pathFound;
	
	class Node
	{
		public PathNode pathNode;
		public Node parentNode;
		public float gValue;
		public float hValue;
		public float fValue;
		
		public void Set(PathNode node, float g_Value, float h_Value, Node parent)
		{
			pathNode = node;
			parentNode = parent;
			gValue = g_Value;
			hValue = h_Value;
			fValue = hValue+gValue;
		}
	}
	
	public List<PathNode> pathfind(PathNode closestNode, Dictionary<PathNode, bool> targetNodes, Humanoid toFindFor)
	{
		for(int i = 0; i < closestNode.Neighbors.Count; i++)
		{
			if(targetNodes.TryGetValue(closestNode.Neighbors[i], out whoCares))
			{
				List<PathNode> toReturn = new List<PathNode>();
				toReturn.Add(closestNode.Neighbors[i]);
				return toReturn;
			}
		}
		
		pathFound = false;
		
		if(targetNodes.TryGetValue(closestNode, out whoCares) || targetNodes.Count == 0)
		{
			return new List<PathNode>();
		}
		
		startNode = new Node();
		destinations = targetNodes;
		openList = new List<Node>();
		closedList = new List<Node>();
		
		startNode.pathNode = closestNode;
		currentNode = startNode;
		
		
		openList.Add(currentNode);
		
		for(int i = 0; i < 2000; i+= 0)
		{
			bool weStuckYo = false;
			if (pathFound == false)
			{
				weStuckYo = findPath();
			}
			else if (pathFound == true)
			{
				currentNode = destinatioNode;
				return traceBack(toFindFor);
			}
			
			if (weStuckYo == true)
			{
				return new List<PathNode>();
			}
		}
		
		return null;
	}
	
	bool findPath()
	{
		closedList.Add(currentNode);
		openList.Remove(currentNode);
		
		for(int i = 0; i < currentNode.pathNode.Neighbors.Count; i++)
		{
			if(destinations.TryGetValue(currentNode.pathNode.Neighbors[i], out whoCares))
			{
				pathFound = true;
				destinatioNode = new Node();
				destinatioNode.pathNode = currentNode.pathNode.Neighbors[i];
				destinatioNode.parentNode = currentNode;
				return false;
			}
			
			checkNodeDirection(currentNode.pathNode.Neighbors[i]);
		}
		
		if (openList.Count > 0)
		{
			currentNode = openList[0];
			for(int i = 0; i < openList.Count; i++)
			{
				if(openList[i].fValue < currentNode.fValue)
				{
					currentNode = openList[i];
				}
			}
			return false;
		}
		
		return true;
	}
	
	void checkNodeDirection(PathNode nodeToCheck)
	{
		bool inOpenList = false;
		bool inClosedList = false;

		float movementCost = Vector3.Distance(currentNode.pathNode.mainTransform.position, nodeToCheck.mainTransform.position)
			*((10f/currentNode.pathNode.gridHere.speedMultiplier)+(10f/nodeToCheck.gridHere.speedMultiplier));
		
		for (int i = 0; i < openList.Count; i++)
		{
			if (nodeToCheck == openList[i].pathNode)
			{
				inOpenList = true;
				if (openList[i].gValue > currentNode.gValue + movementCost)
				{
					openList[i].gValue = currentNode.gValue + movementCost;
					openList[i].parentNode = currentNode;
				}
				break;
			}
		}
		if (inOpenList == false)
		{
			for (int i = 0; i < closedList.Count; i++)
			{
				if (closedList[i].pathNode == nodeToCheck)
				{
					inClosedList = true;
					break;
				}
			}
			if (inClosedList == false)
			{
				Node node = new Node();
				List<PathNode> keys = new List<PathNode>(destinations.Keys);
				float distance = 100000;
				for(int i = 0; i < keys.Count; i++)
				{
					float tempDist = Vector3.Distance(nodeToCheck.mainTransform.position, keys[i].mainTransform.position);
					if(tempDist < distance)
					{
						distance = tempDist;
					}
				}
				node.Set(nodeToCheck, movementCost + currentNode.gValue, distance, currentNode);
				openList.Add(node);
			}
		}
	}
	
	
	List<PathNode> traceBack(Humanoid toTreadHere)
	{
		List<PathNode> destinations = new List<PathNode>();
		for (int i = 0; i < 1; )
		{
			if (currentNode.parentNode != null)
			{
				destinations.Add(currentNode.pathNode);
                currentNode.pathNode.toTreadHere.Add(toTreadHere);
				currentNode = currentNode.parentNode;
			}
			else
			{
				destinations.Reverse();
				return destinations;
			}
		}
		return null;
	}





















	/*public List<PathNode> pathfind(PathNode closestNode, List<PathNode> targetNode)
	{
		for(int n = 0; n < closestNode.Neighbors.Count; n++)
		{
			for(int t = 0; t < targetNode.Count; t++)
			{
				if(targetNode[t] == closestNode.Neighbors[n])
				{
					List<PathNode> toReturn = new List<PathNode>();
					toReturn.Add(targetNode[t]);
					return toReturn;
			}
			}
		}
		
		pathFound = false;
		
		if(targetNode.Contains(closestNode) || targetNode == null)
		{
			return new List<PathNode>();
		}
		
		startNode = new Node();
		multiDestinationNode = new List<Node>();
		openList = new List<Node>();
		closedList = new List<Node>();
		
		startNode.pathNode = closestNode;
		for(int i = 0; i < targetNode.Count; i++)
		{
			Node nodeToAdd = new Node();
			nodeToAdd.pathNode = targetNode[i];
			multiDestinationNode.Add (nodeToAdd);
		}
		currentNode = startNode;
		
		
		openList.Add(currentNode);
		
		for(int i = 0; i < 2000; i+= 0)
		{
			bool weStuckYo = false;
			if (pathFound == false)
			{
				weStuckYo = findMultiPath();
			}
			else if (pathFound == true)
			{
				return traceBack();
			}
			
			if (weStuckYo == true)
			{
				return new List<PathNode>();
			}
		}
		
		return null;
	}
	
	bool findMultiPath()
	{
		closedList.Add(currentNode);
		openList.Remove(currentNode);
		
		for(int n = 0; n < currentNode.pathNode.Neighbors.Count; n++)
		{
			for(int t = 0; t < multiDestinationNode.Count; t++)
			{
				if(currentNode.pathNode.Neighbors[n] == multiDestinationNode[t].pathNode)
				{
					pathFound = true;
					multiDestinationNode[t].parentNode = currentNode;
					currentNode = multiDestinationNode[t];
					return false;
				}
			}
			
			checkMultiNodeDirection(currentNode.pathNode.Neighbors[n]);
		}
		
		if (openList.Count > 0)
		{
			currentNode = openList[0];
			for(int i = 0; i < openList.Count; i++)
			{
				if(openList[i].fValue < currentNode.fValue)
				{
					currentNode = openList[i];
				}
			}
			return false;
		}
		
		return true;
	}
	
	void checkMultiNodeDirection(PathNode nodeToCheck)
	{
		bool inOpenList = false;
		bool inClosedList = false;
		
		if(nodeToCheck.gridHere == null)
		{
			Debug.Log("????");
		}
		
		float movementCost = Vector3.Distance(currentNode.pathNode.transform.position, nodeToCheck.transform.position)
			*((10f/currentNode.pathNode.gridHere.speedMultiplier)+(10f/nodeToCheck.gridHere.speedMultiplier));
		
		for (int i = 0; i < openList.Count; i++)
		{
			if (nodeToCheck == openList[i].pathNode)
			{
				inOpenList = true;
				if (openList[i].gValue > currentNode.gValue + movementCost)
				{
					openList[i].gValue = currentNode.gValue + movementCost;
					openList[i].parentNode = currentNode;
				}
				break;
			}
		}
		if (inOpenList == false)
		{
			for (int i = 0; i < closedList.Count; i++)
			{
				if (closedList[i].pathNode == nodeToCheck)
				{
					inClosedList = true;
					break;
				}
			}
			if (inClosedList == false)
			{
				float closestDistance = 10000;

				for(int i = 0; i < multiDestinationNode.Count; i++)
				{
					float distanceCheck = Vector3.Distance(nodeToCheck.transform.position,multiDestinationNode[i].pathNode.transform.position);
					if(closestDistance > distanceCheck)
					{
						closestDistance = distanceCheck;
					}
				}

				Node node = new Node();
				node.Set(nodeToCheck, movementCost + currentNode.gValue, closestDistance, currentNode);
				openList.Add(node);
			}
		}
	}*/
	
}
