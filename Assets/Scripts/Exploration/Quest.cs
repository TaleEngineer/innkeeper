﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Quest
{
	public Dungeon dungeon;
	public float travelTime;
	public float setUpTime;

	public int rewardGold;
	public List<Equipment> equipmentReward = new List<Equipment>();
	Area.DungeonSetting area;

	public bool activeOnBoard = false;

	public Monster.MonsterType monster;

	public int totalXpAccumulated = 0;

	public void Set(int level, Area.DungeonSetting _setting, float _setUpTime, float _travelTime, Monster.MonsterType _monster)
	{
		area = _setting;
		dungeon = new Dungeon();
		travelTime = 30*level;
		monster = _monster;
		setUpTime = _setUpTime;

		for(int i = 0; i < (level/4)+1; i++)
		{
			Equipment toAdd = MainMechanics.equipmentFactory.getEquipment(level);
			equipmentReward.Add(toAdd);
			toAdd.gameObject.SetActive(false);
		}

		dungeon.Start(level, _setting, _monster);
	}

    public void Set(int level)
    {
        area = Area.DungeonSetting.Roads;
        dungeon = new Dungeon();
        travelTime = 0;
        setUpTime = 0;

        activeOnBoard = true;

        dungeon.Start(level);
    }


    public void OffloadReward()
	{
		MainMechanics.boarderingArea[dungeon.setting].addLostLoot(equipmentReward);
		equipmentReward.Clear();
	}

	public void Offload()
	{
		MainMechanics.boarderingArea[dungeon.setting].addLostMonsters(dungeon);
		OffloadReward();
        if(dungeon.setting == Area.DungeonSetting.Roads)
        {
            MainMechanics.mechanics.calculateMerchantCost();
        }
	}

	public void getReward(List<Humanoid> party)
	{
		int rewards = equipmentReward.Count;
		int personalXP = totalXpAccumulated/party.Count;
		for(int i = 0; i < rewards; i++)
		{
			if(MainMechanics.boarderingArea[area].lostLoot.Count > 0)
			{
				equipmentReward.Add(MainMechanics.boarderingArea[area].takeLostLoot());
			}
		}
		for(int i = 0; i <party.Count; i++)
		{
			party[i].giveCombatXP(personalXP);

			if(party[i].GetComponent<Adventurer>())
			{
				Adventurer toReward = (Adventurer)party[i];
				equipmentReward = equipmentReward.OrderByDescending(e => e.bonusStats[toReward.secondaryStat]).ToList();
				equipmentReward = equipmentReward.OrderByDescending(e => e.bonusStats[toReward.mainStat]).ToList();

				bool weaponEquipped = false;
				bool armorEquipped = false;

				for(int r = 0; r < equipmentReward.Count; r++)
				{
					if(equipmentReward[r].canEquip(toReward.chosenClass))
					{
						if(equipmentReward[r].type == Equipment.EquipmentType.Armor && !armorEquipped)
						{
							armorEquipped = true;
							//Debug.Log(toReward.armor.name+" has been added to the loot pile");
							equipmentReward.Add(toReward.armor);
							toReward.equipItem(equipmentReward[r]);
							//Debug.Log(equipmentReward[r].name+" has been removed from the loot pile");
							equipmentReward.RemoveAt(r);
							r--;
							//Debug.Log(toReward.name+" has upgraded his armor to "+toReward.armor.name);
						}
						else if(!weaponEquipped)
						{
							weaponEquipped = true;
							//Debug.Log(toReward.weapon.name+" has been added to the loot pile");
							equipmentReward.Add(toReward.weapon);
                            toReward.weapon.mainTransform.parent = null;

							toReward.equipItem(equipmentReward[r]);
							//Debug.Log(equipmentReward[r].name+" has been removed from the loot pile");
							equipmentReward.RemoveAt(r);
							r--;
							//Debug.Log(toReward.name+" has upgraded his weapon to "+toReward.weapon.name);
						}

					}

					if(weaponEquipped && armorEquipped)
					{
						break;
					}
				}
			}
		}

		Offload();
	}
}
