﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdventurerParty : Party 
{
	public Quest questEmbarkedOn;
	public float returnTimer;
    public List<Humanoid> hitTable = new List<Humanoid>();

    public override void setHitTable()
    {
        hitTable.Clear();
        for(int i = 0; i < Members.Count; i++)
        {
            int timesToAdd = 1;
            if(Members[i].chosenClass == Humanoid.CharacterClass.Cleric)
            {
                timesToAdd = 2;
            }
            else if(Members[i].chosenClass == Humanoid.CharacterClass.Paladin)
            {
                timesToAdd = 3;
            }
            else if(Members[i].chosenClass == Humanoid.CharacterClass.Warrior)
            {
                timesToAdd = 5;
            }

            for(int t = 0; t < timesToAdd; t++)
            {
                hitTable.Add(Members[i]);
            }
        }
    }


    public override void offScreenUpdate ()
	{
		base.offScreenUpdate();
		returnTimer -= Time.deltaTime;
		if(questEmbarkedOn != null)
		{
			if(Members.Count > 0)
			{
				questEmbarkedOn.dungeon.Update(this);
				if(questEmbarkedOn.dungeon.rooms.Count <= 0)
				{
					questEmbarkedOn.getReward(Members);
					questEmbarkedOn = null;
				}
			}
			else
			{
				questEmbarkedOn.Offload();
				MainMechanics.mechanics.AdventurerParties.Remove(this);
				questEmbarkedOn = null;
			}
		}
	}

	public void setQuest(Quest toSet)
	{
		questEmbarkedOn = toSet;
		returnTimer = toSet.travelTime;
	}

}
