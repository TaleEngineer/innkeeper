﻿using UnityEngine;
using System.Collections;

public class DungeonRoom
{
	public DungeonEncounter encounter;
	public DungeonTrap trap;

	// Use this for initialization
	public void Set (int level, Area.DungeonSetting setting, Monster.MonsterType monsterType) 
	{
		trap = new DungeonTrap();
		trap.Set (15+level, 1+(level/4));

		encounter = new DungeonEncounter();
		encounter.Set(level, setting, monsterType);
	}

    //For Exterminating area inhabitants
    public void Set(int level, Area.DungeonSetting setting)
    {
        if (setting != Area.DungeonSetting.Roads)
        {
            trap = new DungeonTrap();
            trap.Set(15 + level, 1 + (level / 4));
        }
        
        encounter = new DungeonEncounter();
        encounter.Set(level, setting);
    }


    // Update is called once per frame
    public void Update (AdventurerParty party) 
	{
		if(trap != null)
		{
			trap.DisarmTrap(party);
			trap = null;
		}
		else if(encounter != null)
		{
			encounter.Update (party);
			if(encounter.monsterParty.Count <= 0)
			{
				encounter = null;
			}
		}
	}
}
