﻿using UnityEngine;
using System.Collections;

public class Adventurer : Humanoid
{
	public Equipment armor;
	public Equipment weapon;

	public override void Awake ()
	{
		base.Awake ();

		MainMechanics.equipmentFactory.EquipAdventurer(this);

		calculateCurrentCombatStats();

		/*Debug.Log(chosenClass.ToString()+"\n| Str: "+baseCharaStats[CharacterStatistics.Str]+" |"+currentCharaStats[CharacterStatistics.Str]
		          +"\n| Dex: "+baseCharaStats[CharacterStatistics.Dex]+" |"+currentCharaStats[CharacterStatistics.Dex]
		          +"\n| Int: "+baseCharaStats[CharacterStatistics.Int]+" |"+currentCharaStats[CharacterStatistics.Int]
		          +"\n| HP: "+currentHP+"/"+currentMaxHP);*/
	}

	public override void OnEnable ()
	{
		base.OnEnable ();

		if(armor != null)
		{
			armor.gameObject.SetActive(true);
		}
		if(weapon != null)
		{
			weapon.gameObject.SetActive(true);
		}
	}

	public override void Update ()
	{
		base.Update ();

		if(armor != null)
		{
			armor.transform.position = transform.position+new Vector3(0, 0,-0.001f);
			armor.transform.rotation = transform.rotation;
			armor.spriteRenderer.sortingOrder = spriteRenderer.sortingOrder;
		}
		if(weapon != null)
		{
			if(weapon.placementType == Equipment.PlacementType.Front)
			{
				weapon.transform.position = transform.position+new Vector3(0, 0,-0.002f);
			}
			else
			{
				weapon.transform.position = transform.position+new Vector3(0, 0, 0.002f);
			}
			weapon.transform.rotation = transform.rotation;
			weapon.spriteRenderer.sortingOrder = spriteRenderer.sortingOrder;
		}
	}

	public void equipItem(Equipment toEquip)
	{
		bool statChange = false;
		if(toEquip.type == Equipment.EquipmentType.Armor && toEquip.canEquip(chosenClass))
		{
			statChange = true;
			armor = toEquip;
            armor.mainTransform.parent = mainTransform;
		}
		else if(toEquip.type == Equipment.EquipmentType.Weapon && toEquip.canEquip(chosenClass))
		{
			weapon = toEquip;
            weapon.mainTransform.parent = mainTransform;
            statChange = true;
		}
		if(statChange)
		{
			calculateCurrentCombatStats();
		}
	}

	public override void calculateCurrentCombatStats()
	{
		base.calculateCurrentCombatStats();

		if(armor != null)
		{
			currentCharaStats[CharacterStatistics.Str]+= armor.bonusStats[CharacterStatistics.Str];
			currentCharaStats[CharacterStatistics.Dex]+= armor.bonusStats[CharacterStatistics.Dex];
			currentCharaStats[CharacterStatistics.Int]+= armor.bonusStats[CharacterStatistics.Int];
		}

		if(weapon != null)
		{
			currentCharaStats[CharacterStatistics.Str]+= weapon.bonusStats[CharacterStatistics.Str];
			currentCharaStats[CharacterStatistics.Dex]+= weapon.bonusStats[CharacterStatistics.Dex];
			currentCharaStats[CharacterStatistics.Int]+= weapon.bonusStats[CharacterStatistics.Int];
		}
	}

	public override void ExitArea ()
	{
		base.ExitArea ();

		
		if(armor != null)
		{
			armor.gameObject.SetActive(false);
		}
		if(weapon != null)
		{
			weapon.gameObject.SetActive(false);
		}
	}

	public void dropEquipment(bool dropWeapon)
	{
		if( dropWeapon && weapon != null)
		{
			currentGrid.holdablesHere.Add(weapon);
			MainMechanics.mechanics.addHoldableToGround(weapon);
			weapon.transform.position = currentGrid.transform.position;
			weapon.GetComponent<SpriteRenderer>().sortingLayerName = "ItemOnFloor";
            weapon.mainTransform.parent = null;
			weapon = null;
		}
		else if(!dropWeapon && armor != null)
		{
			currentGrid.holdablesHere.Add(armor);
			MainMechanics.mechanics.addHoldableToGround(armor);
			armor.transform.position = currentGrid.transform.position;
			armor.GetComponent<SpriteRenderer>().sortingLayerName = "ItemOnFloor";
            armor.mainTransform.parent = null;
            armor = null;
		}
	}

	public override void killHumanoid ()
	{
		base.killHumanoid ();
		if(gameObject.activeInHierarchy)
		{
			dropEquipment(true);
			dropEquipment(false);
		}
		else
		{
			if(partyApartOf != null)
			{
				AdventurerParty party = (AdventurerParty)partyApartOf;
				if(party.questEmbarkedOn != null)
				{
					party.questEmbarkedOn.equipmentReward.Add(weapon);
					party.questEmbarkedOn.equipmentReward.Add(armor);
                    weapon.mainTransform.parent = null;
                    armor.mainTransform.parent = null;
                    weapon = null;
					armor = null;
				}
				else
				{
					Destroy(weapon.gameObject);
					Destroy(armor.gameObject);
					weapon = null;
					armor = null;
				}
			}
			else
			{
				Destroy(weapon.gameObject);
				Destroy(armor.gameObject);
				weapon = null;
				armor = null;
			}
		}
	}
}
