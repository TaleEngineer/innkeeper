﻿using UnityEngine;
using System.Collections;

public class Monster
{
	int currentHealth;
	int maxHealth;
	int damage;
	int xp;
    public int level;
	public enum MonsterType
	{
		Rat,
		Wolf,
		Bear,
		Skeleton,
		Zombie,
		Kobold,
		Goblin,
		Bandit,
		Troll,
		Ogre,
		Dragon,
		Lich
	}
	public MonsterType monsterType;

	public void Set(int health, int DMG, int xpValue, int lvl, MonsterType type)
	{
		maxHealth = health;
		currentHealth = maxHealth;
		damage = DMG;
		xp = xpValue;
		monsterType = type;
        level = lvl;
	}

	public void takeTurn(AdventurerParty enemies)
	{
		if(enemies.Members.Count > 0)
        {
            int toHit = Random.Range(0, enemies.hitTable.Count);
            if (Random.Range(1, 21) < 15+level-enemies.hitTable[toHit].combatLevel)
            {
                //Debug.Log(monsterType.ToString() + " deals " + damage + " to " + enemies.hitTable[toHit].name);
                enemies.hitTable[toHit].takeDamage(damage);
            }
            /*else
            {
                Debug.Log(monsterType.ToString() + " missed");
            }*/
		}
	}

	public void takeDamage(int damage, DungeonEncounter encounter, Quest quest)
	{
		currentHealth -= damage;
		if(currentHealth <= 0)
		{
			//Debug.Log(""+monsterType+" Slain");
			quest.totalXpAccumulated += xp;
			encounter.monsterParty.Remove(this);
            MainMechanics.boarderingArea[quest.dungeon.setting].monstersInArea--;
		}
	}
}
