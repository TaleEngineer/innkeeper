﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DungeonEncounter
{
	public List<Monster> monsterParty = new List<Monster>();
	public bool isMonsterTurn = false;

	public void Set(int level, Area.DungeonSetting setting, Monster.MonsterType monsterType)
	{
		for(int i = 0; i < 4; i++)
		{
			Monster toAdd = MainMechanics.boarderingArea[setting].takeLostMonster(Random.Range(0,100));

			if(toAdd == null)
			{
				toAdd = new Monster();
				Monster.MonsterType type;
				type = monsterType;
				toAdd.Set (level*5, level*2, level, level, type);

                MainMechanics.boarderingArea[setting].monstersInArea++;
            }
			else
			{
				Debug.Log(toAdd.monsterType+" has invaded a dungeon!");
			}
			monsterParty.Add(toAdd);
		}
	}

    public void Set(int level, Area.DungeonSetting setting)
    {
        for (int i = 0; i < 4; i++)
        {
            if (MainMechanics.boarderingArea[setting].lostMonsters.Count > 0)
            {
                Monster toAdd = MainMechanics.boarderingArea[setting].takeLostMonster(Random.Range(0, MainMechanics.boarderingArea[setting].lostMonsters.Count));
                monsterParty.Add(toAdd);
            }
            else
            {
                return;
            }
        }
    }

    public void Update(AdventurerParty party)
	{
		if(isMonsterTurn)
		{
			for(int i = 0; i < monsterParty.Count; i++)
			{
				monsterParty[i].takeTurn(party);
			}
		}
		else
		{
			for(int i = 0; i < party.Members.Count; i++)
			{
				party.Members[i].takeDungeonTurn(this);
			}
		}

		isMonsterTurn = !isMonsterTurn;
	}
}
