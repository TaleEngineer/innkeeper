﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dungeon
{
	public int dungeonLevel;
	public List<DungeonRoom> rooms = new List<DungeonRoom>();
	public Area.DungeonSetting setting;
	// Use this for initialization
	public void Start (int level, Area.DungeonSetting _setting, Monster.MonsterType monsterType) 
	{
		setting = _setting;

		dungeonLevel = level;
		int numberOfRooms = 3 + Random.Range(0,5)+(level/2);
		for(int i = 0; i < numberOfRooms; i++)
		{
			DungeonRoom toAdd = new DungeonRoom();
			toAdd.Set (dungeonLevel, setting, monsterType);
			rooms.Add(toAdd);
		}
		//Debug.Log(""+numberOfRooms+" Rooms Set");
	}


    public void Start(int level)
    {
        setting = Area.DungeonSetting.Roads;

        dungeonLevel = level;
        int numberOfRooms = 3 + Random.Range(0, 5) + (level / 2);
        for (int i = 0; i < numberOfRooms; i++)
        {
            DungeonRoom toAdd = new DungeonRoom();
            toAdd.Set(dungeonLevel, setting);
            rooms.Add(toAdd);
        }
        //Debug.Log(""+numberOfRooms+" Rooms Set");
    }


    // Update is called once per frame
    public void Update (AdventurerParty party) 
	{
		if(rooms.Count > 0)
		{
			rooms[0].Update(party);
			if(rooms[0].trap == null && rooms[0].encounter == null)
			{
				rooms.RemoveAt (0);
			}
		}
	}
}
