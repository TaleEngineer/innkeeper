﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Area
{
	public enum DungeonSetting
	{
		Inn,
		Roads,
		Forest,
		Mountain,
		Cave,
		Ruins,
		Crypt
	}
	public DungeonSetting thisSetting;

	public List<Equipment> lostLoot = new List<Equipment>();
	public List<Monster> lostMonsters = new List<Monster>();

    public int levelAverage = 0;
    public int monstersInArea = 0;

	/// <summary>
	/// Adds the lost monsters from a dungeon.
	/// </summary>
	/// <param name="toAddFrom">To add from.</param>
	public void addLostMonsters(Dungeon toAddFrom)
	{
		for(int i = 0; i < toAddFrom.rooms.Count; i++)
		{
			for(int m = 0; m < toAddFrom.rooms[i].encounter.monsterParty.Count; m++)
			{
				//Debug.Log(toAddFrom.rooms[i].encounter.monsterParty[m].monsterType+" has been offloaded to "+thisSetting);
				lostMonsters.Add(toAddFrom.rooms[i].encounter.monsterParty[m]);
			}
		}

		if(lostMonsters.Count > 100)
		{
			while(lostMonsters.Count > 100)
			{
				moveLostMonster();
			}
            MainMechanics.mechanics.calculateMerchantCost();
        }
        calculateLevelAverage();
	}

	/// <summary>
	/// Adds a lost monsters.
	/// </summary>
	/// <param name="toAddFrom">To add from.</param>
	public void addLostMonsters(Monster toAddFrom)
	{
        monstersInArea++;
		//Debug.Log(toAddFrom+" has been transfered to "+thisSetting);
		lostMonsters.Add(toAddFrom);
        calculateLevelAverage();
	}

	/// <summary>
	/// Returns a lost monster and removes it from the list.
	/// </summary>
	/// <returns>The lost monster.</returns>
	/// <param name="roll">Roll.</param>
	public Monster takeLostMonster(int roll)
	{
		if(roll < lostMonsters.Count)
		{
			Monster toReturn;
			toReturn = lostMonsters[roll];
			lostMonsters.RemoveAt(roll);
			return toReturn;
		}
		return null;
	}

	/// <summary>
	/// Adds the lost loot to the area's lost loot list.
	/// </summary>
	/// <param name="lostItems">Lost items.</param>
	public void addLostLoot (List<Equipment> lostItems)
	{
		for(int i = 0; i < lostItems.Count; i++)
		{
			//Debug.Log(lostItems[i].name+" has been offloaded to "+thisSetting);
			lostLoot.Add(lostItems[i]);
		}
	}

	/// <summary>
	/// Removes item from the lost loot table and returns it.
	/// </summary>
	/// <returns>The lost loot.</returns>
	public Equipment takeLostLoot()
	{
		int rand = Random.Range(0, lostLoot.Count);
		Equipment toReturn = lostLoot[rand];
		lostLoot.RemoveAt(rand);
		return toReturn;
	}

	public void moveLostMonster()
	{
		int rand = Random.Range(0, lostMonsters.Count);
		Monster toTransfer = takeLostMonster(rand);

        monstersInArea--;

		if(thisSetting == DungeonSetting.Forest)
		{
			if(MainMechanics.boarderingArea[DungeonSetting.Mountain].lostMonsters.Count < 100)
			{
				MainMechanics.boarderingArea[DungeonSetting.Mountain].addLostMonsters(toTransfer);
			}
			else
			{
				MainMechanics.boarderingArea[DungeonSetting.Roads].addLostMonsters(toTransfer);
			}
		}
		else if(thisSetting == DungeonSetting.Crypt)
		{
			if(MainMechanics.boarderingArea[DungeonSetting.Ruins].lostMonsters.Count < 100)
			{
				MainMechanics.boarderingArea[DungeonSetting.Ruins].addLostMonsters(toTransfer);
			}
			else
			{
				MainMechanics.boarderingArea[DungeonSetting.Roads].addLostMonsters(toTransfer);
			}
		}
		else if(thisSetting != DungeonSetting.Roads)
		{
			int whereGo = Random.Range(0,2);
			if((whereGo == 0 || MainMechanics.boarderingArea[thisSetting-1].lostMonsters.Count < 100) 
			   		&& MainMechanics.boarderingArea[thisSetting+1].lostMonsters.Count < 100)
			{
				MainMechanics.boarderingArea[thisSetting+1].addLostMonsters(toTransfer);
			}
			else if(MainMechanics.boarderingArea[thisSetting-1].lostMonsters.Count < 100)
			{
				MainMechanics.boarderingArea[thisSetting-1].addLostMonsters(toTransfer);
			}
			else
			{
				MainMechanics.boarderingArea[DungeonSetting.Roads].addLostMonsters(toTransfer);
			}
		}
        else if(thisSetting == DungeonSetting.Roads)
        {

        }
	}

    public void calculateLevelAverage()
    {
        if(lostMonsters.Count <= 0)
        {
            levelAverage = 0;
            return;
        }
        float total = 0;
        for(int i = 0; i < lostMonsters.Count; i++)
        {
            total += lostMonsters[i].level;
        }
        levelAverage = (int)Mathf.Round(total / lostMonsters.Count);
    }
}
