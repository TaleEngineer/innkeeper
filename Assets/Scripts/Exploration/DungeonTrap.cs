using UnityEngine;
using System.Collections;

public class DungeonTrap
{
	int difficulty;
	int damage;

	public void Set(int diff, int DMG)
	{
		difficulty = diff;
		damage = DMG;
	}

	public void DisarmTrap(AdventurerParty attempters)
	{
		bool success = false;
		for(int i = 0; i < attempters.Members.Count; i++)
		{
			if(attempters.Members[i].currentCharaStats[Humanoid.CharacterStatistics.Dex]+Random.Range(1,11) >= difficulty)
			{
				//Debug.Log("Trap disarmed by "+attempters.Members[i].name);
				success = true;
				break;
			}
		}
		if(!success)
		{
			for(int i = 0; i < attempters.Members.Count; i++)
			{
				attempters.Members[i].takeDamage(damage);
			}
			//Debug.Log("Trap Sprung");
		}
	}
}
