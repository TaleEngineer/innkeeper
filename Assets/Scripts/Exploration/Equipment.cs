﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Equipment : Holdable 
{
	public enum EquipmentType
	{
		Error,
		Armor,
		Weapon
	}
	public enum WeaponType
	{
		Error,
		Sword,
		Bow,
		Mace,
		Daggers,
		Staff
	}
	public enum ArmorType
	{
		Error,
		Cloth,
		Leather,
		Plate

	}
	public enum PlacementType
	{
		Error,
		Front,
		Back
	}

	public EquipmentType type;
	public WeaponType weaponType;
	public ArmorType armorType;
	public PlacementType placementType;

	public Dictionary<Humanoid.CharacterStatistics, int> bonusStats = new Dictionary<Humanoid.CharacterStatistics, int>();

    public override void Start()
    {
        base.Start();
        if (type == EquipmentType.Armor)
        {
            name = armorType.ToString();
        }
        else
        {
            name = weaponType.ToString();
        }
    }

    public bool canEquip(Humanoid.CharacterClass classToCheck)
	{
		if(classToCheck == Humanoid.CharacterClass.Archer)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Leather)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Bow)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(classToCheck == Humanoid.CharacterClass.Cleric)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Plate)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Mace)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(classToCheck == Humanoid.CharacterClass.Paladin)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Plate)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Sword)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(classToCheck == Humanoid.CharacterClass.Thief)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Leather)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Daggers)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(classToCheck == Humanoid.CharacterClass.Warrior)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Plate)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Sword)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(classToCheck == Humanoid.CharacterClass.Wizard)
		{
			if(type == EquipmentType.Armor && armorType == ArmorType.Cloth)
			{
				return true;
			}
			else if(type == EquipmentType.Weapon && weaponType == WeaponType.Staff)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	public void Set(int level, float strMod, float dexMod, float intMod, Equipment.WeaponType wepType)
	{
		float strMulti = strMod*5*level;
		float dexMulti = dexMod*5*level;
		float intMulti = intMod*5*level;
		
		bonusStats.Add(Humanoid.CharacterStatistics.Str, (int)strMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Dex, (int)dexMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Int, (int)intMulti);

		type = EquipmentType.Weapon;
		weaponType = wepType;

		GetComponent<SpriteRenderer>().sprite = MainMechanics.equipmentFactory.getTexture(weaponType);
		if(weaponType == WeaponType.Bow || weaponType == WeaponType.Staff || weaponType == WeaponType.Sword || weaponType == WeaponType.Mace)
		{
			placementType = PlacementType.Back;
		}
		else
		{
			placementType = PlacementType.Front;
		}
	}

	public void Set(int level, float strMod, float dexMod, float intMod, Equipment.ArmorType armType)
	{
		float strMulti = strMod*5*level;
		float dexMulti = dexMod*5*level;
		float intMulti = intMod*5*level;
		
		bonusStats.Add(Humanoid.CharacterStatistics.Str, (int)strMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Dex, (int)dexMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Int, (int)intMulti);
		
		type = EquipmentType.Armor;
		armorType = armType;

		GetComponent<SpriteRenderer>().sprite = MainMechanics.equipmentFactory.getTexture(armorType);
		placementType = PlacementType.Front;
	}

	public void Set(int level)
	{
		float strMulti = Random.Range(0.9f, 1.2f)*5*level;
		float dexMulti = Random.Range(0.9f, 1.2f)*5*level;
		float intMulti = Random.Range(0.9f, 1.2f)*5*level;

		bonusStats.Add(Humanoid.CharacterStatistics.Str, (int)strMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Dex, (int)dexMulti);
		bonusStats.Add(Humanoid.CharacterStatistics.Int, (int)intMulti);

		int whatIs = Random.Range(0,2);
		if(whatIs == 0)
		{
			type = EquipmentType.Weapon;
			if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				int typeToBe = Random.Range (0,5);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Bow;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Daggers;
				}
				else if(typeToBe == 2)
				{
					weaponType = WeaponType.Mace;
				}
				else if(typeToBe == 3)
				{
					weaponType = WeaponType.Staff;
				}
				else if(typeToBe == 4)
				{
					weaponType = WeaponType.Sword;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Bow;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Sword;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Str])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Daggers;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Staff;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Mace;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Sword;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				weaponType = WeaponType.Sword;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Bow;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Daggers;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					weaponType = WeaponType.Mace;
				}
				else if(typeToBe == 1)
				{
					weaponType = WeaponType.Staff;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				weaponType = WeaponType.Sword;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Dex] < bonusStats[Humanoid.CharacterStatistics.Int])
			{
				weaponType = WeaponType.Sword;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Str] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				weaponType = WeaponType.Bow;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] < bonusStats[Humanoid.CharacterStatistics.Int])
			{
				weaponType = WeaponType.Daggers;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Str] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				weaponType = WeaponType.Mace;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] < bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				weaponType = WeaponType.Staff;
			}

		}
		else
		{
			type = EquipmentType.Armor;
			if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				int typeToBe = Random.Range (0,3);
				if(typeToBe == 0)
				{
					armorType = ArmorType.Cloth;
				}
				else if(typeToBe == 1)
				{
					armorType = ArmorType.Leather;
				}
				else if(typeToBe == 2)
				{
					armorType = ArmorType.Plate;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					armorType = ArmorType.Plate;
				}
				else if(typeToBe == 1)
				{
					armorType = ArmorType.Leather;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Str])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					armorType = ArmorType.Leather;
				}
				else if(typeToBe == 1)
				{
					armorType = ArmorType.Cloth;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				armorType = ArmorType.Plate;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Plate;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Leather;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] == bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				int typeToBe = Random.Range (0,2);
				if(typeToBe == 0)
				{
					armorType = ArmorType.Plate;
				}
				else if(typeToBe == 1)
				{
					armorType = ArmorType.Cloth;
				}
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Plate;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Dex] < bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Plate;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Str] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Leather;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Dex] > bonusStats[Humanoid.CharacterStatistics.Int] && bonusStats[Humanoid.CharacterStatistics.Str] < bonusStats[Humanoid.CharacterStatistics.Int])
			{
				armorType = ArmorType.Leather;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Str] && bonusStats[Humanoid.CharacterStatistics.Str] > bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				armorType = ArmorType.Plate;
			}
			else if(bonusStats[Humanoid.CharacterStatistics.Int] > bonusStats[Humanoid.CharacterStatistics.Dex] && bonusStats[Humanoid.CharacterStatistics.Str] < bonusStats[Humanoid.CharacterStatistics.Dex])
			{
				armorType = ArmorType.Cloth;
			}
		}

		if(type == EquipmentType.Armor)
		{
			GetComponent<SpriteRenderer>().sprite = MainMechanics.equipmentFactory.getTexture(armorType);
			placementType = PlacementType.Front;
		}
		else
		{
			GetComponent<SpriteRenderer>().sprite = MainMechanics.equipmentFactory.getTexture(weaponType);
			if(weaponType == WeaponType.Bow || weaponType == WeaponType.Staff || weaponType == WeaponType.Sword || weaponType == WeaponType.Mace)
			{
				placementType = PlacementType.Back;
			}
			else
			{
				placementType = PlacementType.Front;
			}
		}
	}
}
