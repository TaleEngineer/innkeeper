﻿using UnityEngine;
using System.Collections;

public class EquipmentFactory : MonoBehaviour 
{
	public Equipment blankPrefab;

	public Sprite swordTexture;
	public Sprite bowTexture;
	public Sprite maceTexture;
	public Sprite daggerTexture;
	public Sprite staffTexture;
	
	public Sprite clothTexture;
	public Sprite leatherTexture;
	public Sprite plateTexture;

	public void EquipAdventurer(Adventurer adventurer)
	{
		Equipment weapon = Instantiate(blankPrefab);
		Equipment armor = Instantiate(blankPrefab);
		if(adventurer.chosenClass == Humanoid.CharacterClass.Archer)
		{
			weapon.Set(adventurer.combatLevel, 0.9f, 1f, 0.8f, Equipment.WeaponType.Bow);
			armor.Set(adventurer.combatLevel, 0.9f, 1f, 0.8f, Equipment.ArmorType.Leather);
		}
		else if(adventurer.chosenClass == Humanoid.CharacterClass.Cleric)
		{
			weapon.Set(adventurer.combatLevel, 0.9f, 0.8f, 1f, Equipment.WeaponType.Mace);
			armor.Set(adventurer.combatLevel, 0.9f, 0.8f, 1f, Equipment.ArmorType.Plate);
		}
		else if(adventurer.chosenClass == Humanoid.CharacterClass.Paladin)
		{
			weapon.Set(adventurer.combatLevel, 1f, 0.8f, 0.9f, Equipment.WeaponType.Sword);
			armor.Set(adventurer.combatLevel, 1f, 0.8f, 0.9f, Equipment.ArmorType.Plate);
		}
		else if(adventurer.chosenClass == Humanoid.CharacterClass.Thief)
		{
			weapon.Set(adventurer.combatLevel, 0.8f, 1f, 0.9f, Equipment.WeaponType.Daggers);
			armor.Set(adventurer.combatLevel, 0.8f, 1f, 0.9f, Equipment.ArmorType.Leather);
		}
		else if(adventurer.chosenClass == Humanoid.CharacterClass.Warrior)
		{
			weapon.Set(adventurer.combatLevel, 1f, 0.9f, 0.8f, Equipment.WeaponType.Sword);
			armor.Set(adventurer.combatLevel, 1f, 0.9f, 0.8f, Equipment.ArmorType.Plate);
		}
		else if(adventurer.chosenClass == Humanoid.CharacterClass.Wizard)
		{
			weapon.Set(adventurer.combatLevel, 0.8f, 0.9f, 1f, Equipment.WeaponType.Staff);
			armor.Set(adventurer.combatLevel, 0.8f, 0.9f, 1f, Equipment.ArmorType.Cloth);
		}

		adventurer.armor = armor;
		adventurer.weapon = weapon;

        armor.mainTransform.parent = adventurer.mainTransform;
        weapon.mainTransform.parent = adventurer.mainTransform;
    }

	public Equipment getEquipment(int level)
	{
		Equipment toReturn = Instantiate(blankPrefab).GetComponent<Equipment>();
		toReturn.Set(level);
		return toReturn;
	}

	public Sprite getTexture(Equipment.ArmorType armorType)
	{
		if(armorType == Equipment.ArmorType.Cloth)
		{
			return clothTexture;
		}
		else if(armorType == Equipment.ArmorType.Leather)
		{
			return leatherTexture;
		}
		else
		{
			return plateTexture;
		}
	}

	public Sprite getTexture(Equipment.WeaponType weaponType)
	{
		if(weaponType == Equipment.WeaponType.Bow)
		{
			return bowTexture;
		}
		else if(weaponType == Equipment.WeaponType.Daggers)
		{
			return daggerTexture;
		}
		else if(weaponType == Equipment.WeaponType.Mace)
		{
			return maceTexture;
		}
		else if(weaponType == Equipment.WeaponType.Staff)
		{
			return staffTexture;
		}
		else
		{
			return swordTexture;
		}
	}
}
