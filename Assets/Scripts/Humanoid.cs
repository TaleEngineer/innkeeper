﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Humanoid : MonoBehaviour, IFeelable
{
	public Transform mainTransform;
    public Vector3 truePosition = new Vector3();
    public Vector3 displayLerpPosition = new Vector3();

    public List<string> orderProperties = new List<string>();
	public SpriteRenderer spriteRenderer;

	public bool cameFromRight;
	public Bed bedRestingIn = null;

    //ThoughtDialogSystem
    public FeelingsSystem feelings;
    Dictionary<DialogEngine.EmotionType, int> emotionalState;
    public DialogEngine.EmotionType shownEmotionalState;
    public DialogEngine.PersonalityType personalityType;

    //ADVENTURING STATS
    public int combatLevel = 1;
	public int currentXp = 0;
	public int currentMaxXp = 10;

    //WANTS STATS
	public float tiredness = 0f;
	public float tirednessTrigger = 100;
	public float hunger = 0f;
	public float hungerTrigger = 30;
	public float thirst = 0f;
	public float thirstTrigger = 30;
    public bool lookingForParty = false;

    //EMPLOYEE SKILLS STATS
    public Dictionary<EmployeeSkill.WorkSkill, EmployeeSkill> workSkills = new Dictionary<EmployeeSkill.WorkSkill, EmployeeSkill>();
    bool nightShift = false;
    public bool hasNightShift
    {
        get
        {
            return nightShift;
        }
        set
        {
            if(value != nightShift)
            {
                RemoveAllJobs();
            }
            nightShift = value;
        }
    }

	public enum CharacterStatistics
	{
		Str,
		Int,
		Dex
	}

	public Dictionary<CharacterStatistics, int> baseCharaStats = new Dictionary<CharacterStatistics, int>();
	public Dictionary<CharacterStatistics, int> currentCharaStats = new Dictionary<CharacterStatistics, int>();
	public CharacterStatistics mainStat;
	public CharacterStatistics secondaryStat;


	public int baseMaxHP;
	public int currentMaxHP;
	public int currentHP;
	//END ADVENTURING STATAS

	public Holdable[] heldObjects = new Holdable[2];

	public FurnishingLibrary.HoldableID wantedFood = FurnishingLibrary.HoldableID.Bread;
	public FurnishingLibrary.HoldableID wantedDrink = FurnishingLibrary.HoldableID.LiqYBeer;

	public enum NPCType
	{
		Player,
		Builder,
		Adventurer,
		Civilian,
		Employee,
		DeliveryMan,
		QuestMaster,
	}
    [SerializeField] private NPCType currentNPCType;
    public NPCType thisNPCType
    {
        get
        {
            return currentNPCType;
        }
        set
        {
            currentNPCType = value;
            if (spriteRenderer != null)
            {
                switch (value)
                {
                    case NPCType.Civilian:
                        spriteRenderer.sprite = MainMechanics.mechanics.spriteCivilian;
                        break;

                    case NPCType.Employee:
                        spriteRenderer.sprite = MainMechanics.mechanics.spriteEmployee;
                        break;
                }
            }
        }
    }

	public enum CharacterClass
	{
		None,
		Warrior,
		Wizard,
		Cleric,
		Paladin,
		Thief,
		Archer
	}
	public CharacterClass chosenClass;

	public string Name;
	public Pathfinder pathFinder = new Pathfinder();
	public List<PathNode> pathToGo = new List<PathNode>();

	public Vector2 currentCoord;
	public Grid currentGrid;
	float speed = 3f;

	public bool working = false;
	float workTimer;
	float workMaxTimer;
	int optionChosen;
	Furnishing workedFurnish;
	Holdable workedHoldable;

	public bool stuck;
	List<PathNode> stuckVector = new List<PathNode>();

	public RoomPiece calledRoomPiece;

	public bool toResetPath = false;

	public List<InteractableFurnishing> assignedFurnishings;

	public List<WorkOrder> currentWorkOrder = new List<WorkOrder>();
	//public List<WorkOrder> overrideWorkOrder = new List<WorkOrder>();
	public List<Order> takenOrders = new List<Order>();


	//For Customers Only
	public Table tableSittingAt;
	public float timeSinceOrderedDrink = 0f;
	public float timeSinceOrderedDrinkMultiplier = 0f;
	public float timeSinceOrderedFood = 0f;
	public float timeSinceOrderedFoodMultiplier = 0f;
	public Party partyApartOf;
    //END Customer Only Section

    //FOR ADVENTURERS ONLY
    public List<PotionBottle> potions = new List<PotionBottle>();
    //END ADVENTURERS ONLY

    //FOR EMPLOYEES ONLY
    public int wagePerHour = 1;
    float wageTimer = 0;
    //END EMPLOYEES ONLY

    public void calculateWage(float deltaTime)
    {
        wageTimer += deltaTime;
        if (wageTimer >= MainMechanics.mechanics.secondsToMinute * 60)
        {
            wageTimer -= MainMechanics.mechanics.secondsToMinute * 60;
            payWage();
        }
    }

    void payWage()
    {
        MainMechanics.mechanics.changeMoney(-wagePerHour, mainTransform.position);
    }

    public virtual void takeDungeonTurn(DungeonEncounter encounter)
	{
        if (encounter.monsterParty.Count > 0)
        {
            hunger += 0.5f;
            tiredness += 0.5f;
            thirst += 0.5f;

            if(chosenClass == CharacterClass.Cleric || chosenClass == CharacterClass.Paladin)
            {
                Humanoid toHeal = partyApartOf.inNeedOfHealing(currentCharaStats[CharacterStatistics.Int]);
                if(toHeal != null)
                {
                    toHeal.takeHealing(currentCharaStats[CharacterStatistics.Int]);
                    //Debug.Log(name + " has healed " + toHeal.name + " for " + currentCharaStats[CharacterStatistics.Int] + " damage.");
                    return;
                }
            }

            if((float)currentHP/currentMaxHP < 0.5f && potions.Count > 0)
            {
                potions[0].CompleteUse(this);
                if(potions[0].currentLiquidContainment <= 0)
                {
                    Destroy(potions[0].gameObject);
                    potions.RemoveAt(0);
                }
            }

            int chance;
            int damage;

            if (chosenClass == CharacterClass.Archer || chosenClass == CharacterClass.Warrior)
            {
                chance = 18;
                damage = currentCharaStats[mainStat];
            }
            else if (chosenClass == CharacterClass.Cleric || chosenClass == CharacterClass.Paladin)
            {
                chance = 15;
                damage = currentCharaStats[CharacterStatistics.Str];
            }
            else if (chosenClass == CharacterClass.Thief || chosenClass == CharacterClass.Wizard)
            {
                chance = 14;
                damage = currentCharaStats[mainStat] + currentCharaStats[secondaryStat];
            }
            else
            {
                chance = 10;
                damage = currentCharaStats[mainStat];
            }

            if (Random.Range(1, 21) <= chance)
            {
                int hitWhom = Random.Range(0, encounter.monsterParty.Count);
                AdventurerParty party = (AdventurerParty)partyApartOf;
                //Debug.Log(name+" deals "+currentCharaStats[mainStat]+" to "+encounter.monsterParty[hitWhom].monsterType.ToString());
                encounter.monsterParty[hitWhom].takeDamage(damage, encounter, party.questEmbarkedOn);
            }
            /*else
            {
                Debug.Log(name + " missed");
            }*/
		}
	}

	public void giveCombatXP(int ammount)
	{
		currentXp += ammount;
		if(currentXp >= currentMaxXp)
		{
			CombatLevelUp();
		}
	}

	public void CombatLevelUp()
	{
		while(currentXp >= currentMaxXp)
		{
			//Debug.Log(name+" has leveled up!");

			combatLevel++;
			currentXp -= currentMaxXp;
			currentMaxXp += currentMaxXp;

			baseCharaStats[mainStat] += 3;
			baseCharaStats[secondaryStat] += 2;

			if(mainStat != CharacterStatistics.Dex && secondaryStat != CharacterStatistics.Dex)
			{
				baseCharaStats[CharacterStatistics.Dex] ++;
			}
			else if(mainStat != CharacterStatistics.Int && secondaryStat != CharacterStatistics.Int)
			{
				baseCharaStats[CharacterStatistics.Int] ++;
			}
			else if(mainStat != CharacterStatistics.Str && secondaryStat != CharacterStatistics.Str)
			{
				baseCharaStats[CharacterStatistics.Str] ++;
			}


			if(chosenClass == CharacterClass.Archer)
			{
				baseMaxHP += 4;
			}
			else if(chosenClass == CharacterClass.Cleric)
			{
				baseMaxHP += 3;
			}
			else if(chosenClass == CharacterClass.Paladin)
			{
				baseMaxHP += 5;
			}
			else if(chosenClass == CharacterClass.Thief)
			{
				baseMaxHP += 3;
			}
			else if(chosenClass == CharacterClass.Warrior)
			{
				baseMaxHP += 10;
			}
			else if(chosenClass == CharacterClass.Wizard)
			{
				baseMaxHP += 2;
			}
			else
			{
				baseMaxHP += 5;
			}
		}

		calculateCurrentCombatStats();
	}

	public virtual void calculateCurrentCombatStats()
	{
		currentCharaStats.Clear();
		
		currentCharaStats.Add(CharacterStatistics.Str, baseCharaStats[CharacterStatistics.Str]);
		currentCharaStats.Add(CharacterStatistics.Dex, baseCharaStats[CharacterStatistics.Dex]);
		currentCharaStats.Add(CharacterStatistics.Int, baseCharaStats[CharacterStatistics.Int]);
		currentMaxHP =  baseMaxHP;
	}

    public virtual void Awake()
    {
        feelings = new FeelingsSystem();
        shownEmotionalState = DialogEngine.EmotionType.Happy;
        personalityType = DialogEngine.PersonalityType.Normal;

        truePosition = mainTransform.position;

		if(thisNPCType == NPCType.Player)
		{
			Name = "Innkeeper";
			MainMechanics.mechanics.Innkeeper = this;
		}
		else if(thisNPCType == NPCType.Builder)
		{
			Name = "Builder";
			if(!MainMechanics.mechanics.BuildersOnScene.Contains (this))
			{
				MainMechanics.mechanics.BuildersOnScene.Add(this);
			}
		}
		else if(thisNPCType == NPCType.Employee)
		{
			Name = "Employee";
			if(!MainMechanics.mechanics.EmployeesOnScene.Contains (this))
			{
				MainMechanics.mechanics.EmployeesOnScene.Add(this);
			}
		}
		else if(thisNPCType == NPCType.DeliveryMan)
		{
			Name = "Delivery Man";
			if(!MainMechanics.mechanics.DeliveryMenOnScene.Contains (this))
			{
				MainMechanics.mechanics.DeliveryMenOnScene.Add(this);
			}
			if(heldObjects[0] == null)
			{
				heldObjects[0] = FurnishingLibrary.Library.getHoldable(FurnishingLibrary.HoldableID.Sack).GetComponent<Holdable>();
			}
		}
		else if(thisNPCType == NPCType.Civilian)
		{
			Name = "Civilian";
			if(!MainMechanics.mechanics.CiviliansOnScene.Contains (this) 
                && MainMechanics.mechanics.hasSpace(NPCType.Civilian))
			{
				MainMechanics.mechanics.CiviliansOnScene.Add(this);
                lookingForParty = true;
			}
            else
            {
                MainMechanics.mechanics.TownshipOnScene.Add(this);
                lookingForParty = false;
            }
		}
        else if(thisNPCType == NPCType.Adventurer)
		{
			Name = "Adventurer";
			if(!MainMechanics.mechanics.AdventurersOnScene.Contains (this))
			{
				MainMechanics.mechanics.AdventurersOnScene.Add(this);
			}
			if(partyApartOf == null)
			{
				partyApartOf = MainMechanics.mechanics.findPartyForHumanoid(this);
			}
		}
		else if(thisNPCType == NPCType.QuestMaster)
		{
			Name = "Quest Master";
			if(!MainMechanics.mechanics.QuestMasterOnScene.Contains (this))
			{
				MainMechanics.mechanics.QuestMasterOnScene.Add(this);
			}
		}

		if(thisNPCType != NPCType.Player)
		{
			baseMaxHP = Random.Range(1,6) + 5;
			baseCharaStats.Add(CharacterStatistics.Str, Random.Range(1,6) + Random.Range(1,6));
			baseCharaStats.Add(CharacterStatistics.Dex, Random.Range(1,6) + Random.Range(1,6));
			baseCharaStats.Add(CharacterStatistics.Int, Random.Range(1,6) + Random.Range(1,6));

			if(thisNPCType == NPCType.Adventurer)
			{
				if(baseCharaStats[CharacterStatistics.Str] == baseCharaStats[CharacterStatistics.Int] 
				   && baseCharaStats[CharacterStatistics.Int] == baseCharaStats[CharacterStatistics.Dex])
				{
					//ROLL FOR ANY CLASS
					int choice = Random.Range(1,7);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Archer;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Cleric;
					}
					else if(choice == 3)
					{
						chosenClass = CharacterClass.Paladin;
					}
					else if(choice == 4)
					{
						chosenClass = CharacterClass.Thief;
					}
					else if(choice == 5)
					{
						chosenClass = CharacterClass.Warrior;
					}
					else if(choice == 6)
					{
						chosenClass = CharacterClass.Wizard;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Str] > baseCharaStats[CharacterStatistics.Int] 
				        && baseCharaStats[CharacterStatistics.Dex] == baseCharaStats[CharacterStatistics.Str])
				{
					//ROLL FOR ARCHER/WARRIOR
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Archer;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Warrior;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Str] > baseCharaStats[CharacterStatistics.Dex] 
				        && baseCharaStats[CharacterStatistics.Int] == baseCharaStats[CharacterStatistics.Str])
				{
					//ROLL FOR PALADIN/CLERIC
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Cleric;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Paladin;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Int] > baseCharaStats[CharacterStatistics.Str] 
				        && baseCharaStats[CharacterStatistics.Int] == baseCharaStats[CharacterStatistics.Dex])
				{
					//ROLL FOR WIZARD/THIEF
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Wizard;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Thief;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Int] > baseCharaStats[CharacterStatistics.Str]
				        && baseCharaStats[CharacterStatistics.Str] == baseCharaStats[CharacterStatistics.Dex])
				{
					//ROLL WIZARD/CLERIC
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Cleric;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Wizard;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Str] > baseCharaStats[CharacterStatistics.Int]
				        && baseCharaStats[CharacterStatistics.Int] == baseCharaStats[CharacterStatistics.Dex])
				{
					//ROLL WARRIOR/PALADIN
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Warrior;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Paladin;
					}
				}
				else if(baseCharaStats[CharacterStatistics.Dex] > baseCharaStats[CharacterStatistics.Int]
				        && baseCharaStats[CharacterStatistics.Int] == baseCharaStats[CharacterStatistics.Str])
				{
					//ROLL ARCHER/THIEF
					int choice = Random.Range(1,3);
					if(choice == 1)
					{
						chosenClass = CharacterClass.Archer;
					}
					else if(choice == 2)
					{
						chosenClass = CharacterClass.Thief;
					}
				}
				else
				{
					List<CharacterStatistics> keys = new List<CharacterStatistics>(baseCharaStats.Keys);
					keys.Sort(
						delegate(CharacterStatistics x, CharacterStatistics y) 
						{
							return baseCharaStats[y].CompareTo(baseCharaStats[x]);
						}
					);

					string determinate = keys[0].ToString()+keys[1].ToString();
					switch(determinate)
					{
					case "StrDex":
						chosenClass = CharacterClass.Warrior;
						break;

					case "StrInt":
						chosenClass = CharacterClass.Paladin;
						break;

					case "IntStr":
						chosenClass = CharacterClass.Cleric;
						break;

					case "IntDex":
						chosenClass = CharacterClass.Wizard;
						break;

					case "DexStr":
						chosenClass = CharacterClass.Archer;
						break;

					case "DexInt":
						chosenClass = CharacterClass.Thief;
						break;
					}
				}
				SpriteRenderer renderer = GetComponent<SpriteRenderer>();
				if(chosenClass == CharacterClass.Archer)
				{
					baseMaxHP += 2;
					baseCharaStats[CharacterStatistics.Str]+=2;
					baseCharaStats[CharacterStatistics.Dex]+=5;
					baseCharaStats[CharacterStatistics.Int]+=0;
					mainStat = CharacterStatistics.Dex;
					secondaryStat = CharacterStatistics.Str;
					renderer.sprite = MainMechanics.mechanics.spriteArcher;
				}
				else if(chosenClass == CharacterClass.Cleric)
				{
					baseMaxHP += 3;
					baseCharaStats[CharacterStatistics.Str]+=2;
					baseCharaStats[CharacterStatistics.Dex]+=0;
					baseCharaStats[CharacterStatistics.Int]+=5;

					mainStat = CharacterStatistics.Int;
					secondaryStat = CharacterStatistics.Str;

					renderer.sprite = MainMechanics.mechanics.spriteCleric;
				}
				else if(chosenClass == CharacterClass.Paladin)
				{
					baseMaxHP += 5;
					baseCharaStats[CharacterStatistics.Str]+=5;
					baseCharaStats[CharacterStatistics.Dex]+=0;
					baseCharaStats[CharacterStatistics.Int]+=2;

					mainStat = CharacterStatistics.Str;
					secondaryStat = CharacterStatistics.Int;

					renderer.sprite = MainMechanics.mechanics.spritePaladin;
				}
				else if(chosenClass == CharacterClass.Thief)
				{
					baseMaxHP += 3;
					baseCharaStats[CharacterStatistics.Str]+=0;
					baseCharaStats[CharacterStatistics.Dex]+=5;
					baseCharaStats[CharacterStatistics.Int]+=2;
					
					mainStat = CharacterStatistics.Dex;
					secondaryStat = CharacterStatistics.Int;

					renderer.sprite = MainMechanics.mechanics.spriteThief;
				}
				else if(chosenClass == CharacterClass.Warrior)
				{
					baseMaxHP += 5;
					baseCharaStats[CharacterStatistics.Str]+=5;
					baseCharaStats[CharacterStatistics.Dex]+=2;
					baseCharaStats[CharacterStatistics.Int]+=0;
					
					mainStat = CharacterStatistics.Str;
					secondaryStat = CharacterStatistics.Dex;

					renderer.sprite = MainMechanics.mechanics.spriteWarrior;
				}
				else if(chosenClass == CharacterClass.Wizard)
				{
					baseMaxHP += 2;
					baseCharaStats[CharacterStatistics.Str]+=0;
					baseCharaStats[CharacterStatistics.Dex]+=2;
					baseCharaStats[CharacterStatistics.Int]+=5;
					
					mainStat = CharacterStatistics.Int;
					secondaryStat = CharacterStatistics.Dex;

					renderer.sprite = MainMechanics.mechanics.spriteWizard;
				}

				/*Debug.Log(chosenClass.ToString()+"| Str: "+charaStats[CharacterStatistics.Str]
				          +"| Dex: "+charaStats[CharacterStatistics.Dex]
				          +"| Int: "+charaStats[CharacterStatistics.Int]
				          +"| HP: "+currentHP+"/"+maxHP);*/
			}
			else
			{
				mainStat = CharacterStatistics.Str;
				secondaryStat = CharacterStatistics.Str;
			}
			
			currentCharaStats.Add(CharacterStatistics.Str, baseCharaStats[CharacterStatistics.Str]);
			currentCharaStats.Add(CharacterStatistics.Dex, baseCharaStats[CharacterStatistics.Dex]);
			currentCharaStats.Add(CharacterStatistics.Int, baseCharaStats[CharacterStatistics.Int]);
		}
		currentMaxHP = baseMaxHP;
		currentHP = currentMaxHP;

		rekindleWants();

		currentCoord = WhereAmIOnGrid();
        checkGrid(currentCoord);
		currentGrid = MainMechanics.mechanics.mainGrid[(int)currentCoord.x, (int)currentCoord.y];
		if(thisNPCType != NPCType.Adventurer
		   && thisNPCType != NPCType.Player 
		   && thisNPCType != NPCType.Employee)
		{
			ExitArea();
		}

        if (thisNPCType != NPCType.Player)
        {
            setRandomName();
            setEmployeeBackground();
        }
    }

    void setEmployeeBackground()
    {
        Dictionary<EmployeeSkill.WorkSkill, int> xp = new Dictionary<EmployeeSkill.WorkSkill, int>();
        /*xp.Add(EmployeeSkill.WorkSkill.Bartending, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.Brewing, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.BRDwarven, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.BRElven, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.BRHumane, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.BROrcish, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.CKDwarven, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.CKElven, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.CKHumane, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.CKOrcish, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.Cleaning, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.Cooking, Random.Range(-20, 21));
        xp.Add(EmployeeSkill.WorkSkill.Waitering, Random.Range(-20, 21));*/

        xp.Add(EmployeeSkill.WorkSkill.Cooking, 0);
        xp.Add(EmployeeSkill.WorkSkill.Bartending, 0);
        xp.Add(EmployeeSkill.WorkSkill.Brewing, 0);
        xp.Add(EmployeeSkill.WorkSkill.BRDwarven, 0);
        xp.Add(EmployeeSkill.WorkSkill.BRElven, 0);
        xp.Add(EmployeeSkill.WorkSkill.BRHumane, 0);
        xp.Add(EmployeeSkill.WorkSkill.BROrcish, 0);
        xp.Add(EmployeeSkill.WorkSkill.CKDwarven, 0);
        xp.Add(EmployeeSkill.WorkSkill.CKElven, 0);
        xp.Add(EmployeeSkill.WorkSkill.CKHumane, 0);
        xp.Add(EmployeeSkill.WorkSkill.CKOrcish, 0);
        xp.Add(EmployeeSkill.WorkSkill.Cleaning, 0);
        xp.Add(EmployeeSkill.WorkSkill.Waitering, 0);


        int Race = Random.Range(0, 4);

        switch (Race)
        {
            case 0://Human
                xp[EmployeeSkill.WorkSkill.BRHumane] += 10;
                xp[EmployeeSkill.WorkSkill.CKHumane] += 10;
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Baked, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Boiled, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Flour, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Humane, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Raw, -20);

                break;

            case 1://Dwarf
                mainTransform.localScale = new Vector3(1,0.5f,1);

                xp[EmployeeSkill.WorkSkill.BRDwarven] += 10;
                xp[EmployeeSkill.WorkSkill.CKDwarven] += 10;
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Baked, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Boiled, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Fried, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Seared, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cheese, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Dwarven, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Raw, -20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Multiculture, -5);
                break;

            case 2://Elf
                mainTransform.localScale = new Vector3(0.5f, 1f, 1);

                xp[EmployeeSkill.WorkSkill.BRElven] += 10;
                xp[EmployeeSkill.WorkSkill.CKElven] += 10;
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Greens, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Fruit, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Vegetables, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Elven, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Raw, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Multiculture, 10);
                break;

            case 3://Orc
                mainTransform.localScale = new Vector3(1.25f, 1f, 1);

                xp[EmployeeSkill.WorkSkill.BROrcish] += 10;
                xp[EmployeeSkill.WorkSkill.CKOrcish] += 10;
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Meat, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Seared, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Orcish, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, -10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Raw, -20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Multiculture, -20);
                break;

        }


        int Parents = Random.Range(0, 4);

        switch (Parents)//Parents
        {
            case 0://Son of the Baker
                xp[EmployeeSkill.WorkSkill.Cooking] += 20;
                xp[EmployeeSkill.WorkSkill.Waitering] += 20;
                xp[EmployeeSkill.WorkSkill.CKHumane] += 20;


                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Baked, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Flour, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 5);
                break;

            case 1://Son of the Farmer
                xp[EmployeeSkill.WorkSkill.Brewing] += 35;
                xp[EmployeeSkill.WorkSkill.CKHumane] += 10;
                xp[EmployeeSkill.WorkSkill.BRHumane] += 15;

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Fruit, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Flour, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Vegetables, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 5);
                break;

            case 2://Son of the Local Drunk
                xp[EmployeeSkill.WorkSkill.Bartending] += 30;
                xp[EmployeeSkill.WorkSkill.Waitering] += 10;
                xp[EmployeeSkill.WorkSkill.Brewing] += 10;
                xp[EmployeeSkill.WorkSkill.BRHumane] += 10;

                int whatSayYou = Random.Range(-30, 31);

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, whatSayYou);

                break;

            case 3://Son of the Local Butcher
                xp[EmployeeSkill.WorkSkill.Cooking] += 20;
                xp[EmployeeSkill.WorkSkill.Cleaning] += 20;
                xp[EmployeeSkill.WorkSkill.CKOrcish] += 20;

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Meat, 10);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Orcish, 10);
                break;

        }

        int TeenEvent = Random.Range(0, 4);

        switch (TeenEvent)//Apprenticeship
        {
            case 0://Baker
                xp[EmployeeSkill.WorkSkill.Cooking] += 50;
                xp[EmployeeSkill.WorkSkill.Waitering] += 50;
                xp[EmployeeSkill.WorkSkill.CKHumane] += 50;

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Baked, 30);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Flour, 20);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 10);
                break;

            case 1://Brewer
                xp[EmployeeSkill.WorkSkill.Brewing] += 100;
                xp[EmployeeSkill.WorkSkill.BRHumane] += 50;

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 30);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Fruit, 10);
                break;

            case 2://Local Bar
                xp[EmployeeSkill.WorkSkill.Bartending] += 50;
                xp[EmployeeSkill.WorkSkill.Waitering] += 50;
                xp[EmployeeSkill.WorkSkill.Brewing] += 20;
                xp[EmployeeSkill.WorkSkill.BRHumane] += 30;

                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Alchohol, 30);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Elven, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Orcish, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Dwarven, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Humane, 5);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Multiculture, 5);
                break;

            case 3://Butcher
                xp[EmployeeSkill.WorkSkill.Cooking] += 50;
                xp[EmployeeSkill.WorkSkill.Cleaning] += 50;
                xp[EmployeeSkill.WorkSkill.CKOrcish] += 50;
                
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Meat, 30);
                feelings.updateFeelings(FurnishingLibrary.ItemFamilyTag.Cooked, 30);
                break;

        }

        List<EmployeeSkill.WorkSkill> keys = new List<EmployeeSkill.WorkSkill>(xp.Keys);
        for(int i = 0; i < keys.Count; i++)
        {
            if(xp[keys[i]] > 0)
            {
                char[] name = keys[i].ToString().ToCharArray();
                if ((name[1] == 'R' && xp[EmployeeSkill.WorkSkill.Brewing] > 10)
                    ||
                    (name[1] == 'K' && xp[EmployeeSkill.WorkSkill.Cooking] > 10)
                    ||
                    (name[1] != 'R' && name[1] != 'K'))
                {
                    EmployeeSkill toAdd = new EmployeeSkill();
                    toAdd.Set(keys[i], xp[keys[i]], this);
                    workSkills.Add(keys[i], toAdd);
                }
            }
        }
    }


    public void addWorkXP(EmployeeSkill.WorkSkill skill, int toAdd)
    {
        if(skill == EmployeeSkill.WorkSkill.Generic)
        {
            return;
        }
        EmployeeSkill skillStat;
        if (workSkills.TryGetValue(skill, out skillStat))
        {
            skillStat.addXP(toAdd);
        }
        else
        {
            skillStat = new EmployeeSkill();
                skillStat.Set(skill, toAdd, this);
                workSkills.Add(skill, skillStat);
        }
    }

    /// <summary>
    /// Set's a random name for the character
    /// </summary>
    void setRandomName()
    {
        int roll = Random.Range(0, 11);

        //FIRST NAMES
        switch (roll)
        {
            case 0:
                Name = "Bob";
                break;

            case 1:
                Name = "Jim";
                break;


            case 2:
                Name = "Steve";
                break;

            case 3:
                Name = "Steven";
                break;

            case 4:
                Name = "James";
                break;

            case 5:
                Name = "Matt";
                break;

            case 6:
                Name = "Michael";
                break;

            case 7:
                Name = "Loyd";
                break;

            case 8:
                Name = "Topher";
                break;

            case 9:
                Name = "King";
                break;

            case 10:
                Name = "Grant";
                break;
        }

        roll = Random.Range(0, 10);
        switch (roll)
        {
            case 0:
                Name = "McRoyce";
                break;

            case 1:
                Name = "Jimmington";
                break;

            case 2:
                Name = "Stevenson";
                break;

            case 3:
                Name = "Salvidor";
                break;

            case 4:
                Name = "Jamison";
                break;

            case 5:
                Name = "Mathews";
                break;

            case 6:
                Name = "Michaels";
                break;

            case 7:
                Name = "Jones";
                break;

            case 8:
                Name = "Torpid";
                break;

            case 9:
                Name = "Kingston";
                break;
        }
    
        name = Name;
    }

    public virtual void OnEnable()
	{
		if(heldObjects[0] != null)
		{
			heldObjects[0].gameObject.SetActive(true);
		}
		if(heldObjects[1] != null)
		{
			heldObjects[1].gameObject.SetActive(true);
		}


		if(truePosition.x > 1)
		{
			cameFromRight = true;
		}
		else
		{
			cameFromRight = false;
		}

        if(thisNPCType == NPCType.Employee)
        {

            if (assignedFurnishings.Count > 0)
            {
                WorkOrder goingOrder = new WorkOrder();
                List<PathNode> whereGo = assignedFurnishings[0].reachableNodes;
                goingOrder.Set(whereGo, this);
                currentWorkOrder.Add(goingOrder);
            }
            else
            {
                List<Vector2> whereGo = new List<Vector2>();
                if (mainTransform.position.x < 1)
                {
                    pathToGo.Add(MainMechanics.mechanics.mainGrid[(int)mainTransform.position.x + 1, (int)mainTransform.position.y].pathNode);
                }
                else
                {
                    pathToGo.Add(MainMechanics.mechanics.mainGrid[(int)mainTransform.position.x - 1, (int)mainTransform.position.y].pathNode);
                }
            }
        }
	}



	// Update is called once per frame
	public virtual void Update () 
	{
        if(Time.timeScale == 0)
        {
            return;
        }

		orderProperties.Clear();
		for(int i = 0; i < currentWorkOrder.Count; i++)
		{
			orderProperties.Add(currentWorkOrder[i].getWorkOrderProperty());
		}

		if(bedRestingIn != null)
		{
			return;
		}

		timeSinceOrderedDrink += Time.deltaTime * timeSinceOrderedDrinkMultiplier;
		timeSinceOrderedFood += Time.deltaTime * timeSinceOrderedFoodMultiplier;


		currentCoord = WhereAmIOnGrid();
        checkGrid(currentCoord);
        currentGrid = MainMechanics.mechanics.mainGrid[(int)currentCoord.x, (int)currentCoord.y];

		if(thisNPCType == NPCType.Employee
		   || thisNPCType == NPCType.Player)
		{
			for(int i = 0; i < takenOrders.Count; i++)
			{
				bool needsSatisfied = true;
				for(int members = 0; members < takenOrders[i].orderers.Members.Count; members++)
				{
					if(!takenOrders[i].needSatisfied(members))
					{
						needsSatisfied = false;
						break;
					}
				}
				if(needsSatisfied)
				{
					takenOrders.RemoveAt(i);
					i--;
				}
			}
		}
		
		if(working)
		{
			workTimer += Time.deltaTime;
			if(workTimer >= workMaxTimer)
			{
				workDone();
			}
		}
		else if(pathToGo.Count > 0)
		{
            float toTravel = speed * currentGrid.speedMultiplier * Time.deltaTime;
            while (toTravel > 0 && pathToGo.Count > 0)
            {
                float distanceToTravel = Vector2.Distance(pathToGo[0].mainTransform.position, truePosition);
                if (distanceToTravel > toTravel)
                {
                    if (pathToGo[0].mainTransform.position.x > truePosition.x)
                    {
                        truePosition += new Vector3(toTravel, 0, 0);
                    }
                    if (pathToGo[0].mainTransform.position.x < truePosition.x)
                    {
                        truePosition -= new Vector3(toTravel, 0, 0);
                    }
                    if (pathToGo[0].mainTransform.position.y > truePosition.y)
                    {
                        truePosition += new Vector3(0, toTravel, 0);
                    }
                    if (pathToGo[0].mainTransform.position.y < truePosition.y)
                    {
                        truePosition -= new Vector3(0, toTravel, 0);
                    }
                    toTravel = 0;
                }
                else
                {
                    truePosition = pathToGo[0].mainTransform.position;
                    toTravel -= distanceToTravel;

                    pathToGo[0].toTreadHere.Remove(this);
                    pathToGo.RemoveAt(0);

                    if (toResetPath)
                    {
                        resetPath();
                        toResetPath = false;
                    }
                }
            }
			
		}
		else if(thisNPCType == NPCType.Adventurer)
		{
			if(partyApartOf == null)
			{
				partyApartOf = MainMechanics.mechanics.findPartyForHumanoid(this);
			}

			AdventurerParty party = (AdventurerParty)partyApartOf;
			int questToChoose = 0;
			QuestBoard toFind;

			if(partyApartOf != null && !partyApartOf.Members[0].gameObject.activeInHierarchy)
			{
				currentWorkOrder.Clear();
			}

			
			if(currentWorkOrder.Count > 0 && tableSittingAt == null)
			{
				WorkOrder.TaskEndState endState = currentWorkOrder[0].GetOrder();
				if(endState == WorkOrder.TaskEndState.TaskFailure)
				{
					currentWorkOrder.Add(currentWorkOrder[0]);
					currentWorkOrder.RemoveAt(0);
				}
				else if(endState == WorkOrder.TaskEndState.TaskComplete)
				{
					currentWorkOrder.RemoveAt(0);
				}
			}
			else if(heldObjects[0] != null)
			{
					checkHand(true);
			}
			else if(heldObjects[1] != null)
			{
					checkHand(false);
			}
			else if(!partyApartOf.readyToLeaveTable() && tableSittingAt == null)
			{
				WorkOrder goToTable = new WorkOrder();
				goToTable.Set(this, partyApartOf);
				currentWorkOrder.Add(goToTable);
			}
			else if(wantedFood == FurnishingLibrary.HoldableID.Error 
                && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                && tableSittingAt != null && partyApartOf.readyToLeaveTable())
			{
				partyApartOf.rekindleWants();

				if(partyApartOf.readyToLeaveTable())
				{
					tableSittingAt.getInteraction(this, -1);
				}
			}
			else if(wantedFood == FurnishingLibrary.HoldableID.Error 
			        && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                    && tableSittingAt == null 
			        && party.Members[0] == this
			        && MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.Bed)
			        && partyApartOf.readyToLeaveTable() && !partyApartOf.readyToGo()
				    && partyApartOf.rentRooms())
			{

			}
			else if(wantedFood == FurnishingLibrary.HoldableID.Error 
			        && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                    && tableSittingAt == null && partyApartOf.readyToLeaveTable()
			        && party.Members[0] == this
			        && party.Members.Count >= 4
			        && party.questEmbarkedOn == null 
			        && MainMechanics.mechanics.applicableQuestAvailable(party, out toFind, out questToChoose))
			{
				WorkOrder goToBoard = new WorkOrder();
				goToBoard.Set(toFind, questToChoose+1, this, null);
				currentWorkOrder.Add(goToBoard);
			}
			else if(wantedFood == FurnishingLibrary.HoldableID.Error 
			        && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                    && tableSittingAt == null && (partyApartOf.readyToGo() || partyApartOf.readyToLeaveTable()))
			{
				HeadForExit();
			}
		}
		else if(thisNPCType == NPCType.Civilian)
		{
			if(partyApartOf == null && lookingForParty)
			{
				partyApartOf = MainMechanics.mechanics.findPartyForHumanoid(this);
			}

            if (currentWorkOrder.Count > 0 && tableSittingAt != null)
            {
                currentWorkOrder.RemoveAt(0);
            }
            else if (currentWorkOrder.Count > 0)
            {
                WorkOrder.TaskEndState endState = currentWorkOrder[0].GetOrder();
                if (endState == WorkOrder.TaskEndState.TaskFailure)
                {
                    currentWorkOrder.Add(currentWorkOrder[0]);
                    currentWorkOrder.RemoveAt(0);
                    if (currentWorkOrder.Count == 0)
                    {
                        HeadForExit();
                    }
                }
                else if (endState == WorkOrder.TaskEndState.TaskComplete)
                {
                    currentWorkOrder.RemoveAt(0);
                }
            }
            else if (heldObjects[0] != null)
            {
                checkHand(true);
            }
            else if (heldObjects[1] != null)
            {
                checkHand(false);
            }
            else if (partyApartOf != null)
            {
                if (!partyApartOf.readyToLeaveTable() && tableSittingAt == null)
                {
                    if (TableAvailable())
                    {
                        WorkOrder goToTable = new WorkOrder();
                        goToTable.Set(this, partyApartOf);
                        currentWorkOrder.Add(goToTable);
                    }
                    else
                    {
                        HeadForExit();
                    }
                }
                else if (wantedFood == FurnishingLibrary.HoldableID.Error 
                    && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                    && tableSittingAt != null && partyApartOf.readyToLeaveTable())
                {
                    partyApartOf.rekindleWants();

                    if (partyApartOf.readyToLeaveTable())
                    {
                        tableSittingAt.getInteraction(this, -1);
                    }
                }
                else if (wantedFood == FurnishingLibrary.HoldableID.Error 
                    && (wantedDrink == FurnishingLibrary.HoldableID.Error || wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink)
                    && tableSittingAt == null && partyApartOf.readyToLeaveTable())
                {
                    HeadForExit();
                }
            }
            else
            {
                HeadForExit();
            }
		}
		else if(thisNPCType == NPCType.Employee)
		{
			
			if(currentWorkOrder.Count > 0)
			{
				WorkOrder.TaskEndState endState = currentWorkOrder[0].GetOrder();
				if(endState == WorkOrder.TaskEndState.TaskFailure)
				{
					if(currentWorkOrder[0].orderer != null)
					{
                        if (currentWorkOrder[0].currentWorkOrderType != WorkOrder.WorkOrderType.RetrieveItem)
                        {
                            currentWorkOrder.Add(currentWorkOrder[0]);
                        }
						if(currentWorkOrder[0].orderer = assignedFurnishings[0])
						{
							assignedFurnishings.Add(assignedFurnishings[0]);
							assignedFurnishings.RemoveAt(0);
						}
					}
					

					currentWorkOrder.RemoveAt(0);
				}
				else if(endState == WorkOrder.TaskEndState.TaskComplete)
				{
					currentWorkOrder.RemoveAt(0);
				}
			}
            else if (assignedFurnishings.Count > 0)
            {
                bool toSkip = false;
                if (assignedFurnishings[0].assignedEmployee != this && assignedFurnishings[0].assignedEmployee == null)
                {
                    assignedFurnishings[0].assignedEmployee = this;
                }
                else if(assignedFurnishings[0].assignedEmployee != this && assignedFurnishings[0].assignedEmployee != null)
                {
                    toSkip = true; 
                }
                if (!toSkip)
                {
                    toSkip = assignedFurnishings[0].getWorkOrder();
                }
                if (toSkip)
                {
                    if (canLeave())
                    {
                        dropItemOnFloor(true);
                        dropItemOnFloor(false);
                        HeadForExit();
                    }
                    else
                    {
                        assignedFurnishings.Add(assignedFurnishings[0]);
                        assignedFurnishings.RemoveAt(0);
                    }
                }
            }
            else if(canLeave())
            {
                dropItemOnFloor(true);
                dropItemOnFloor(false);
                HeadForExit();
            }
        }
		else if(thisNPCType == NPCType.Player)
		{
			
		}//END IS PLAYER
		else if(thisNPCType == NPCType.Builder)
		{
			if((stuck && calledRoomPiece != null) 
                || (calledRoomPiece != null && calledRoomPiece.buildComplete) 
                || (calledRoomPiece != null && MainMechanics.mechanics.currency >= MainMechanics.mechanics.floorCost))
			{
				calledRoomPiece.calledBuilder = null;
				calledRoomPiece = null;
			}
			int wallToBuild = -1;
            if (MainMechanics.mechanics.currency >= MainMechanics.mechanics.wallCost)
            {
                for (int i = 0; i < currentGrid.pathNode.walls.Count; i++)
                {
                    if (!currentGrid.pathNode.walls[i].buildComplete)
                    {
                        wallToBuild = i;
                        break;
                    }
                }
            }
			
			if(currentGrid.roomPieceHere != null
			   && !currentGrid.roomPieceHere.buildComplete
               && MainMechanics.mechanics.currency >= MainMechanics.mechanics.floorCost)
			{
				currentGrid.roomPieceHere.buildObject(Time.deltaTime/2);
			}
			else if(wallToBuild != -1)
			{
				currentGrid.pathNode.walls[wallToBuild].buildObject(Time.deltaTime/2);
			}
			else 
			{
				bool buildingRoom = false;
				Vector2 foundCoord = new Vector2(-1,-1);
				
				if((MainMechanics.mechanics.roomsToBuild.Count > 0 || MainMechanics.mechanics.wallsToBuild.Count > 0)
				    && !stuck && calledRoomPiece == null && MainMechanics.mechanics.currency >= MainMechanics.mechanics.floorCost)
				{
					
					for(int i = 0; i < MainMechanics.mechanics.roomsToBuild.Count; i++)
					{
						while(MainMechanics.mechanics.roomsToBuild[i] == null)
						{
							MainMechanics.mechanics.roomsToBuild.RemoveAt(i);
							if(MainMechanics.mechanics.roomsToBuild.Count <= i)
							{
								break;
							}
						}
						if(MainMechanics.mechanics.roomsToBuild.Count <= i)
						{
							break;
						}
						
						if(MainMechanics.mechanics.roomsToBuild[i].calledBuilder == null 
						   && (foundCoord.x == -1 
						    || Vector2.Distance(MainMechanics.mechanics.roomsToBuild[i].mainTransform.position, truePosition) < Vector2.Distance(foundCoord, truePosition)))
						{
							buildingRoom = true;
							foundCoord = MainMechanics.mechanics.roomsToBuild[i].gridCoord;
						}
						
						if(foundCoord.x != -1 && Vector2.Distance(foundCoord, truePosition) <= 1)
						{
							break;
						}
					}
					if(foundCoord.x == -1)
					{
						for(int i = 0; i < MainMechanics.mechanics.wallsToBuild.Count; i++)
						{
							while(MainMechanics.mechanics.wallsToBuild[i] == null)
							{
								MainMechanics.mechanics.wallsToBuild.RemoveAt(i);
								if(MainMechanics.mechanics.wallsToBuild.Count <= i)
								{
									break;
								}
							}
							if(MainMechanics.mechanics.wallsToBuild.Count <= i)
							{
								break;
							}
							
							if(foundCoord.x == -1 
							    || Vector2.Distance(MainMechanics.mechanics.wallsToBuild[i].mainTransform.position, truePosition) < Vector2.Distance(foundCoord, truePosition))
							{
								foundCoord = MainMechanics.mechanics.wallsToBuild[i].mainTransform.position;
							}
							
							if(foundCoord.x != -1 && Vector2.Distance(foundCoord, truePosition) <= 1)
							{
								break;
							}
						}
					}
				}
				else if(calledRoomPiece != null && MainMechanics.mechanics.currency >= MainMechanics.mechanics.floorCost)
				{
					foundCoord = calledRoomPiece.mainTransform.position;
				}
				if(foundCoord.x != -1)
				{
					if(buildingRoom)
					{
						MainMechanics.mechanics.mainGrid[(int)foundCoord.x,(int)foundCoord.y].roomPieceHere.calledBuilder = this;
						calledRoomPiece = MainMechanics.mechanics.mainGrid[(int)foundCoord.x,(int)foundCoord.y].roomPieceHere;
					}
					List<Vector2> list = new List<Vector2>();
					list.Add(foundCoord);
					goToPlace(list);
				}
				else
				{
					HeadForExit();
				}
			}
		}//END BUILDER
		else if(thisNPCType == NPCType.DeliveryMan)
		{
			Vector2 dropPosition = MainMechanics.mechanics.merchantDropoffPoint;
			if(heldObjects[0] != null
			   && heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Sack
			   && 
			   (heldObjects[0].holdablesInThisHoldable.Count > 0 || heldObjects[1] != null))
			{
				if(currentCoord == dropPosition)
				{
					if(heldObjects[1] != null)
					{
						dropItemOnFloor(false);
					}
					else
					{
						heldObjects[0].Use(this);
					}
				}
				else
				{
					List<Vector2> list = new List<Vector2>();
					list.Add(dropPosition);
					goToPlace(list);
				}
			}
			else
			{
				HeadForExit();
			}
			
		}//END DELIVERY MAN
		else if(thisNPCType == NPCType.QuestMaster)
		{

			QuestBoard toGoTo;
			int toPost;
			if(pathToGo.Count <= 0 && currentWorkOrder.Count > 0)
			{
				WorkOrder.TaskEndState endState = currentWorkOrder[0].GetOrder();
				if(endState == WorkOrder.TaskEndState.TaskFailure)
				{
					if(currentWorkOrder[0].orderer != null)
					{
						currentWorkOrder.Add(currentWorkOrder[0]);
						if(currentWorkOrder[0].orderer = assignedFurnishings[0])
						{
							assignedFurnishings.Add(assignedFurnishings[0]);
							assignedFurnishings.RemoveAt(0);
						}
					}
					
					
					currentWorkOrder.RemoveAt(0);
				}
				else if(endState == WorkOrder.TaskEndState.TaskComplete)
				{
					currentWorkOrder.RemoveAt(0);
				}
			}
			else if(MainMechanics.mechanics.questToPostAvailable(out toGoTo, out toPost))
			{
				WorkOrder order = new WorkOrder();
				order.Set(toGoTo, (toPost+1)*-1, this, null);
				currentWorkOrder.Add(order);
			}
			else
			{
				HeadForExit();
			}
		}
		
		//ENDER UPDATES
		spriteRenderer.sortingOrder = MainMechanics.mechanics.getSortingLayerNumber(this);

        RecalculateDistance();

        //mainTransform.position = Vector3.Lerp(mainTransform.position, truePosition, 7f * Time.deltaTime);

        if (heldObjects[0] != null)
		{
			heldObjects[0].mainTransform.position = mainTransform.position+new Vector3(0.2f, 0,-0.01f);
			heldObjects[0].transform.rotation = transform.rotation;
			heldObjects[0].spriteRenderer.sortingOrder = spriteRenderer.sortingOrder;
		}
		if(heldObjects[1] != null)
		{
			heldObjects[1].mainTransform.position = mainTransform.position+new Vector3(-0.2f, 0,-0.01f);
			heldObjects[1].transform.rotation = transform.rotation;
			heldObjects[1].spriteRenderer.sortingOrder = spriteRenderer.sortingOrder;
		}
		//END ENDER UPDATES
	}

    /// <summary>
    /// Checks current grid to see if the grid has changed.
    /// </summary>
    /// <param name="coords"></param>
    void checkGrid(Vector2 coords)
    {
        if(MainMechanics.mechanics.mainGrid[(int)coords.x, (int)coords.y] != currentGrid)
        {
            if(currentGrid != null)
            {
                currentGrid.peopleHere.Remove(this);
            }
            MainMechanics.mechanics.mainGrid[(int)coords.x, (int)coords.y].peopleHere.Add(this);
        }
    }

    /// <summary>
    /// Causes two "Fake" positions to repell eachother
    /// </summary>
    /// <param name="comparingSpace"></param>
    public void FakePositionRepulsion(Humanoid comparingSpace)
    {
        var distance = Vector2.Distance(comparingSpace.displayLerpPosition, displayLerpPosition);
        if(distance == 0 || comparingSpace.displayLerpPosition.x == mainTransform.position.x || comparingSpace.displayLerpPosition.y == displayLerpPosition.y)
        {
            comparingSpace.displayLerpPosition += new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), 0);
            distance = Vector2.Distance(comparingSpace.displayLerpPosition, displayLerpPosition);
        }
        if (distance < 0.2f)
        {
            Vector3 normalized = comparingSpace.displayLerpPosition - displayLerpPosition;
            normalized = Vector3.Normalize(normalized);
            comparingSpace.displayLerpPosition = comparingSpace.mainTransform.position + normalized * (0.2f - distance);
            displayLerpPosition = mainTransform.position - normalized * (0.4f-distance);
        }
    }

    /// <summary>
    /// Ensures the character is never more than a distance of 0.5f from their true position
    /// </summary>
    public void RecalculateDistance()
    {
        var distance = Vector2.Distance(truePosition, displayLerpPosition);
        if (distance > 0.5f)
        {
            Vector3 normalized = truePosition - displayLerpPosition;
            normalized = Vector3.Normalize(normalized);
            displayLerpPosition += normalized * (distance - 0.5f);
        }
        if(currentGrid.peopleHere.Count == 1 || working || pathToGo.Count > 0)
        {
            displayLerpPosition = new Vector3(truePosition.x, truePosition.y);
        }

        mainTransform.position = Vector3.Lerp(mainTransform.position, displayLerpPosition, 7f * Time.deltaTime);
    }

    /// <summary>
    /// Character moves to random adjacent tile
    /// </summary>
    public void Meander()
    {
        if(currentGrid.pathNode.Neighbors.Count == 0 || pathToGo.Count != 0)
        {
            return;
        }

        int toGoTo = Random.Range(0, currentGrid.pathNode.Neighbors.Count);
        pathToGo.Add(currentGrid.pathNode.Neighbors[toGoTo]);
    }

	/// <summary>
	/// Sets the drink order time multiplier.
	/// </summary>
	/// <param name="baseMultiplier">Base multiplier.</param>
	public void SetDrinkOrderTimeMultiplier(float baseMultiplier)
	{
		timeSinceOrderedDrinkMultiplier = baseMultiplier;
	}

	/// <summary>
	/// Sets the food order time multiplier.
	/// </summary>
	/// <param name="baseMultiplier">Base multiplier.</param>
	public void SetFoodOrderTimeMultiplier(float baseMultiplier)
	{
		timeSinceOrderedFoodMultiplier = baseMultiplier;
	}

    public void clearPath()
    {
        for (int i = 0; i < pathToGo.Count; i++)
        {
            pathToGo[i].toTreadHere.Remove(this);
        }
        pathToGo.Clear();
    }

	public void resetPath()
	{
		if(thisNPCType != NPCType.Player)
		{
            clearPath();
		}
		else
		{
			if(pathToGo.Count > 0)
			{
				List<PathNode> list = new List<PathNode>();
				list.Add(pathToGo[pathToGo.Count-1]);
				goToPlace(list);
			}
		}
	}

	public void Draw()
	{
		if(working)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(mainTransform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);

			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
			GUI.color = new Color(0,0,0,0.75f);
			GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			
			Rect progressRect = new Rect(drawRect.x+1, drawRect.y+1, (workTimer/workMaxTimer)*(drawRect.width-2), drawRect.height-2);
			
			GUI.color = Color.white;
			
			GUI.DrawTexture(progressRect, MainMechanics.mechanics.progressMarker);
		}

		if(thisNPCType == NPCType.Builder && pathToGo.Count == 0)
		{
			if(currentGrid.roomPieceHere != null && currentGrid.roomPieceHere.percentBuilt > 0 && currentGrid.roomPieceHere.percentBuilt < 1)
			{
				Vector2 midPoint = Camera.main.WorldToScreenPoint(mainTransform.position);
				midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
				
				float halfSize = Screen.height/(Camera.main.orthographicSize*2);
				
				Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
				GUI.color = new Color(0,0,0,0.75f);
				GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
				
				Rect progressRect = new Rect(drawRect.x+1, drawRect.y+1, currentGrid.roomPieceHere.percentBuilt*(drawRect.width-2), drawRect.height-2);
				
				GUI.color = Color.white;
				
				GUI.DrawTexture(progressRect, MainMechanics.mechanics.progressMarker);
			}
			else
			{
				
				int wallToBuild = -1;
				for(int i = 0; i < currentGrid.pathNode.walls.Count; i++)
				{
					if(!currentGrid.pathNode.walls[i].buildComplete)
					{
						wallToBuild = i;
						break;
					}
				}
				if(wallToBuild != -1)
				{
					if(currentGrid.pathNode.walls[wallToBuild].percentBuilt > 0 && currentGrid.pathNode.walls[wallToBuild].percentBuilt < 1)
					{
						Vector2 midPoint = Camera.main.WorldToScreenPoint(mainTransform.position);
						midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
						
						float halfSize = Screen.height/(Camera.main.orthographicSize*2);
						
						Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
						GUI.color = new Color(0,0,0,0.75f);
						GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
						
						Rect progressRect = new Rect(drawRect.x+1, drawRect.y+1, currentGrid.pathNode.walls[wallToBuild].percentBuilt*(drawRect.width-2), drawRect.height-2);
						
						GUI.color = Color.white;
						
						GUI.DrawTexture(progressRect, MainMechanics.mechanics.progressMarker);
					}
				}
			}
		}

		if(heldObjects[0] != null)
		{
			heldObjects[0].Draw();
		}
		if(heldObjects[1] != null)
		{
			heldObjects[1].Draw();
		}
	}

	public void dropItemOnFloor(bool left)
	{
		int handNumber;
		if(left)
		{
			handNumber = 0;
		}
		else
		{
			handNumber = 1;
		}

		if(heldObjects[handNumber] != null)
		{
			currentGrid.holdablesHere.Add(heldObjects[handNumber]);
			MainMechanics.mechanics.addHoldableToGround(heldObjects[handNumber]);
			heldObjects[handNumber].mainTransform.position = currentGrid.mainTransform.position;
			heldObjects[handNumber].GetComponent<SpriteRenderer>().sortingLayerName = "ItemOnFloor";
			heldObjects[handNumber] = null;
		}
	}

	public void PickUpItem(bool left, int listNumber)
	{
		int handNumber;
		if(left)
		{
			handNumber = 0;
		}
		else
		{
			handNumber = 1;
		}

		if(heldObjects[handNumber] == null)
		{
			heldObjects[handNumber] = currentGrid.holdablesHere[listNumber];
			MainMechanics.mechanics.removeHoldableToGround(heldObjects[handNumber]);
			heldObjects[handNumber].GetComponent<SpriteRenderer>().sortingLayerName = "EverythingElse";
			currentGrid.holdablesHere.RemoveAt(listNumber);
		}
	}

	/// <summary>
	/// Starts working a Furnishing. When timer is complete, Holdable's CompleteInteraction is called.
	/// </summary>
	/// <param name="maxTimer">Max timer.</param>
	/// <param name="option">Option.</param>
	/// <param name="furnish">Furnish.</param>
	public void startWorking(float maxTimer, int option, Furnishing furnish, EmployeeSkill.WorkSkill skill, int skillXP)
	{
		working = true;
		workTimer = 0;
        EmployeeSkill skillStat;
        if (workSkills.TryGetValue(skill, out skillStat))
        {
            workMaxTimer = maxTimer / (skillStat.level+1) ;
        }
        else
        {
            workMaxTimer = maxTimer;
        }
        addWorkXP(skill, skillXP);
        workedHoldable = null;
		optionChosen = option;
		workedFurnish = furnish;
	}
	/// <summary>
	/// Starts working a Holdable. When timer is complete, activates the Holdable.
	/// </summary>
	/// <param name="maxTimer">Max timer.</param>
	/// <param name="holdable">Holdable.</param>
	public void startWorking(float maxTimer, Holdable holdable, EmployeeSkill.WorkSkill skill, int skillXP)
	{
		working = true;
		workTimer = 0;
        EmployeeSkill skillStat;
        if (workSkills.TryGetValue(skill, out skillStat))
        {
            workMaxTimer = maxTimer / (skillStat.level+1);
        }
        else
        {
            workMaxTimer = maxTimer;
        }
        addWorkXP(skill, 1);
        workedHoldable = holdable;
		workedFurnish = null;
	}

	void workDone()
	{
		working = false;
		if(workedHoldable != null)
		{
			workedHoldable.CompleteUse(this);
		}
		else if(workedFurnish != null)
		{
			workedFurnish.CompleteInteract(this, optionChosen);
		}

		workedFurnish = null;
		workedHoldable = null;
	}

	public void offScreenUpdate()
	{
		
		switch (thisNPCType)
        {
            case NPCType.Adventurer:
                float multiplier = Random.Range(0f, 1f);
                hunger += multiplier * Time.deltaTime;


                multiplier = Random.Range(0f, 1f);
                thirst += multiplier * Time.deltaTime;

                multiplier = Random.Range(0f, 1f);
                tiredness += multiplier * Time.deltaTime;
                return;

            case NPCType.Civilian:
                multiplier = Random.Range(0f, 1f);
                hunger += multiplier * Time.deltaTime * MainMechanics.mechanics.civTrafficMod;


                multiplier = Random.Range(0f, 1f);
                thirst += multiplier * Time.deltaTime * MainMechanics.mechanics.civTrafficMod;
                return;

            case NPCType.Employee:

                return;
        }
	}

    public bool goToPlace(List<Vector2> destinationCoords)
    {
        List<PathNode> toGo = new List<PathNode>();
        for (int i = 0; i < destinationCoords.Count; i++)
        {
            PathNode destination = MainMechanics.mechanics.mainGrid[(int)destinationCoords[i].x, (int)destinationCoords[i].y].pathNode;
            toGo.Add(destination);
        }

        return goToPlace(toGo);
    }

    public bool goToPlace(List<PathNode> destinationCoords)
    {
        clearPath();
        if (stuck == true)
        {
            return false;
        }

        if (thisNPCType != NPCType.Player && !MainMechanics.mechanics.MovementQueueReady(this))
        {
            return true;
        }

        for (int i = 0; i < destinationCoords.Count; i++)
        {
            bool foundWay = false;
            for (int k = 0; k < destinationCoords[i].Neighbors.Count; k++)
            {
                PathNode destination = destinationCoords[i];

                if (destination.Neighbors[k].Neighbors.Contains(destination))
                {
                    foundWay = true;
                    break;
                }
            }
            if (!foundWay)
            {
                //Debug.Log("Can't go here");
                destinationCoords.RemoveAt(i);
                i--;
                break;
            }
        }

        if (destinationCoords.Count <= 0)
        {
            return false;
        }

        /*if(foundAWay == false)
		{
			Debug.Log("Invalid Place To Stand: "+name);
			return false;
		}*/

        Dictionary<PathNode, bool> destinations = new Dictionary<PathNode, bool>();
		bool whoCares;
		for(int i = 0; i < destinationCoords.Count; i++)
		{
			if(!destinations.TryGetValue(destinationCoords[i], out whoCares))
			{
				destinations.Add(destinationCoords[i], false);
			}
		}

		Vector2 innkeeperGrid = WhereAmIOnGrid();
		List<PathNode> potentialPath = pathFinder.pathfind(
			MainMechanics.mechanics.mainGrid[(int)innkeeperGrid.x, (int)innkeeperGrid.y].pathNode,
			destinations, this);
		if(potentialPath != null && potentialPath.Count > 0)
		{
			pathToGo = potentialPath;
			return true;
		}
		if(!destinationCoords.Contains(MainMechanics.mechanics.mainGrid[(int)currentCoord.x, (int)currentCoord.y].pathNode))
		{
			Debug.Log(name+" is Stuck trying to go to "+destinationCoords+" from "+currentCoord);
			if(thisNPCType != NPCType.Player)
			{
				stuck = true;
				stuckVector = destinationCoords;
			}
		}
		return false;
	}


	public virtual void ExitArea()
	{
        currentGrid.peopleHere.Remove(this);
        mainTransform.position = truePosition;

		if(thisNPCType == NPCType.Adventurer)
		{
			if(partyApartOf != null)
			{
				AdventurerParty partyToCheck = (AdventurerParty)partyApartOf;
				if(partyToCheck.questEmbarkedOn == null && partyApartOf.Members.Count < 4)
				{
					partyToCheck.returnTimer = 60*(4 - partyApartOf.Members.Count); 
				}
                else
                {
                    while(potions.Count < 4)
                    {
                        potions.Add(FurnishingLibrary.Library.getHoldable(FurnishingLibrary.HoldableID.Bottle).GetComponent<PotionBottle>());
                        potions[potions.Count - 1].liquidHere = new MagicLiquid();
                        potions[potions.Count - 1].liquidHere.Set(FurnishingLibrary.HoldableID.LiqRPotion);
                        potions[potions.Count - 1].currentLiquidContainment = potions[potions.Count - 1].maxLiquidContainment;
                        potions[potions.Count - 1].mainTransform.parent = mainTransform;
                        potions[potions.Count - 1].gameObject.SetActive(false);
                    }
                }
			}
			
			MainMechanics.mechanics.AdventurersOnScene.Remove(this);
			MainMechanics.mechanics.AdventurersInReserve.Add(this);
			/*timeSinceOrderedDrink = 0;
			timeSinceOrderedDrinkMultiplier = 0;
			timeSinceOrderedFood = 0;
			timeSinceOrderedFoodMultiplier = 0;*/
			
			if((hunger > hungerTrigger || thirst > thirstTrigger || tiredness > tirednessTrigger)
                &&
                (timeSinceOrderedDrink > 0 || timeSinceOrderedFood > 0))

            {
				if(hunger > hungerTrigger)
				{
					Debug.Log(name+"'s Hunger not met");
				}
				if(thirst > thirstTrigger)
				{
					Debug.Log(name+"'s Thirst not met");
				}
				if(tiredness > tirednessTrigger)
				{
					Debug.Log(name+"'s Tiredness not met");
				}
				
				Debug.Log("It's been done Adv");
				
				hunger = -100;
				thirst = -100;
				tiredness = -100;
			}
		}
		else if(thisNPCType == NPCType.Builder)
		{
			MainMechanics.mechanics.BuildersOnScene.Remove(this);
			MainMechanics.mechanics.BuildersInReserve.Add(this);
		}
		else if(thisNPCType == NPCType.Civilian)
		{
            if (lookingForParty)
            {
                if (partyApartOf != null)
                {
                    MainMechanics.mechanics.CivilianParties.Remove(partyApartOf);
                    partyApartOf.removeMember(this);
                    partyApartOf = null;
                }

                if (!MainMechanics.mechanics.needToCull(NPCType.Civilian))
                {
                    MainMechanics.mechanics.CiviliansInReserve.Add(this);
                }
                else
                {
                    MainMechanics.mechanics.TownshipInReserve.Add(this);
                }

                MainMechanics.mechanics.CiviliansOnScene.Remove(this);
            }
            else
            {
                if (MainMechanics.mechanics.hasSpace(NPCType.Civilian))
                {
                    MainMechanics.mechanics.CiviliansInReserve.Add(this);
                }
                else
                {
                    MainMechanics.mechanics.TownshipInReserve.Add(this);
                }

                MainMechanics.mechanics.TownshipOnScene.Remove(this);
            }
            /*timeSinceOrderedDrink = 0;
			timeSinceOrderedDrinkMultiplier = 0;
			timeSinceOrderedFood = 0;
			timeSinceOrderedFoodMultiplier = 0;*/
			
			if((hunger >= hungerTrigger || thirst >= thirstTrigger || tiredness >= tirednessTrigger)
                &&
                (timeSinceOrderedDrink > 0 || timeSinceOrderedFood > 0))
			{
				if(hunger >= hungerTrigger)
				{
					Debug.Log(name+"'s Hunger not met");
				}
				if(thirst >= thirstTrigger)
				{
					Debug.Log(name+"'s Thirst not met");
				}
				if(tiredness >= tirednessTrigger)
				{
					Debug.Log(name+"'s Tiredness not met");
				}
				
				Debug.LogError("It's been done Civ");
				
				hunger = -100;
				thirst = -100;
			}
		}
		else if(thisNPCType == NPCType.Employee)
		{
			MainMechanics.mechanics.EmployeesOnScene.Remove(this);
			MainMechanics.mechanics.EmployeesInReserve.Add(this);
		}
		else if(thisNPCType == NPCType.DeliveryMan)
		{
			MainMechanics.mechanics.DeliveryMenOnScene.Remove(this);
			MainMechanics.mechanics.DeliveryMenInReserve.Add(this);
		}
		else if(thisNPCType == NPCType.QuestMaster)
		{
			MainMechanics.mechanics.QuestMasterOnScene.Remove(this);
			MainMechanics.mechanics.QuestMasterInReserve.Add(this);
        }
        if(MainMechanics.mechanics.targetHumanoid == this)
        {
            MainMechanics.mechanics.targetHumanoid = null;
        }
        gameObject.SetActive(false);
		if(heldObjects[0] != null)
		{
			heldObjects[0].gameObject.SetActive(false);
		}
		if(heldObjects[1] != null)
		{
			heldObjects[1].gameObject.SetActive(false);
		}
	}

	


	public void HeadForExit()
	{
        if (thisNPCType == NPCType.Civilian || thisNPCType == NPCType.Adventurer)
        {
            if (((currentCoord.x == 0 && cameFromRight) || (currentCoord.x == MainMechanics.GRID_X - 1 && !cameFromRight))
               && currentCoord.y < 5
               && Vector2.Distance(truePosition, mainTransform.position) < 0.25)
            {
                ExitArea();
                return;
            }
        }
        else
        {
            if (((currentCoord.x == 0 && !cameFromRight) || (currentCoord.x == MainMechanics.GRID_X - 1 && cameFromRight))
               && currentCoord.y < 5
               && Vector2.Distance(truePosition, mainTransform.position) < 0.25)
            {
                ExitArea();
                return;
            }
        }

        if (currentCoord.y < 5)
        {
            if (cameFromRight)
            {
                List<PathNode> path = new List<PathNode>();
                if (thisNPCType == NPCType.Civilian || thisNPCType == NPCType.Adventurer)
                {
                    for (int x = (int)currentCoord.x; x >= 0; x--)
                    {
                        path.Add(MainMechanics.mechanics.mainGrid[x, (int)currentCoord.y].pathNode);
                    }
                }
                else
                {
                    for (int x = (int)currentCoord.x; x < MainMechanics.GRID_X; x++)
                    {
                        path.Add(MainMechanics.mechanics.mainGrid[x, (int)currentCoord.y].pathNode);
                    }
                }
                pathToGo = path;
            }
            else
            {
                List<PathNode> path = new List<PathNode>();
                if (thisNPCType == NPCType.Civilian || thisNPCType == NPCType.Adventurer)
                {
                    for (int x = (int)currentCoord.x; x < MainMechanics.GRID_X; x++)
                    {
                        path.Add(MainMechanics.mechanics.mainGrid[x, (int)currentCoord.y].pathNode);
                    }
                }
                else
                {
                    for (int x = (int)currentCoord.x; x >= 0; x--)
                    {
                        path.Add(MainMechanics.mechanics.mainGrid[x, (int)currentCoord.y].pathNode);
                    }
                }
                pathToGo = path;
            }
        }
        else
        {
            if (thisNPCType == NPCType.Adventurer && partyApartOf != null && partyApartOf.Members[0] != this)
            {
                cameFromRight = partyApartOf.Members[0].cameFromRight;
            }
            int y = Random.Range(0, 5);
            List<Vector2> list = new List<Vector2>();
            for (int i = 0; i < MainMechanics.GRID_X; i++)
            {
                list.Add(new Vector2(i, y));
            }
            goToPlace(list);
        }
	}

	public Vector2 WhereAmIOnGrid()
	{
		Vector2 characterPos = truePosition;
		int x;
		int y;
		if(characterPos.x >= 0)
		{
			x = (int)(characterPos.x+0.5f);
		}
		else
		{
			x = (int)(characterPos.x-0.5f);
		}
		
		if(characterPos.y >= 0)
		{
			y = (int)(characterPos.y+0.5f);
		}
		else
		{
			y = (int)(characterPos.y-0.5f);
		}

		return new Vector2(x,y);
	}

	public void rekindleWants()
    {
        if(tableSittingAt != null)
        {
            tableSittingAt.fameToGive += ServiceFameToGive();
        }

        if (hunger > hungerTrigger - 10 && MainMechanics.menu.Count > 0)
		{
            List<FurnishingLibrary.HoldableID> sortedList = new List<FurnishingLibrary.HoldableID>();
            List<int> valueOfList = new List<int>();

            for(int i = 0; i < MainMechanics.menu.Count; i++)
            {
                if(sortedList.Count == 0)
                {
                    sortedList.Add(MainMechanics.menu[i]);
                    valueOfList.Add(feelings.getBasicFeelings(MainMechanics.menu[i]));
                }
                else
                {
                    int score = feelings.getBasicFeelings(MainMechanics.menu[i]);
                    if(score >= valueOfList[valueOfList.Count-1])
                    {
                        sortedList.Add(MainMechanics.menu[i]);
                        valueOfList.Add(score);
                    }
                }
            }

            for(int i = sortedList.Count-1; i >= 0; i--)
            {
                if(valueOfList[valueOfList.Count-1] > valueOfList[i])
                {
                    valueOfList.RemoveAt(i);
                    sortedList.RemoveAt(i);
                }
            }

            int rando = Random.Range(0,sortedList.Count);
            wantedFood = sortedList[rando];
        }

		if(thirst > thirstTrigger - 10)
		{
            if(tableSittingAt == null)
            {
                wantedDrink = FurnishingLibrary.HoldableID.HasWantOfDrink;
            }
            else
            {
                setDrinkWant(tableSittingAt.roomInsideOf.drinksAvailable);
            }
        }
	}

    public void setDrinkWant(List<FurnishingLibrary.HoldableID> drinkOpions)
    {
        if(drinkOpions.Count == 0)
        {
            return;
        }

        List<FurnishingLibrary.HoldableID> sortedList = new List<FurnishingLibrary.HoldableID>();
        List<int> valueOfList = new List<int>();

        for (int i = 0; i < drinkOpions.Count; i++)
        {
            if (sortedList.Count == 0)
            {
                int score = feelings.getBasicFeelings(drinkOpions[i]);
                sortedList.Add(drinkOpions[i]);
                valueOfList.Add(score);
            }
            else
            {
                int score = feelings.getBasicFeelings(drinkOpions[i]);
                if (score >= valueOfList[valueOfList.Count - 1])
                {
                    sortedList.Add(drinkOpions[i]);
                    valueOfList.Add(score);
                }
            }
        }

        for (int i = sortedList.Count - 1; i >= 0; i--)
        {
            if (valueOfList[valueOfList.Count - 1] > valueOfList[i])
            {
                valueOfList.RemoveAt(i);
                sortedList.RemoveAt(i);
            }
        }

        int rando = Random.Range(0, sortedList.Count);
        wantedDrink = sortedList[rando];
        Debug.Log(sortedList[rando].ToString());
    }

	public void takeDamage(int damage)
	{
		currentHP -= damage;
		if(currentHP <= 0)
		{
			killHumanoid();
		}
	}

    public void takeHealing(int damage)
    {
        currentHP += damage;
        if (currentHP > currentMaxHP)
        {
            currentHP = currentMaxHP;
        }
    }

    public virtual void killHumanoid()
	{
		if(gameObject.activeInHierarchy)
		{
			if(heldObjects[0] != null)
			{
				dropItemOnFloor(true);
			}
			if(heldObjects[1] != null)
			{
				dropItemOnFloor(false);
			}
		}
		else
		{
			if(heldObjects[0] != null)
			{
				Destroy (heldObjects[0].gameObject);
			}
			if(heldObjects[1] != null)
			{
				Destroy (heldObjects[1].gameObject);
			}
		}

        if (partyApartOf != null)
        {
            partyApartOf.removeMember(this);
        }

		Destroy(gameObject);
	}

	public void removeWorkStation(InteractableFurnishing workStation)
	{
        workStation.UnasignWorker(this);
		assignedFurnishings.Remove(workStation);
		for(int i = 0; i < currentWorkOrder.Count; i++)
		{
			if(currentWorkOrder[i].orderer == workStation)
			{
				currentWorkOrder.RemoveAt(i);
				i--;
			}
		}
	}

	/// <summary>
	/// Checks the hand for a consumable or cup. If the object is wanted, it consumes it. Else, it places it on the table.
	/// </summary>
	/// <param name="isLeft">If set to <c>true</c> is left.</param>
	void checkHand(bool isLeft)
	{
		int handnumber;
		if(isLeft)
		{
			handnumber = 0;
		}
		else
		{
			handnumber = 1;
		}

		if((wantedFood == heldObjects[handnumber].holdableID)
		   || (heldObjects[handnumber].liquidHere != null && heldObjects[handnumber].liquidHere.liquidID == wantedDrink && heldObjects[handnumber].currentLiquidContainment > 0))
		{
			heldObjects[handnumber].Use(this);
		}
		else
		{
			tableSittingAt.PutHoldablesOnTable(this);
		}
	}

    public bool TableAvailable()
    {
        List<Furnishing> tables;
        if (MainMechanics.mechanics.FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.Table, out tables))
        {
            for(int i = 0; i < tables.Count; i++)
            {
                Table toInspect = tables[i].GetComponent<Table>();
                if (!toInspect.called)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public float ServiceFameToGive()
    {
        float drinkTimeBonus = 0;
        float foodTimeBonus = 0;
        if(timeSinceOrderedDrink > 0.00001f)
        {
            drinkTimeBonus = (20 - timeSinceOrderedDrink)/5f;
        }
        if(timeSinceOrderedFood > 0.00001f)
        {
            foodTimeBonus = (40 - timeSinceOrderedFood)/10f;
        }
        float toReturn = drinkTimeBonus + foodTimeBonus;

        timeSinceOrderedDrink = 0;
        timeSinceOrderedDrinkMultiplier = 0;
        timeSinceOrderedFood = 0;
        timeSinceOrderedFoodMultiplier = 0;

        if (tableSittingAt.assignedEmployee != null)
        {
            EmployeeSkill skill;
            if (tableSittingAt.assignedEmployee.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Waitering, out skill))
            {
                if (toReturn < 0)
                {
                    toReturn /= 1f + (skill.level / 10f);
                }
                else
                {
                    toReturn *= 1f + (skill.level / 10f);
                }
            }
        }
        else
        {
            EmployeeSkill skill;
            if (MainMechanics.mechanics.Innkeeper.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Waitering, out skill))
            {
                if (toReturn < 0)
                {
                    toReturn /= 1f + skill.level / 10f;
                }
                else
                {
                    toReturn *= 1f + skill.level / 10f;
                }
            }
        }

        return toReturn;
    }

    public bool withinWorkingTime()
    {
        if(!hasNightShift && MainMechanics.currentTime == MainMechanics.TimeType.Day)
        {
            return true;
        }
        if(hasNightShift && MainMechanics.currentTime != MainMechanics.TimeType.Day)
        {
            return true;
        }
        return false;
    }

    public bool canLeave()
    {
        if(withinWorkingTime())
        {
            return false;
        }
        for(int i = 0; i < assignedFurnishings.Count; i++)
        {
            if(assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.Table 
                && (assignedFurnishings[i].assignedEmployee == this || assignedFurnishings[i].assignedEmployee == null))
            {
                Table toLookAt = assignedFurnishings[i].GetComponent<Table>();
                if (toLookAt.peopleHere > 0 || toLookAt.thingsThatNeedToBeCleaned.Count > 0)
                {
                    return false;
                }
            }
            else if(assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
            {
                List<Furnishing> toLookAt;
                if(MainMechanics.mechanics.FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.Stove, out toLookAt) && toLookAt.Count > 0)
                {
                    for(int s = 0; s < toLookAt.Count; s++)
                    {
                        if(toLookAt[s].GetComponent<Stove>().chefsCooking.Contains(this))
                        {
                            return false;
                        }
                    }
                }

                if (MainMechanics.mechanics.FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.Oven, out toLookAt) && toLookAt.Count > 0)
                {
                    for (int s = 0; s < toLookAt.Count; s++)
                    {
                        if (toLookAt[s].GetComponent<Oven>().chefsCooking.Contains(this))
                        {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public void RemoveAllJobs()
    {
        for (int i = 0; i < assignedFurnishings.Count; i++)
        {
            removeWorkStation(assignedFurnishings[i]);
        }
    }
    #region IFeelable
    public AdvancedFeeling getFeeling(Humanoid target)
    {
        return target.feelings.getFeeling(this);
    }

    public string getName()
    {
        return Name;
    }
    #endregion
}
