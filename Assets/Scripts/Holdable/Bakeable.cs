﻿using UnityEngine;
using System.Collections;

public class Bakeable : Holdable
{
	public float timeCooked = 0;
	public float perfectCookTime;
	public float burnCookTime;
	public float ashCookTime;
	public bool hasCalledChef = false;


	public void updateBakeTimer()
	{
		timeCooked += Time.deltaTime;
	}

	public Holdable pullFromOven(Humanoid chef)
	{
		Consumable toReturn;
		if(timeCooked < perfectCookTime)
		{
			toReturn = FurnishingLibrary.Library.getBakedObject (holdableID).GetComponent<Consumable>();
			toReturn.Set (chef, Consumable.CookedAmmount.UnderCooked);
			return toReturn;
		}
		else if(timeCooked >= perfectCookTime && timeCooked < burnCookTime)
		{
			toReturn = FurnishingLibrary.Library.getBakedObject (holdableID).GetComponent<Consumable>();
			toReturn.Set (chef, Consumable.CookedAmmount.Perfect);
			return toReturn;
		}
		else if(timeCooked >= burnCookTime && timeCooked < ashCookTime)
		{
			toReturn = FurnishingLibrary.Library.getBakedObject (holdableID).GetComponent<Consumable>();
			toReturn.Set (chef, Consumable.CookedAmmount.Burnt);
			return toReturn;
		}

		return null;
	}
}
