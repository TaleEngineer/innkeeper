﻿using UnityEngine;
using System.Collections;

public class PotionBottle : Cup
{
    public SpriteRenderer liquidRenderer;
    public Sprite[] fillSprites;
    public override void Use(Humanoid User)
    {
        if (currentLiquidContainment > 0)
        {
            User.startWorking(1, this, EmployeeSkill.WorkSkill.Generic, 0);
        }
    }

    public override void CompleteUse(Humanoid User)
    {
        if (currentLiquidContainment > 0)
        {
            currentLiquidContainment--;
            liquidHere.ConsumptionEffect(User);
        }

        if(currentLiquidContainment <= 0)
        {
            liquidHere = null;
        }
    }

    public override void Draw()
    {

    }

    public override void Update()
    {
        base.Update();

        if(liquidHere == null && liquidRenderer.enabled)
        {
            liquidRenderer.enabled = false;
        }
        else if(liquidHere != null && !liquidRenderer.enabled)
        {
            liquidRenderer.enabled = true;
            liquidRenderer.color = liquidHere.liquidColor;
        }
        else if(currentLiquidContainment > 0)
        {
            liquidRenderer.sprite = fillSprites[currentLiquidContainment - 1];
        }
    }
}
