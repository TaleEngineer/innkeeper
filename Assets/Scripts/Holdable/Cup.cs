﻿using UnityEngine;
using System.Collections;

public class Cup : Holdable
{
	public override void Use (Humanoid User)
	{
		if(liquidHere != null)
		{
			User.startWorking(currentLiquidContainment, this, EmployeeSkill.WorkSkill.Generic, 0);
		}
	}

	public override void CompleteUse (Humanoid User)
	{
		if(liquidHere != null)
		{
			liquidHere.ConsumptionEffect(User);
			currentLiquidContainment = 0;
			liquidHere = null;
		}
	}
}
