﻿using UnityEngine;
using System.Collections;

public class Sack : Holdable
{
	public override void Use (Humanoid User)
	{
		if(User.heldObjects[1] == null && holdablesInThisHoldable.Count > 0)
		{
			User.heldObjects[1] = FurnishingLibrary.Library.getHoldable(holdablesInThisHoldable[0]).GetComponent<Holdable>();
			holdablesInThisHoldable.RemoveAt(0);
		}
	}
}
