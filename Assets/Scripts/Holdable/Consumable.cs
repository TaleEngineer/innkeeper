﻿using UnityEngine;
using System.Collections;

public class Consumable : Holdable
{
	public enum CookedAmmount
	{
		Error,
		UnderCooked,
		Perfect,
		Burnt
	}
	public CookedAmmount howWellCooked; 
	public float TasteQuality = 0;
	public float hungerQuench;

	/// <summary>
	/// Set the specified Quality and How well Cooked.
	/// </summary>
	/// <param name="Quality">Quality.</param>
	/// <param name="howCooked">How cooked.</param>
	public void Set(Humanoid chef, CookedAmmount howCooked)
	{
		howWellCooked = howCooked;
	}

	public override void Use (Humanoid User)
	{
		User.startWorking(1, this, EmployeeSkill.WorkSkill.Generic, 0);
	}

	public override void CompleteUse (Humanoid User)
	{
		base.CompleteUse (User);
		if(User.wantedFood == holdableID)
		{
			User.wantedFood = FurnishingLibrary.HoldableID.Error;
		}


        if (User.thisNPCType == Humanoid.NPCType.Civilian || User.thisNPCType == Humanoid.NPCType.Adventurer)
        {
            User.tableSittingAt.moneyToGive += cost;
            User.tableSittingAt.fameToGive += (int)TasteQuality;
            User.timeSinceOrderedFoodMultiplier = 0;
            //MainMechanics.mechanics.moneyChange(cost, User.mainTransform.position);
        }

        User.hunger -= hungerQuench;
		Destroy (this.gameObject);
	}
}
