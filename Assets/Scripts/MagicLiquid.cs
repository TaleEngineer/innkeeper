﻿using UnityEngine;
using System.Collections;

public class MagicLiquid : Liquid {
    public int healAmmount;

    public override void Set(FurnishingLibrary.HoldableID ID)
    {
        liquidID = ID;

        liquidName = FurnishingLibrary.Library.getItemName(ID, true);

        liquidColor = FurnishingLibrary.Library.getLiquidColor(ID);

        thirstQuench = 10f;

        if (ID == FurnishingLibrary.HoldableID.LiqRPotion)
        {
            healAmmount = 25;
        }
        else if (ID == FurnishingLibrary.HoldableID.LiqBPotion)
        {
            healAmmount = 50;
        }
        else if (ID == FurnishingLibrary.HoldableID.LiqGPotion)
        {
            healAmmount = 100;
        }
    }

    public override void ConsumptionEffect(Humanoid Consumer)
    {
        Debug.Log("Potion used by "+Consumer.name);
        base.ConsumptionEffect(Consumer);
        Consumer.takeHealing(healAmmount);
    }
}
