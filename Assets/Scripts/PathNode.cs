﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathNode : MonoBehaviour 
{
	public List<Furnishing> reachableFurnishing = new List<Furnishing>();
	public List<PathNode> Neighbors = new List<PathNode>();
	public List<Wall> walls = new List<Wall>();
	public Grid gridHere;
	public Transform mainTransform;
    public List<Humanoid> toTreadHere = new List<Humanoid>();

	void Awake()
	{
		mainTransform = GetComponent<Transform>();
	}

	// Use this for initialization
	void Start () 
	{
		mainTransform.parent = MainMechanics.mechanics.pathFolder.transform;
		name = "Pathnode X:"+mainTransform.position.x+ " Y: "+mainTransform.position.y;
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	public void setNeighbors()
	{
		Neighbors.Clear();
		walls.Clear();
		reachableFurnishing.Clear();
        while(toTreadHere.Count > 0)
        {
            toTreadHere[0].resetPath();
        }

		addNeighbor(new Vector2(1,0));
		addNeighbor(new Vector2(0,1));
		addNeighbor(new Vector2(-1,0));
		addNeighbor(new Vector2(0,-1));
	}

	public bool canFindWayHere(Vector2 dir)
	{
		Physics2D.queriesStartInColliders = false;
		var castHit = Physics2D.Raycast((Vector2)transform.position+dir, -dir, 1f);
		
		if(castHit)
		{
			if(castHit.collider.GetComponent<PathNode>())
			{
				return true;
			}
			else if(castHit.collider.CompareTag("Wall"))
			{
				Wall wall = castHit.collider.GetComponent<Wall>();
				if(!wall.buildComplete)
				{
					castHit = Physics2D.Raycast(castHit.collider.transform.position, -dir, 0.5f);
					
					if(castHit)
					{
						if(castHit.collider.GetComponent<PathNode>())
						{
							return true;
						}
					}
				}
			}
			else if(castHit.collider.CompareTag("Door"))
			{
				Wall wallHere = castHit.collider.GetComponentInParent<Wall>();
				castHit = Physics2D.Raycast(castHit.collider.transform.position, -dir, 0.5f);
				if(castHit)
				{
					if(castHit.collider.GetComponent<PathNode>())
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	void addNeighbor(Vector2 dir)
	{
		RaycastHit2D castHit;

		Physics2D.queriesStartInColliders = false;
		castHit = Physics2D.Raycast(mainTransform.position, dir, 1f);
		
		if(castHit)
		{
			if(castHit.collider.GetComponent<PathNode>())
			{
				Neighbors.Add(castHit.collider.GetComponent<PathNode>());
			}
			else if(castHit.collider.CompareTag("Wall"))
			{
				Wall wall = castHit.collider.GetComponent<Wall>();
				if(!wall.buildComplete)
				{
					castHit = Physics2D.Raycast(castHit.collider.transform.position, dir, 0.5f);

					if(castHit)
					{
						if(castHit.collider.GetComponent<PathNode>())
						{
							Neighbors.Add(castHit.collider.GetComponent<PathNode>());
						}
						else if(castHit.collider.GetComponent<Furnishing>() && (castHit.collider.GetComponent<Furnishing>().wallMounted || canFindWayHere(dir))) //(gridHere.furnishingHere == null || gridHere.furnishingHere == castHit.collider.GetComponent<Furnishing>()))
						{
							Furnishing furnishToAdd = castHit.collider.GetComponent<Furnishing>();
							reachableFurnishing.Add(furnishToAdd);
							if(!furnishToAdd.reachableNodes.Contains(this))
							{
								furnishToAdd.reachableNodes.Add(this);
							}
						}
					}
				}
				walls.Add(wall);
			}
			else if(castHit.collider.CompareTag("Door"))
			{
				Wall wallHere = castHit.collider.GetComponentInParent<Wall>();
				if(wallHere != null)
				{
					walls.Add(wallHere);
				}
				castHit = Physics2D.Raycast(castHit.collider.transform.position, dir, 0.5f);
				if(castHit)
				{
					if(castHit.collider.GetComponent<PathNode>())
					{
						Neighbors.Add(castHit.collider.GetComponent<PathNode>());
					}
					else if(castHit.collider.GetComponent<Furnishing>() && (castHit.collider.GetComponent<Furnishing>().wallMounted || canFindWayHere(dir))) //(gridHere.furnishingHere == null || gridHere.furnishingHere == castHit.collider.GetComponent<Furnishing>()))
					{
						Furnishing furnishToAdd = castHit.collider.GetComponent<Furnishing>();
						reachableFurnishing.Add(furnishToAdd);
						if(!furnishToAdd.reachableNodes.Contains(this))
						{
							furnishToAdd.reachableNodes.Add(this);
						}
					}
				}
			}
			else if(castHit.collider.GetComponent<Furnishing>()  && (castHit.collider.GetComponent<Furnishing>().wallMounted || canFindWayHere(dir))) //(gridHere.furnishingHere == null || gridHere.furnishingHere == castHit.collider.GetComponent<Furnishing>()))
			{
				Furnishing furnishToAdd = castHit.collider.GetComponent<Furnishing>();
				reachableFurnishing.Add(furnishToAdd);
				if(!furnishToAdd.reachableNodes.Contains(this))
				{
					furnishToAdd.reachableNodes.Add(this);
				}

				if(furnishToAdd.wallMounted)
				{
					RaycastHit2D[] castHits;
					
					Physics2D.queriesStartInColliders = true;
					castHits = Physics2D.RaycastAll(mainTransform.position, dir, 1f);
					for(int i = 0; i < castHits.Length; i++)
					{
						if(castHits[i].collider.GetComponent<Wall>())
						{
							walls.Add(castHits[i].collider.GetComponent<Wall>());
						}
					}
				}
			}
		}
	}
}
