﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorkOrder
{
	public enum WorkOrderType
	{
		Error,
		RetrieveItem,
		ActivateSpecificFurnishing,
		ActiveateFurnishingID,
		ActiveateFurnishingIDWithLiquidID,
		FindTable,
		ActivateFurnishingFromList,
		RetrieveSpecificItem,
		GoToBed,
		PutOrderedFoodOnKitchenWindow,
        PutBakeableIntoStove,
        PutBakeableIntoOven,
        GoToSpot
	}
	public WorkOrderType currentWorkOrderType;

	public enum TaskEndState
	{
		Error,
		TaskComplete,
		TaskMovingForward,
		TaskFailure
	}

	public FurnishingLibrary.HoldableID holdableToFindID;
	public Furnishing furnishingToActivate;
	public FurnishingLibrary.FurnishingID furnishingIDToFind;
	public int actionToPerform;
	public Recipe toCraft;
	public Holdable holdableToFind;

	public Humanoid worker;
	public Furnishing orderer;
	public Party party;

    public List<PathNode> ToGoTo;

    /// <summary>
	/// Set to go to the closest of a spot.
	/// </summary>
	public void Set(List<PathNode> placement, Humanoid _worker)
    {
        ToGoTo = placement;
        worker = _worker;
        currentWorkOrderType = WorkOrderType.GoToSpot;
    }

    /// <summary>
    /// Set to go to an empty bed.
    /// </summary>
    /// <param name="furnishingIDToGo">Furnishing identifier to go.</param>
    /// <param name="action">Action.</param>
    /// <param name="_worker">_worker.</param>
    /// <param name="_orderer">_orderer.</param>
    public void Set(Humanoid _worker)
	{
		worker = _worker;
		currentWorkOrderType = WorkOrderType.GoToBed;
	}


	/// <summary>
	/// Set to retrieve an Item
	/// </summary>
	/// <param name="itemToGet">Item to get.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(FurnishingLibrary.HoldableID itemToGet, Humanoid _worker, Furnishing _orderer)
	{
		holdableToFindID = itemToGet;
		worker = _worker;
		orderer = _orderer;
		currentWorkOrderType = WorkOrderType.RetrieveItem;
	}

	/// <summary>
	/// Set to retrieve a specific Holdable
	/// </summary>
	/// <param name="itemToGet">Item to get.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(Holdable itemToGet, Humanoid _worker, Furnishing _orderer)
	{
		holdableToFind = itemToGet;
		worker = _worker;
		orderer = _orderer;
		currentWorkOrderType = WorkOrderType.RetrieveSpecificItem;
	}

	/// <summary>
	/// Set to go to specific Furnishing
	/// </summary>
	/// <param name="furnishingToGo">Furnishing to go.</param>
	/// <param name="action">Action.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(Furnishing furnishingToGo, int action, Humanoid _worker, Furnishing _orderer)
	{
		furnishingToActivate = furnishingToGo;
		worker = _worker;
		orderer = _orderer;
		actionToPerform = action;
		currentWorkOrderType = WorkOrderType.ActivateSpecificFurnishing;
	}

	/// <summary>
	/// Put Holdable into KitchenWindow
	/// </summary>
	/// <param name="furnishingToGo">ID of Holdable to place.</param>
	/// <param name="action">Action.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(FurnishingLibrary.HoldableID ID, int action, Humanoid _worker, Furnishing _orderer)
	{
		holdableToFindID = ID;
		worker = _worker;
		orderer = _orderer;
		actionToPerform = action;
		currentWorkOrderType = WorkOrderType.PutOrderedFoodOnKitchenWindow;
	}

	/// <summary>
	/// Set to go to specific Furnishing
	/// </summary>
	/// <param name="furnishingToGo">Furnishing to go.</param>
	/// <param name="action">Action.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(List<Furnishing> furnishingToGo, int action, Humanoid _worker, Furnishing _orderer)
	{
		Furnishing closestFurnishing = null;
		for(int i = 0; i < furnishingToGo.Count; i++)
		{
			if(closestFurnishing == null ||
			   Vector2.Distance(furnishingToGo[i].mainTransform.position, _worker.truePosition) 
			   < Vector2.Distance(closestFurnishing.mainTransform.position, _worker.truePosition))
			{
				closestFurnishing = furnishingToGo[i];
			}
		}
		furnishingToActivate = closestFurnishing;
		worker = _worker;
		orderer = _orderer;
		actionToPerform = action;
		currentWorkOrderType = WorkOrderType.ActivateSpecificFurnishing;
	}

	/// <summary>
	/// Set to go to a Furnishing of a specific ID
	/// </summary>
	/// <param name="furnishingIDToGo">Furnishing identifier to go.</param>
	/// <param name="action">Action.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(FurnishingLibrary.FurnishingID furnishingIDToGo, int action, Humanoid _worker, Furnishing _orderer)
	{
		furnishingIDToFind = furnishingIDToGo;
		worker = _worker;
		orderer = _orderer;
		actionToPerform = action;
        if (furnishingIDToGo == FurnishingLibrary.FurnishingID.Stove && action == 0)
        {
            currentWorkOrderType = WorkOrderType.PutBakeableIntoStove;
        }
        else if (furnishingIDToGo == FurnishingLibrary.FurnishingID.Oven && action == 1)
        {
            currentWorkOrderType = WorkOrderType.PutBakeableIntoOven;
        }
        else
        {
            currentWorkOrderType = WorkOrderType.ActiveateFurnishingID;
        }
	}

	/// <summary>
	/// Set to go to a Furnishing of a specific ID with a liquid of a specific ID
	/// </summary>
	/// <param name="furnishingIDToGo">Furnishing identifier to go.</param>
	/// <param name="action">Action.</param>
	/// <param name="_worker">_worker.</param>
	/// <param name="_orderer">_orderer.</param>
	public void Set(FurnishingLibrary.FurnishingID furnishingIDToGo, FurnishingLibrary.HoldableID liquidID, int action, Humanoid _worker, Furnishing _orderer)
	{
		furnishingIDToFind = furnishingIDToGo;
		holdableToFindID = liquidID;
		worker = _worker;
		orderer = _orderer;
		actionToPerform = action;
		currentWorkOrderType = WorkOrderType.ActiveateFurnishingIDWithLiquidID;
	}

	/// <summary>
	/// Set to find empty table or follow party leader.
	/// </summary>
	/// <param name="_worker">_worker.</param>
	/// <param name="_party">_party.</param>
	public void Set(Humanoid _worker, Party _party)
	{
		if(_party.Members[0] == _worker)
		{
			furnishingIDToFind = FurnishingLibrary.FurnishingID.Table;
		}
		party = _party;
		worker = _worker;
		currentWorkOrderType = WorkOrderType.FindTable;
	}


	/// <summary>
	/// Gets the order.
	/// </summary>
	/// <returns>The order.</returns>
	public TaskEndState GetOrder()
	{
		if(currentWorkOrderType == WorkOrderType.Error)
		{
			return TaskEndState.Error;
		}
        else if(currentWorkOrderType == WorkOrderType.GoToSpot)
        {
            for(int i = 0; i < ToGoTo.Count; i++)
            {
                if(worker.truePosition == ToGoTo[i].mainTransform.position)
                {
                    return TaskEndState.TaskComplete;
                }
            }

            if(worker.goToPlace(ToGoTo))
            {
                return TaskEndState.TaskMovingForward;
            }

            return TaskEndState.TaskFailure;
        }
		else if(currentWorkOrderType == WorkOrderType.PutOrderedFoodOnKitchenWindow)
		{
			List<Furnishing> windows;
			if(MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.KitchenWindow, out windows))
			{
				List<Furnishing> applicableWindows = new List<Furnishing>();
				for(int i = 0; i < windows.Count; i++)
				{
					KitchenWindow lookin = (KitchenWindow) windows[i];

					if(!lookin.doesHaveEnough(holdableToFindID))
					{
						applicableWindows.Add(lookin);
					}
				}
				if(applicableWindows.Count > 0)
				{
					List<PathNode> whereGo = new List<PathNode>();

					for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
					{
						if(applicableWindows.Contains(worker.currentGrid.pathNode.reachableFurnishing[i]))
						{
							worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
							return TaskEndState.TaskComplete;
							//Debug.Log("Job Over");
						}
					}

					for(int i = 0; i < applicableWindows.Count; i++)
					{
						for(int n = 0; n < applicableWindows[i].reachableNodes.Count; n++)
						{
							whereGo.Add(applicableWindows[i].reachableNodes[n]);
						}
					}

					if(whereGo.Count > 0)
					{
						worker.goToPlace(whereGo);
						return TaskEndState.TaskMovingForward;
					}
				}
			}
            else
            {
                PrepTable whereGo = null;
                for(int i = 0; i < worker.assignedFurnishings.Count; i++)
                {
                    if(worker.assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
                    {
                        whereGo = (PrepTable)worker.assignedFurnishings[i];
                        break;
                    }
                }
                if(whereGo != null)
                {
                    Set(whereGo, actionToPerform, worker, orderer);
                }
            }
			return TaskEndState.TaskFailure;
		}
		else if(currentWorkOrderType == WorkOrderType.GoToBed)
		{
			for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
			{
				Furnishing toExamine = worker.currentGrid.pathNode.reachableFurnishing[i];
				if(toExamine.FurnishingID == FurnishingLibrary.FurnishingID.Bed)
				{
					Bed bed = (Bed)toExamine;
					if(bed.roomInsideOf.bedsAvailable(worker))
					{
						bed.getInteraction(worker, 1);
						return TaskEndState.TaskComplete;
					}
				}
			}

			List<Bed> beds = new List<Bed>();
			List<Furnishing> output;
			List<PathNode> places = new List<PathNode>();
			if(MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.Bed, out output))
			{
				for(int i = 0; i < output.Count; i++)
				{
					Bed hat = (Bed)output[i];
					if(hat.occupant == null && hat.roomInsideOf != null && hat.roomInsideOf.bedsAvailable(worker))
					{
						beds.Add(hat);
					}
				}

				for(int i = 0; i < beds.Count; i++)
				{
					for(int n = 0; n < beds[i].reachableNodes.Count; n++)
					{
						places.Add(beds[i].reachableNodes[n]);
					}
				}

				bool canDo = worker.goToPlace(places);
				if(canDo)
				{
					return TaskEndState.TaskMovingForward;
				}
			}
			return TaskEndState.TaskFailure;
		}
		else if(currentWorkOrderType == WorkOrderType.FindTable)
		{
			if(furnishingToActivate != null && worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
			{
				for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
				{
					if(worker.currentGrid.pathNode.reachableFurnishing[i] == furnishingToActivate)
					{
						worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, 1);
						return TaskEndState.TaskComplete;
					}
				}
			}
			
			Furnishing closestFurnishing = null;
			List<PathNode> closestNode = null;

			List<Furnishing> output;

			if(party.Members[0] == worker && furnishingToActivate == null && MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.Table, out output))
			{
				for(int i = 0; i < output.Count; i++)
				{
					Table toLookAt = output[i].GetComponent<Table>();
					if(output[i].FurnishingID == FurnishingLibrary.FurnishingID.Table
					   && (toLookAt.customersHere[0] == null
					    || toLookAt.customersHere[0].partyApartOf == worker.partyApartOf)
					   && toLookAt.called == false
					   && toLookAt.thingsThatNeedToBeCleaned.Count == 0)
					{
						if(toLookAt.customersHere[0] != null && toLookAt.customersHere[0].partyApartOf == worker.partyApartOf)
						{
							closestFurnishing = output[i];
							furnishingToActivate = output[i];
							break;
						}
						else if(closestFurnishing == null
						   || Vector2.Distance(output[i].mainTransform.position, worker.truePosition) 
						   < Vector2.Distance(closestFurnishing.mainTransform.position, worker.truePosition))
						{
							closestFurnishing = output[i];
							furnishingToActivate = output[i];
						}
					}
				}
			}
			else if(party.Members[0].currentWorkOrder.Count > 0
			        && party.Members[0].currentWorkOrder[0].furnishingToActivate != null)
			{
				closestFurnishing = party.Members[0].currentWorkOrder[0].furnishingToActivate;
				furnishingToActivate = party.Members[0].currentWorkOrder[0].furnishingToActivate;
			}
			else if(party.Members[0].tableSittingAt != null)
			{
				closestFurnishing = party.Members[0].tableSittingAt;
				furnishingToActivate = party.Members[0].tableSittingAt;
			}

			if(closestFurnishing != null)
			{
                closestNode = closestFurnishing.reachableNodes;
				/*for(int i = 0; i < closestFurnishing.reachableNodes.Count; i++)
				{
					if(closestNode == null || 
					   Vector2.Distance(closestFurnishing.reachableNodes[i].mainTransform.position, worker.truePosition) 
					   < Vector2.Distance(closestNode.mainTransform.position, worker.truePosition))
					{
						closestNode = closestFurnishing.reachableNodes[i];
					}
				}*/
			}
			if(closestNode != null && closestNode.Count > 0)
			{
				bool canDo = worker.goToPlace(closestNode);
				if(canDo)
				{
					closestFurnishing.GetComponent<Table>().called = true;
					return TaskEndState.TaskMovingForward;
				}
				else
				{
					return TaskEndState.TaskFailure;
				}
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
		else if(currentWorkOrderType == WorkOrderType.ActivateSpecificFurnishing)
		{
			if(worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
			{
				for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
				{
					if(worker.currentGrid.pathNode.reachableFurnishing[i] == furnishingToActivate)
					{
						worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
						return TaskEndState.TaskComplete;
					}
				}
			}
			List<PathNode> closest = furnishingToActivate.reachableNodes;
			/*for(int i = 0; i < furnishingToActivate.reachableNodes.Count; i++)
			{

				closest.Add(furnishingToActivate.reachableNodes[i]);
			}*/
			if(closest != null && closest.Count > 0)
			{
				bool canDo = worker.goToPlace(closest);
				if(canDo)
				{
					return TaskEndState.TaskMovingForward;
				}
				else
				{
					return TaskEndState.TaskFailure;
				}
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
		else if(currentWorkOrderType == WorkOrderType.ActiveateFurnishingID)
		{
			if(worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
			{
				for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
				{
					if(worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == furnishingIDToFind)
					{
						worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
						return TaskEndState.TaskComplete;
					}
				}
			}
			
			List<Furnishing> closestFurnishing = new List<Furnishing>();
			List<PathNode> closestNode = new List<PathNode>();

			List<Furnishing> output;
			if(MainMechanics.mechanics.hasFurnishing(furnishingIDToFind, out output))
			{
				for(int i = 0; i < output.Count; i++)
				{

					closestFurnishing.Add(output[i]);
				}
			}

			if(closestFurnishing != null)
			{
				for(int j = 0; j < closestFurnishing.Count; j++)
				{
					for(int i = 0; i < closestFurnishing[j].reachableNodes.Count; i++)
					{
						 
						closestNode.Add(closestFurnishing[j].reachableNodes[i]);
					}
				}
			}
			if(closestNode != null)
			{
				bool canDo = worker.goToPlace(closestNode);
				if(canDo)
				{
					return TaskEndState.TaskMovingForward;
				}
				else
				{
					return TaskEndState.TaskFailure;
				}
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
		else if(currentWorkOrderType == WorkOrderType.RetrieveSpecificItem)
		{
			if(worker.currentGrid.holdablesHere.Count > 0)
			{
				for(int i = 0; i < worker.currentGrid.holdablesHere.Count; i++)
				{
					if(worker.currentGrid.holdablesHere[i] == holdableToFind)
					{
						if(worker.heldObjects[0] == null)
						{
							worker.PickUpItem(true, i);
							return TaskEndState.TaskComplete;
						}
						if(worker.heldObjects[1] == null)
						{
							worker.PickUpItem(false, i);
							return TaskEndState.TaskComplete;
						}
						
						worker.dropItemOnFloor(true);
						worker.PickUpItem(true, i);
						return TaskEndState.TaskComplete;
					}
				}
				
				
			}
			
			if(holdableToFind != null)
			{
				List<Vector2> list = new List<Vector2>();
				list.Add(holdableToFind.mainTransform.position);
				worker.goToPlace(list);
				return TaskEndState.TaskMovingForward;
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
		else if(currentWorkOrderType == WorkOrderType.RetrieveItem)
		{
			bool handleLiquid = false;
			
			string String = holdableToFindID.ToString();
			var charArray = String.ToCharArray(0,3);
			if(charArray[0].Equals('L') && charArray[1].Equals('i') && charArray[2].Equals('q'))
			{
				handleLiquid = true;
			}
			
			
			if(worker.currentGrid.holdablesHere.Count > 0)
			{
				for(int i = 0; i < worker.currentGrid.holdablesHere.Count; i++)
				{
					if(handleLiquid)
					{
						if(worker.currentGrid.holdablesHere[i].liquidHere != null
						   && worker.currentGrid.holdablesHere[i].liquidHere.liquidID == holdableToFindID)
						{
							if(worker.heldObjects[0] == null)
							{
								worker.PickUpItem(true, i);
								return TaskEndState.TaskComplete;
							}
							if(worker.heldObjects[1] == null)
							{
								worker.PickUpItem(false, i);
								return TaskEndState.TaskComplete;
							}
							
							worker.dropItemOnFloor(true);
							worker.PickUpItem(true, i);
							return TaskEndState.TaskComplete;
						}
					}
					else
					{
						if(worker.currentGrid.holdablesHere[i].holdableID == holdableToFindID)
						{
							if(worker.heldObjects[0] == null)
							{
								worker.PickUpItem(true, i);
								return TaskEndState.TaskComplete;
							}
							if(worker.heldObjects[1] == null)
							{
								worker.PickUpItem(false, i);
								return TaskEndState.TaskComplete;
							}

							worker.dropItemOnFloor(true);
							worker.PickUpItem(true, i);
							return TaskEndState.TaskComplete;
						}
					}
				}


			}

            List<Holdable> whocares;
            if(!handleLiquid && orderer.FurnishingID != FurnishingLibrary.FurnishingID.CellarStairs && CellarStairs.CellarStorage.TryGetValue(holdableToFindID, out whocares))
            {
                List<Furnishing> cellarStairsList;
                if (MainMechanics.mechanics.FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.CellarStairs, out cellarStairsList))
                {
                    for (int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
                    {
                        if (worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == FurnishingLibrary.FurnishingID.CellarStairs)
                        {
                            worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, -(int)holdableToFindID);
                            return TaskEndState.TaskComplete;
                        }
                    }

                    List<PathNode> adjacentTiles = new List<PathNode>();
                    for (int i = 0; i < cellarStairsList.Count; i++)
                    {
                        for (int n = 0; n < cellarStairsList[i].reachableNodes.Count; n++)
                        {
                            adjacentTiles.Add(cellarStairsList[i].reachableNodes[n]);
                        }
                    }

                    if(adjacentTiles.Count > 0 && worker.goToPlace(adjacentTiles))
                    {
                        return TaskEndState.TaskMovingForward;
                    }
                }
            }


			List<Holdable> closestHoldable = new List<Holdable>();
			if(handleLiquid)
			{
				List<FurnishingLibrary.HoldableID> hKeys = new List<FurnishingLibrary.HoldableID>(MainMechanics.mechanics.HoldablesOnGround.Keys);
				for(int k = 0; k < hKeys.Count; k++)
				{
					for(int i = 0; i < MainMechanics.mechanics.HoldablesOnGround[hKeys[k]].Count; i++)
					{
						if(MainMechanics.mechanics.HoldablesOnGround[hKeys[k]][i].liquidHere != null
						   && MainMechanics.mechanics.HoldablesOnGround[hKeys[k]][i].liquidHere.liquidID == holdableToFindID)
						{
							closestHoldable.Add(MainMechanics.mechanics.HoldablesOnGround[hKeys[k]][i]);
						}
					}
				}
			}
			else
			{
                List<Holdable> holdableList;
                if (MainMechanics.mechanics.HoldablesOnGround.TryGetValue(holdableToFindID, out holdableList))
                {
                    for (int i = 0; i < holdableList.Count; i++)
                    {
                        if (holdableList[i].liquidHere == null)
                        {
                            closestHoldable.Add(holdableList[i]);
                        }
                    }
                }
			}

			if(closestHoldable.Count > 0)
			{
				List<Vector2> list = new List<Vector2>();
				for(int i = 0; i < closestHoldable.Count; i++)
				{
					list.Add(closestHoldable[i].mainTransform.position);
				}
				worker.goToPlace(list);
				return TaskEndState.TaskMovingForward;
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
		else if(currentWorkOrderType == WorkOrderType.ActiveateFurnishingIDWithLiquidID)
		{
			if(worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
			{
				Furnishing tempHolder = null;
				for(int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
				{
					if(worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == furnishingIDToFind
					   && worker.currentGrid.pathNode.reachableFurnishing[i].liquidHere != null
					   && worker.currentGrid.pathNode.reachableFurnishing[i].liquidHere.liquidID == holdableToFindID
					   && worker.currentGrid.pathNode.reachableFurnishing[i].currentLiquidContainment >= 20)
					{
						worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
						return TaskEndState.TaskComplete;
					}
					else if(worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == furnishingIDToFind
					        && worker.currentGrid.pathNode.reachableFurnishing[i].liquidHere != null
					        && worker.currentGrid.pathNode.reachableFurnishing[i].liquidHere.liquidID == holdableToFindID
					        && (tempHolder == null || worker.currentGrid.pathNode.reachableFurnishing[i].currentLiquidContainment > tempHolder.currentLiquidContainment))
					{
						tempHolder = worker.currentGrid.pathNode.reachableFurnishing[i];
					}
				}
				if(tempHolder != null)
				{
					tempHolder.getInteraction(worker, actionToPerform);
					return TaskEndState.TaskComplete;
				}
			}

			List<Furnishing> closestTempHolder = new List<Furnishing>();
			List<Furnishing> closestFurnishing = new List<Furnishing>();
			List<PathNode> closestNode = new List<PathNode>();

			List<Furnishing> output;
			if(MainMechanics.mechanics.hasFurnishing(furnishingIDToFind, out output))
			{
			
				for(int i = 0; i < output.Count; i++)
				{
					if(output[i].liquidHere != null
					   && output[i].liquidHere.liquidID == holdableToFindID
					   && output[i].currentLiquidContainment >= 20)
					{
						closestFurnishing.Add(output[i]);
					}
					else if(output[i].liquidHere != null
					        && output[i].liquidHere.liquidID == holdableToFindID)
					{
						closestTempHolder.Add(output[i]);
					}
				}
			}
			
			if(closestFurnishing.Count > 0)
			{
				for(int j = 0; j < closestFurnishing.Count; j++)
				{
					for(int i = 0; i < closestFurnishing[j].reachableNodes.Count; i++)
					{
						closestNode.Add(closestFurnishing[j].reachableNodes[i]);
					}
				}
			}
			else if(closestTempHolder.Count > 0)
			{
				for(int j = 0; j < closestTempHolder.Count; j++)
				{
					for(int i = 0; i < closestTempHolder[j].reachableNodes.Count; i++)
					{
						closestNode.Add(closestTempHolder[j].reachableNodes[i]);
					}
				}
			}
			if(closestNode != null)
			{
				bool canDo = worker.goToPlace(closestNode);
				if(canDo)
				{
					return TaskEndState.TaskMovingForward;
				}
				else
				{
					return TaskEndState.TaskFailure;
				}
			}
			else
			{
				return TaskEndState.TaskFailure;
			}
		}
        else if (currentWorkOrderType == WorkOrderType.PutBakeableIntoStove)
        {
            if (worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
            {
                for (int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
                {
                    if (worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == furnishingIDToFind)
                    {
                        Stove toCheck = (Stove)worker.currentGrid.pathNode.reachableFurnishing[i];
                        if (toCheck.stoveTop[0] == null || toCheck.stoveTop[1] == null)
                        {
                            worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
                            return TaskEndState.TaskComplete;
                        }
                    }
                }
            }

            List<Stove> closestFurnishing = new List<Stove>();
            List<PathNode> closestNode = new List<PathNode>();

            List<Furnishing> output;
            if (MainMechanics.mechanics.hasFurnishing(furnishingIDToFind, out output))
            {
                for (int i = 0; i < output.Count; i++)
                {
                    Stove toCheck = (Stove)output[i];
                    if (toCheck.stoveTop[0] == null || toCheck.stoveTop[1] == null)
                    {
                        closestFurnishing.Add((Stove)output[i]);
                    }
                }
            }

            if (closestFurnishing != null)
            {
                for (int j = 0; j < closestFurnishing.Count; j++)
                {
                    for (int i = 0; i < closestFurnishing[j].reachableNodes.Count; i++)
                    {

                        closestNode.Add(closestFurnishing[j].reachableNodes[i]);
                    }
                }
            }
            if (closestNode != null)
            {
                bool canDo = worker.goToPlace(closestNode);
                if (canDo)
                {
                    return TaskEndState.TaskMovingForward;
                }
                else
                {
                    return TaskEndState.TaskFailure;
                }
            }
            else
            {
                return TaskEndState.TaskFailure;
            }
        }
        else if (currentWorkOrderType == WorkOrderType.PutBakeableIntoOven)
        {
            if (worker.currentGrid.pathNode.reachableFurnishing.Count > 0)
            {
                for (int i = 0; i < worker.currentGrid.pathNode.reachableFurnishing.Count; i++)
                {
                    if (worker.currentGrid.pathNode.reachableFurnishing[i].FurnishingID == furnishingIDToFind)
                    {
                        Oven toCheck = (Oven)worker.currentGrid.pathNode.reachableFurnishing[i];
                        if (toCheck.count < Oven.maxSlots)
                        {
                            worker.currentGrid.pathNode.reachableFurnishing[i].getInteraction(worker, actionToPerform);
                            return TaskEndState.TaskComplete;
                        }
                    }
                }
            }

            List<Oven> closestFurnishing = new List<Oven>();
            List<PathNode> closestNode = new List<PathNode>();

            List<Furnishing> output;
            if (MainMechanics.mechanics.hasFurnishing(furnishingIDToFind, out output))
            {
                for (int i = 0; i < output.Count; i++)
                {
                    Oven toCheck = (Oven)output[i];
                    if (toCheck.count < Oven.maxSlots)
                    {
                        closestFurnishing.Add((Oven)output[i]);
                    }
                }
            }

            if (closestFurnishing != null)
            {
                for (int j = 0; j < closestFurnishing.Count; j++)
                {
                    for (int i = 0; i < closestFurnishing[j].reachableNodes.Count; i++)
                    {

                        closestNode.Add(closestFurnishing[j].reachableNodes[i]);
                    }
                }
            }
            if (closestNode != null)
            {
                bool canDo = worker.goToPlace(closestNode);
                if (canDo)
                {
                    return TaskEndState.TaskMovingForward;
                }
                else
                {
                    return TaskEndState.TaskFailure;
                }
            }
            else
            {
                return TaskEndState.TaskFailure;
            }
        }


        return TaskEndState.TaskFailure;
	}

	public string getWorkOrderProperty()
	{
		if(currentWorkOrderType == WorkOrderType.Error)
		{
			return "Error";
		}
		else if(currentWorkOrderType == WorkOrderType.PutOrderedFoodOnKitchenWindow)
		{
			return "Put ordered Food on Kitchen Window";
		}
		else if(currentWorkOrderType == WorkOrderType.GoToBed)
		{
			return "Go To Bed";
		}
		else if(currentWorkOrderType == WorkOrderType.FindTable)
		{
			return "Find Table";
		}
		else if(currentWorkOrderType == WorkOrderType.ActivateSpecificFurnishing)
		{
			return "Activate Specific Furnishing: "+furnishingToActivate+"| "+actionToPerform;
		}
		else if(currentWorkOrderType == WorkOrderType.ActiveateFurnishingID)
		{
			return "Activate Furnishing ID: "+furnishingIDToFind.ToString()+"| "+actionToPerform;
		}
		else if(currentWorkOrderType == WorkOrderType.RetrieveSpecificItem)
		{
			return "Retrieve Specific Item"+holdableToFind.name;
		}
		else if(currentWorkOrderType == WorkOrderType.RetrieveItem)
		{
			return "Retrieve Item"+holdableToFindID.ToString();
		}
		else if(currentWorkOrderType == WorkOrderType.ActiveateFurnishingIDWithLiquidID)
		{
			return "Activate FurnishingID: "+furnishingIDToFind.ToString()+" With LiquidID: "+holdableToFindID.ToString();
		}
		return "Error";
	}
}
