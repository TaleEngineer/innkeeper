﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogEngine
{
    public enum ActionType
    {
        Avoid,
        Seek,
        Fight,
        Trust,
        Beware
    }

    public enum PersonalityType
    {
        Error,
        Normal
    }

    public enum ResponseType
    {
        Error,
        Agree,
        Disagree,
        Thanks,
        NoThanks,
        LaughWith,
        LaughAt,
        Suspect
    }

    public enum EmotionType
    {
        Error,
        Happy,
        Sad,
        Angry
    }

    public enum ConceptType
    {
        Person,
        Place,
        Monster,
        Thing
    }

    #region Get Dialog
    public string getDialogString(Humanoid speaker)
    {
        return speaker.Name + ": " + greetingString(speaker);
    }
    
    public string getDialogString(Humanoid speaker, ActionType action, FurnishingLibrary.HoldableID item)
    {
        ResponseType response = speaker.feelings.getResponse(item, action);
        string dialog = getGenericResponse(speaker,
           response,
           speaker.feelings.getEmotionalOutburst(item, action));

        List<Feeling> toFeel = new List<Feeling>();
        List<FurnishingLibrary.ItemFamilyTag> toSeek = FurnishingLibrary.Library.getItemTags(item);
        foreach(FurnishingLibrary.ItemFamilyTag tag in toSeek)
        {
            toFeel.Add(speaker.feelings.getFeeling(tag));
        }

        EffectThoughts(speaker, response, action, toFeel);
        return dialog;
    }

    public string getDialogString(Humanoid speaker, ActionType action, Monster.MonsterType item)
    {
        ResponseType response = speaker.feelings.getResponse(item, action);
        string dialog = getGenericResponse(speaker,
           response,
           speaker.feelings.getEmotionalOutburst(item, action));
        EffectThoughts(speaker, response, action, speaker.feelings.getFeeling(item));
        return dialog;
    }

    public string getDialogString(Humanoid speaker, ActionType action, Area.DungeonSetting item)
    {
        ResponseType response = speaker.feelings.getResponse(item, action);
        string dialog = getGenericResponse(speaker,
           response,
           speaker.feelings.getEmotionalOutburst(item, action));
        EffectThoughts(speaker, response, action, speaker.feelings.getFeeling(item));
        return dialog;
    }

    public string getDialogString(Humanoid speaker, ActionType action, IFeelable item)
    {
        ResponseType response = speaker.feelings.getResponse(item, speaker, action);
        string dialog = getGenericResponse(speaker,
           response,
           speaker.feelings.getEmotionalOutburst(item, speaker, action));
        EffectThoughts(speaker, response, action, item.getFeeling(speaker));
        return dialog;
    }
    #endregion

    #region Dialog Constuctors
    string greetingString(Humanoid target)
    {
        PersonalityType personality = target.personalityType;
        EmotionType emotion = target.shownEmotionalState;

        switch (personality)
        {
            case PersonalityType.Normal:
                switch(emotion)
                {
                    case EmotionType.Happy:
                        return "Hello there, friend!";

                    case EmotionType.Sad:
                        return "Oh... Hello...";

                    case EmotionType.Angry:
                        return "I'd rather not talk to you right now.";
                }
                return "ERROR GREETING TEXT EMOTION ERROR";
        }
        return "ERROR GREETING TEXT PERSONALITY ERROR";
    }

    string getGenericResponse(Humanoid targetSpeaker, ResponseType response, string emotionalOutburst)
    {
        switch(targetSpeaker.personalityType)
        {
            case PersonalityType.Error:
                return "RESPONSE PERSONALITY ERROR";
            case PersonalityType.Normal:
                switch(targetSpeaker.shownEmotionalState)
                {
                    case EmotionType.Error:
                        return "RESPONSE EMOTION ERROR";

                    case EmotionType.Happy:
                        switch(response)
                        {
                            case ResponseType.Error:
                                return "RESPONSE RESPONSE ERROR";

                            case ResponseType.Agree:
                                return emotionalOutburst + ".\nSo, I'll be sure to do that, friend.";

                            case ResponseType.Disagree:
                                return emotionalOutburst + ".\nSo, I won't do that, friend.";

                            case ResponseType.Thanks:
                                return emotionalOutburst + ".\nThanks so much, friend!";

                            case ResponseType.NoThanks:
                                return emotionalOutburst + ".\nThanks, but no thanks, friend.";

                            case ResponseType.LaughWith:
                                return "Ha! You are such the kidder, friend! \n" + emotionalOutburst + ".";

                            case ResponseType.LaughAt:
                                return "Ha! You speak such foolishness, friend. \n" + emotionalOutburst + ".";

                            case ResponseType.Suspect:
                                return emotionalOutburst + ".\nYou've got some strange ideas, friend.";
                        }
                        return emotionalOutburst + ".\nHAPPY RESPONSE ERROR";

                    case EmotionType.Sad:
                        switch (response)
                        {
                            case ResponseType.Error:
                                return "RESPONSE RESPONSE ERROR";

                            case ResponseType.Agree:
                                return emotionalOutburst + ".\nSo... Okay...";

                            case ResponseType.Disagree:
                                return emotionalOutburst + ".\nSo, I'm sorry, but I don't want to do that.";

                            case ResponseType.Thanks:
                                return emotionalOutburst + ".\nThank you...";

                            case ResponseType.NoThanks:
                                return emotionalOutburst + ".\nI'm sorry, but no thank you...";

                            case ResponseType.LaughWith:
                                return "Heh... That cheered me up a little. \n" + emotionalOutburst+".";

                            case ResponseType.LaughAt:
                                return "\n Heh... That's pretty stupid. \n" + emotionalOutburst+".";

                            case ResponseType.Suspect:
                                return emotionalOutburst + ".\nI don't think I want to talk to you anymore.";
                        }
                        return emotionalOutburst + ".\nSAD RESPONSE ERROR";

                    case EmotionType.Angry:
                        switch (response)
                        {
                            case ResponseType.Error:
                                return "RESPONSE RESPONSE ERROR";

                            case ResponseType.Agree:
                                return emotionalOutburst + "!\nSo, yeah, fine!";

                            case ResponseType.Disagree:
                                return emotionalOutburst + "!\nSo, I'm sorry, but that is fucking retarded!";

                            case ResponseType.Thanks:
                                return emotionalOutburst + "!\nYeah, fine! Thanks a lot!";

                            case ResponseType.NoThanks:
                                return emotionalOutburst + "!\nSo I don't fucking want that shit!";

                            case ResponseType.LaughWith:
                                return "Ha! Holy shit. \n" + emotionalOutburst+"!";

                            case ResponseType.LaughAt:
                                return "Ha ha! Holy shit you are retarded! \n" + emotionalOutburst+"!";

                            case ResponseType.Suspect:
                                return emotionalOutburst + "!\nNow you're just pissing me off!";
                        }
                        return emotionalOutburst + "!\nANGRY RESPONSE ERROR";
                }
                return "PERSONALITY NORMAL RESPONSE ERROR";
        }

        return "RESPONSE FULL ERROR";
    }
    #endregion

    #region Get Singluar/Plural
    public string getSingular(FurnishingLibrary.HoldableID item)
    {

        return FurnishingLibrary.Library.getItemName(item, false);
    }

    public string getPlural(FurnishingLibrary.HoldableID item)
    {
        return FurnishingLibrary.Library.getItemName(item, true);
    }

    public string getSingular(Monster.MonsterType item)
    {
        return item.ToString() + "_SINGULAR_ERROR";
    }

    public string getPlural(Monster.MonsterType item)
    {
        return item.ToString() + "_PLURAL_ERROR";
    }

    public string getSingular(Area.DungeonSetting item)
    {
        return item.ToString() + "_SINGULAR_ERROR";
    }

    public string getPlural(Area.DungeonSetting item)
    {
        return item.ToString() + "_PLURAL_ERROR";
    }
    #endregion

    #region Effect Thoughts
    public void EffectThoughts (Humanoid Speaker, ResponseType Response, ActionType Action, AdvancedFeeling item)
    {
        switch (Speaker.personalityType)
        {
            case PersonalityType.Error:
                return;
            case PersonalityType.Normal:
                switch (Speaker.shownEmotionalState)
                {
                    case EmotionType.Error:
                        return;

                    case EmotionType.Happy:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch(Action)
                                {
                                    case ActionType.Avoid:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] -= 1;
                                        return;
                                    case ActionType.Seek:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 1;
                                        return;
                                    case ActionType.Fight:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 1;
                                        item.howFeel[AdvancedFeeling.FeelingType.LoveHate] -= 1;
                                        return;
                                    case ActionType.Trust:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] += 1;
                                        return;
                                    case ActionType.Beware:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] -= 1;
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -5);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;
                        }
                        return;

                    case EmotionType.Sad:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] -= 2;
                                        return;
                                    case ActionType.Seek:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 2;
                                        return;
                                    case ActionType.Fight:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 2;
                                        item.howFeel[AdvancedFeeling.FeelingType.LoveHate] -= 2;
                                        return;
                                    case ActionType.Trust:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] += 2;
                                        return;
                                    case ActionType.Beware:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] -= 2;
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 2);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 4);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;
                        }
                        return;

                    case EmotionType.Angry:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] -= 2;
                                        return;
                                    case ActionType.Seek:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 1;
                                        return;
                                    case ActionType.Fight:
                                        item.howFeel[AdvancedFeeling.FeelingType.WantFear] += 1;
                                        item.howFeel[AdvancedFeeling.FeelingType.LoveHate] -= 3;
                                        return;
                                    case ActionType.Trust:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] += 1;
                                        return;
                                    case ActionType.Beware:
                                        item.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] -= 3;
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -2);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -4);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                    case ActionType.Fight:
                                        return;
                                    case ActionType.Trust:
                                        return;
                                    case ActionType.Beware:
                                        return;
                                }
                                return;
                        }
                        return;
                }
                return;
        }

        return;
    }



    public void EffectThoughts(Humanoid Speaker, ResponseType Response, ActionType Action, List<Feeling> item)
    {
        switch (Speaker.personalityType)
        {
            case PersonalityType.Error:
                return;
            case PersonalityType.Normal:
                switch (Speaker.shownEmotionalState)
                {
                    case EmotionType.Error:
                        return;

                    case EmotionType.Happy:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        foreach(Feeling i in item)
                                        {
                                            i.howFeelSimple -= 1;
                                        }
                                        return;
                                    case ActionType.Seek:
                                        foreach (Feeling i in item)
                                        {
                                            i.howFeelSimple += 1;
                                        } 
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -5);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;
                        }
                        return;

                    case EmotionType.Sad:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        foreach (Feeling i in item)
                                        {
                                            i.howFeelSimple -= 2;
                                        }
                                        return;
                                    case ActionType.Seek:
                                        foreach (Feeling i in item)
                                        {
                                            i.howFeelSimple += 2;
                                        }
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 2);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 4);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;
                        }
                        return;

                    case EmotionType.Angry:
                        switch (Response)
                        {
                            case ResponseType.Error:
                                return;

                            case ResponseType.Agree:
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        foreach (Feeling i in item)
                                        {
                                            i.howFeelSimple -= 2;
                                        }
                                        return;
                                    case ActionType.Seek:
                                        foreach (Feeling i in item)
                                        {
                                            i.howFeelSimple += 1;
                                        }
                                        return;
                                }
                                return;

                            case ResponseType.Disagree:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Thanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.NoThanks:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughWith:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, 1);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, 1);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.LaughAt:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -2);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -2);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;

                            case ResponseType.Suspect:
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.LoveHate, -4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.TrustSuspect, -4);
                                Speaker.feelings.updateFeelings(MainMechanics.mechanics.Innkeeper, AdvancedFeeling.FeelingType.WantFear, -4);
                                switch (Action)
                                {
                                    case ActionType.Avoid:
                                        return;
                                    case ActionType.Seek:
                                        return;
                                }
                                return;
                        }
                        return;
                }
                return;
        }

        return;
    }




    #endregion
}
