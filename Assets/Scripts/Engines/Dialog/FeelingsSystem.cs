﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FeelingsSystem
{

    public Dictionary<FurnishingLibrary.ItemFamilyTag, Feeling> itemsGeneral = new Dictionary<FurnishingLibrary.ItemFamilyTag, Feeling>();
    public Dictionary<Monster.MonsterType, AdvancedFeeling> monsterGeneral = new Dictionary<Monster.MonsterType, AdvancedFeeling>();
    public Dictionary<Area.DungeonSetting, AdvancedFeeling> areaTypeGeneral = new Dictionary<Area.DungeonSetting, AdvancedFeeling>();

    public Dictionary<IFeelable, AdvancedFeeling> feelableSpecific = new Dictionary<IFeelable, AdvancedFeeling>();
    #region UpdateFeelings
    void updateFeelings(AdvancedFeeling output, AdvancedFeeling.FeelingType type, int change)
    {
        output.howFeel[type] += change;
    }

    void updateFeelings(Feeling output, int change)
    {
        output.howFeelSimple += change;
    }

    public void updateFeelings(FurnishingLibrary.ItemFamilyTag item, int feelingChange)
    {
        Feeling output;
        if(itemsGeneral.TryGetValue(item, out output))
        {
            updateFeelings(output, feelingChange);
        }
        else
        {
            Feeling toAdd = new Feeling();
            toAdd.Set();
            toAdd.howFeelSimple = feelingChange;
            itemsGeneral.Add(item, toAdd);
        }
    }

    public void updateFeelings(Monster.MonsterType item, AdvancedFeeling.FeelingType type, int feelingChange)
    {
        AdvancedFeeling output;
        if (monsterGeneral.TryGetValue(item, out output))
        {
            updateFeelings(output, type, feelingChange);
        }
        else
        {
            AdvancedFeeling toAdd = new AdvancedFeeling();
            toAdd.Set();
            toAdd.howFeel[type] = feelingChange;
            monsterGeneral.Add(item, toAdd);
        }
    }

    public void updateFeelings(Area.DungeonSetting item, AdvancedFeeling.FeelingType type, int feelingChange)
    {
        AdvancedFeeling output;
        if (areaTypeGeneral.TryGetValue(item, out output))
        {
            updateFeelings(output, type, feelingChange);
        }
        else
        {
            AdvancedFeeling toAdd = new AdvancedFeeling();
            toAdd.Set();
            toAdd.howFeel[type] = feelingChange;
            areaTypeGeneral.Add(item, toAdd);
        }
    }

    public void updateFeelings(IFeelable item, AdvancedFeeling.FeelingType type, int feelingChange)
    {
        AdvancedFeeling output;
        if (feelableSpecific.TryGetValue(item, out output))
        {
            updateFeelings(output, type, feelingChange);
        }
        else
        {
            AdvancedFeeling toAdd = new AdvancedFeeling();
            toAdd.Set();
            toAdd.howFeel[type] = feelingChange;
            feelableSpecific.Add(item, toAdd);
        }
    }
    #endregion

    #region GetResponse
    DialogEngine.ResponseType getResponse(int TotalFeelings, DialogEngine.ActionType type)
    {

        switch (type)
        {
            case DialogEngine.ActionType.Avoid:
                if (TotalFeelings > 0)
                {
                    return DialogEngine.ResponseType.Disagree;
                }
                return DialogEngine.ResponseType.Agree;

            case DialogEngine.ActionType.Seek:
                if (TotalFeelings > 0)
                {
                    return DialogEngine.ResponseType.Agree;
                }
                return DialogEngine.ResponseType.Disagree;

            case DialogEngine.ActionType.Fight:
                return DialogEngine.ResponseType.Disagree;

            case DialogEngine.ActionType.Trust:
                return DialogEngine.ResponseType.Disagree;

            case DialogEngine.ActionType.Beware:
                return DialogEngine.ResponseType.Disagree;
        }
        return DialogEngine.ResponseType.Error;
    }

    DialogEngine.ResponseType getResponse(AdvancedFeeling output, DialogEngine.ActionType type)
    {
        if(output == null)
        {
            return DialogEngine.ResponseType.Error;
        }

        switch(type)
        {
            case DialogEngine.ActionType.Avoid:
                if(output.howFeel[AdvancedFeeling.FeelingType.WantFear] > 0)
                {
                    return DialogEngine.ResponseType.Disagree;
                }
                return DialogEngine.ResponseType.Agree;

            case DialogEngine.ActionType.Seek:
                if(output.howFeel[AdvancedFeeling.FeelingType.WantFear] > 0)
                {
                    return DialogEngine.ResponseType.Agree;
                }
                return DialogEngine.ResponseType.Disagree;

            case DialogEngine.ActionType.Fight:
                if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] < -50 && output.howFeel[AdvancedFeeling.FeelingType.WantFear] > 0)
                {
                    return DialogEngine.ResponseType.Agree;
                }
                return DialogEngine.ResponseType.Disagree;

            case DialogEngine.ActionType.Trust:
                if(output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] > -10)
                {
                    return DialogEngine.ResponseType.Agree;
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] > -50)
                {
                    return DialogEngine.ResponseType.LaughAt;
                }
                return DialogEngine.ResponseType.Suspect;

            case DialogEngine.ActionType.Beware:
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] > 10)
                {
                    return DialogEngine.ResponseType.LaughAt;
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] > 50)
                {
                    return DialogEngine.ResponseType.Suspect;
                }
                return DialogEngine.ResponseType.Agree;
        }
        return DialogEngine.ResponseType.Error;
    }

    public DialogEngine.ResponseType getResponse(FurnishingLibrary.HoldableID item, DialogEngine.ActionType type)
    {
        int output = getBasicFeelings(item);
        return getResponse(output, type);
    }

    public DialogEngine.ResponseType getResponse(Monster.MonsterType item, DialogEngine.ActionType type)
    {
        AdvancedFeeling output;
        if (monsterGeneral.TryGetValue(item, out output))
        {
            return getResponse(output, type);
        }
        else
        {
            updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
            monsterGeneral.TryGetValue(item, out output);
            return getResponse(output, type);
        }
    }

    public DialogEngine.ResponseType getResponse(Area.DungeonSetting item, DialogEngine.ActionType type)
    {
        AdvancedFeeling output;
        if (areaTypeGeneral.TryGetValue(item, out output))
        {
            return getResponse(output, type);
        }
        else
        {
            updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
            areaTypeGeneral.TryGetValue(item, out output);
            return getResponse(output, type);
        }
    }

    public DialogEngine.ResponseType getResponse(IFeelable item, Humanoid target, DialogEngine.ActionType type)
    {
        AdvancedFeeling output = item.getFeeling(target);
        return getResponse(output, type);
    }

    #endregion
    #region GetEmotionalOutburst
    string getEmotionalOutburst(AdvancedFeeling output, AdvancedFeeling.FeelingType tested)
    {
        if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] == 0 && output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] == 0 && output.howFeel[AdvancedFeeling.FeelingType.WantFear] == 0)
        {
            return "I don't know anything about";
        }
        if (tested == AdvancedFeeling.FeelingType.LoveHate)
        {
            if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] == 0)
            {
                return "I don't know how to feel about";
            }
            if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] > 0)
            {
                if(output.howFeel[AdvancedFeeling.FeelingType.LoveHate] >= 90)
                {
                    return "I am in love with";
                }
                if(output.howFeel[AdvancedFeeling.FeelingType.LoveHate] >= 75)
                {
                    return "I love";
                }
                if(output.howFeel[AdvancedFeeling.FeelingType.LoveHate] >= 50)
                {
                    return "I really like";
                }
                if(output.howFeel[AdvancedFeeling.FeelingType.LoveHate] >= 25)
                {
                    return "I like";
                }
                return "I kinda like";
            }
            else
            {
                if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] <= -90)
                {
                    return "Destruction is too good for";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] <= -75)
                {
                    return "I despise";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] <= -50)
                {
                    return "I hate";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.LoveHate] <= -25)
                {
                    return "I greatly dislike";
                }
                return "I dislike";
            }
        }
        else if(tested == AdvancedFeeling.FeelingType.WantFear)
        {
            if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] == 0)
            {
                return "I don't know how to feel about";
            }
            if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] > 0)
            {
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] >= 90)
                {
                    return "I NEED to find";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] >= 75)
                {
                    return "I must find";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] >= 50)
                {
                    return "I want to find";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] >= 25)
                {
                    return "I like to find";
                }
                return "I kinda like to find";
            }
            else
            {
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] <= -90)
                {
                    return "I cannot go near";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] <= -75)
                {
                    return "I am deathly afraid of";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] <= -50)
                {
                    return "I am afraid of";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.WantFear] <= -25)
                {
                    return "I don't like being near";
                }
                return "I would rather not be near";
            }
        }
        else//Trust
        {
            if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] == 0)
            {
                return "I don't know how to feel about";
            }
            if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] > 0)
            {
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] >= 90)
                {
                    return "I trust in";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] >= 75)
                {
                    return "I fully trust";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] >= 50)
                {
                    return "I trust";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] >= 25)
                {
                    return "I want to trust";
                }
                return "I kinda trust";
            }
            else
            {
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] <= -90)
                {
                    return "I cannot go near";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] <= -75)
                {
                    return "I can't trust";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] <= -50)
                {
                    return "I am suspicious of";
                }
                if (output.howFeel[AdvancedFeeling.FeelingType.TrustSuspect] <= -25)
                {
                    return "I am kinda suspicious of";
                }
                return "A little suspicious, that";
            }
        }
    }

    string getEmotionalOutburst(int totalBasicFeeling)
    {
        if (totalBasicFeeling == 0)
        {
            return "I don't know anything about";
        }
        if (totalBasicFeeling > 0)
        {
            if (totalBasicFeeling >= 90)
            {
                return "I love";
            }
            if (totalBasicFeeling >= 75)
            {
                return "I enjoy";
            }
            if (totalBasicFeeling >= 50)
            {
                return "I really like";
            }
            if (totalBasicFeeling >= 25)
            {
                return "I like";
            }
            return "I kinda like";
        }
        else
        {
            if (totalBasicFeeling <= -90)
            {
                return "There is nothing I hate more than";
            }
            if (totalBasicFeeling <= -75)
            {
                return "I find it disgusting,";
            }
            if (totalBasicFeeling <= -50)
            {
                return "I hate";
            }
            if (totalBasicFeeling <= -25)
            {
                return "I greatly dislike";
            }
            return "I dislike";
        }
    }

    public string getEmotionalOutburst(FurnishingLibrary.HoldableID item, DialogEngine.ActionType type)
    {
        return getEmotionalOutburst(getBasicFeelings(item)) + " " + MainMechanics.dialogEngine.getPlural(item);
    }

    public string getEmotionalOutburst(Monster.MonsterType item, DialogEngine.ActionType type)
    {
        AdvancedFeeling output;
        if (monsterGeneral.TryGetValue(item, out output))
        {
            return getEmotionalOutburst(output, getTestType(type)) + " " + MainMechanics.dialogEngine.getPlural(item);
        }
        else
        {
            updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
            monsterGeneral.TryGetValue(item, out output);
            return getEmotionalOutburst(output, getTestType(type)) + " " + MainMechanics.dialogEngine.getPlural(item);
        }
    }

    public string getEmotionalOutburst(Area.DungeonSetting item, DialogEngine.ActionType type)
    {
        AdvancedFeeling output;
        if (areaTypeGeneral.TryGetValue(item, out output))
        {
            return getEmotionalOutburst(output, getTestType(type)) + " " + MainMechanics.dialogEngine.getPlural(item);
        }
        else
        {
            updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
            areaTypeGeneral.TryGetValue(item, out output);
            return getEmotionalOutburst(output, getTestType(type)) + " " + MainMechanics.dialogEngine.getPlural(item);
        }
    }

    public string getEmotionalOutburst(IFeelable item, Humanoid target, DialogEngine.ActionType type)
    {
        AdvancedFeeling output = item.getFeeling(target);
        return getEmotionalOutburst(output, getTestType(type)) + " " + item.getName();
    }
        #endregion
    #region TestType
    AdvancedFeeling.FeelingType getTestType(DialogEngine.ActionType type)
    {
        switch(type)
        {
            case DialogEngine.ActionType.Avoid:
                return AdvancedFeeling.FeelingType.WantFear;

            case DialogEngine.ActionType.Seek:
                return AdvancedFeeling.FeelingType.WantFear;

            case DialogEngine.ActionType.Fight:
                return AdvancedFeeling.FeelingType.LoveHate;

            case DialogEngine.ActionType.Trust:
                return AdvancedFeeling.FeelingType.TrustSuspect;

            case DialogEngine.ActionType.Beware:
                return AdvancedFeeling.FeelingType.TrustSuspect;
        }
        Debug.LogError("getTestType Full Error");
        return AdvancedFeeling.FeelingType.LoveHate;
    }
    #endregion
    #region GetFeelings

    public Feeling getFeeling(FurnishingLibrary.ItemFamilyTag item)
    {
        Feeling output;
        if(itemsGeneral.TryGetValue(item, out output))
        {
            return output;
        }
        updateFeelings(item, 0);
        return itemsGeneral[item];
    }

    public AdvancedFeeling getFeeling(Monster.MonsterType item)
    {
        AdvancedFeeling output;
        if (monsterGeneral.TryGetValue(item, out output))
        {
            return output;
        }
        updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
        return monsterGeneral[item];
    }

    public AdvancedFeeling getFeeling(Area.DungeonSetting item)
    {
        AdvancedFeeling output;
        if (areaTypeGeneral.TryGetValue(item, out output))
        {
            return output;
        }
        updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
        return areaTypeGeneral[item];
    }

    public AdvancedFeeling getFeeling(IFeelable item)
    {
        AdvancedFeeling output;
        if (feelableSpecific.TryGetValue(item, out output))
        {
            return output;
        }
        updateFeelings(item, AdvancedFeeling.FeelingType.LoveHate, 0);
        return feelableSpecific[item];
    }
    #endregion

    public int getBasicFeelings(FurnishingLibrary.HoldableID item)
    {
        int output = 0;
        List<FurnishingLibrary.ItemFamilyTag> tags = FurnishingLibrary.Library.getItemTags(item);
        for (int i = 0; i < tags.Count; i++)
        {
            Feeling howFeel;
            if (itemsGeneral.TryGetValue(tags[i], out howFeel))
            {
                output += howFeel.howFeelSimple;
            }
        }
        //output /= tags.Count;
        return output;
    }
}
