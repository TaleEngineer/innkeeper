﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdvancedFeeling: Feeling
{
    public enum FeelingType
    {
        LoveHate,
        WantFear,
        TrustSuspect
    }

    public Dictionary<FeelingType, int> howFeel = new Dictionary<FeelingType, int>();

    public override void Set()
    {
        howFeel.Add(FeelingType.LoveHate, 0);
        howFeel.Add(FeelingType.WantFear, 0);
        howFeel.Add(FeelingType.TrustSuspect, 0);
    }
}
