﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Holdable : MonoBehaviour, IFeelable
{
	public Transform mainTransform;

	public SpriteRenderer spriteRenderer;
	public bool canHoldLiquid;
	
	public int maxLiquidContainment;
	public int currentLiquidContainment;

	public Liquid liquidHere;

	public bool canBeUsed;

	public FurnishingLibrary.HoldableID holdableID;

	public Texture2D holdableTexture;

	public FurnishingLibrary.FurnishingID isCookedIn;

    public List<FurnishingLibrary.HoldableID> holdablesInThisHoldable = new List<FurnishingLibrary.HoldableID>();

    public int cost;

	public void PourLiquidIntoFurnishing(Bar container, int whereFill)
	{
		if(liquidHere != null && currentLiquidContainment > 0)
		{
			if(container.ammountOnTap[whereFill] == 0)
			{
				if(container.maxLiquidContainment <= currentLiquidContainment)
				{
					container.ammountOnTap[whereFill] = container.maxLiquidContainment;
					currentLiquidContainment -= container.maxLiquidContainment;
					container.liquidsOnTap[whereFill] = liquidHere;
                    container.UpdateAvailableDrinks();
                }
				else
				{
					container.ammountOnTap[whereFill] = currentLiquidContainment;
					container.liquidsOnTap[whereFill] = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
                    container.UpdateAvailableDrinks();
                }
			}
			else if(container.liquidsOnTap[whereFill].liquidID == liquidHere.liquidID)
			{
				int ammountEmpty = container.maxLiquidContainment - container.ammountOnTap[whereFill];
				
				if(ammountEmpty <= currentLiquidContainment)
				{
					container.ammountOnTap[whereFill] = container.maxLiquidContainment;
					currentLiquidContainment -= ammountEmpty;
					container.liquidsOnTap[whereFill] = liquidHere;
                    container.UpdateAvailableDrinks();
                }
				else
				{
					container.ammountOnTap[whereFill] += currentLiquidContainment;
					container.liquidsOnTap[whereFill] = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
                    container.UpdateAvailableDrinks();
                }
			}
		}
	}

	public void PourLiquidIntoFurnishing(InteractableFurnishing container)
	{
		if(liquidHere != null && currentLiquidContainment > 0)
		{
			if(container.currentLiquidContainment == 0)
			{
				if(container.maxLiquidContainment <= currentLiquidContainment)
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					currentLiquidContainment -= container.maxLiquidContainment;
					container.liquidHere = liquidHere;
				}
				else
				{
					container.currentLiquidContainment = currentLiquidContainment;
					container.liquidHere = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
				}
			}
			else if(container.liquidHere.liquidID == liquidHere.liquidID)
			{
				int ammountEmpty = container.maxLiquidContainment - container.currentLiquidContainment;
				
				if(ammountEmpty <= currentLiquidContainment)
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					currentLiquidContainment -= ammountEmpty;
					container.liquidHere = liquidHere;
				}
				else
				{
					container.currentLiquidContainment += currentLiquidContainment;
					container.liquidHere = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
				}
			}
		}
	}

	public virtual void Use(Humanoid User)
	{

	}

	public virtual void CompleteUse(Humanoid User)
	{

	}

	void Awake()
	{
		mainTransform = transform;
	}

	// Use this for initialization
	public virtual void Start () 
	{
        name = holdableID.ToString();
		if(spriteRenderer.sprite != null)
		{
			holdableTexture = spriteRenderer.sprite.texture;
		}
		//name = holdableID.ToString();
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		if(liquidHere != null && currentLiquidContainment <= 0)
		{
			currentLiquidContainment = 0;
			liquidHere = null;
		}
	}

	public virtual void Draw()
	{
		if(liquidHere != null && currentLiquidContainment > 0)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);
			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize/10, halfSize/2)/2, new Vector2(halfSize/10,halfSize/2));
			GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidBar);
			
			drawRect = new Rect(new Vector2(drawRect.x+1, drawRect.y+1+(drawRect.height-2)*(maxLiquidContainment-currentLiquidContainment)/maxLiquidContainment), new Vector2(drawRect.width-2, (drawRect.height-2)*currentLiquidContainment/maxLiquidContainment));
			GUI.color = liquidHere.liquidColor;
			GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidMarker);
			
			GUI.color = Color.white;
		}
	}

    #region IFeelable
    public AdvancedFeeling getFeeling(Humanoid target)
    {
        return target.feelings.getFeeling(this);
    }
    public string getName()
    {
        return holdableID.ToString();
    }
    #endregion
}
