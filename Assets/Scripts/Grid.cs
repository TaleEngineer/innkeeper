﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour 
{
	public Transform mainTransform;

	public List<Holdable> holdablesHere = new List<Holdable>();

	public bool buildable;

	public PathNode pathNode;

	public float speedMultiplier;

	public RoomPiece roomPieceHere;

	public Furnishing furnishingHere;

	public SpriteRenderer gridRender;
	public GameObject ground;

    public List<Humanoid> peopleHere = new List<Humanoid>();



	// Use this for initialization
	void Awake () 
	{
		mainTransform = transform;
	}

    // Update is called once per frame
    void Update()
    {
        if (roomPieceHere != null && roomPieceHere.buildComplete)
        {
            ground.SetActive(false);
        }
        else
        {
            ground.SetActive(true);
        }

        if (MainMechanics.mechanics.showGrid)
        {
            gridRender.enabled = true;
        }
        else
        {
            gridRender.enabled = false;
        }

        if (peopleHere.Count > 1)
        {
            for (int a = 0; a < peopleHere.Count; a++)
            {
                for (int b = a + 1; b < peopleHere.Count; b++)
                {
                    peopleHere[a].FakePositionRepulsion(peopleHere[b]);
                }
            }
            /*bool foundBase = false;
            for (int a = 0; a < peopleHere.Count; a++)
            {
                if (peopleHere[a].thisNPCType != Humanoid.NPCType.Player && peopleHere[a].pathToGo.Count == 0 && !peopleHere[a].working)
                {
                    if (foundBase)
                    {
                        peopleHere[a].Meander();
                    }
                    else
                    {
                        foundBase = true;
                    }
                }
            }*/
        }
    }
}
