﻿using UnityEngine;
using System.Collections;

public class CameraControls : MonoBehaviour 
{
	float cameraSpeed = 10f;
    float ZoomScroll = 4f;
    // Use this for initialization
    void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (Input.GetAxis("Mouse ScrollWheel") != 0 && !MainMechanics.mechanics.mouseOverGUI)//GET ZOOM
        {
            ZoomScroll -= Input.GetAxis("Mouse ScrollWheel") * 2;
            if (ZoomScroll < 1)
            {
                ZoomScroll = 1;
            }
            else if (ZoomScroll > 10)
            {
                ZoomScroll = 10;
            }

            Camera.main.orthographicSize = ZoomScroll;
        }

        cameraSpeed = Camera.main.orthographicSize*2;

		Rect screenRect = new Rect(1, 1, Screen.width-2, Screen.height-2);

		Vector2 mousPos = Input.mousePosition;



        if (screenRect.Contains(Input.mousePosition) == false)
		{
			if(mousPos.x < 1)
			{
				transform.position += new Vector3(-cameraSpeed*Time.deltaTime, 0, 0);
			}
			else if(mousPos.x > screenRect.width)
			{
				transform.position += new Vector3(cameraSpeed*Time.deltaTime, 0, 0);
			}

			if(mousPos.y < 1)
			{
				transform.position += new Vector3(0, -cameraSpeed*Time.deltaTime, 0);
			}
			else if(mousPos.y > screenRect.height)
			{
				transform.position += new Vector3(0, cameraSpeed*Time.deltaTime, 0);
			}
        }

        Vector2 topLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        Vector2 botRight = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));

        //if (transform.position.x < 0)
        if (topLeft.x < 0.5f)
		{
            //transform.position = new Vector3(0, transform.position.y, transform.position.z);
            transform.position = new Vector3(transform.position.x + (topLeft.x * -1) + 0.5f, transform.position.y, transform.position.z);
		}
        //else if (transform.position.x > MainMechanics.GRID_X)
        else if(botRight.x > MainMechanics.GRID_X-1.5f)
		{
            //transform.position = new Vector3(MainMechanics.GRID_X, transform.position.y, transform.position.z);
            transform.position = new Vector3(transform.position.x - (botRight.x - (MainMechanics.GRID_X)) - 1.5f, transform.position.y, transform.position.z);
		}

        if (topLeft.y < -0.5f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + (topLeft.y * -1) - 0.5f, transform.position.z);
        }
        else if (botRight.y > MainMechanics.GRID_Y - 0.5f)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - (botRight.y - (MainMechanics.GRID_Y)) - 0.5f, transform.position.z);
        }

        /*if(transform.position.y < 0)
		{
			transform.position = new Vector3(transform.position.x, 0, transform.position.z);
		}
		else if(transform.position.y > MainMechanics.GRID_Y)
		{
			transform.position = new Vector3(transform.position.x, MainMechanics.GRID_Y, transform.position.z);
		}*/

    }
}
