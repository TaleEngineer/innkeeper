﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class MainMechanics : MonoBehaviour 
{
	public static int GRID_X = 50+4;
	public static int GRID_Y = 25+4;
    public static List<Humanoid> sortingLayer = new List<Humanoid>();
    public static List<FurnishingLibrary.HoldableID> menu = new List<FurnishingLibrary.HoldableID>();
	public static Dictionary<Area.DungeonSetting, Area> boarderingArea = new Dictionary<Area.DungeonSetting, Area>();
	public static MainMechanics mechanics;
	public static RecipeBook recipeBook;
	public static EquipmentFactory equipmentFactory;
    public static DialogEngine dialogEngine;
    static int HireSlots = 10;
    int prevHireSlots = HireSlots;
	public Grid[,] mainGrid = new Grid[GRID_X,GRID_Y];
	public Grid grass;
	public Grid road;
	int advCap = 0;
	int civCap = 0;
	int startingBuilders = 10;
    int startingEmploy = 0;
    int startingTownship = 300;
    public int minimumFootTraffic = 30;
    public float civTrafficMod = 1;
    float footTrafficTimer = 0.5f;

    public int currency = 5000;
    int currencyDisplayMax = 99999999;

    int doorCost = 5;
    public int wallCost = 10;
    public int floorCost = 10;

    public int fame = 0;
    public int civBonus = 4;
    public int advBonus = 0;

    public float secondsToMinute;
    public float timeOfDayMinutes;
    public enum TimeType
    {
        Dawn,
        Day,
        Dusk,
        Night
    }
    public static TimeType currentTime;

    public Vector2 merchantDropoffPoint = new Vector2(5, 5);

    public bool showGrid;

	public GameObject wall;
	public GameObject floor;
	public GameObject door;

	public GameObject FurnishPrefabToBuild;
    public GameObject FurnishToMove;

    public Humanoid Innkeeper;
    private Humanoid target;
    public Humanoid targetHumanoid
    {
        get
        {
            return target;
        }
        set
        {
            target = value;
            Innkeeper.pathToGo.Clear();
        }
    }

	public GameObject innkeeperPrefab;
	public GameObject adventurerPrefab;
	public GameObject builderPrefab;
	public GameObject civilianPrefab;
	public GameObject employeePrefab;
	public GameObject deliveryManPrefab;
	public GameObject questMasterPrefab;

    Humanoid[] toHire = new Humanoid[HireSlots];
    public List<Humanoid> MoveQueue = new List<Humanoid>();

	public List<Humanoid> BuildersInReserve = new List<Humanoid>();
	public List<Humanoid> BuildersOnScene = new List<Humanoid>();
	
	public List<Humanoid> AdventurersInReserve = new List<Humanoid>();
	public List<Humanoid> AdventurersOnScene = new List<Humanoid>();
	
	public List<Humanoid> CiviliansInReserve = new List<Humanoid>();
	public List<Humanoid> CiviliansOnScene = new List<Humanoid>();

	public List<Humanoid> EmployeesInReserve = new List<Humanoid>();
	public List<Humanoid> EmployeesOnScene = new List<Humanoid>();
	
	public List<Humanoid> DeliveryMenInReserve = new List<Humanoid>();
	public List<Humanoid> DeliveryMenOnScene = new List<Humanoid>();

	public List<Humanoid> QuestMasterInReserve = new List<Humanoid>();
	public List<Humanoid> QuestMasterOnScene = new List<Humanoid>();

    public List<Humanoid> TownshipOnScene = new List<Humanoid>();
    public List<Humanoid> TownshipInReserve = new List<Humanoid>();

    public Dictionary<FurnishingLibrary.HoldableID, List<Holdable>> HoldablesOnGround = new Dictionary<FurnishingLibrary.HoldableID, List<Holdable>>();
	public Dictionary<FurnishingLibrary.FurnishingID, List<Furnishing>> FurnishingOnMap = new Dictionary<FurnishingLibrary.FurnishingID, List<Furnishing>>();
	public float currentButtonWait = 0f;
	public float buttonWaitAmmount = 0.1f;

	public GameObject gridFolder;
	public GameObject pathFolder;
	public GameObject roomFolder;

    List<NumberEffectDisplay> displayWorldEffects = new List<NumberEffectDisplay>();
    List<NumberEffectDisplay> displayUIEffects = new List<NumberEffectDisplay>();
    Vector2 moneyDisplayPos;
    Vector2 fameDisplayPos;

    List<Vector2> gridsToPlaceRoomOn = new List<Vector2>();

	List<Vector2> furnishingPlacement;
	Furnishing toDelete;
	Door doorDelete;
	Vector2 doorPlacement;
	float rotation = 0f;
    
    float merchantCostMultiplier = 1f;
    bool tradeAvailable = true;

	Vector2 mousGridPos;
	Vector2 UIMousPos;

	Vector2 roomBuildOriginCoord;

	Room roomToExpand;

	public List<RoomPiece> roomsToBuild = new List<RoomPiece>();
	public List<Wall> wallsToBuild = new List<Wall>();

	public enum screenState
	{
		Normal,
		BuildWorkScreenOpen,
		ChoosingFurnishingSpace,
		DeletingFurnishing,
		ChoosingRoomOriginPoint,
		BuildingRoom,
		BuildingDoor,
		PickUpMenu,
		AssignWorkerMenu,
		FurnishingMenu,
		MerchantBuyOrder,
        EmployeeShiftScreen,
        MoveFurnishingMenu,
        ChoosingMoveFurnishingSpot,
        ConfirmFireEmployee,
        HireEmployeeMenu,
        ConfirmHireEmployee,
        ChooseMerchantDropPoint,
        ChoosingTalkTarget,
        TalkMenu,
        ChoosingCombatTarget
	}
	public screenState currentScreenState;
    bool lookingAtNightShift = false;
    Humanoid ShiftHumanoidLookingAt = null;
    int HireHumanoidLookingAt = -1;
    public Dictionary<screenState, Menu> menuDictionary = new Dictionary<screenState, Menu>();

    public enum roomDrawState
	{
		Box,
		Pencil,
		EraseBox,
		ErasePencil
	}
	public roomDrawState currentRoomDrawState;

	public InteractableFurnishing employeeFurnish;

	public Dictionary<FurnishingLibrary.HoldableID, int> BuyableList = new Dictionary<FurnishingLibrary.HoldableID, int>();
	public Dictionary<FurnishingLibrary.HoldableID, int> BuyableOrderAmmount = new Dictionary<FurnishingLibrary.HoldableID, int>();

	public Texture2D progressMarker;

	public bool mouseOverGUI;

	public Texture2D GUIFooter;
	public Sprite GUIFooterL;
	public Sprite GUIFooterM;
	public Sprite GUIFooterR;
	public Texture2D GUIFooterRoomButton;
	public Texture2D GUIFooterWorkButton;
    public Texture2D GUIFooterDecorationButton;
    public Texture2D GUIFooterPickUpButton;
    public Texture2D GUIFooterTalkButton;
    public Texture2D GUIFooterAttackButton;

    public Texture2D GUIHeader;
    public Sprite GUIHeaderL;
    public Sprite GUIHeaderM;
    public Sprite GUIHeaderR;
    public Texture2D GUIHeaderClockStand;
    public Texture2D GUIHeaderClockRotary;
    public Texture2D GUIHeaderCurrencySpace;
    public Texture2D GUIHeaderIlluminatiButton;
    public Texture2D GUIHeaderEmployeeShiftButton;
    public Texture2D GUIHeaderHireEmployeeButton;
    public Texture2D GUIHeaderMerchantPlacementButton;

    float PickUpScroll;


	public Texture2D UseButton;
	public Texture2D DropButton;

	public Texture2D GUIBoxButton;
	public Texture2D GUIPencilButton;
	public Texture2D GUIEraseBoxButton;
	public Texture2D GUIErasePencilButton;

	public Texture2D GUIDoorButton;
	public Texture2D GUIWellButton;
	public Texture2D GUICookingStationButton;
	public Texture2D GUIStillButton;
	public Texture2D GUIOvenButton;
	public Texture2D GUIKitchenWindowButton;
	public Texture2D GUITableButton;
	public Texture2D GUIBarButton;
	public Texture2D GUIQuestBoardButton;
	public Texture2D GUIBedButton;
    public Texture2D GUIStoveButton;
    public Texture2D GUIMeatGrinderButton;
    public Texture2D GUICellarStairsButton;
    public Texture2D GUIGrapeVatButton;
    public Texture2D GUIDeleteFurnishingButton;
    public Texture2D GUIMoveFurnishingButton;

    public Texture2D selectionCircle;
    List<FurnishingButton> WorkMenuButton = new List<FurnishingButton>();
    MenuButton deleteButton = new MenuButton();
    MenuButton moveFurnishingButton = new MenuButton();
    CostingButton doorButton = new CostingButton();

    MenuButton hireButton = new MenuButton();
    MenuButton shiftsButton = new MenuButton();
    MenuButton illuminatiButton = new MenuButton();
    MenuButton merchantDropPointButton = new MenuButton();

    MenuButton workFurnishingButton = new MenuButton();
    MenuButton roomButton = new MenuButton();
    MenuButton decorationFurnishingButton = new MenuButton();

    MenuButton pickUpItemButton = new MenuButton();
    MenuButton attackButton = new MenuButton();
    MenuButton talkButton = new MenuButton();

    public Texture2D GUIAssignEmployeeButton;
	public Texture2D employeeArrow;
    public Texture2D daySymbol;
    public Texture2D nightSymbol;

    public Texture2D LiquidBar;
	public Texture2D LiquidMarker;
	public Texture2D LiquidDrop;
    public Texture2D CurrencySymbol;
    public Texture2D FameSymbol;


    public Texture2D GUINegButton;
	public Texture2D GUIPosButton;
	public Texture2D GUIBuyButton;

    public Texture2D GUIEmptyBlip;
    public Texture2D GUIFilledBlip;

    public List<Party> CivilianParties = new List<Party>();
	public List<AdventurerParty> AdventurerParties = new List<AdventurerParty>();

	public Sprite spriteArcher;
	public Sprite spriteWarrior;
	public Sprite spriteCleric;
	public Sprite spriteWizard;
	public Sprite spriteThief;
	public Sprite spritePaladin;

    public Sprite spriteCivilian;

    public Sprite spriteEmployee;

    bool movementDone = false;

	/// <summary>
	/// Sets the boardering areas.
	/// </summary>
	void SetBoarderingAreas()
	{
		List<Area.DungeonSetting> keys = System.Enum.GetValues(typeof(Area.DungeonSetting)).Cast<Area.DungeonSetting>().ToList();
		keys.RemoveAt(0);
		for(int i = 0; i < keys.Count; i++)
		{
			Area toAdd = new Area();
			toAdd.thisSetting = keys[i];
			boarderingArea.Add(keys[i], toAdd);

		}
	}

    void SetMenus()
    {
        menuDictionary.Add(screenState.TalkMenu, new TalkMenu());
        menuDictionary[screenState.TalkMenu].Start();
    }

    void SetHUDButtons()
    {
        merchantDropPointButton.Set(GUIHeaderMerchantPlacementButton, "Designate where the merchant will place items you have ordered from him.");
        hireButton.Set(GUIHeaderHireEmployeeButton, "Hire new workers from the town.");
        shiftsButton.Set(GUIHeaderEmployeeShiftButton, "Set the working hours of your employees.");
        illuminatiButton.Set(GUIHeaderIlluminatiButton, "View information regarding the Council of Innkeepers.");

        workFurnishingButton.Set(GUIFooterWorkButton, "Build Furnishings required for work.");
        roomButton.Set(GUIFooterRoomButton, "Build Rooms.");
        decorationFurnishingButton.Set(GUIFooterDecorationButton, "Build decorative Furnishing.");

        pickUpItemButton.Set(GUIFooterPickUpButton, "View objects which can be picked up.");
        attackButton.Set(GUIFooterAttackButton, "Choose an entitiy to attack.");
        talkButton.Set(GUIFooterTalkButton, "Choose an entitiy to talk to.");
    }

    void SetWorkMenuButtons()
    {
        doorButton = new CostingButton();
        doorButton.Set(GUIDoorButton, "A door.", doorCost);

        deleteButton = new MenuButton();
        deleteButton.Set(GUIDeleteFurnishingButton, "Remove furnishing.");

        moveFurnishingButton = new MenuButton();
        moveFurnishingButton.Set(GUIMoveFurnishingButton, "Move furnishing.");


        WorkMenuButton.Clear();
        FurnishingButton toAdd;

        toAdd = new FurnishingButton();
        toAdd.Set(GUIWellButton, "A well. Gives water.", FurnishingLibrary.FurnishingID.Well);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUICookingStationButton, "A cooking station. Assigned employees act as chefs. Allows for the preperation of ingredients into food.", FurnishingLibrary.FurnishingID.PrepTable);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIStillButton, "A still. Assigned employees act as brewers. Turns water into beer via magic.", FurnishingLibrary.FurnishingID.Still);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIOvenButton, "An oven. Allows the cooking of foods that are cooked in an oven.", FurnishingLibrary.FurnishingID.Oven);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIKitchenWindowButton, "A kitchen window. When food is ordered, the orders are placed him. Assigns orders to prep tables in attached kitchen.", FurnishingLibrary.FurnishingID.KitchenWindow);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUITableButton, "A table. Assigned employees will become waiters. A place for customers to sit and order food and drinks.", FurnishingLibrary.FurnishingID.Table);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIBarButton, "A bar. Assigned employees will become bartenders. Ordered drinks are poured and handed out here.", FurnishingLibrary.FurnishingID.Bar);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIQuestBoardButton, "A quest board. Quests are put up here for Adventurers to partake in. You can create quests here with the help of the Innkeepers.", FurnishingLibrary.FurnishingID.QuestBoard);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIBedButton, "A bed. Assigned employees will clean the bed when it becomes dirty. Adventurers that are tired will sleep here if there are enough beds in the same room for their entire party. Adventurers will not sleep in the same room as a different party.", FurnishingLibrary.FurnishingID.Bed);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIMeatGrinderButton, "A meat grinder. Can be used to make ground meat.", FurnishingLibrary.FurnishingID.MeatGrinder);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIStoveButton, "A stove. Can be used to cook things that require a stove.", FurnishingLibrary.FurnishingID.Stove);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUICellarStairsButton, "Cellar stairs. Can be used to store or take objects from the cellar.", FurnishingLibrary.FurnishingID.CellarStairs);
        WorkMenuButton.Add(toAdd);

        toAdd = new FurnishingButton();
        toAdd.Set(GUIGrapeVatButton, "Grape Vat. Can be used to turn grapes into grape juice.", FurnishingLibrary.FurnishingID.GrapeVat);
        WorkMenuButton.Add(toAdd);
    }

	/// <summary>
	/// Checks the movement queue. If character is first in the queue. Else, they will be added to the queue.
	/// </summary>
	/// <returns><c>true</c>, if movement queue was checked, <c>false</c> otherwise.</returns>
	/// <param name="checker">Checker.</param>
	/// <param name="toGoTo">To go to.</param>
	public bool MovementQueueReady(Humanoid checker)
	{
		if(MoveQueue.Count > 0)
		{
			while(!MoveQueue[0].gameObject.activeInHierarchy)
			{
				MoveQueue.RemoveAt(0);
				if(MoveQueue.Count == 0)
				{
					break;
				}
			}
		}

		if(MoveQueue.Count > 0 && MoveQueue[0] == checker && !movementDone)
		{
			MoveQueue.RemoveAt(0);
			movementDone = true;
			return true;
		}
		else if(MoveQueue.Count == 0 && !movementDone)
		{
			movementDone = true;
			return true;
		}
		else if(!MoveQueue.Contains(checker))
		{
			MoveQueue.Add(checker);
		}
		
		return false;
	}

	/// <summary>
	/// Finds a party for a humanoid based on NPC Type. Adds NPC to party returned. Will make new party if none are found.
	/// Returns null if character is already in party or is not the correct NPC type.
	/// </summary>
	/// <returns>The party for humanoid.</returns>
	/// <param name="toFindFor">To find for.</param>
	public Party findPartyForHumanoid(Humanoid toFindFor)
	{
		if(toFindFor.partyApartOf != null)
		{
			return null;
		}
		if(toFindFor.thisNPCType == Humanoid.NPCType.Civilian && 
            (toFindFor.wantedDrink != FurnishingLibrary.HoldableID.Error || toFindFor.wantedFood != FurnishingLibrary.HoldableID.Error))
		{
			for(int i = 0; i < CivilianParties.Count; i++)
			{
				if(CivilianParties[i].Members.Count < 4 
				   && CivilianParties[i].Members[0].tableSittingAt == null 
				   && CivilianParties[i].Members[0].gameObject.activeInHierarchy
				   && CivilianParties[i].Members[0].timeSinceOrderedDrink == 0
				   && CivilianParties[i].Members[0].timeSinceOrderedFood == 0)
				{
					CivilianParties[i].addMember(toFindFor);
					return CivilianParties[i];
				}
			}

			Party newParty = new Party();
			newParty.addMember(toFindFor);
			CivilianParties.Add (newParty);
			newParty.partyName = ""+Random.Range(0,100);
			toFindFor.name = "Civilian Leader of "+newParty.partyName;
			return newParty;
		}
		else if(toFindFor.thisNPCType == Humanoid.NPCType.Adventurer)
		{
			for(int i = 0; i < AdventurerParties.Count; i++)
			{
				if(AdventurerParties[i].Members.Count < 4 
				   && AdventurerParties[i].Members[0].tableSittingAt == null 
				   && AdventurerParties[i].Members[0].gameObject.activeInHierarchy
				   && AdventurerParties[i].Members[0].timeSinceOrderedDrink == 0
				   && AdventurerParties[i].Members[0].timeSinceOrderedFood == 0)
				{
					AdventurerParties[i].addMember(toFindFor);
					return AdventurerParties[i];
				}
			}
			
			AdventurerParty newParty = new AdventurerParty();
			newParty.addMember(toFindFor);
			AdventurerParties.Add (newParty);
			newParty.partyName = ""+Random.Range(0,100);
			toFindFor.name = "Party Leader of "+newParty.partyName;
			/*newParty.dungeonInsideOf = new Dungeon();
			newParty.dungeonInsideOf.Start(1);
			newParty.returnTimer = 120f;*/
			return newParty;
		}

		return null;
	}

	public void addToBuildList(RoomPiece toBuild)
	{
		if(!roomsToBuild.Contains(toBuild))
		{
			roomsToBuild.Add(toBuild);
		}
	}

	public void addToBuildList(Wall toBuild)
	{
		if(!wallsToBuild.Contains(toBuild))
		{
			wallsToBuild.Add(toBuild);
		}
	}

	void Awake () 
	{
		makeGrid();
		mechanics = this;
		recipeBook = new RecipeBook();
		equipmentFactory = GetComponent<EquipmentFactory>();
        dialogEngine = new DialogEngine();
		useGUILayout = false;
	}

	void Start ()
	{
		SetKnownRecipes();
		SetBuyables();
		spawnBeginningHumanoids();
		SetBoarderingAreas();
        SetWorkMenuButtons();
        SetHUDButtons();
        SetMenus();
        SetCellarStorage();
        calculateCustomerCap();
        StockHireableEmployees();
    }

    void SetCellarStorage()
    {
        if (CellarStairs.CellarStorage == null)
        {
            CellarStairs.CellarStorage = new Dictionary<FurnishingLibrary.HoldableID, List<Holdable>>();
        }
    }

	void spawnBeginningHumanoids()
	{
		spawnHumanoid(Humanoid.NPCType.Player, new Vector2(0,0));

		spawnHumanoid(Humanoid.NPCType.DeliveryMan, new Vector2(0,0));

		spawnHumanoid(Humanoid.NPCType.QuestMaster, new Vector2(0,0));

        for (int i = 0; i < startingEmploy; i++)
        {
            int spawnY = Random.Range(0, 5);
            spawnHumanoid(Humanoid.NPCType.Employee, new Vector2(0, spawnY));
        }

        for (int i = 0; i < startingBuilders; i++)
		{
			int spawnY = Random.Range(0, 5);
			spawnHumanoid(Humanoid.NPCType.Builder, new Vector2(0, spawnY));
		}

		for(int i = 0; i < advCap; i++)
		{
			int spawnY = Random.Range(0, 5);
			int left = Random.Range(0,2);
			spawnHumanoid(Humanoid.NPCType.Adventurer, new Vector2(left*(GRID_X-1), spawnY));
		}

        for (int i = 0; i < startingTownship; i++)
        {
            int spawnY = Random.Range(0, 5);
            int left = Random.Range(0, 2);
            spawnHumanoid(Humanoid.NPCType.Civilian, new Vector2(left * (GRID_X - 1), spawnY));
        }
    }

	void SetBuyables()
	{
		BuyableList.Clear();
		BuyableOrderAmmount.Clear();
		BuyableList.Add(FurnishingLibrary.HoldableID.Barrel, 5);
		BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Barrel, 0);
		BuyableList.Add(FurnishingLibrary.HoldableID.Cup, 20);
		BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Cup, 0);
		BuyableList.Add(FurnishingLibrary.HoldableID.Egg, 20);
		BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Egg, 0);
		BuyableList.Add(FurnishingLibrary.HoldableID.Flour, 50);
		BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Flour, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.RawMeat, 50);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.RawMeat, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.Cheese, 20);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Cheese, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.Greens, 50);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Greens, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.Gruit, 12);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Gruit, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.Hops, 12);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Hops, 0);
        BuyableList.Add(FurnishingLibrary.HoldableID.Grapes, 8);
        BuyableOrderAmmount.Add(FurnishingLibrary.HoldableID.Grapes, 0);
    }

	public void SetKnownRecipes()
	{
        recipeBook.KnownPrepables.Clear();
        recipeBook.knownPrepableIDs.Clear();

        Dictionary<EmployeeSkill.WorkSkill, int> cookLevels = new Dictionary<EmployeeSkill.WorkSkill, int>();
        cookLevels.Add(EmployeeSkill.WorkSkill.CKDwarven, 0);
        cookLevels.Add(EmployeeSkill.WorkSkill.CKElven, 0);
        cookLevels.Add(EmployeeSkill.WorkSkill.CKHumane, 0);
        cookLevels.Add(EmployeeSkill.WorkSkill.CKOrcish, 0);

        Dictionary<EmployeeSkill.WorkSkill, int> brewLevels = new Dictionary<EmployeeSkill.WorkSkill, int>();
        brewLevels.Add(EmployeeSkill.WorkSkill.BRDwarven, 0);
        brewLevels.Add(EmployeeSkill.WorkSkill.BRElven, 0);
        brewLevels.Add(EmployeeSkill.WorkSkill.BRHumane, 0);
        brewLevels.Add(EmployeeSkill.WorkSkill.BROrcish, 0);

        for (int i = 0; i < EmployeesInReserve.Count; i++)
        {
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.CKDwarven, cookLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.CKElven, cookLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.CKHumane, cookLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.CKOrcish, cookLevels);

            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.BRDwarven, brewLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.BRElven, brewLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.BRHumane, brewLevels);
            checkSkillLevel(EmployeesInReserve[i], EmployeeSkill.WorkSkill.BROrcish, brewLevels);
        }

        for (int i = 0; i < EmployeesOnScene.Count; i++)
        {
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.CKDwarven, cookLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.CKElven, cookLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.CKHumane, cookLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.CKOrcish, cookLevels);

            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.BRDwarven, brewLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.BRElven, brewLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.BRHumane, brewLevels);
            checkSkillLevel(EmployeesOnScene[i], EmployeeSkill.WorkSkill.BROrcish, brewLevels);
        }

        if (Innkeeper != null)
        {
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.CKDwarven, cookLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.CKElven, cookLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.CKHumane, cookLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.CKOrcish, cookLevels);

            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.BRDwarven, brewLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.BRElven, brewLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.BRHumane, brewLevels);
            checkSkillLevel(Innkeeper, EmployeeSkill.WorkSkill.BROrcish, brewLevels);
        }

        var recipes = recipeBook.GetKnownFoodRecipes(cookLevels, out menu);
        var drinkRecipes = recipeBook.GetKnownDrinkRecipes(brewLevels);
        for(int i = 0; i < recipes.Count; i++)
        {
            recipeBook.makeRecipeKnown(recipes[i]);
        }
        for(int i = 0; i < drinkRecipes.Count; i++)
        {
            recipeBook.makeDrinkRecipeKnown(drinkRecipes[i]);
        }
    }

    void checkSkillLevel(Humanoid toCheck, EmployeeSkill.WorkSkill skillToCheck, Dictionary<EmployeeSkill.WorkSkill, int> skillLevels)
    {
        EmployeeSkill skill = null;
        if (toCheck.workSkills.TryGetValue(skillToCheck, out skill)
            && skillLevels[skillToCheck] < skill.level)
        {
            skillLevels[skillToCheck] = skill.level;
        }
    }

	void BuyBuyables(int cost)
	{
        if(cost > currency)
        {
            return;
        }

		if(DeliveryMenInReserve.Count > 0)
		{
            bool hasDoneSomething = false; 
			List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(BuyableOrderAmmount.Keys);
			for(int i = 0; i < keys.Count; i++)
			{
				for(int a = 0; a < BuyableOrderAmmount[keys[i]]; a++)
				{
                    hasDoneSomething = true;
					DeliveryMenInReserve[0].heldObjects[0].holdablesInThisHoldable.Add(keys[i]);
				}
				BuyableList[keys[i]] -= BuyableOrderAmmount[keys[i]];
				BuyableOrderAmmount[keys[i]] = 0;
			}
            if (hasDoneSomething)
            {
                activateHumanoid(DeliveryMenInReserve[0]);
                /*
                DeliveryMenInReserve[0].gameObject.SetActive(true);
                DeliveryMenOnScene.Add(DeliveryMenInReserve[0]);
                DeliveryMenInReserve.RemoveAt(0);*/
                changeMoney(-cost);
            }
		}
	}

	Vector2 WhereMouseOnGrid()
	{
		Vector2 mousPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		int x;
		int y;
		if(mousPos.x >= 0)
		{
			x = (int)(mousPos.x+0.5f);
		}
		else
		{
			x = (int)(mousPos.x-0.5f);
		}

		if(mousPos.y >= 0)
		{
			y = (int)(mousPos.y+0.5f);
		}
		else
		{
			y = (int)(mousPos.y-0.5f);
		}

		return new Vector2(x,y);
	}






















	void OnGUI()
	{
		Time.timeScale = 1;
		mouseOverGUI = false;
		UIMousPos = new Vector2(Input.mousePosition.x, Screen.height - Input.mousePosition.y);
		float halfSize = Screen.height/(Camera.main.orthographicSize*2);

		bool normalishScreen = true;

		if(currentScreenState == screenState.AssignWorkerMenu
		   || currentScreenState == screenState.FurnishingMenu
           || currentScreenState == screenState.MerchantBuyOrder
           || currentScreenState == screenState.EmployeeShiftScreen
           || currentScreenState == screenState.ConfirmFireEmployee
           || currentScreenState == screenState.HireEmployeeMenu
           || currentScreenState == screenState.ConfirmHireEmployee
           || currentScreenState == screenState.TalkMenu)
		{
			normalishScreen = false;
		}

        //TIME COLOR SYSTEM
        if(normalishScreen)
        {
            Rect screen = new Rect(0,0,Screen.width, Screen.height);
            if (timeOfDayMinutes < 720)//Day
            {
                GUI.color = new Color(0, 0, 0, 0);
            }
            else if (timeOfDayMinutes < 780)//Dusk Transition (orange)
            {
                GUI.color = new Color(0.91f * (timeOfDayMinutes - 720) / 60f,
                    0.51f * (timeOfDayMinutes - 720) / 60f,
                    0.23f * (timeOfDayMinutes - 720) / 60f,
                    0.25f * (timeOfDayMinutes - 720) / 60f);
            }
            else if (timeOfDayMinutes < 840)//Dusk Transition (black)
            {
                GUI.color = new Color(0.91f - 0.91f * (timeOfDayMinutes - 780) / 60f,
                    0.51f - 0.51f * (timeOfDayMinutes - 780) / 60f,
                    0.23f - 0.08f * (timeOfDayMinutes - 780) / 60f,
                    0.25f + 0.25f * (timeOfDayMinutes - 780) / 60f);
            }
            else if (timeOfDayMinutes < 1320)//Night
            {
                GUI.color = new Color(0, 0, 0.15f, 0.5f);
            }
            else if (timeOfDayMinutes < 1380)//Dawn Transition(Yellow)
            {
                GUI.color = new Color(
                    0.96f * (timeOfDayMinutes - 1320) / 60f,
                    0.65f * (timeOfDayMinutes - 1320) / 60f,
                    0.15f + 0.03f * (timeOfDayMinutes - 1320) / 60f,
                    0.5f - 0.25f * (timeOfDayMinutes - 1320) / 60f);
            }
            else//Dawn Transition(White)
            {
                GUI.color = new Color(
                    0.96f + 0.04f * (timeOfDayMinutes - 1380) / 60f,
                    0.65f + 0.35f * (timeOfDayMinutes - 1380) / 60f,
                    0.18f + 0.82f * (timeOfDayMinutes - 1380) / 60f,
                    0.25f - 0.25f * (timeOfDayMinutes - 1380) / 60f);
            }
                GUI.DrawTexture(screen, Texture2D.whiteTexture);
            GUI.color = Color.white;
        }
        //END TIME COLOR SYSTEM

		//FOOTER UI
		Rect LDrawRect = new Rect(0, Screen.height-Screen.height/32, Screen.height/64, Screen.height/32);
		Rect LFindRect = new Rect(GUIFooterL.rect.x/GUIFooter.width, GUIFooterL.rect.y/GUIFooter.height, GUIFooterL.rect.width/GUIFooter.width, GUIFooterL.rect.height/GUIFooter.height);
		
		Rect RDrawRect = new Rect(Screen.width-LDrawRect.width, LDrawRect.y, LDrawRect.width, LDrawRect.height);
		Rect RFindRect = new Rect(GUIFooterR.rect.x/GUIFooter.width, GUIFooterR.rect.y/GUIFooter.height, GUIFooterR.rect.width/GUIFooter.width, GUIFooterR.rect.height/GUIFooter.height);
		
		Rect MDrawRect = new Rect(LDrawRect.width, LDrawRect.y, Screen.width-LDrawRect.width*2, LDrawRect.height);
		Rect MFindRect = new Rect(GUIFooterM.rect.x/GUIFooter.width, GUIFooterM.rect.y/GUIFooter.height, GUIFooterM.rect.width/GUIFooter.width, GUIFooterM.rect.height/GUIFooter.height);
		
		Rect RoomButtonDrawRect = new Rect(Screen.width- LDrawRect.height*2, MDrawRect.y, LDrawRect.height, LDrawRect.height);
		Rect WorkButtonDrawRect = new Rect(RoomButtonDrawRect.x-RoomButtonDrawRect.width, RoomButtonDrawRect.y, RoomButtonDrawRect.height, RoomButtonDrawRect.height);
        Rect DecortationButtonDrawRect = new Rect(WorkButtonDrawRect.x - RoomButtonDrawRect.width, RoomButtonDrawRect.y, RoomButtonDrawRect.height, RoomButtonDrawRect.height);

        Rect LeftHandDropRect = new Rect(Screen.width/2-Screen.height/8, Screen.height-Screen.height/32, Screen.height/8, Screen.height/32);
		Rect LeftHandUseRect = new Rect(LeftHandDropRect.x, LeftHandDropRect.y-Screen.height/6, Screen.height/8, Screen.height/6);
		Vector2 leftMidPoint = new Vector2(LeftHandUseRect.x, LeftHandUseRect.y)+ new Vector2(LeftHandUseRect.width, LeftHandUseRect.height)/2;
		Rect leftHeldImage = new Rect(leftMidPoint-new Vector2(Screen.height/16, Screen.height/16)/2,new Vector2(Screen.height/16, Screen.height/16));

		Rect RightHandDropRect = new Rect(Screen.width/2, Screen.height-Screen.height/32, Screen.height/8, Screen.height/32);
		Rect RightHandUseRect = new Rect(RightHandDropRect.x, RightHandDropRect.y-Screen.height/6, Screen.height/8, Screen.height/6);
		Vector2 rightMidPoint = new Vector2(RightHandUseRect.x, RightHandUseRect.y)+ new Vector2(RightHandUseRect.width, RightHandUseRect.height)/2;
		Rect rightHeldImage = new Rect(rightMidPoint-new Vector2(Screen.height/16, Screen.height/16)/2,new Vector2(Screen.height/16, Screen.height/16));

		Rect PickUpButtonDrawRect = new Rect(LDrawRect.x+LDrawRect.width, LDrawRect.y, RoomButtonDrawRect.height, RoomButtonDrawRect.height);
        Rect TalkButtonDrawRect = new Rect(PickUpButtonDrawRect.x+PickUpButtonDrawRect.width, PickUpButtonDrawRect.y, PickUpButtonDrawRect.height, PickUpButtonDrawRect.height);
        Rect AttackButtonDrawRect = new Rect(TalkButtonDrawRect.x + PickUpButtonDrawRect.width, PickUpButtonDrawRect.y, PickUpButtonDrawRect.height, PickUpButtonDrawRect.height);
        //FOOTER UI END




        //HEADER UI
        Rect LHDrawRect = new Rect(0, 0, LDrawRect.width, LDrawRect.height);
        Rect LHFindRect = new Rect(GUIHeaderL.rect.x / GUIHeader.width, GUIHeaderL.rect.y / GUIHeader.height, GUIHeaderL.rect.width / GUIHeader.width, GUIHeaderL.rect.height / GUIHeader.height);

        Rect RHDrawRect = new Rect(Screen.width - LHDrawRect.width, 0, LHDrawRect.width, LHDrawRect.height);
        Rect RHFindRect = new Rect(GUIHeaderR.rect.x / GUIHeader.width, GUIHeaderR.rect.y / GUIHeader.height, GUIHeaderR.rect.width / GUIHeader.width, GUIHeaderR.rect.height / GUIHeader.height);

        Rect MHDrawRect = new Rect(LHDrawRect.width, 0, Screen.width - LHDrawRect.width*2, LHDrawRect.height);
        Rect MHFindRect = new Rect(GUIHeaderM.rect.x / GUIHeader.width, GUIHeaderM.rect.y / GUIHeader.height, GUIHeaderM.rect.width / GUIHeader.width, GUIHeaderM.rect.height / GUIHeader.height);

        Rect CurrencyBarDrawRect = new Rect(Screen.width/2- LHDrawRect.height * 2.5f, 0, LHDrawRect.height*5, LHDrawRect.height);
        Rect CurrencySymbolDrawRect = new Rect(CurrencyBarDrawRect.x, CurrencyBarDrawRect.y, CurrencyBarDrawRect.height, CurrencyBarDrawRect.height);

        Rect FameBarDrawRect = new Rect(CurrencyBarDrawRect.x-CurrencyBarDrawRect.width, CurrencyBarDrawRect.y, CurrencyBarDrawRect.width, CurrencyBarDrawRect.height);
        Rect FameSymbolDrawRect = new Rect(FameBarDrawRect.x, CurrencyBarDrawRect.y, CurrencyBarDrawRect.height, CurrencyBarDrawRect.height);

        moneyDisplayPos = new Vector2(CurrencyBarDrawRect.x, CurrencyBarDrawRect.y);
        fameDisplayPos = new Vector2(FameBarDrawRect.x, FameBarDrawRect.y);
        Rect IlluminatiButtonDrawRect = new Rect(CurrencyBarDrawRect.x+CurrencyBarDrawRect.width, 0, LHDrawRect.height, LHDrawRect.height);

        Rect clockRotery = new Rect(RHDrawRect.x - LHDrawRect.height * 5, RHDrawRect.y, LHDrawRect.height * 5, LHDrawRect.height * 5);
        Rect clockBase = new Rect(clockRotery.x - clockRotery.width * 0.165f / 2f,
            clockRotery.y - clockRotery.width * 0.165f / 2f,
            clockRotery.width * 1.165f, clockRotery.width * 1.165f);

        Rect shiftButtonRect = new Rect(clockRotery.x-LHDrawRect.height, 0, LHDrawRect.height, LHDrawRect.height);
        Rect hireButtonRect = new Rect(shiftButtonRect.x - shiftButtonRect.height, 0, shiftButtonRect.height, shiftButtonRect.height);
        Rect dropPointButtonRect = new Rect(hireButtonRect.x - shiftButtonRect.height, 0, shiftButtonRect.height, shiftButtonRect.height);
        //HEADER UI END






        if (currentScreenState == screenState.Normal
            &&
            (UIMousPos.y >= Screen.height - LDrawRect.height
            || UIMousPos.y <= LDrawRect.height
            || LeftHandDropRect.Contains(UIMousPos)
            || LeftHandUseRect.Contains(UIMousPos)
            || RightHandDropRect.Contains(UIMousPos)
            || RightHandUseRect.Contains(UIMousPos)))
        {
            mouseOverGUI = true;
        }
        else if (normalishScreen
            &&
            (UIMousPos.y >= Screen.height - LDrawRect.height
            || UIMousPos.y <= LDrawRect.height))
        {
            mouseOverGUI = true;
        }
        else if(!normalishScreen)
        {
            mouseOverGUI = true;
        }

		//If current menu is normal or pickupMenu, buttons can be pressed
		if (currentScreenState == screenState.Normal || currentScreenState == screenState.PickUpMenu)
		{
			if(Input.GetMouseButtonDown(0)
			   && LeftHandDropRect.Contains(UIMousPos)
			   && Innkeeper.heldObjects[0] != null)
			{
				Innkeeper.dropItemOnFloor(true);
			}

			if(Input.GetMouseButtonDown(0)
			   && RightHandDropRect.Contains(UIMousPos)
			   && Innkeeper.heldObjects[1] != null)
			{
				Innkeeper.dropItemOnFloor(false);
			}

			if(Input.GetMouseButtonDown(0)
			   && LeftHandUseRect.Contains(UIMousPos)
			   && Innkeeper.heldObjects[0] != null)
			{
				Innkeeper.heldObjects[0].Use(Innkeeper);
			}
			
			if(Input.GetMouseButtonDown(0)
			   && RightHandUseRect.Contains(UIMousPos)
			   && Innkeeper.heldObjects[1] != null)
			{
				Innkeeper.heldObjects[1].Use(Innkeeper);
			}
		}




		//DRAW ALL THIS SHIT IN ORDER BECAUSE UNITY SUCKS MASSIVE DONG
		//Drawing all the GUI elements of all objects
		if(currentScreenState != screenState.AssignWorkerMenu && currentScreenState != screenState.FurnishingMenu && currentScreenState != screenState.MerchantBuyOrder)
		{
			Innkeeper.Draw();
			for(int i = 0; i < AdventurersOnScene.Count; i++)
			{
				AdventurersOnScene[i].Draw();
			}
			for(int i = 0; i < BuildersOnScene.Count; i++)
			{
				BuildersOnScene[i].Draw();
			}
			for(int i = 0; i < CiviliansOnScene.Count; i++)
			{
				CiviliansOnScene[i].Draw();
			}
			for(int i = 0; i < EmployeesOnScene.Count; i++)
			{
				EmployeesOnScene[i].Draw();
			}
			for(int i = 0; i < QuestMasterOnScene.Count; i++)
			{
				QuestMasterOnScene[i].Draw();
			}

			List<FurnishingLibrary.FurnishingID> keys = new List<FurnishingLibrary.FurnishingID>(FurnishingOnMap.Keys);
			for(int k = 0; k < keys.Count; k++)
			{
				for(int i = 0; i < FurnishingOnMap[keys[k]].Count; i++)
				{
					FurnishingOnMap[keys[k]][i].Draw();
				}
			}

			List<FurnishingLibrary.HoldableID> hKeys = new List<FurnishingLibrary.HoldableID>(HoldablesOnGround.Keys);
			for(int k = 0; k < hKeys.Count; k++)
			{
				for(int i = 0; i < HoldablesOnGround[hKeys[k]].Count; i++)
				{
					HoldablesOnGround[hKeys[k]][i].Draw();
				}
			}

            for (int i = 0; i < displayWorldEffects.Count; i++)
            {
                displayWorldEffects[i].Draw();
            }
            GUI.color = Color.white;
		}
		//END GUI OBJECT DRAWS









        if(currentScreenState == screenState.TalkMenu)
        {
            Time.timeScale = 0;
            menuDictionary[currentScreenState].Draw(UIMousPos);
        }
        else if(currentScreenState == screenState.ChoosingTalkTarget || currentScreenState == screenState.ChoosingCombatTarget)
        {
            if(targetHumanoid != null)
            {
                Vector2 midPoint = Camera.main.WorldToScreenPoint(targetHumanoid.mainTransform.position);
                midPoint = new Vector2(midPoint.x, Screen.height - midPoint.y);

                Rect drawRect = new Rect(midPoint - new Vector2(halfSize, halfSize) / 2, new Vector2(halfSize, halfSize));

                if(currentScreenState == screenState.ChoosingTalkTarget)
                {
                    GUI.color = Color.green;
                }
                else
                {
                    GUI.color = Color.red;
                }

                GUI.DrawTexture(drawRect, selectionCircle);

                GUI.color = Color.white;
            }
        }
        else if(currentScreenState == screenState.EmployeeShiftScreen || currentScreenState == screenState.ConfirmFireEmployee)
        {
            mouseOverGUI = true;

            Humanoid toLookAt = ShiftHumanoidLookingAt;
            Time.timeScale = 0;
            Rect background = new Rect(0, 0, Screen.width, Screen.height);
            GUI.DrawTexture(background, Texture2D.whiteTexture);

            var labelStyle = GUI.skin.GetStyle("Label");
            labelStyle.fontSize = Screen.height / 40;
            labelStyle.alignment = TextAnchor.MiddleCenter;
            GUIContent textSizer = new GUIContent("Xy");
            Vector2 textSize = labelStyle.CalcSize(textSizer);
            Rect sideRect = new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height);
            GUI.color = Color.black;
            GUI.DrawTexture(new Rect(sideRect.x - 1, sideRect.y - 1, sideRect.width + 2, sideRect.height + 2), Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(sideRect, Texture2D.whiteTexture);

            int y = 0;
            for (int i = 0; i < EmployeesOnScene.Count; i++)
            {
                Rect textRect = new Rect(new Vector2(0, PickUpScroll) + new Vector2(0, Screen.height / 20) * y, new Vector2(Screen.width / 2 - Screen.height / 20, Screen.height / 20));
                //GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                GUI.color = Color.black;
                GUI.DrawTexture(new Rect(textRect.x - 1, textRect.y - 1, textRect.width + 2, textRect.height + 2), Texture2D.whiteTexture);
                if (EmployeesOnScene[i] != ShiftHumanoidLookingAt)
                {
                    GUI.color = Color.white;
                }
                GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                labelStyle.alignment = TextAnchor.MiddleLeft;
                if (EmployeesOnScene[i] == ShiftHumanoidLookingAt)
                {
                    GUI.color = Color.white;
                }
                else
                {
                    GUI.color = Color.black;
                }
                GUI.Label(textRect, EmployeesOnScene[i].Name);
                labelStyle.alignment = TextAnchor.MiddleRight;
                GUI.Label(textRect, "Jobs: " + EmployeesOnScene[i].assignedFurnishings.Count);
                GUI.color = Color.white;

                Rect symbolRext = new Rect(textRect.width, textRect.y, textRect.height, textRect.height);
                if (EmployeesOnScene[i].hasNightShift)
                {
                    GUI.DrawTexture(symbolRext, nightSymbol);
                }
                else
                {
                    GUI.DrawTexture(symbolRext, daySymbol);
                }

                if ((textRect.Contains(UIMousPos) || symbolRext.Contains(UIMousPos)) && currentScreenState == screenState.EmployeeShiftScreen)
                {
                    toLookAt = EmployeesOnScene[i];
                    if (textRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        ShiftHumanoidLookingAt = toLookAt;
                    }
                    else if (symbolRext.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        EmployeesOnScene[i].hasNightShift = !EmployeesOnScene[i].hasNightShift;
                    }
                }
                y++;
            }
            for (int i = 0; i < EmployeesInReserve.Count; i++)
            {
                Rect textRect = new Rect(new Vector2(0, PickUpScroll) + new Vector2(0, Screen.height / 20) * y, new Vector2(Screen.width / 2 - Screen.height / 20, Screen.height / 20));
                GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                GUI.color = Color.black;
                GUI.DrawTexture(new Rect(textRect.x - 1, textRect.y - 1, textRect.width + 2, textRect.height + 2), Texture2D.whiteTexture);
                if (EmployeesInReserve[i] != ShiftHumanoidLookingAt)
                {
                    GUI.color = Color.white;
                }
                GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                labelStyle.alignment = TextAnchor.MiddleLeft;
                GUI.color = Color.black;
                GUI.Label(textRect, EmployeesInReserve[i].Name);
                labelStyle.alignment = TextAnchor.MiddleRight;
                GUI.Label(textRect, "Jobs: " + EmployeesInReserve[i].assignedFurnishings.Count);
                GUI.color = Color.white;

                Rect symbolRext = new Rect(textRect.width, textRect.y, textRect.height, textRect.height);
                if (EmployeesInReserve[i].hasNightShift)
                {
                    GUI.DrawTexture(symbolRext, nightSymbol);
                }
                else
                {
                    GUI.DrawTexture(symbolRext, daySymbol);
                }

                if ((textRect.Contains(UIMousPos) || symbolRext.Contains(UIMousPos)) && currentScreenState == screenState.EmployeeShiftScreen)
                {
                    toLookAt = EmployeesInReserve[i];
                    if (textRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        ShiftHumanoidLookingAt = toLookAt;
                    }
                    else if (symbolRext.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        EmployeesInReserve[i].hasNightShift = !EmployeesInReserve[i].hasNightShift;
                    }
                }
                y++;
            }



            if (toLookAt != null)
            {
                Rect imageBox = new Rect(sideRect.x + sideRect.width / 2 - sideRect.height / 8, sideRect.y, sideRect.height / 4, sideRect.height / 4);
                Rect nameBox = new Rect(sideRect.x, imageBox.y + imageBox.height, sideRect.width, Screen.height / 20);
                Rect FireBox = new Rect(sideRect.x+sideRect.width/2-sideRect.width/8, nameBox.y + nameBox.height, sideRect.width/4, nameBox.width/8);
                Rect assignedTitleBox = new Rect(nameBox.x, FireBox.y + FireBox.height, nameBox.width, nameBox.height);
                Rect line = new Rect(assignedTitleBox.x, assignedTitleBox.y + assignedTitleBox.height, assignedTitleBox.width, Screen.height / 100);

                labelStyle.alignment = TextAnchor.MiddleCenter;
                GUI.DrawTexture(imageBox, toLookAt.GetComponent<SpriteRenderer>().sprite.texture);
                GUI.color = Color.black;
                GUI.Label(nameBox, toLookAt.Name);
                GUI.DrawTexture(FireBox, Texture2D.whiteTexture);
                GUI.color = Color.white;
                GUI.DrawTexture(new Rect(FireBox.x + 1, FireBox.y + 1, FireBox.width - 2, FireBox.height - 2), Texture2D.whiteTexture);
                GUI.color = Color.black;
                labelStyle.alignment = TextAnchor.MiddleCenter;
                GUI.Label(FireBox, "Fire");

                labelStyle.fontSize = Screen.height / 50;
                labelStyle.alignment = TextAnchor.LowerCenter;
                GUI.Label(assignedTitleBox, "Assigned");
                GUI.DrawTexture(line, Texture2D.whiteTexture);
                int x = 0;
                y = 0;
                int xAmmount = (int)(sideRect.width / (Screen.height / 10));
                int whoCares;
                Dictionary<FurnishingLibrary.FurnishingID, int> ammountDic = new Dictionary<FurnishingLibrary.FurnishingID, int>();
                for (int i = 0; i < toLookAt.assignedFurnishings.Count; i++)
                {
                    if (ammountDic.TryGetValue(toLookAt.assignedFurnishings[i].FurnishingID, out whoCares))
                    {
                        ammountDic[toLookAt.assignedFurnishings[i].FurnishingID]++;
                    }
                    else
                    {
                        ammountDic.Add(toLookAt.assignedFurnishings[i].FurnishingID, 1);
                    }

                }

                List<FurnishingLibrary.FurnishingID> keys = new List<FurnishingLibrary.FurnishingID>(ammountDic.Keys);
                keys.Sort();

                for (int i = 0; i < keys.Count; i++)
                {
                    Rect imageRect = new Rect(sideRect.x + Screen.height / 10 * x, line.y + line.height + 1 + Screen.height / 20 * y, Screen.height / 20, Screen.height / 20);
                    Rect multiRect = new Rect(imageRect.x + imageRect.width, imageRect.y, imageRect.width, imageRect.height);

                    GUI.color = Color.white;
                    GUI.DrawTexture(imageRect, FurnishingLibrary.Library.getFurnishingPrefab(keys[i]).GetComponent<SpriteRenderer>().sprite.texture);
                    GUI.color = Color.black;
                    GUI.Label(multiRect, "x" + ammountDic[keys[i]]);

                    if (x < xAmmount - 1)
                    {
                        x++;
                    }
                    else
                    {
                        x = 0;
                        y++;
                    }
                }

                List<EmployeeSkill.WorkSkill> skillKeys = new List<EmployeeSkill.WorkSkill>(toLookAt.workSkills.Keys);
                skillKeys.Sort();
                for (int i = 0; i < skillKeys.Count; i++)
                {
                    GUI.color = Color.black;
                    Rect nameRect = new Rect(sideRect.x, line.y + line.height + 2 + Screen.height / 20 * (i + 1), sideRect.width / 2 - 2, sideRect.height / 20);

                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(nameRect, skillKeys[i].ToString());


                    Rect xpBarRect = new Rect(nameRect.x + nameRect.width, nameRect.y, nameRect.height * 5, nameRect.height);
                    Rect currentXP = new Rect(xpBarRect.x + 1, xpBarRect.y + 1,
                        xpBarRect.width * toLookAt.workSkills[skillKeys[i]].currentXP / toLookAt.workSkills[skillKeys[i]].maxXP - 2, xpBarRect.height - 2);
                    GUI.DrawTexture(xpBarRect, Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(new Rect(xpBarRect.x + 1, xpBarRect.y + 1, xpBarRect.width - 2, xpBarRect.height - 2), Texture2D.whiteTexture);
                    if (toLookAt.workSkills[skillKeys[i]].currentXP > 0)
                    {
                        GUI.color = new Color(0.5f, 0.25f, 0.5f);
                        GUI.DrawTexture(currentXP, Texture2D.whiteTexture);
                    }

                    GUI.color = Color.white;
                    for (int b = 0; b < 5; b++)
                    {
                        Rect levelRect = new Rect(nameRect.x + nameRect.width + nameRect.height * b, nameRect.y, nameRect.height, nameRect.height);
                        if (b < toLookAt.workSkills[skillKeys[i]].level)
                        {
                            GUI.DrawTexture(levelRect, GUIFilledBlip);
                        }
                        else
                        {
                            GUI.DrawTexture(levelRect, GUIEmptyBlip);
                        }
                    }
                }

                if(currentScreenState == screenState.ConfirmFireEmployee)
                {
                    Rect MenuBox = new Rect(Screen.width/2-Screen.width/4, Screen.height/2 - Screen.height/4, Screen.width/2, Screen.height/2);
                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(MenuBox.x-4, MenuBox.y-4, MenuBox.width+8, MenuBox.height+8), Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(MenuBox, Texture2D.whiteTexture);

                    Rect cancleButton = new Rect(MenuBox.x+MenuBox.width*2/3-MenuBox.width/8, MenuBox.y+MenuBox.height*2/3, MenuBox.width/4, MenuBox.height/5);
                    Rect confirmButton = new Rect(MenuBox.x+MenuBox.width/3 - MenuBox.width / 8, cancleButton.y, cancleButton.width, cancleButton.height);
                    Rect TextRect = new Rect(Screen.width/2-MenuBox.width*0.45f, MenuBox.y, MenuBox.width*0.90f, MenuBox.height*2/3);

                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(cancleButton.x-1, cancleButton.y-1, cancleButton.width+2, cancleButton.height+2), Texture2D.whiteTexture);
                    GUI.DrawTexture(new Rect(confirmButton.x - 1, confirmButton.y - 1, confirmButton.width + 2, confirmButton.height + 2), Texture2D.whiteTexture);

                    GUI.color = Color.white;
                    GUI.DrawTexture(cancleButton, Texture2D.whiteTexture);
                    GUI.DrawTexture(confirmButton, Texture2D.whiteTexture);

                    labelStyle.fontSize = Screen.height / 40;
                    GUI.color = Color.black;
                    labelStyle.alignment = TextAnchor.MiddleCenter;
                    GUI.Label(cancleButton, "Cancel");
                    GUI.Label(confirmButton, "Confirm");
                    GUI.Label(TextRect, "Are you sure you want to fire this person?\nYou may never be able to hire them again.");

                    if(cancleButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        currentScreenState = screenState.EmployeeShiftScreen;
                    }
                    else if (confirmButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        if (EmployeesOnScene.Contains(ShiftHumanoidLookingAt))
                        {
                            EmployeesOnScene.Remove(ShiftHumanoidLookingAt);
                            CiviliansOnScene.Add(ShiftHumanoidLookingAt);
                        }
                        else
                        {
                            EmployeesInReserve.Remove(ShiftHumanoidLookingAt);
                            CiviliansInReserve.Add(ShiftHumanoidLookingAt);
                        }
                        ShiftHumanoidLookingAt.dropItemOnFloor(true);
                        ShiftHumanoidLookingAt.dropItemOnFloor(false);
                        ShiftHumanoidLookingAt.RemoveAllJobs();
                        ShiftHumanoidLookingAt.thisNPCType = Humanoid.NPCType.Civilian;
                        ShiftHumanoidLookingAt = null;
                        currentScreenState = screenState.EmployeeShiftScreen;
                    }
                }
                else if (FireBox.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                {
                    currentButtonWait = buttonWaitAmmount;
                    currentScreenState = screenState.ConfirmFireEmployee;
                }
            }



        }
        if (currentScreenState == screenState.HireEmployeeMenu || currentScreenState == screenState.ConfirmHireEmployee)
        {
            mouseOverGUI = true;
            Humanoid toLookAt = null;
            if (HireHumanoidLookingAt >= 0)
            {
                toLookAt = toHire[HireHumanoidLookingAt];
            }
            Time.timeScale = 0;
            Rect background = new Rect(0, 0, Screen.width, Screen.height);
            GUI.DrawTexture(background, Texture2D.whiteTexture);

            var labelStyle = GUI.skin.GetStyle("Label");
            labelStyle.fontSize = Screen.height / 40;
            labelStyle.alignment = TextAnchor.MiddleCenter;
            GUIContent textSizer = new GUIContent("Xy");
            Vector2 textSize = labelStyle.CalcSize(textSizer);
            Rect sideRect = new Rect(Screen.width / 2, 0, Screen.width / 2, Screen.height);
            GUI.color = Color.black;
            GUI.DrawTexture(new Rect(sideRect.x - 1, sideRect.y - 1, sideRect.width + 2, sideRect.height + 2), Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(sideRect, Texture2D.whiteTexture);

            int y = 0;
            for (int i = 0; i < HireSlots; i++)
            {
                Rect textRect = new Rect(new Vector2(0, PickUpScroll) + new Vector2(0, Screen.height / 20) * y, new Vector2(Screen.width / 2 - Screen.height / 20, Screen.height / 20));
                //GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                GUI.color = Color.black;
                GUI.DrawTexture(new Rect(textRect.x - 1, textRect.y - 1, textRect.width + 2, textRect.height + 2), Texture2D.whiteTexture);
                if (i != HireHumanoidLookingAt || toHire[i] == null)
                {
                    GUI.color = Color.white;
                }
                GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                labelStyle.alignment = TextAnchor.MiddleLeft;
                if (i == HireHumanoidLookingAt && toHire[i] != null)
                {
                    GUI.color = Color.white;
                }
                else
                {
                    GUI.color = Color.black;
                }
                if (toHire[i] != null)
                {
                    GUI.Label(textRect, toHire[i].Name);
                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(textRect, "Jobs: " + toHire[i].assignedFurnishings.Count);
                    GUI.color = Color.white;

                    Rect symbolRext = new Rect(textRect.width, textRect.y, textRect.height, textRect.height);
                    if (toHire[i].hasNightShift)
                    {
                        GUI.DrawTexture(symbolRext, nightSymbol);
                    }
                    else
                    {
                        GUI.DrawTexture(symbolRext, daySymbol);
                    }

                    if ((textRect.Contains(UIMousPos) || symbolRext.Contains(UIMousPos)) && currentScreenState == screenState.HireEmployeeMenu)
                    {
                        toLookAt = toHire[i];
                        if (textRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                        {
                            currentButtonWait = buttonWaitAmmount;
                            HireHumanoidLookingAt = i;
                        }
                        else if (symbolRext.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                        {
                            currentButtonWait = buttonWaitAmmount;
                            toHire[i].hasNightShift = !toHire[i].hasNightShift;
                        }
                    }
                }
                y++;
            }



            if (toLookAt != null)
            {
                GUI.color = Color.white;
                Rect imageBox = new Rect(sideRect.x + sideRect.width / 2 - sideRect.height / 8, sideRect.y, sideRect.height / 4, sideRect.height / 4);
                Rect nameBox = new Rect(sideRect.x, imageBox.y + imageBox.height, sideRect.width, Screen.height / 20);
                Rect FireBox = new Rect(sideRect.x + sideRect.width / 2 - sideRect.width / 8, nameBox.y + nameBox.height, sideRect.width / 4, nameBox.width / 8);
                Rect assignedTitleBox = new Rect(nameBox.x, FireBox.y + FireBox.height, nameBox.width, nameBox.height);
                Rect line = new Rect(assignedTitleBox.x, assignedTitleBox.y + assignedTitleBox.height, assignedTitleBox.width, Screen.height / 100);

                labelStyle.alignment = TextAnchor.MiddleCenter;
                GUI.DrawTexture(imageBox, toLookAt.GetComponent<SpriteRenderer>().sprite.texture);
                GUI.color = Color.black;
                GUI.Label(nameBox, toLookAt.Name);
                GUI.DrawTexture(FireBox, Texture2D.whiteTexture);
                GUI.color = Color.white;
                GUI.DrawTexture(new Rect(FireBox.x + 1, FireBox.y + 1, FireBox.width - 2, FireBox.height - 2), Texture2D.whiteTexture);
                GUI.color = Color.black;
                labelStyle.alignment = TextAnchor.MiddleCenter;
                GUI.Label(FireBox, "Hire");

                labelStyle.fontSize = Screen.height / 50;
                labelStyle.alignment = TextAnchor.LowerCenter;
                GUI.Label(assignedTitleBox, "Assigned");
                GUI.DrawTexture(line, Texture2D.whiteTexture);
                int x = 0;
                y = 0;
                int xAmmount = (int)(sideRect.width / (Screen.height / 10));
                int whoCares;
                Dictionary<FurnishingLibrary.FurnishingID, int> ammountDic = new Dictionary<FurnishingLibrary.FurnishingID, int>();
                for (int i = 0; i < toLookAt.assignedFurnishings.Count; i++)
                {
                    if (ammountDic.TryGetValue(toLookAt.assignedFurnishings[i].FurnishingID, out whoCares))
                    {
                        ammountDic[toLookAt.assignedFurnishings[i].FurnishingID]++;
                    }
                    else
                    {
                        ammountDic.Add(toLookAt.assignedFurnishings[i].FurnishingID, 1);
                    }

                }

                List<FurnishingLibrary.FurnishingID> keys = new List<FurnishingLibrary.FurnishingID>(ammountDic.Keys);
                keys.Sort();

                for (int i = 0; i < keys.Count; i++)
                {
                    Rect imageRect = new Rect(sideRect.x + Screen.height / 10 * x, line.y + line.height + 1 + Screen.height / 20 * y, Screen.height / 20, Screen.height / 20);
                    Rect multiRect = new Rect(imageRect.x + imageRect.width, imageRect.y, imageRect.width, imageRect.height);

                    GUI.color = Color.white;
                    GUI.DrawTexture(imageRect, FurnishingLibrary.Library.getFurnishingPrefab(keys[i]).GetComponent<SpriteRenderer>().sprite.texture);
                    GUI.color = Color.black;
                    GUI.Label(multiRect, "x" + ammountDic[keys[i]]);

                    if (x < xAmmount - 1)
                    {
                        x++;
                    }
                    else
                    {
                        x = 0;
                        y++;
                    }
                }

                List<EmployeeSkill.WorkSkill> skillKeys = new List<EmployeeSkill.WorkSkill>(toLookAt.workSkills.Keys);
                skillKeys.Sort();
                for (int i = 0; i < skillKeys.Count; i++)
                {
                    GUI.color = Color.black;
                    Rect nameRect = new Rect(sideRect.x, line.y + line.height + 2 + Screen.height / 20 * (i + 1), sideRect.width / 2 - 2, sideRect.height / 20);

                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(nameRect, skillKeys[i].ToString());


                    Rect xpBarRect = new Rect(nameRect.x + nameRect.width, nameRect.y, nameRect.height * 5, nameRect.height);
                    Rect currentXP = new Rect(xpBarRect.x + 1, xpBarRect.y + 1,
                        xpBarRect.width * toLookAt.workSkills[skillKeys[i]].currentXP / toLookAt.workSkills[skillKeys[i]].maxXP - 2, xpBarRect.height - 2);
                    GUI.DrawTexture(xpBarRect, Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(new Rect(xpBarRect.x + 1, xpBarRect.y + 1, xpBarRect.width - 2, xpBarRect.height - 2), Texture2D.whiteTexture);
                    if (toLookAt.workSkills[skillKeys[i]].currentXP > 0)
                    {
                        GUI.color = new Color(0.5f, 0.25f, 0.5f);
                        GUI.DrawTexture(currentXP, Texture2D.whiteTexture);
                    }

                    GUI.color = Color.white;
                    for (int b = 0; b < 5; b++)
                    {
                        Rect levelRect = new Rect(nameRect.x + nameRect.width + nameRect.height * b, nameRect.y, nameRect.height, nameRect.height);
                        if (b < toLookAt.workSkills[skillKeys[i]].level)
                        {
                            GUI.DrawTexture(levelRect, GUIFilledBlip);
                        }
                        else
                        {
                            GUI.DrawTexture(levelRect, GUIEmptyBlip);
                        }
                    }
                }

                if (currentScreenState == screenState.ConfirmHireEmployee)
                {
                    Rect MenuBox = new Rect(Screen.width / 2 - Screen.width / 4, Screen.height / 2 - Screen.height / 4, Screen.width / 2, Screen.height / 2);
                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(MenuBox.x - 4, MenuBox.y - 4, MenuBox.width + 8, MenuBox.height + 8), Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(MenuBox, Texture2D.whiteTexture);

                    Rect cancleButton = new Rect(MenuBox.x + MenuBox.width * 2 / 3 - MenuBox.width / 8, MenuBox.y + MenuBox.height * 2 / 3, MenuBox.width / 4, MenuBox.height / 5);
                    Rect confirmButton = new Rect(MenuBox.x + MenuBox.width / 3 - MenuBox.width / 8, cancleButton.y, cancleButton.width, cancleButton.height);
                    Rect TextRect = new Rect(Screen.width / 2 - MenuBox.width * 0.45f, MenuBox.y, MenuBox.width * 0.90f, MenuBox.height * 2 / 3);

                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(cancleButton.x - 1, cancleButton.y - 1, cancleButton.width + 2, cancleButton.height + 2), Texture2D.whiteTexture);
                    GUI.DrawTexture(new Rect(confirmButton.x - 1, confirmButton.y - 1, confirmButton.width + 2, confirmButton.height + 2), Texture2D.whiteTexture);

                    GUI.color = Color.white;
                    GUI.DrawTexture(cancleButton, Texture2D.whiteTexture);
                    GUI.DrawTexture(confirmButton, Texture2D.whiteTexture);

                    labelStyle.fontSize = Screen.height / 40;
                    GUI.color = Color.black;
                    labelStyle.alignment = TextAnchor.MiddleCenter;
                    GUI.Label(cancleButton, "Cancel");
                    GUI.Label(confirmButton, "Confirm");
                    GUI.Label(TextRect, "Are you sure you want to hire this person?");

                    if (cancleButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        currentScreenState = screenState.HireEmployeeMenu;
                    }
                    else if (confirmButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                    {
                        currentButtonWait = buttonWaitAmmount;
                        HireEmployee(HireHumanoidLookingAt);
                        currentScreenState = screenState.HireEmployeeMenu;
                    }
                }
                else if (FireBox.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
                {
                    currentButtonWait = buttonWaitAmmount;
                    currentScreenState = screenState.ConfirmHireEmployee;
                }
            }



        }
        else if(currentScreenState == screenState.MerchantBuyOrder)
        {
            mouseOverGUI = true;
            Time.timeScale = 0;
			Rect background = new Rect(0,0, Screen.width, Screen.height);
			GUI.DrawTexture(background, Texture2D.whiteTexture);
			
			var labelStyle = GUI.skin.GetStyle ("Label");
			labelStyle.fontSize = Screen.height/40;
			labelStyle.alignment = TextAnchor.MiddleCenter;
			GUIContent textSizer = new GUIContent("XyXX");
			Vector2 textSize = labelStyle.CalcSize(textSizer);

			List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(BuyableList.Keys);
            int totalCost = 0;
			for(int i = 0; i < BuyableList.Count; i++)
			{
				Rect plateRect = new Rect(new Vector2(0,PickUpScroll) + new Vector2(0, Screen.height/10)*i, new Vector2(Screen.width*2/3, Screen.height/10));
				Rect picRect = new Rect(plateRect.x, plateRect.y, plateRect.height, plateRect.height);
				Rect nameRect = new Rect(picRect.x+picRect.width, picRect.y, plateRect.width/2-picRect.width, plateRect.height);
				Rect negButton = new Rect(nameRect.x+nameRect.width, nameRect.y+nameRect.height/4, nameRect.height/2, nameRect.height/2);
				Rect ammountRect = new Rect(negButton.x+negButton.width+2, negButton.y, textSize.x, negButton.height);
				Rect posButton = new Rect(ammountRect.x+ammountRect.width+2, nameRect.y+nameRect.height/4, nameRect.height/2, nameRect.height/2);
				Texture2D objectTexture = null;
				objectTexture = FurnishingLibrary.Library.getHoldablePrefab(keys[i]).holdableTexture;
				GUI.color = Color.black;
				GUI.DrawTexture(new Rect(plateRect.x-1, plateRect.y-1, plateRect.width+2, plateRect.height+2), Texture2D.whiteTexture);
				GUI.color = Color.white;
				GUI.DrawTexture(plateRect, Texture2D.whiteTexture);
				GUI.color = Color.black;
				labelStyle.alignment = TextAnchor.MiddleLeft;
				GUI.Label(nameRect, FurnishingLibrary.Library.getItemName(keys[i], true));
				labelStyle.alignment = TextAnchor.MiddleCenter;
				GUI.Label(ammountRect, BuyableOrderAmmount[keys[i]].ToString());
				GUI.color = Color.white;
				GUI.DrawTexture(picRect, objectTexture);
				GUI.DrawTexture(negButton, GUINegButton);
				GUI.DrawTexture(posButton, GUIPosButton);

				if(negButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
				{
					currentButtonWait = buttonWaitAmmount;
					if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
					{
						BuyableOrderAmmount[keys[i]]-= 10;
					}
					else
					{
						BuyableOrderAmmount[keys[i]]--;
					}
					if(BuyableOrderAmmount[keys[i]] < 0)
					{
						BuyableOrderAmmount[keys[i]] = 0;
					}
				}
				else
				{
					if(posButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0)
					{
						currentButtonWait = buttonWaitAmmount;
						if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
						{
							BuyableOrderAmmount[keys[i]]+= 10;
						}
						else
						{
							BuyableOrderAmmount[keys[i]]++;
						}
						if(BuyableOrderAmmount[keys[i]] > BuyableList[keys[i]])
						{
							BuyableOrderAmmount[keys[i]] = BuyableList[keys[i]];
						}
					}
				}

                int pieceCost = (int)(FurnishingLibrary.Library.getHoldablePrefab(keys[i]).cost * BuyableOrderAmmount[keys[i]] * merchantCostMultiplier);

                Rect currencySymbolRect = new Rect(posButton.x + posButton.width + 2, ammountRect.y, ammountRect.height, ammountRect.height);
                Rect costRect = new Rect(currencySymbolRect.x + currencySymbolRect.width, ammountRect.y, ammountRect.width*2, ammountRect.height);
                GUI.DrawTexture(currencySymbolRect, CurrencySymbol);
                GUI.color = Color.black;
                labelStyle.alignment = TextAnchor.MiddleLeft;
                GUI.Label(costRect, ""+pieceCost);

                totalCost += pieceCost;
			}

			Rect buyWindow = new Rect(Screen.width*2/3, 0, Screen.width/3, Screen.height);
			Rect buyButton = new Rect(buyWindow.x+buyWindow.width/4, buyWindow.height*18.5f/20, buyWindow.width/2, buyWindow.height/20);
            Rect totalWordRect = new Rect(buyWindow.x, buyButton.y - buyButton.height, buyButton.width/2, buyButton.height);
            Rect currencSymbolRect = new Rect(totalWordRect.x+totalWordRect.width, totalWordRect.y, totalWordRect.height, totalWordRect.height);
            Rect totalCostRect = new Rect(currencSymbolRect.x+currencSymbolRect.width, currencSymbolRect.y, buyWindow.width - (totalWordRect.width + currencSymbolRect.width), buyButton.height);
            
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(buyWindow.x-1, buyWindow.y-1, buyWindow.width+2, buyWindow.height+2), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(buyWindow, Texture2D.whiteTexture);
			GUI.DrawTexture(buyButton, GUIBuyButton);
            GUI.DrawTexture(currencSymbolRect, CurrencySymbol);
            GUI.color = Color.black;
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(totalWordRect, "Total:");
            if(totalCost > currency)
            {
                GUI.color = Color.red;
            }
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(totalCostRect,""+totalCost);


            if (buyButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && currentButtonWait <= 0 && currency >= totalCost)
			{
				currentButtonWait = buttonWaitAmmount;
				BuyBuyables(totalCost);
				currentScreenState = screenState.Normal;
			}
		}




		else if(currentScreenState == screenState.FurnishingMenu)
		{
			Time.timeScale = 0;
			employeeFurnish.DrawMenu(UIMousPos, PickUpScroll);
			mouseOverGUI = true;
		}
		else if(currentScreenState == screenState.AssignWorkerMenu)
		{
            mouseOverGUI = true;

			Humanoid toLookAt = employeeFurnish.assignedEmployee;
			Time.timeScale = 0;
			Rect background = new Rect(0,0, Screen.width, Screen.height);
			GUI.DrawTexture(background, Texture2D.whiteTexture);

			var labelStyle = GUI.skin.GetStyle ("Label");
			labelStyle.fontSize = Screen.height/40;
			labelStyle.alignment = TextAnchor.MiddleCenter;
			GUIContent textSizer = new GUIContent("Xy");
			Vector2 textSize = labelStyle.CalcSize(textSizer);
			Rect sideRect = new Rect(Screen.width/2, 0, Screen.width/2, Screen.height);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(sideRect.x-1, sideRect.y-1, sideRect.width+2, sideRect.height+2), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(sideRect, Texture2D.whiteTexture);

            Rect dayNightIcon = new Rect(sideRect.x, sideRect.y, sideRect.height/20, sideRect.height/20);

            if(lookingAtNightShift)
            {
                GUI.DrawTexture(dayNightIcon, nightSymbol);
            }
            else
            {
                GUI.DrawTexture(dayNightIcon, daySymbol);
            }

            if (Input.GetMouseButtonDown(0) && currentButtonWait <= 0 && dayNightIcon.Contains(UIMousPos))
            {
                lookingAtNightShift = !lookingAtNightShift;
                currentButtonWait = buttonWaitAmmount;
            }

            int y = 0;
            for (int i = 0; i < EmployeesOnScene.Count; i++)
            {
                if ((EmployeesOnScene[i].hasNightShift && lookingAtNightShift) ||(!EmployeesOnScene[i].hasNightShift && !lookingAtNightShift))
                {
                    Rect textRect = new Rect(new Vector2(0, PickUpScroll) + new Vector2(0, Screen.height / 20) * y, new Vector2(Screen.width / 2 - Screen.height / 20, Screen.height / 20));
                    GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(textRect.x - 1, textRect.y - 1, textRect.width + 2, textRect.height + 2), Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                    labelStyle.alignment = TextAnchor.MiddleLeft;
                    GUI.color = Color.black;
                    GUI.Label(textRect, EmployeesOnScene[i].Name);
                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(textRect, "Jobs: " + EmployeesOnScene[i].assignedFurnishings.Count);
                    GUI.color = Color.white;

                    if ((lookingAtNightShift && employeeFurnish.nightEmployee == EmployeesOnScene[i]) 
                        || 
                        (!lookingAtNightShift && employeeFurnish.dayEmployee == EmployeesOnScene[i]))
                    {
                        Rect arrowRect = new Rect(textRect.width, textRect.y, textRect.height, textRect.height);
                        GUI.DrawTexture(arrowRect, employeeArrow);
                    }

                    if (textRect.Contains(UIMousPos))
                    {
                        toLookAt = EmployeesOnScene[i];

                        if (Input.GetMouseButtonDown(0) && employeeFurnish.assignedEmployee != EmployeesOnScene[i] && currentButtonWait <= 0)
                        {
                            if (lookingAtNightShift)
                            {
                                currentTime = TimeType.Night;
                            }
                            else
                            {
                                currentTime = TimeType.Day;
                            }

                            employeeFurnish.assignedEmployee = EmployeesOnScene[i];
                            EmployeesOnScene[i].assignedFurnishings.Add(employeeFurnish);
                            currentButtonWait = buttonWaitAmmount;
                        }
                        else if (Input.GetMouseButtonDown(0) && employeeFurnish.assignedEmployee == EmployeesOnScene[i] && currentButtonWait <= 0)
                        {
                            if (lookingAtNightShift)
                            {
                                currentTime = TimeType.Night;
                            }
                            else
                            {
                                currentTime = TimeType.Day;
                            }

                            employeeFurnish.assignedEmployee = null;
                            EmployeesOnScene[i].removeWorkStation(employeeFurnish);
                            currentButtonWait = buttonWaitAmmount;
                        }
                    }
                    y++;
                }
			}
            for (int i = 0; i < EmployeesInReserve.Count; i++)
            {
                if ((EmployeesInReserve[i].hasNightShift && lookingAtNightShift) || (!EmployeesInReserve[i].hasNightShift && !lookingAtNightShift))
                {
                    Rect textRect = new Rect(new Vector2(0, PickUpScroll) + new Vector2(0, Screen.height / 20) * y, new Vector2(Screen.width / 2 - Screen.height / 20, Screen.height / 20));
                    GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                    GUI.color = Color.black;
                    GUI.DrawTexture(new Rect(textRect.x - 1, textRect.y - 1, textRect.width + 2, textRect.height + 2), Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(textRect, Texture2D.whiteTexture);
                    labelStyle.alignment = TextAnchor.MiddleLeft;
                    GUI.color = Color.black;
                    GUI.Label(textRect, EmployeesInReserve[i].Name);
                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(textRect, "Jobs: " + EmployeesInReserve[i].assignedFurnishings.Count);
                    GUI.color = Color.white;

                    if ((lookingAtNightShift && employeeFurnish.nightEmployee == EmployeesInReserve[i]) 
                        || 
                        (!lookingAtNightShift && employeeFurnish.dayEmployee == EmployeesInReserve[i]))
                    {
                        Rect arrowRect = new Rect(textRect.width, textRect.y, textRect.height, textRect.height);
                        GUI.DrawTexture(arrowRect, employeeArrow);
                    }

                    if (textRect.Contains(UIMousPos))
                    {
                        toLookAt = EmployeesInReserve[i];

                        if (Input.GetMouseButtonDown(0) && employeeFurnish.assignedEmployee != EmployeesInReserve[i] && currentButtonWait <= 0)
                        {
                            if(lookingAtNightShift)
                            {
                                currentTime = TimeType.Night;
                            }
                            else
                            {
                                currentTime = TimeType.Day;
                            }

                            employeeFurnish.assignedEmployee = EmployeesInReserve[i];
                            EmployeesInReserve[i].assignedFurnishings.Add(employeeFurnish);
                            currentButtonWait = buttonWaitAmmount;
                        }
                        else if (Input.GetMouseButtonDown(0) && employeeFurnish.assignedEmployee == EmployeesInReserve[i] && currentButtonWait <= 0)
                        {
                            if (lookingAtNightShift)
                            {
                                currentTime = TimeType.Night;
                            }
                            else
                            {
                                currentTime = TimeType.Day;
                            }

                            employeeFurnish.assignedEmployee = null;
                            EmployeesInReserve[i].removeWorkStation(employeeFurnish);
                            currentButtonWait = buttonWaitAmmount;
                        }
                    }
                    y++;
                }
            }

            if (toLookAt != null)
			{
				Rect imageBox = new Rect(sideRect.x+sideRect.width/2-sideRect.height/8, sideRect.y, sideRect.height/4, sideRect.height/4);
				Rect nameBox = new Rect(sideRect.x, imageBox.y+imageBox.height, sideRect.width, Screen.height/10);
				Rect assignedTitleBox = new Rect(nameBox.x, nameBox.y+nameBox.height, nameBox.width, nameBox.height);
				Rect line = new Rect(assignedTitleBox.x, assignedTitleBox.y+assignedTitleBox.height, assignedTitleBox.width, Screen.height/100);

				labelStyle.alignment = TextAnchor.MiddleCenter;
				GUI.DrawTexture(imageBox, toLookAt.GetComponent<SpriteRenderer>().sprite.texture);
				GUI.color = Color.black;
				GUI.Label(nameBox, toLookAt.Name);

				labelStyle.fontSize = Screen.height/50;
				labelStyle.alignment = TextAnchor.LowerCenter;
				GUI.Label(assignedTitleBox, "Assigned");
				GUI.DrawTexture(line, Texture2D.whiteTexture);
				int x = 0;
				y = 0;
				int xAmmount = (int)(sideRect.width/(Screen.height/10));
				int whoCares;
				Dictionary <FurnishingLibrary.FurnishingID, int> ammountDic = new Dictionary<FurnishingLibrary.FurnishingID, int>(); 
				for(int i = 0; i < toLookAt.assignedFurnishings.Count; i++)
				{
					if(ammountDic.TryGetValue(toLookAt.assignedFurnishings[i].FurnishingID, out whoCares))
					{
						ammountDic[toLookAt.assignedFurnishings[i].FurnishingID]++;
					}
					else
					{
						ammountDic.Add(toLookAt.assignedFurnishings[i].FurnishingID, 1);
					}

				}

				List<FurnishingLibrary.FurnishingID> keys = new List<FurnishingLibrary.FurnishingID> (ammountDic.Keys);
				keys.Sort();

				for(int i = 0; i < keys.Count; i++)
				{
					Rect imageRect = new Rect(sideRect.x+Screen.height/10*x, line.y+line.height+1+Screen.height/20*y, Screen.height/20, Screen.height/20);
					Rect multiRect = new Rect(imageRect.x+imageRect.width, imageRect.y, imageRect.width, imageRect.height);

					GUI.color = Color.white;
					GUI.DrawTexture(imageRect, FurnishingLibrary.Library.getFurnishingPrefab(keys[i]).GetComponent<SpriteRenderer>().sprite.texture);
					GUI.color = Color.black;
					GUI.Label(multiRect, "x"+ammountDic[keys[i]]);

					if(x < xAmmount-1)
					{
						x++;
					}
					else
					{
						x = 0;
						y++;
					}
				}

                List<EmployeeSkill.WorkSkill> skillKeys = new List<EmployeeSkill.WorkSkill>(toLookAt.workSkills.Keys);
                skillKeys.Sort();
                for(int i = 0; i < skillKeys.Count; i++)
                {
                    GUI.color = Color.black;
                    Rect nameRect = new Rect(sideRect.x, line.y + line.height + 2 + Screen.height / 20 * (i+1), sideRect.width / 2 - 2, sideRect.height / 20);

                    labelStyle.alignment = TextAnchor.MiddleRight;
                    GUI.Label(nameRect, skillKeys[i].ToString());

                    
                    Rect xpBarRect = new Rect(nameRect.x + nameRect.width, nameRect.y, nameRect.height * 5, nameRect.height);
                    Rect currentXP = new Rect(xpBarRect.x + 1, xpBarRect.y + 1,
                        xpBarRect.width * toLookAt.workSkills[skillKeys[i]].currentXP / toLookAt.workSkills[skillKeys[i]].maxXP - 2, xpBarRect.height - 2);
                    GUI.DrawTexture(xpBarRect, Texture2D.whiteTexture);
                    GUI.color = Color.white;
                    GUI.DrawTexture(new Rect(xpBarRect.x + 1, xpBarRect.y + 1, xpBarRect.width - 2, xpBarRect.height - 2), Texture2D.whiteTexture);
                    if (toLookAt.workSkills[skillKeys[i]].currentXP > 0)
                    {
                        GUI.color = new Color(0.5f, 0.25f, 0.5f);
                        GUI.DrawTexture(currentXP, Texture2D.whiteTexture);
                    }

                    GUI.color = Color.white;
                    for (int b = 0; b < 5; b++)
                    {
                        Rect levelRect = new Rect(nameRect.x + nameRect.width + nameRect.height*b, nameRect.y, nameRect.height, nameRect.height);
                        if (b < toLookAt.workSkills[skillKeys[i]].level)
                        {
                            GUI.DrawTexture(levelRect, GUIFilledBlip);
                        }
                        else
                        {
                            GUI.DrawTexture(levelRect, GUIEmptyBlip);
                        }
                    }
                }
			}
		}
		else if(currentScreenState == screenState.PickUpMenu)
		{
			Rect backGroundRect = new Rect(PickUpButtonDrawRect.x, LHDrawRect.height, Screen.height/4, Screen.height-PickUpButtonDrawRect.height*2);
			GUI.DrawTexture(backGroundRect, Texture2D.whiteTexture);

			if(backGroundRect.Contains(UIMousPos))
			{
				mouseOverGUI = true;
			}

			var labelStyle = GUI.skin.GetStyle ("Label");
			labelStyle.fontSize = Screen.height/40;
			labelStyle.alignment = TextAnchor.MiddleLeft;

			for(int i = 0; i < Innkeeper.currentGrid.holdablesHere.Count; i++)
			{
				Rect textRect = new Rect(backGroundRect.x, backGroundRect.y+i*backGroundRect.height/20+PickUpScroll, backGroundRect.width, backGroundRect.height/20);
				Rect pictureRect = new Rect(backGroundRect.x+backGroundRect.width-backGroundRect.height/20, textRect.y, backGroundRect.height/20, backGroundRect.height/20);
				GUI.color = Color.black;
				if(Innkeeper.currentGrid.holdablesHere[i].liquidHere == null)
				{
					GUI.Label(textRect, FurnishingLibrary.Library.getItemName(Innkeeper.currentGrid.holdablesHere[i].holdableID, false));
				}
				else
				{
					GUI.Label(textRect, FurnishingLibrary.Library.getItemName(Innkeeper.currentGrid.holdablesHere[i].holdableID, false) 
                        + " of "+Innkeeper.currentGrid.holdablesHere[i].liquidHere.liquidName);
				}
				GUI.color = Color.white;
				GUI.DrawTexture(pictureRect, Innkeeper.currentGrid.holdablesHere[i].GetComponent<SpriteRenderer>().sprite.texture);

                if (Input.GetMouseButtonDown(0)
                    && (Innkeeper.heldObjects[0] == null || Innkeeper.heldObjects[1] == null)
                    && backGroundRect.Contains(UIMousPos)
                    && currentButtonWait <= 0 && textRect.Contains(UIMousPos))
				{
					currentButtonWait = buttonWaitAmmount;
					if(Innkeeper.heldObjects[0] == null)
					{
						Innkeeper.PickUpItem(true, i);
					}
					else if(Innkeeper.heldObjects[1] == null)
					{
						Innkeeper.PickUpItem(false, i);
					}
				}
				//RETURN HERE
			}
		}
		else if(currentScreenState == screenState.Normal)
		{
			if(!Innkeeper.working && Innkeeper.currentGrid.pathNode.reachableFurnishing != null)
			{
				for(int i = 0; i < Innkeeper.currentGrid.pathNode.reachableFurnishing.Count; i++)
				{
					Innkeeper.currentGrid.pathNode.reachableFurnishing[i].DisplayOptions(UIMousPos);
				}
			}
		}
		else if(currentScreenState == screenState.ChoosingFurnishingSpace)
		{
			if(mousGridPos.x >= 0 && mousGridPos.x < GRID_X
			   && mousGridPos.y >= 0 && mousGridPos.y < GRID_Y
			   && !mouseOverGUI)
			{
				if(FurnishPrefabToBuild.GetComponent<Furnishing>().wallMounted)
				{
					furnishingPlacement = PotentialWallFurnishing(Camera.main.ScreenToWorldPoint(Input.mousePosition) ,mousGridPos);
				}
				else
				{
					furnishingPlacement = PotentialFurnishingPlacement(mousGridPos);
				}
			}
        }
        else if (currentScreenState == screenState.ChoosingMoveFurnishingSpot)
        {
            if (mousGridPos.x >= 0 && mousGridPos.x < GRID_X
               && mousGridPos.y >= 0 && mousGridPos.y < GRID_Y
               && !mouseOverGUI)
            {
                if (FurnishToMove.GetComponent<Furnishing>().wallMounted)
                {
                    furnishingPlacement = PotentialWallFurnishing(Camera.main.ScreenToWorldPoint(Input.mousePosition), mousGridPos);
                }
                else
                {
                    furnishingPlacement = PotentialFurnishingPlacement(mousGridPos);
                }
            }
        }
        else if (currentScreenState == screenState.MoveFurnishingMenu)
        {
            RaycastHit2D castHit;

            Physics2D.queriesStartInColliders = true;
            Physics2D.queriesHitTriggers = false;
            castHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector2(0, 0), 0);

            if (castHit && castHit.collider.GetComponent<Furnishing>())
            {
                FurnishToMove = castHit.collider.gameObject;
                //toDelete.GetComponent<SpriteRenderer>().color = new Color(0.25f,0,0);
            }
            else
            {
                FurnishToMove = null;
            }

            Physics2D.queriesHitTriggers = true;
            Physics2D.queriesStartInColliders = false;
        }
        else if(currentScreenState == screenState.DeletingFurnishing)
		{
			RaycastHit2D castHit;
			
			Physics2D.queriesStartInColliders = true;
			Physics2D.queriesHitTriggers = false;
			castHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), new Vector2(0,0), 0);

			if(castHit && castHit.collider.GetComponent<Furnishing>())
			{
				doorDelete = null;
				toDelete = castHit.collider.GetComponent<Furnishing>();
				//toDelete.GetComponent<SpriteRenderer>().color = new Color(0.25f,0,0);
			}
			else if(castHit && castHit.collider.GetComponentInChildren<Door>())
			{
				doorDelete = castHit.collider.GetComponentInChildren<Door>();
				//doorDelete.GetComponent<SpriteRenderer>().color = new Color(0.25f,0,0);
				toDelete = null;
			}
			else
			{
				doorDelete = null;
				toDelete = null;
			}
			
			Physics2D.queriesHitTriggers = true;
			Physics2D.queriesStartInColliders = false;
		}
		else if(currentScreenState == screenState.BuildingRoom)
		{
			for(int i = 0; i < gridsToPlaceRoomOn.Count; i++)
			{
				Vector2 midPoint = Camera.main.WorldToScreenPoint(gridsToPlaceRoomOn[i]);
				midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);

				Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize)/2, new Vector2(halfSize,halfSize));
				GUI.color = new Color(0.909f,0.647f,0.274f,0.5f);
				GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
				GUI.color = Color.white;
			}
		}
		else if(currentScreenState == screenState.ChoosingRoomOriginPoint)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(mousGridPos);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);

			Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize)/2, new Vector2(halfSize,halfSize));
			
			if(mousGridPos.x >= 0 && mousGridPos.x < GRID_X
			   && mousGridPos.y >= 0 && mousGridPos.y < GRID_Y)
			{
				if(mainGrid[(int)mousGridPos.x,(int)mousGridPos.y].buildable
				   &&
				   (currentRoomDrawState == roomDrawState.Box || currentRoomDrawState == roomDrawState.Pencil))
				{
					GUI.color = new Color(0.909f,0.647f,0.274f,0.5f);
				}
				else if(mainGrid[(int)mousGridPos.x,(int)mousGridPos.y].roomPieceHere != null
				        &&
				        (currentRoomDrawState == roomDrawState.EraseBox || currentRoomDrawState == roomDrawState.ErasePencil))
				{
					GUI.color = new Color(0.909f,0.647f,0.274f,0.5f);
				}
				else
				{
					GUI.color = new Color(0.75f,0,0,0.5f);
				}
			}
			else
			{
				GUI.color = new Color(0.75f,0,0,0.5f);
			}

			Rect drawToolBackground = new Rect(RoomButtonDrawRect.x, RoomButtonDrawRect.y-Screen.height/2, RoomButtonDrawRect.width, Screen.height/2);
			
			if(drawToolBackground.Contains(UIMousPos))
			{
				mouseOverGUI = true;
			}

			if(mouseOverGUI == false)
			{
				GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			}
			GUI.color = Color.white;


			//DRAW BUTTONS
			GUI.DrawTexture(drawToolBackground, Texture2D.whiteTexture);

			Rect boxButtonRect = new Rect(drawToolBackground.x, drawToolBackground.y, RoomButtonDrawRect.width, RoomButtonDrawRect.width);
			if(currentRoomDrawState == roomDrawState.Box)
			{
				GUI.color = Color.yellow;
				GUI.DrawTexture(boxButtonRect, Texture2D.whiteTexture);
				GUI.color = Color.white;
			}
			GUI.DrawTexture(boxButtonRect, GUIBoxButton);

			Rect pencilButtonRect = new Rect(boxButtonRect.x, boxButtonRect.y+boxButtonRect.height, boxButtonRect.height, boxButtonRect.height);
			if(currentRoomDrawState == roomDrawState.Pencil)
			{
				GUI.color = Color.yellow;
				GUI.DrawTexture(pencilButtonRect, Texture2D.whiteTexture);
				GUI.color = Color.white;
			}
			GUI.DrawTexture(pencilButtonRect, GUIPencilButton);
			GUI.color = Color.white;

			Rect erasePencilButtonRect = new Rect(boxButtonRect.x, pencilButtonRect.y+pencilButtonRect.height, boxButtonRect.height, boxButtonRect.height);
			if(currentRoomDrawState == roomDrawState.ErasePencil)
			{
				GUI.color = Color.yellow;
				GUI.DrawTexture(erasePencilButtonRect, Texture2D.whiteTexture);
				GUI.color = Color.white;
			}
			GUI.DrawTexture(erasePencilButtonRect, GUIErasePencilButton);
			GUI.color = Color.white;

			Rect eraseBoxButtonRect = new Rect(boxButtonRect.x, erasePencilButtonRect.y+erasePencilButtonRect.height, boxButtonRect.height, boxButtonRect.height);
			if(currentRoomDrawState == roomDrawState.EraseBox)
			{
				GUI.color = Color.yellow;
				GUI.DrawTexture(eraseBoxButtonRect, Texture2D.whiteTexture);
				GUI.color = Color.white;
			}
			GUI.DrawTexture(eraseBoxButtonRect, GUIEraseBoxButton);
			GUI.color = Color.white;
			//END DRAW BUTTONS

			if(Input.GetMouseButtonUp(0)
			   && boxButtonRect.Contains(UIMousPos))
			{
				currentRoomDrawState = roomDrawState.Box;
			}
			else if(Input.GetMouseButtonUp(0)
			        && pencilButtonRect.Contains(UIMousPos))
			{
				currentRoomDrawState = roomDrawState.Pencil;
			}
			else if(Input.GetMouseButtonUp(0)
			        && erasePencilButtonRect.Contains(UIMousPos))
			{
				currentRoomDrawState = roomDrawState.ErasePencil;
			}
			else if(Input.GetMouseButtonUp(0)
			        && eraseBoxButtonRect.Contains(UIMousPos))
			{
				currentRoomDrawState = roomDrawState.EraseBox;
			}

		}
		else if (currentScreenState == screenState.BuildingDoor)
		{
			if(mousGridPos.x >= 0 && mousGridPos.x < GRID_X
			   && mousGridPos.y >= 0 && mousGridPos.y < GRID_Y
			   && !mouseOverGUI)
			{
				doorPlacement = PotentialDoorPlacement(Camera.main.ScreenToWorldPoint(Input.mousePosition), mousGridPos);
			}
		}
        else if (currentScreenState == screenState.ChooseMerchantDropPoint)
        {
            Vector2 midPoint = Camera.main.WorldToScreenPoint(mousGridPos);
            midPoint = new Vector2(midPoint.x, Screen.height - midPoint.y);

            Vector2 dropSymbolPoint = Camera.main.WorldToScreenPoint(merchantDropoffPoint);
            dropSymbolPoint = new Vector2(dropSymbolPoint.x, Screen.height - dropSymbolPoint.y);

            Rect drawRect = new Rect(midPoint - new Vector2(halfSize, halfSize) / 2, new Vector2(halfSize, halfSize));
            Rect currentPosRect = new Rect(dropSymbolPoint - new Vector2(halfSize, halfSize) / 2, new Vector2(halfSize, halfSize));

            GUI.color = new Color(0.909f, 0.647f, 0.274f, 0.5f);

            if (mouseOverGUI == false)
            {
                GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
            }
            GUI.color = Color.white;
            GUI.DrawTexture(currentPosRect, GUIHeaderMerchantPlacementButton);
        }



        if (currentScreenState == screenState.BuildWorkScreenOpen
		   || currentScreenState == screenState.BuildingDoor
		   || currentScreenState == screenState.ChoosingFurnishingSpace
		   || currentScreenState == screenState.DeletingFurnishing)
		{
			Rect workFurnishBackground = new Rect(WorkButtonDrawRect.x+WorkButtonDrawRect.width-Screen.height/3, WorkButtonDrawRect.y-Screen.height/2, Screen.height/3, Screen.height/2);
			
			if(workFurnishBackground.Contains(UIMousPos))
			{
				mouseOverGUI = true;
			}
			GUI.color = Color.white;
		    
			GUI.DrawTexture(workFurnishBackground, Texture2D.whiteTexture);





            Rect doorButtonRect = new Rect(workFurnishBackground.x, workFurnishBackground.y, workFurnishBackground.width / 4, workFurnishBackground.width / 4);
            doorButton.Draw(doorButtonRect);


            int x = 1;
            int y = 0;
            for(int i = 0; i < WorkMenuButton.Count; i++)
            {
                Rect whereDisplay = new Rect(workFurnishBackground.x+workFurnishBackground.width/4*x, workFurnishBackground.y+workFurnishBackground.width/4*y,
                    workFurnishBackground.width / 4, workFurnishBackground.width / 4);
                WorkMenuButton[i].Draw(whereDisplay);

                if(x < 3)
                {
                    x++;
                }
                else
                {
                    x = 0;
                    y++;
                }
            }

            Rect deleteButtonRect = new Rect(workFurnishBackground.x+workFurnishBackground.width*3/4, 
                workFurnishBackground.y+workFurnishBackground.height-workFurnishBackground.width/4, 
                workFurnishBackground.width / 4, workFurnishBackground.width / 4);
            deleteButton.Draw(deleteButtonRect);

            Rect moveButtonRect = new Rect(deleteButtonRect.x - deleteButtonRect.width,
                deleteButtonRect.y,
                deleteButtonRect.width, deleteButtonRect.width);
            moveFurnishingButton.Draw(moveButtonRect);

            doorButton.DrawTooltip(UIMousPos);
            for (int i = 0; i < WorkMenuButton.Count; i++)
            {
                WorkMenuButton[i].DrawTooltip(UIMousPos);
            }
            deleteButton.DrawTooltip(UIMousPos);
            moveFurnishingButton.DrawTooltip(UIMousPos);

        }//END FURNISHING CHOOSE SCREEN


		//FOOTER UI DRAW
		if(normalishScreen)
		{
			GUI.DrawTextureWithTexCoords(LDrawRect, GUIFooter, LFindRect);
			
			GUI.DrawTextureWithTexCoords(RDrawRect, GUIFooter, RFindRect);
			
			GUI.DrawTextureWithTexCoords(MDrawRect, GUIFooter, MFindRect);
			
            roomButton.Draw(RoomButtonDrawRect);
            workFurnishingButton.Draw(WorkButtonDrawRect);
            decorationFurnishingButton.Draw(DecortationButtonDrawRect);

            pickUpItemButton.Draw(PickUpButtonDrawRect);
            talkButton.Draw(TalkButtonDrawRect);
            attackButton.Draw(AttackButtonDrawRect);

            //If current menu is normal or pickupMenu, buttons can be shown.
            if (currentScreenState == screenState.Normal || currentScreenState == screenState.PickUpMenu)
			{
				GUI.DrawTexture(LeftHandDropRect, DropButton);
				GUI.DrawTexture(LeftHandUseRect, UseButton);
				if(Innkeeper.heldObjects[0] != null)
				{
					GUI.DrawTexture(leftHeldImage, Innkeeper.heldObjects[0].GetComponent<SpriteRenderer>().sprite.texture);
				}
				
				GUI.DrawTexture(RightHandDropRect, DropButton);
				GUI.DrawTexture(RightHandUseRect, UseButton);
				if(Innkeeper.heldObjects[1] != null)
				{
					GUI.DrawTexture(rightHeldImage, Innkeeper.heldObjects[1].GetComponent<SpriteRenderer>().sprite.texture);
				}
			}
		
        //FOOTER UI DRAW END

        //HEADER UI DRAW

            GUI.DrawTextureWithTexCoords(LHDrawRect, GUIHeader, LHFindRect);

            GUI.DrawTextureWithTexCoords(RHDrawRect, GUIHeader, RHFindRect);

            GUI.DrawTextureWithTexCoords(MHDrawRect, GUIHeader, MHFindRect);

            illuminatiButton.Draw(IlluminatiButtonDrawRect);
            GUI.DrawTexture(CurrencyBarDrawRect, GUIHeaderCurrencySpace);
            GUI.DrawTexture(CurrencySymbolDrawRect, CurrencySymbol);

            GUI.DrawTexture(FameBarDrawRect, GUIHeaderCurrencySpace);
            GUI.DrawTexture(FameSymbolDrawRect, FameSymbol);




            Matrix4x4 baseGUIMatrix = GUI.matrix;

            GUIUtility.RotateAroundPivot(timeOfDayMinutes / 4f - 90, new Vector2(clockRotery.x + clockRotery.width / 2f, clockRotery.y + clockRotery.width / 2f));

            GUI.DrawTexture(clockRotery, GUIHeaderClockRotary);

            GUI.matrix = baseGUIMatrix;

            GUI.DrawTexture(clockBase, GUIHeaderClockStand);
            shiftsButton.Draw(shiftButtonRect);
            hireButton.Draw(hireButtonRect);
            merchantDropPointButton.Draw(dropPointButtonRect);
            //GUI.DrawTexture(shiftButtonRect, GUIHeaderEmployeeShiftButton);

            var labelStyle = GUI.skin.GetStyle("Label");
            labelStyle.fontSize = Screen.height / 40;
            labelStyle.alignment = TextAnchor.MiddleRight;
            if (currency < 0)
            {
                GUI.color = Color.red;
            }
            else
            {
                GUI.color = Color.black;
            }
            if (currency < currencyDisplayMax)
            {
                GUI.Label(CurrencyBarDrawRect, "" + currency);
            }
            else
            {
                GUI.Label(CurrencyBarDrawRect, "" + currencyDisplayMax);
            }
            if (fame < currencyDisplayMax)
            {
                GUI.Label(FameBarDrawRect, "" + fame);
            }
            else
            {
                GUI.Label(FameBarDrawRect, "" + currencyDisplayMax);
            }
            GUI.color = Color.white;
            for(int i = 0; i < displayUIEffects.Count; i++)
            {
                displayUIEffects[i].Draw();
            }
        
        //HEADER UI DRAW END

        //BUTTONS DRAW
            roomButton.DrawTooltip(UIMousPos);
            workFurnishingButton.DrawTooltip(UIMousPos);
            decorationFurnishingButton.DrawTooltip(UIMousPos);
            
            pickUpItemButton.DrawTooltip(UIMousPos);
            attackButton.DrawTooltip(UIMousPos);
            talkButton.DrawTooltip(UIMousPos);

            illuminatiButton.DrawTooltip(UIMousPos);
            shiftsButton.DrawTooltip(UIMousPos);
            hireButton.DrawTooltip(UIMousPos);
            merchantDropPointButton.DrawTooltip(UIMousPos);
            //BUTTONS DRAW END
        }
	}








	public void updateSortingLayer()
	{
		sortingLayer = sortingLayer.OrderByDescending(hum => hum.thisNPCType).ToList();
		sortingLayer = sortingLayer.OrderByDescending(hum => hum.transform.position.y).ToList();
	}

	public void setSortingLayer()
	{
		sortingLayer.Clear();
		sortingLayer.Add(Innkeeper);
		for(int i = 0; i < QuestMasterOnScene.Count; i++)
		{
			sortingLayer.Add(QuestMasterOnScene[i]);
		}
		for(int i = 0; i < AdventurersOnScene.Count; i++)
		{
			sortingLayer.Add(AdventurersOnScene[i]);
		}
		for(int i = 0; i < BuildersOnScene.Count; i++)
		{
			sortingLayer.Add(BuildersOnScene[i]);
		}
		for(int i = 0; i < CiviliansOnScene.Count; i++)
		{
			sortingLayer.Add(CiviliansOnScene[i]);
		}
		for(int i = 0; i < DeliveryMenOnScene.Count; i++)
		{
			sortingLayer.Add(DeliveryMenOnScene[i]);
		}
		for(int i = 0; i < EmployeesOnScene.Count; i++)
		{
			sortingLayer.Add(EmployeesOnScene[i]);
		}
        for (int i = 0; i < TownshipOnScene.Count; i++)
        {
            sortingLayer.Add(TownshipOnScene[i]);
        }

		updateSortingLayer();
	}

	public int getSortingLayerNumber(Humanoid toCheck)
	{
		for(int i = 0; i < sortingLayer.Count; i++)
		{
			if(sortingLayer[i] == toCheck)
			{
				return i;
			}
		}
		return -1;
	}













	// Update is called once per frame
	void Update () 
	{
        if(hasSpace(Humanoid.NPCType.Adventurer))
        {
            for (int i = 0; i < 4; i++)
            {
                spawnHumanoid(Humanoid.NPCType.Adventurer);
            }
        }
        

        if (timeOfDayMinutes < 300)//Morning
        {
            if (currentTime != TimeType.Day)
            {
                currentTime = TimeType.Day;
            }
            civTrafficMod = 1;
        }
        else if (timeOfDayMinutes < 360)//Midday Upper
        {
            if (currentTime != TimeType.Day)
            {
                currentTime = TimeType.Day;
            }
            civTrafficMod = 1 + 1f * (timeOfDayMinutes - 300) / 60f;
        }
        else if (timeOfDayMinutes < 420)//Midday Lower
        {
            if (currentTime != TimeType.Day)
            {
                currentTime = TimeType.Day;
            }
            civTrafficMod = 2 - 1f * (timeOfDayMinutes - 360) / 60f;
        }
        else if (timeOfDayMinutes < 720)//Afternoon
        {
            if (currentTime != TimeType.Day)
            {
                currentTime = TimeType.Day;
            }
            civTrafficMod = 1;
        }
        else if (timeOfDayMinutes < 840)//Dusk
        {
            if (currentTime != TimeType.Dusk)
            {
                currentTime = TimeType.Dusk;
            }
            civTrafficMod = 1f + 0.5f * (timeOfDayMinutes - 720)/120f;
        }
        else if (timeOfDayMinutes < 1080)//Night
        {
            if (currentTime != TimeType.Night)
            {
                currentTime = TimeType.Night;
            }
            civTrafficMod = 1.5f - 1.5f * (timeOfDayMinutes - 840) / 240f; ;
        }
        else if (timeOfDayMinutes < 1320)//Late Night
        {
            if (currentTime != TimeType.Night)
            {
                currentTime = TimeType.Night;
            }
            civTrafficMod = 0f;
        }
        else//Dawn Transition
        {
            if (currentTime != TimeType.Dawn)
            {
                currentTime = TimeType.Dawn;
            }
            civTrafficMod = 0.1f + 0.9f * (timeOfDayMinutes - 1320) / 120f;
        }

        minimumFootTraffic = (int)(30f * civTrafficMod);

        float deltaTime = Time.deltaTime;
        timeOfDayMinutes += deltaTime/secondsToMinute;
        if(timeOfDayMinutes >= 1440)
        {
            timeOfDayMinutes -= 1440;
            EndOfDayUpdates();
        }
        for(int i = 0; i < EmployeesOnScene.Count; i++)
        {
            EmployeesOnScene[i].calculateWage(deltaTime);
        }
        for (int i = 0; i < EmployeesInReserve.Count; i++)
        {
            if(EmployeesInReserve[i].withinWorkingTime())
            {
                activateHumanoid(EmployeesInReserve[i]);
            }
        }

        for (int i = 0; i < displayWorldEffects.Count; i++)
        {
            if(displayWorldEffects[i].Update())
            {
                displayWorldEffects.RemoveAt(i);
                i--;
            }
        }
        for (int i = 0; i < displayUIEffects.Count; i++)
        {
            if(displayUIEffects[i].Update())
            {
                displayUIEffects.RemoveAt(i);
                i--;
            }
        }

        if(TownshipOnScene.Count < minimumFootTraffic && TownshipInReserve.Count > 0 && footTrafficTimer <= 0)
        {
            activateHumanoid(TownshipInReserve[0]);
            footTrafficTimer = Random.Range(0f, 1f) / civTrafficMod;
        }
        else if(footTrafficTimer > 0)
        {
            footTrafficTimer -= deltaTime * civTrafficMod;
        }

        setSortingLayer();

		movementDone = false;
		showGrid = false;
		if(currentButtonWait > 0)
		{
			currentButtonWait -= Time.unscaledDeltaTime;
		}

		if(questToPostAvailable() && QuestMasterOnScene.Count <= 0 && QuestMasterInReserve.Count > 0)
		{
            activateHumanoid(QuestMasterInReserve[0]);
			/*QuestMasterOnScene.Add(QuestMasterInReserve[0]);
			QuestMasterInReserve[0].gameObject.SetActive(true);
			QuestMasterInReserve.RemoveAt(0);*/
		}

		if((roomsToBuild.Count > 0 && currency > floorCost) || (wallsToBuild.Count > 0 && currency > wallCost))
		{
			if((roomsToBuild.Count > 0 && roomsToBuild[0] != null) || wallsToBuild.Count > 0)
			{
				if((BuildersInReserve.Count > 0 && roomsToBuild.Count > BuildersOnScene.Count)
				   ||
				   (BuildersInReserve.Count > 0 && roomsToBuild.Count <= 0 && wallsToBuild.Count > 0 && BuildersOnScene.Count < 1+wallsToBuild.Count / 4))
				{
                    activateHumanoid(BuildersInReserve[0]);
					/*BuildersOnScene.Add(BuildersInReserve[0]);
					BuildersInReserve[0].gameObject.SetActive(true);
					BuildersInReserve.RemoveAt(0);*/
				}
				/*if(roomsToBuild[0].buildComplete)
				{
					roomsToBuild.RemoveAt(0);
				}*/
			}
			/*else
			{
				roomsToBuild.RemoveAt(0);
			}*/
		}
		//END BUILDER STUFF

		for(int i = 0; i < CiviliansInReserve.Count; i++)
		{
			CiviliansInReserve[i].offScreenUpdate();
            if (CiviliansInReserve[i].hunger > CiviliansInReserve[i].hungerTrigger && CiviliansInReserve[i].thirst > CiviliansInReserve[i].thirstTrigger)
            {
                CiviliansInReserve[i].rekindleWants();
            }

			if(CiviliansInReserve[i].wantedDrink != FurnishingLibrary.HoldableID.Error && CiviliansInReserve[i].wantedFood != FurnishingLibrary.HoldableID.Error)
            {
                activateHumanoid(CiviliansInReserve[i]);
				/*CiviliansOnScene.Add(CiviliansInReserve[i]);
				CiviliansInReserve[i].gameObject.SetActive(true);
				CiviliansInReserve.RemoveAt(i);*/
				i--;
			}
		}
		for(int i = 0; i < AdventurerParties.Count; i++)
		{
			if(AdventurerParties[i].isOffScreen() && AdventurerParties[i].questEmbarkedOn == null && AdventurerParties[i].returnTimer <= 0 && AdventurerParties[i].hasWants())
			{
				AdventurerParties[i].rekindleWants();
				for(int m = 0; m < AdventurerParties[i].Members.Count; m++)
				{
                    activateHumanoid(AdventurerParties[i].Members[m]);
					/*AdventurersOnScene.Add(AdventurerParties[i].Members[m]);
					AdventurerParties[i].Members[m].gameObject.SetActive(true);
					AdventurersInReserve.Remove(AdventurerParties[i].Members[m]);*/
				}

				if(AdventurerParties[i].Members.Count < 4)
				{
					int ammountToAdd = 4 - AdventurerParties[i].Members.Count;


					for(int s = 0; s < ammountToAdd; s++)
					{
						spawnHumanoid(Humanoid.NPCType.Adventurer, new Vector2(AdventurerParties[i].Members[0].transform.position.x, Random.Range(0,5)));
					}
				}
			}
			else if(AdventurerParties[i].isOffScreen())
			{
				AdventurerParties[i].offScreenUpdate();
			}
		}


		mousGridPos = WhereMouseOnGrid();


		
		if(Input.GetKeyDown(KeyCode.D))
		{
			if(currentScreenState != screenState.BuildingDoor)
			{
				currentScreenState = screenState.BuildingDoor;
			}
			else
			{
				currentScreenState = screenState.Normal;
			}
		}
		
		if(Input.GetKeyDown(KeyCode.W))
		{
			if(currentScreenState != screenState.BuildWorkScreenOpen)
			{
				currentScreenState = screenState.BuildWorkScreenOpen;
			}
			else
			{
				currentScreenState = screenState.Normal;
			}
		}
		
		if(Input.GetKeyDown(KeyCode.R))
		{
			if(currentScreenState != screenState.ChoosingRoomOriginPoint)
			{
				currentScreenState = screenState.ChoosingRoomOriginPoint;
			}
			else
			{
				currentScreenState = screenState.Normal;
			}
		}

		if(Input.GetKeyDown(KeyCode.P))
		{
			if(currentScreenState != screenState.PickUpMenu)
			{
				currentScreenState = screenState.PickUpMenu;
			}
			else
			{
				currentScreenState = screenState.Normal;
			}
		}

		if(Input.GetKeyDown(KeyCode.M))
		{
			if(currentScreenState != screenState.MerchantBuyOrder)
			{
				currentScreenState = screenState.MerchantBuyOrder;
			}
			else
			{
				currentScreenState = screenState.Normal;
			}
		}



        //MENU CREATION STUFF
        if(currentScreenState == screenState.ChoosingCombatTarget || currentScreenState == screenState.ChoosingTalkTarget)
        {
            UpdateHUDButtons();

            if(targetHumanoid != null)
            {
                if(Innkeeper.currentGrid.peopleHere.Contains(targetHumanoid))
                {
                    if(currentScreenState == screenState.ChoosingTalkTarget)
                    {
                        currentScreenState = screenState.TalkMenu;
                    }
                    else
                    {

                    }
                }
                else
                {
                    List<PathNode> whereGo = new List<PathNode>();
                    whereGo.Add(targetHumanoid.currentGrid.pathNode);
                    Innkeeper.goToPlace(whereGo);
                }
            }

            if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
            {
                float halfSize = Screen.height / (Camera.main.orthographicSize * 2);

                for (int i = sortingLayer.Count-1; i >= 0; i--)
                {
                    if (sortingLayer[i] != Innkeeper)
                    {
                        Vector2 midPoint = Camera.main.WorldToScreenPoint(sortingLayer[i].mainTransform.position);
                        midPoint = new Vector2(midPoint.x, Screen.height - midPoint.y);

                        Rect drawRect = new Rect(midPoint - new Vector2(halfSize/2f, halfSize*3/4f) / 2, new Vector2(halfSize/2f, halfSize*3/4f));

                        if (drawRect.Contains(UIMousPos))
                        {
                            targetHumanoid = sortingLayer[i];
                            break;
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.Normal;
                targetHumanoid = null;
            }
        }
        if (currentScreenState == screenState.TalkMenu)
        {
            menuDictionary[screenState.TalkMenu].Update();
            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.Normal;
                targetHumanoid = null;
            }
        }
        else if (currentScreenState == screenState.ChooseMerchantDropPoint)
        {
            UpdateHUDButtons();
            if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
            {
                if (mousGridPos.x >= 0 && mousGridPos.x < GRID_X
                   && mousGridPos.y >= 0 && mousGridPos.y < GRID_Y)
                {
                    merchantDropoffPoint = mousGridPos;
                    
                }
            }
            else if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if (currentScreenState == screenState.ConfirmHireEmployee)
        {
            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.HireEmployeeMenu;
            }
        }
        else if (currentScreenState == screenState.HireEmployeeMenu)
        {
            /*if (Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
            {
                PickUpScroll += Input.GetAxis("Mouse ScrollWheel") * (Screen.height);
            }
            if (PickUpScroll > 0 || EmployeesOnScene.Count + EmployeesInReserve.Count < 20)
            {
                PickUpScroll = 0;
            }
            else if (PickUpScroll < -(EmployeesOnScene.Count + EmployeesInReserve.Count - 20) * Screen.height * 0.05f)
            {
                PickUpScroll = -(EmployeesOnScene.Count + EmployeesInReserve.Count - 20) * Screen.height * 0.05f;
            }*/

            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if (currentScreenState == screenState.ConfirmFireEmployee)
        {
            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.EmployeeShiftScreen;
            }
        }
        else if (currentScreenState == screenState.EmployeeShiftScreen)
        {
            if (Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
            {
                PickUpScroll += Input.GetAxis("Mouse ScrollWheel") * (Screen.height);
            }
            if (PickUpScroll > 0 || EmployeesOnScene.Count+EmployeesInReserve.Count < 20)
            {
                PickUpScroll = 0;
            }
            else if (PickUpScroll < -(EmployeesOnScene.Count + EmployeesInReserve.Count - 20) * Screen.height * 0.05f)
            {
                PickUpScroll = -(EmployeesOnScene.Count + EmployeesInReserve.Count - 20) * Screen.height * 0.05f;
            }

            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if (currentScreenState == screenState.MerchantBuyOrder)
		{
			if(Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
			{
				PickUpScroll += Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
			}
			if(PickUpScroll > 0 || BuyableList.Count < 20)
			{
				PickUpScroll = 0;
			}
			else if(PickUpScroll < -(BuyableList.Count-20)*Screen.height*0.05f)
			{
				PickUpScroll = -(BuyableList.Count-20)*Screen.height* 0.05f;
			}
			
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
		}
		else if(currentScreenState == screenState.FurnishingMenu)
		{
			if(employeeFurnish.FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
			{
				if(Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
				{
					PickUpScroll += Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
				}
				if(PickUpScroll > 0 || recipeBook.KnownPrepables.Count < 10)
				{
					PickUpScroll = 0;
				}
				else if(PickUpScroll < -(recipeBook.KnownPrepables.Count-10)*Screen.height*0.05f)
				{
					PickUpScroll = -(recipeBook.KnownPrepables.Count-10)*Screen.height* 0.05f;
				}
			}

            employeeFurnish.UpdateDrawMenu(UIMousPos);

			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
		}
		else if(currentScreenState == screenState.AssignWorkerMenu)
		{
			if(Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
			{
				PickUpScroll += Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
			}
				if(PickUpScroll > 0 || EmployeesOnScene.Count < 20)
				{
					PickUpScroll = 0;
				}
				else if(PickUpScroll < -(EmployeesOnScene.Count-20)*Screen.height*0.05f)
				{
					PickUpScroll = -(EmployeesOnScene.Count-20)*Screen.height* 0.05f;
				}
			
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
		}
		else if(currentScreenState == screenState.PickUpMenu)
        {
            UpdateHUDButtons();
            if (Input.GetAxis("Mouse ScrollWheel") != 0 && mouseOverGUI)
			{
				PickUpScroll += Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
			}
				if(PickUpScroll > 0 || Innkeeper.currentGrid.holdablesHere.Count < 20)
				{
					PickUpScroll = 0;
				}
				else if(PickUpScroll < -(Innkeeper.currentGrid.holdablesHere.Count-20)*Screen.height*0.0475f)
				{
					PickUpScroll = -(Innkeeper.currentGrid.holdablesHere.Count-20)*Screen.height*0.0475f;
				}

			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
		}
		else if(currentScreenState == screenState.BuildWorkScreenOpen)
		{
            UpdateHUDButtons();
            UpdateWorkButtons();
            if (Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
        }
        else if (currentScreenState == screenState.MoveFurnishingMenu)
        {
            UpdateWorkButtons();
            UpdateHUDButtons();
            showGrid = true;
            if (Input.GetMouseButtonDown(0) && mouseOverGUI == false && FurnishToMove != null)
            {
                currentScreenState = screenState.ChoosingMoveFurnishingSpot;
            }

            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.BuildWorkScreenOpen;
            }
        }
        else if (currentScreenState == screenState.ChoosingMoveFurnishingSpot)
        {
            UpdateWorkButtons();
            UpdateHUDButtons();
            showGrid = true;
            if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
            {
                if (furnishingPlacement.Count > 0)
                {
                    moveFurnishing(furnishingPlacement);
                    currentScreenState = screenState.MoveFurnishingMenu;
                }
            }

            if (Input.GetMouseButtonDown(1))
            {
                currentScreenState = screenState.MoveFurnishingMenu;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                rotation += 90;
                if (rotation >= 360)
                {
                    rotation -= 360;
                }
            }
        }
        else if(currentScreenState == screenState.ChoosingFurnishingSpace)
        {
            UpdateWorkButtons();
            UpdateHUDButtons();
            showGrid = true;
			if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
			{
				if(furnishingPlacement.Count > 0)
				{
					placeFurnishing(furnishingPlacement);
				}
			}
			
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.BuildWorkScreenOpen;
			}

			if(Input.GetKeyDown(KeyCode.E))
			{
				rotation += 90;
				if(rotation >= 360)
				{
					rotation -= 360;
				}
			}
		}
		else if(currentScreenState == screenState.DeletingFurnishing)
        {
            UpdateWorkButtons();
            UpdateHUDButtons();
            showGrid = true;
			if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
			{
				if(toDelete != null)
				{
					deleteFurnishing(toDelete);
				}
				else if(doorDelete != null)
				{
					deleteFurnishing(doorDelete);
				}
			}
			
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.BuildWorkScreenOpen;
			}
		}
		else if(currentScreenState == screenState.BuildingDoor)
        {
            UpdateWorkButtons();
            UpdateHUDButtons();
            showGrid = true;
			if (Input.GetMouseButtonDown(0) && mouseOverGUI == false)
			{
				if(doorPlacement.x != -1)
				{
					makeDoor(doorPlacement);
				}
			}
			
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.BuildWorkScreenOpen;
			}
		}
		else if (currentScreenState == screenState.BuildingRoom)
		{
			showGrid = true;
			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.ChoosingRoomOriginPoint;
			}

			if (Input.GetMouseButtonUp(0)
			    &&
			    (currentRoomDrawState == roomDrawState.Pencil || currentRoomDrawState == roomDrawState.Box))
			{
				currentScreenState = screenState.ChoosingRoomOriginPoint;
				makeRoom(gridsToPlaceRoomOn);
				gridsToPlaceRoomOn.Clear();
			}
			else if (Input.GetMouseButtonUp(0)
			         &&
			         (currentRoomDrawState == roomDrawState.ErasePencil || currentRoomDrawState == roomDrawState.EraseBox))
			{
				currentScreenState = screenState.ChoosingRoomOriginPoint;
				deleteRoom(gridsToPlaceRoomOn);
				gridsToPlaceRoomOn.Clear();
			}
			else if(mousGridPos.x >= 2 && mousGridPos.x < GRID_X-2
			        && mousGridPos.y >= 5 && mousGridPos.y < GRID_Y-1)
			{
				if(currentRoomDrawState == roomDrawState.Pencil)
				{
					if(!gridsToPlaceRoomOn.Contains(mousGridPos)
                        && mainGrid[(int)mousGridPos.x, (int)mousGridPos.y].buildable
                       && (mainGrid[(int)mousGridPos.x, (int) mousGridPos.y].roomPieceHere == null
					    || !mainGrid[(int)mousGridPos.x, (int) mousGridPos.y].roomPieceHere.buildComplete)
					   && mainGrid[(int)mousGridPos.x, (int) mousGridPos.y].furnishingHere == null)
					{	
						gridsToPlaceRoomOn.Add(mousGridPos);
					}
				}
				else if(currentRoomDrawState == roomDrawState.Box)
				{
					gridsToPlaceRoomOn.Clear();
					Vector2 min;
					Vector2 max;

					if(mousGridPos.x < roomBuildOriginCoord.x)
					{
						min.x = mousGridPos.x;
						max.x = roomBuildOriginCoord.x;
					}
					else
					{
						max.x = mousGridPos.x;
						min.x = roomBuildOriginCoord.x;
					}

					if(mousGridPos.y < roomBuildOriginCoord.y)
					{
						min.y = mousGridPos.y;
						max.y = roomBuildOriginCoord.y;
					}
					else
					{
						max.y = mousGridPos.y;
						min.y = roomBuildOriginCoord.y;
					}

					for(int x = 0; x < GRID_X; x++)
					{
						for(int y = 0; y < GRID_Y; y++)
						{
							if(x>= min.x && x <= max.x && y >= min.y && y <= max.y
                                && mainGrid[x, y].buildable
                               && (mainGrid[x, y].roomPieceHere == null
							    || !mainGrid[x, y].roomPieceHere.buildComplete)
							   && mainGrid[x, y].furnishingHere == null)
							{
								gridsToPlaceRoomOn.Add(mainGrid[x,y].transform.position);
							}
						}
					}
				}
				else if(currentRoomDrawState == roomDrawState.ErasePencil)
				{
					if(!gridsToPlaceRoomOn.Contains(mousGridPos)
					   && mainGrid[(int)mousGridPos.x, (int) mousGridPos.y].roomPieceHere != null
					   && mainGrid[(int)mousGridPos.x, (int) mousGridPos.y].furnishingHere == null)
					{	
						gridsToPlaceRoomOn.Add(mousGridPos);
					}
				}
				else if(currentRoomDrawState == roomDrawState.EraseBox)
				{
					gridsToPlaceRoomOn.Clear();
					Vector2 min;
					Vector2 max;
					
					if(mousGridPos.x < roomBuildOriginCoord.x)
					{
						min.x = mousGridPos.x;
						max.x = roomBuildOriginCoord.x;
					}
					else
					{
						max.x = mousGridPos.x;
						min.x = roomBuildOriginCoord.x;
					}
					
					if(mousGridPos.y < roomBuildOriginCoord.y)
					{
						min.y = mousGridPos.y;
						max.y = roomBuildOriginCoord.y;
					}
					else
					{
						max.y = mousGridPos.y;
						min.y = roomBuildOriginCoord.y;
					}
					
					for(int x = 0; x < GRID_X; x++)
					{
						for(int y = 0; y < GRID_Y; y++)
						{
							if(x>= min.x && x <= max.x && y >= min.y && y <= max.y
							   && mainGrid[x, y].roomPieceHere != null
							   && mainGrid[x, y].furnishingHere == null)
							{
								gridsToPlaceRoomOn.Add(mainGrid[x,y].transform.position);
							}
						}
					}
				}
			}
		}
		else if(currentScreenState == screenState.Normal)
        {
            UpdateHUDButtons();
            if (Input.GetMouseButtonDown(1))
			{
				if(mousGridPos.x >= 0 && mousGridPos.x <= GRID_X
				   && mousGridPos.y >= 0 && mousGridPos.y <= GRID_Y)
				{
					List<Vector2> dest = new List<Vector2>();
					dest.Add(mousGridPos);
					Innkeeper.goToPlace (dest);
				}
			}
		}
		else if (currentScreenState == screenState.ChoosingRoomOriginPoint)
        {
            UpdateHUDButtons();
            showGrid = true;
			if(Input.GetMouseButtonDown(0) && mouseOverGUI == false)
			{
				if(mousGridPos.x >= 0 && mousGridPos.x < GRID_X
				   && mousGridPos.y >= 5 && mousGridPos.y < GRID_Y)
				{
					currentScreenState = screenState.BuildingRoom;
					roomBuildOriginCoord = mousGridPos;
					gridsToPlaceRoomOn.Clear();
					if(mainGrid[(int)mousGridPos.x, (int)mousGridPos.y].roomPieceHere != null)
					{
						roomToExpand = mainGrid[(int)mousGridPos.x, (int)mousGridPos.y].roomPieceHere.parentRoom;
					}
					else
					{
						roomToExpand = null;
					}
				}
			}

			if(Input.GetMouseButtonDown(1))
			{
				currentScreenState = screenState.Normal;
			}
		}
	}






	Vector2 PotentialDoorPlacement(Vector2 mousWorldPos, Vector2 gridMousPos)
	{
		Vector2 returnVector = new Vector2(-1,-1);
		Vector2 midPoint = Input.mousePosition;
		midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);

		float halfSize = Screen.height/(Camera.main.orthographicSize*2);
		
		Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));


		for(int i = 0; i < mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls.Count; i++)
		{
			if(currency >= doorCost
               && (returnVector.x == -1 
			   || Vector2.Distance(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position, mousWorldPos) < Vector2.Distance (returnVector, mousWorldPos))
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x >= 0
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x <= GRID_X-1
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.y >= 0
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.y <= GRID_Y-1
			   && !mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].GetComponentInChildren<Door>())
			{
				returnVector = mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position;
				if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.transform.position.x == mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x)
				{
					midPoint = Camera.main.WorldToScreenPoint(returnVector);
					midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
					drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
				}
				else
				{
					midPoint = Camera.main.WorldToScreenPoint(returnVector);
					midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
					drawRect = new Rect(midPoint-new Vector2(halfSize*0.3f, halfSize)/2, new Vector2(halfSize*0.3f,halfSize));
				}
			}
		}

		if(returnVector.x == -1)
		{
			GUI.color = new Color(1,0,0,0.5f);
			GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			GUI.color = Color.white;
		}
		else
		{
			GUI.color = new Color(1,1,1,0.5f);
			GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			GUI.color = Color.white;
		}

		return returnVector;
	}






	List<Vector2> PotentialWallFurnishing(Vector2 mousWorldPos, Vector2 gridMousPos)
	{
		List<Vector2> returnVector = new List<Vector2>();
		Vector2 placementVector = new Vector2(-1,-1);
		Vector2 midPoint = Input.mousePosition;
		midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
		
		float halfSize = Screen.height/(Camera.main.orthographicSize*2);

        Furnishing furnishProperties;
        if(currentScreenState == screenState.ChoosingFurnishingSpace)
        {
            furnishProperties = FurnishPrefabToBuild.GetComponent<Furnishing>();
        }
        else
        {
            furnishProperties = FurnishToMove.GetComponent<Furnishing>();
        }
            
		Texture2D toDraw = furnishProperties.furnishingTexture;
		int Width = toDraw.width/100;
		int Height = toDraw.height/100;
		int wallI = -1;

		Vector2 min = new Vector2(-1,-1);
		Vector2 max = new Vector2(-1,-1);
		
		//bool foundError = false;
		
		Rect drawRect = new Rect(midPoint-new Vector2(halfSize*Width, halfSize*Height)/2, new Vector2(halfSize*Width,halfSize*Height));
		
		for(int i = 0; i < mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls.Count; i++)
		{
			if((placementVector.x == -1 
			    || Vector2.Distance(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position, mousWorldPos) < Vector2.Distance (placementVector, mousWorldPos))
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x >= 0
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x <= GRID_X-1
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.y >= 0
			   && mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.y <= GRID_Y-1
			   && !mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].GetComponentInChildren<Door>())
			{
				placementVector = mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position;
				if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.transform.position.x == mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[i].transform.position.x)
				{
					midPoint = Camera.main.WorldToScreenPoint(placementVector);
					midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
					rotation = 0;
					wallI = i;
				}
				else
				{
					midPoint = Camera.main.WorldToScreenPoint(placementVector);
					midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
					rotation = 90;
					wallI = i;
				}
			}
		}
		//RETURN
		if(rotation == 90 || rotation == 270)
		{
			Width = toDraw.height/100;
			Height = toDraw.width/100;
		}

		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{
				
				Vector2 positionToFind = new Vector2(placementVector.x+x-Width/2, placementVector.y+y-Height/2);
				
				bool canReachX = false;
				bool canReachY = false;
				
				bool canReachXNeg = false;
				bool canReachYNeg = false;
				
				if(positionToFind.x >= 0 && positionToFind.x < GRID_X && positionToFind.y >= 0 && positionToFind.y < GRID_Y)
				{
					if(y == Height-1 && x == 0)
					{
						Vector2 pos = (Vector2)mainGrid[(int)positionToFind.x, (int)positionToFind.y].transform.position+new Vector2(0, 0.5f);

						min = Camera.main.WorldToScreenPoint(pos+ new Vector2(-0.5f,0.5f));
						min.y = Screen.height-min.y;
					}
					
					if(y == 0 && x == Width-1)
					{
						Vector2 pos = (Vector2)mainGrid[(int)positionToFind.x, (int)positionToFind.y].transform.position+new Vector2(0, 0.5f);

						max = Camera.main.WorldToScreenPoint(pos+ new Vector2(0.5f,-0.5f));
						max.y = Screen.height-max.y;
						/*else
						{
							foundError = true;
							Vector2 midError = Camera.main.WorldToScreenPoint(positionToFind);
							midError.y = Screen.height-midError.y;
							
							GUI.color = new Color(1,0,0,0.5f);
							GUI.DrawTexture(new Rect(midError-new Vector2(halfSize, halfSize)/2, new Vector2(halfSize, halfSize)), Texture2D.whiteTexture);
							GUI.color = Color.white;
							break;
						}*/
					}


					if(positionToFind.y+1 >= GRID_Y)
					{

					}
					else if( y+1 >= Height || rotation == 0)
					{
						canReachY = true;
					}
					else
					{
						for(int i = 0; i < mainGrid[(int)positionToFind.x, (int)positionToFind.y+1].pathNode.walls.Count; i++)
						{
							if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.x 
							   == 
							   	mainGrid[(int)positionToFind.x, (int)positionToFind.y+1].pathNode.walls[i].transform.position.x)
							{
								canReachY = true;
								break;
							}

						}
					}


					if(positionToFind.x+1 >= GRID_X)
					{

					}
					else if(x+1 >= Width || rotation == 90)
					{
						canReachX = true;
					}
					else
					{
						for(int i = 0; i < mainGrid[(int)positionToFind.x+1, (int)positionToFind.y].pathNode.walls.Count; i++)
						{
							if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.y 
							   == 
							   mainGrid[(int)positionToFind.x+1, (int)positionToFind.y].pathNode.walls[i].transform.position.y)
							{
								canReachX = true;
								break;
							}
							
						}
					}

					
					if(positionToFind.y-1 < 0)
					{
						//Debug.Log("Error YNeg Out of Bounds");
					}
					else if( y-1 < 0 || rotation == 0)
					{
						canReachYNeg = true;
					}
					else
					{
						for(int i = 0; i < mainGrid[(int)positionToFind.x, (int)positionToFind.y-1].pathNode.walls.Count; i++)
						{
							//Debug.Log ("Base Y: "+mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.x);
							//Debug.Log("NegY: "+mainGrid[(int)positionToFind.x, (int)positionToFind.y-1].pathNode.walls[i].transform.position.x);

							if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.x 
							   == 
							   mainGrid[(int)positionToFind.x, (int)positionToFind.y-1].pathNode.walls[i].transform.position.x)
							{
								canReachYNeg = true;
								break;
							}
							
						}
					}

					if(positionToFind.x-1 < 0)
					{

					}
					else if(x-1 < 0 || rotation == 90)
					{
						canReachXNeg = true;
					}
					else
					{
						for(int i = 0; i < mainGrid[(int)positionToFind.x-1, (int)positionToFind.y].pathNode.walls.Count; i++)
						{
							//Debug.Log ("BaseX: "+mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.y);
							//Debug.Log("NegX: "+mainGrid[(int)positionToFind.x-1, (int)positionToFind.y].pathNode.walls[i].transform.position.y);

							if(mainGrid[(int)gridMousPos.x, (int)gridMousPos.y].pathNode.walls[wallI].transform.position.y 
							   == 
							   mainGrid[(int)positionToFind.x-1, (int)positionToFind.y].pathNode.walls[i].transform.position.y)
							{
								canReachXNeg = true;
								break;
							}
							
						}
					}
				}
				
				if(currency >= furnishProperties.cost && positionToFind.x >= 0 && positionToFind.x < GRID_X && positionToFind.y >= 0 && positionToFind.y < GRID_Y
				   && mainGrid[(int)positionToFind.x, (int)positionToFind.y].buildable
				   && canReachX && canReachY && canReachXNeg && canReachYNeg)
				{
					returnVector.Add(positionToFind);
				}
				else
				{
					Vector2 midError = Camera.main.WorldToScreenPoint(positionToFind);
					midError.y = Screen.height-midError.y;
					
					GUI.color = new Color(1,0,0,0.5f);
					GUI.DrawTexture(new Rect(midError-new Vector2(halfSize, halfSize)/2, new Vector2(halfSize, halfSize)), Texture2D.whiteTexture);
					GUI.color = Color.white;
					
					//foundError = true;
				}
			}
		}

		Vector2 pivot = midPoint;
		Vector2 lengWid = max-min;
		
		if(min.x != -1 && max.x != -1)
		{
			pivot = min + (max-min)/2;
			drawRect = new Rect(min, max-min);
			
			if(rotation == 90 || rotation == 270)
			{
				lengWid = new Vector2(lengWid.y, lengWid.x);
				drawRect = new Rect(pivot-lengWid/2, lengWid);
			}
		}



		if(placementVector.x == -1)
		{
			GUI.color = new Color(1,0,0,0.5f);
			GUI.DrawTexture(drawRect, toDraw);
			GUI.color = Color.white;
		}
		else
		{
			Matrix4x4 baseGUIMatrix = GUI.matrix;
			
			if(rotation == 90 || rotation == 270)
			{
				GUIUtility.RotateAroundPivot(-rotation, midPoint);
			}
			else
			{
				GUIUtility.RotateAroundPivot(rotation, midPoint);
			}


			GUI.color = new Color(1,1,1,0.5f);
			GUI.DrawTexture(drawRect, toDraw);
			
			GUI.matrix = baseGUIMatrix;
		}
		
		return returnVector;
	}//WALL FURNISHING END







	List<Vector2> PotentialFurnishingPlacement(Vector2 gridMousPos)
	{
		List<Vector2> returnVector = new List<Vector2>();
		Vector2 midPoint = Input.mousePosition;
		midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
		
		float halfSize = Screen.height/(Camera.main.orthographicSize*2);

        Furnishing furnishProperties;
        if (currentScreenState == screenState.ChoosingFurnishingSpace)
        {
            furnishProperties = FurnishPrefabToBuild.GetComponent<Furnishing>();
        }
        else
        {
            furnishProperties = FurnishToMove.GetComponent<Furnishing>();
        }
        Texture2D toDraw = furnishProperties.furnishingTexture;
		int Width = toDraw.width/100;
		int Height = toDraw.height/100;
		
		Rect drawRect = new Rect(midPoint-new Vector2(halfSize*Width, halfSize*Height)/2, new Vector2(halfSize*Width,halfSize*Height));
		
		if(rotation == 90 || rotation == 270)
		{
			Width = toDraw.height/100;
			Height = toDraw.width/100;
		}

		Vector2 min = new Vector2(-1,-1);
		Vector2 max = new Vector2(-1,-1);

		bool foundError = false;
		
		Room roomToCheck = null;

		for(int x = 0; x < Width; x++)
		{
			for(int y = 0; y < Height; y++)
			{

				Vector2 positionToFind = new Vector2(gridMousPos.x+x-Width/2, gridMousPos.y+y-Height/2);

				bool canReachX = false;
				bool canReachY = false;
				
				bool canReachXNeg = false;
				bool canReachYNeg = false;

				if(positionToFind.x >= 0 && positionToFind.x < GRID_X && positionToFind.y >= 0 && positionToFind.y < GRID_Y)
				{
					if(y == Height-1 && x == 0)
					{
						min = Camera.main.WorldToScreenPoint((Vector2)mainGrid[(int)positionToFind.x, (int)positionToFind.y].transform.position+ new Vector2(-0.5f,0.5f));
						min.y = Screen.height-min.y;
					}

					if(y == 0 && x == Width-1)
					{
						max = Camera.main.WorldToScreenPoint((Vector2)mainGrid[(int)positionToFind.x, (int)positionToFind.y].transform.position+ new Vector2(0.5f,-0.5f));
						max.y = Screen.height-max.y;
					}

					if(x == 0 && y == 0)
					{
						if(mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere != null)
						{
							roomToCheck = mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere.parentRoom;
						}
					}

					for(int i = 0; i < mainGrid[(int)positionToFind.x, (int)positionToFind.y].pathNode.Neighbors.Count; i++)
					{

						if(positionToFind.y+1 >= GRID_Y)
						{
							break;
						}
						if( y+1 >= Height
						   || mainGrid[(int)positionToFind.x, (int)positionToFind.y].pathNode.Neighbors[i] == mainGrid[(int)positionToFind.x, (int)positionToFind.y+1].pathNode
                           || (mainGrid[(int)positionToFind.x, (int)positionToFind.y + 1].furnishingHere != null 
                                && mainGrid[(int)positionToFind.x, (int)positionToFind.y + 1].furnishingHere == furnishProperties))
						{
							canReachY = true;
						}

						if(positionToFind.x+1 >= GRID_X)
						{
							break;
						}
						if(x+1 >= Width 
						   || mainGrid[(int)positionToFind.x, (int)positionToFind.y].pathNode.Neighbors[i] == mainGrid[(int)positionToFind.x+1, (int)positionToFind.y].pathNode
                           || (mainGrid[(int)positionToFind.x+1, (int)positionToFind.y].furnishingHere != null
                                && mainGrid[(int)positionToFind.x+1, (int)positionToFind.y].furnishingHere == furnishProperties))
						{
							canReachX = true;
						}

						if(positionToFind.y-1 < 0)
						{
							break;
						}
						if( y-1 < 0
						   || mainGrid[(int)positionToFind.x, (int)positionToFind.y].pathNode.Neighbors[i] == mainGrid[(int)positionToFind.x, (int)positionToFind.y-1].pathNode
                           || (mainGrid[(int)positionToFind.x, (int)positionToFind.y - 1].furnishingHere != null
                                && mainGrid[(int)positionToFind.x, (int)positionToFind.y - 1].furnishingHere == furnishProperties))
                        {
							canReachYNeg = true;
						}

						if(positionToFind.x-1 < 0)
						{
							break;
						}
						if(x-1 < 0 
						   || mainGrid[(int)positionToFind.x, (int)positionToFind.y].pathNode.Neighbors[i] == mainGrid[(int)positionToFind.x-1, (int)positionToFind.y].pathNode
                           || (mainGrid[(int)positionToFind.x - 1, (int)positionToFind.y].furnishingHere != null
                                && mainGrid[(int)positionToFind.x - 1, (int)positionToFind.y].furnishingHere == furnishProperties))
                        {
							canReachXNeg = true;
						}
					}
				}
					
				if(currency >= furnishProperties.cost && positionToFind.x >= 0 && positionToFind.x < GRID_X && positionToFind.y >= 0 && positionToFind.y < GRID_Y
				   &&((mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere == null 
				    	&& furnishProperties.currentPlacability == Furnishing.Placability.OutsideOnly)
				   || (mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere != null 
				    && furnishProperties.currentPlacability == Furnishing.Placability.InsideOnly
				    && mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere.parentRoom == roomToCheck
				    && mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere.buildComplete)
				   || (furnishProperties.currentPlacability == Furnishing.Placability.Anywhere
				    	&&( (mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere == null 
				     			&& roomToCheck == null)
				   || (mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere != null
				    		&& mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere.buildComplete
				    		&& mainGrid[(int)positionToFind.x, (int)positionToFind.y].roomPieceHere.parentRoom == roomToCheck))))
				   && mainGrid[(int)positionToFind.x, (int)positionToFind.y].buildable
				   && (mainGrid[(int)positionToFind.x, (int)positionToFind.y].furnishingHere == null 
                        || mainGrid[(int)positionToFind.x, (int)positionToFind.y].furnishingHere == furnishProperties)
				   && canReachX && canReachY && canReachXNeg && canReachYNeg)
				{
					returnVector.Add(positionToFind);
				}
				else
				{
					Vector2 midError = Camera.main.WorldToScreenPoint(positionToFind);
					midError.y = Screen.height-midError.y;

					GUI.color = new Color(1,0,0,0.5f);
					GUI.DrawTexture(new Rect(midError-new Vector2(halfSize, halfSize)/2, new Vector2(halfSize, halfSize)), Texture2D.whiteTexture);
					GUI.color = Color.white;

					foundError = true;
				}
			}
		}

		Vector2 pivot = midPoint;
		Vector2 lengWid = max-min;

		if(min.x != -1 && max.x != -1)
		{
			pivot = min + (max-min)/2;
			drawRect = new Rect(min, max-min);

			if(rotation == 90 || rotation == 270)
			{
				lengWid = new Vector2(lengWid.y, lengWid.x);
				drawRect = new Rect(pivot-lengWid/2, lengWid);
			}
		}



		
		Matrix4x4 baseGUIMatrix = GUI.matrix;

		if(rotation == 90 || rotation == 270)
		{
			GUIUtility.RotateAroundPivot(-rotation, pivot);
		}
		else
		{
			GUIUtility.RotateAroundPivot(rotation, pivot);
		}

		GUI.color = new Color(1,1,1,0.5f);
		GUI.DrawTexture(drawRect, toDraw);
		
		GUI.matrix = baseGUIMatrix;

		
		if(foundError)
		{
			returnVector.Clear();
		}

		GUI.color = Color.white;
		return returnVector;
	}

    void moveFurnishing(List<Vector2> gridList)
    {
        Furnishing furnishingToAdd = FurnishToMove.GetComponent<Furnishing>();

        Vector2 origMin = new Vector2(-1, -1);
        Vector2 origMax = new Vector2(-1, -1);

        Vector2 min = new Vector2(-1, -1);
        Vector2 max = new Vector2(-1, -1);


        if(furnishingToAdd.mainTransform.rotation.eulerAngles.z == 0 
            || 
            (furnishingToAdd.mainTransform.rotation.eulerAngles.z >= 179
                && furnishingToAdd.mainTransform.rotation.eulerAngles.z <= 181))
        {
            origMin = new Vector2(-furnishingToAdd.furnishingTexture.width / 200, -furnishingToAdd.furnishingTexture.height / 200) + (Vector2)furnishingToAdd.transform.position;
            origMax = new Vector2(furnishingToAdd.furnishingTexture.width / 200, furnishingToAdd.furnishingTexture.height / 200) + (Vector2)furnishingToAdd.transform.position;
        }
        else
        {
            origMin = new Vector2(-furnishingToAdd.furnishingTexture.height / 200, -furnishingToAdd.furnishingTexture.width / 200) + (Vector2)furnishingToAdd.transform.position;
            origMax = new Vector2(furnishingToAdd.furnishingTexture.height / 200, furnishingToAdd.furnishingTexture.width / 200) + (Vector2)furnishingToAdd.transform.position;
        }
        

        for (int y = (int)origMin.y; y <= origMax.y; y++)
        {
            for (int x = (int)origMin.x; x <= origMax.x; x++)
            {
                if (mainGrid[x, y].furnishingHere == furnishingToAdd)
                {
                    mainGrid[x, y].furnishingHere = null;
                }
            }
        }

        for (int i = 0; i < gridList.Count; i++)
        {
            if (min.x == -1 || (gridList[i].x <= min.x && gridList[i].y <= min.y))
            {
                min = gridList[i];
            }

            if (max.x == -1 || (gridList[i].x >= max.x && gridList[i].y >= max.y))
            {
                max = gridList[i];
            }

            if (!furnishingToAdd.wallMounted)
            {
                mainGrid[(int)gridList[i].x, (int)gridList[i].y].furnishingHere = furnishingToAdd;
            }
        }

        Vector2 centerPoint = (min + (max - min) / 2);

        if (furnishingToAdd.FurnishingID == FurnishingLibrary.FurnishingID.Bed && furnishingToAdd.roomInsideOf != null)
        {
            furnishingToAdd.roomInsideOf.bedsHere.Remove((Bed)furnishingToAdd);
        }
        else if(furnishingToAdd.FurnishingID == FurnishingLibrary.FurnishingID.PrepTable && furnishingToAdd.roomInsideOf != null)
        {
            furnishingToAdd.roomInsideOf.prepTablesHere.Remove((PrepTable)furnishingToAdd);
        }

        furnishingToAdd.transform.position = centerPoint;
        furnishingToAdd.transform.rotation = Quaternion.Euler(0, 0, rotation);
        
        setGridPathing(min - new Vector2(1, 1), max + new Vector2(1, 1));
        setGridPathing(origMin - new Vector2(1, 1), origMax + new Vector2(1, 1));

        if (furnishingToAdd.FurnishingID == FurnishingLibrary.FurnishingID.Bed || furnishingToAdd.FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
        {
            furnishingToAdd.Start();
        }
    }


    void placeFurnishing(List<Vector2> gridList)
	{
        if(FurnishPrefabToBuild.GetComponent<Furnishing>().cost > currency)
        {
            return;
        }

		Furnishing furnishingToAdd = FurnishingLibrary.Library.getFurnishing(FurnishPrefabToBuild.GetComponent<Furnishing>().FurnishingID);

		Vector2 min = new Vector2(-1,-1);
		Vector2 max = new Vector2(-1,-1);

		for(int i = 0; i < gridList.Count; i++)
		{
			//REUTNR HERE
			if(min.x == -1 || (gridList[i].x <= min.x && gridList[i].y <= min.y))
			{
				min = gridList[i];
			}

			if(max.x == -1 || (gridList[i].x >= max.x && gridList[i].y >= max.y))
			{
				max = gridList[i];
			}

			if(!furnishingToAdd.wallMounted)
			{
				mainGrid[(int)gridList[i].x, (int)gridList[i].y].furnishingHere = furnishingToAdd;
			}
		}

		Vector2 centerPoint = (min + (max-min)/2);

		furnishingToAdd.transform.position = centerPoint;
		furnishingToAdd.transform.rotation = Quaternion.Euler(0,0, rotation);

		List<Furnishing> pointless;
		//COME BACK HERE
		if(FurnishingOnMap.TryGetValue(furnishingToAdd.FurnishingID, out pointless))
		{
			FurnishingOnMap[furnishingToAdd.FurnishingID].Add(furnishingToAdd);
		}
		else
		{
			List<Furnishing> toAdd = new List<Furnishing>();
			toAdd.Add(furnishingToAdd);
			FurnishingOnMap.Add(furnishingToAdd.FurnishingID, toAdd);
		}
		setGridPathing(min-new Vector2(1,1), max+new Vector2(1,1));

        changeMoney(-furnishingToAdd.cost, centerPoint);
	}

	void makeDoor(Vector2 centerPoint)
	{
		List<Wall> potentialWalls = mainGrid[(int)mousGridPos.x, (int)mousGridPos.y].pathNode.walls;
		Wall foundwall = null;
		for(int i = 0; i < potentialWalls.Count; i++)
		{
			if(potentialWalls[i].transform.position.x == centerPoint.x
			   && potentialWalls[i].transform.position.y == centerPoint.y)
			{
				foundwall = potentialWalls[i];
				break;
			}
		}

		if(mainGrid[(int)mousGridPos.x, (int)mousGridPos.y].pathNode.transform.position.x == centerPoint.x)
		{
			GameObject doorToAdd = Instantiate(door, centerPoint, Quaternion.Euler(0,0,0)) as GameObject;
			doorToAdd.transform.parent = foundwall.transform;
		}
		else
		{
			GameObject doorToAdd = Instantiate(door, centerPoint, Quaternion.Euler(0,0,90)) as GameObject;
			doorToAdd.transform.parent = foundwall.transform;
		}
		setGridPathing(centerPoint-new Vector2(1,1), centerPoint+new Vector2(1,1));
		wallOpening();
        changeMoney(-doorCost, centerPoint);
	}

	void makeRoom(List<Vector2> gridCoord)
	{
		bool expandingRoom = true;
		Room roomToAdd = roomToExpand;
		if(roomToExpand == null)
		{
			expandingRoom = false;
			roomToAdd = new Room();
		}
		List<RoomPiece> allThePieces = new List<RoomPiece>();
		Vector2 min = new Vector2(100, 100);
		Vector2 max = new Vector2(-1,-1);
		for(int i = 0; i < gridCoord.Count; i++)
		{
			if(gridCoord[i].x < min.x)
			{
				min.x = gridCoord[i].x;
			}
			if(gridCoord[i].y < min.y)
			{
				min.y = gridCoord[i].y;
			}

			if(gridCoord[i].x > max.x)
			{
				max.x = gridCoord[i].x;
			}
			if(gridCoord[i].y > max.y)
			{
				max.y = gridCoord[i].y;
			}


			if(mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere != null
			   && mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere.parentRoom != roomToAdd
			   && !mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere.buildComplete
			   && mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].furnishingHere == null)
			{
				DestroyImmediate(mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere.gameObject);
			}
			if(mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere == null
			   && mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].furnishingHere == null)
			{
				GameObject piece = Instantiate(floor, gridCoord[i], Quaternion.Euler (0,0,0)) as GameObject;
				RoomPiece roomPiece = piece.GetComponent<RoomPiece>();
				mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere = roomPiece;
				roomPiece.gridCoord = gridCoord[i];
				roomPiece.parentRoom = roomToAdd;
				allThePieces.Add(roomPiece);
				roomToAdd.AddPiece(roomPiece);
			}
		}

		for(int i = 0; i < allThePieces.Count; i++)
		{
			allThePieces[i].SetWalls(wall, new Vector2(allThePieces[i].transform.position.x, allThePieces[i].transform.position.y));
		}
		if(!expandingRoom)
		{
			float r = Random.Range (0f,1f);
			float g = Random.Range (0f,1f);
			float b = Random.Range (0f,1f);
			roomToAdd.setRoomColor(new Color(r, g, b, 1));
		}
		else
		{
			roomToAdd.setRoomColor();
		}
		setGridPathing(min-new Vector2(1,1), max+new Vector2(1,1));
	}


	void deleteRoom(List<Vector2> gridCoord)
	{
		Vector2 min = new Vector2(100, 100);
		Vector2 max = new Vector2(-1,-1);
		for(int i = 0; i < gridCoord.Count; i++)
		{
			if(gridCoord[i].x < min.x)
			{
				min.x = gridCoord[i].x;
			}
			if(gridCoord[i].y < min.y)
			{
				min.y = gridCoord[i].y;
			}
			
			if(gridCoord[i].x > max.x)
			{
				max.x = gridCoord[i].x;
			}
			if(gridCoord[i].y > max.y)
			{
				max.y = gridCoord[i].y;
			}
			
			
			if(mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere != null)
			{
				DestroyImmediate(mainGrid[(int)gridCoord[i].x, (int)gridCoord[i].y].roomPieceHere.gameObject);
			}
		}
		//Debug.Log("Min: "+min+"| Max: "+max);

		int minX = (int)min.x-1;
		int maxX = (int)max.x+1;
		int minY = (int)min.y-1;
		int maxY = (int)max.y+1;

		if(minX < 0)
		{
			minX = 0;
		}
		if(minY < 0)
		{
			minY = 0;
		}
		if(maxX >= GRID_X)
		{
			maxX = GRID_X-1;
		}
		if(maxY >= GRID_Y)
		{
			maxY = GRID_Y-1;
		}

		for(int x = minX; x <= maxX; x++)
		{
			for(int y = minY; y <= maxY; y++)
			{
				if(mainGrid[x,y].roomPieceHere != null)
				{
					mainGrid[x,y].roomPieceHere.SetWalls(wall, new Vector2(x,y));
				}
			}
		}

		setGridPathing(min-new Vector2(1,1), max+new Vector2(1,1));


	}

	void makeGrid()
	{
		if(gridFolder != null)
		{
			Destroy(gridFolder);
		}
		gridFolder = new GameObject("Grid Folder");
		pathFolder = new GameObject("Path Folder");
		roomFolder = new GameObject("Room Folder");

		for(int x = 0; x < GRID_X; x++)
		{
			for(int y = 0; y < GRID_Y; y++)
			{
				Vector3 positionToPlace = new Vector3(x, y, 0);
				if(y > 4)
				{
					GameObject gridPiece = Instantiate(grass.gameObject, positionToPlace, Quaternion.Euler(0,0,0)) as GameObject;
					mainGrid[x,y] = gridPiece.GetComponent<Grid>();
					gridPiece.transform.parent = gridFolder.transform;
					mainGrid[x,y].pathNode.gridHere = mainGrid[x,y];
					gridPiece.name = "X: "+x+"| Y: "+y;
                    if(x < 2 || x > GRID_X-3 || y == GRID_Y -1)
                    {
                        gridPiece.GetComponent<Grid>().buildable = false;
                    }
				}
				else
				{
					GameObject gridPiece = Instantiate(road.gameObject, positionToPlace, Quaternion.Euler(0,0,0)) as GameObject;
					mainGrid[x,y] = gridPiece.GetComponent<Grid>();
					gridPiece.transform.parent = gridFolder.transform;
					mainGrid[x,y].pathNode.gridHere = mainGrid[x,y];
					gridPiece.name = "X: "+x+"| Y: "+y;
				}
			}
		}
		setGridPathing(new Vector2(0,0), new Vector2(GRID_X, GRID_Y));
	}

	public void setGridPathing(Vector2 min, Vector2 max)
	{
		if(min.x < 0)
		{
			min.x = 0;
		}
		if(min.y < 0)
		{
			min.y = 0;
		}

		if(max.x >= GRID_X)
		{
			max.x = GRID_X-1;
		}
		if(max.y >= GRID_Y)
		{
			max.y = GRID_Y-1;
		}

		List<FurnishingLibrary.FurnishingID> keys = new List<FurnishingLibrary.FurnishingID>(FurnishingOnMap.Keys);
		for(int k = 0; k < keys.Count; k++)
		{
			for(int f = 0; f < FurnishingOnMap[keys[k]].Count; f++)
			{
				for(int n = 0; n < FurnishingOnMap[keys[k]][f].reachableNodes.Count; n++)
				{
					if(FurnishingOnMap[keys[k]][f].reachableNodes[n].transform.position.x >= min.x && FurnishingOnMap[keys[k]][f].reachableNodes[n].transform.position.x <= max.x
					   && FurnishingOnMap[keys[k]][f].reachableNodes[n].transform.position.y >= min.y && FurnishingOnMap[keys[k]][f].reachableNodes[n].transform.position.y <= max.y)
					{
						FurnishingOnMap[keys[k]][f].reachableNodes.RemoveAt(n);
						n--;
					}
				}
			}
		}

		for(int x = (int)min.x; x <= max.x; x++)
		{
			for(int y = (int)min.y; y <= max.y; y++)
			{
				mainGrid[x,y].pathNode.setNeighbors();
			}
		}

        //REPATHING GOES HERE

		/*setPathingChanges(BuildersOnScene, min, max);
		setPathingChanges(EmployeesOnScene, min, max);
		setPathingChanges(AdventurersOnScene, min, max);
		setPathingChanges (CiviliansOnScene, min, max);
		if(Innkeeper != null)
		{
			setPathingChanges(Innkeeper, min, max);
		}
		setPathingChanges(DeliveryMenOnScene, min, max);*/
	}

	void setPathingChanges(List<Humanoid> list, Vector2 min, Vector2 max)
	{
		for(int i = 0; i < list.Count; i++)
		{
			setPathingChanges (list[i], min, max);
		}
	}

	void setPathingChanges(Humanoid humanoid, Vector2 min, Vector2 max)
	{
		bool toReset = false;
		for(int i = 0; i < humanoid.pathToGo.Count; i++)
		{
			if(humanoid.pathToGo[i].transform.position.x >= min.x && humanoid.pathToGo[i].transform.position.x <= max.x
			   && humanoid.pathToGo[i].transform.position.y >= min.y && humanoid.pathToGo[i].transform.position.y <= max.y)
			{
				toReset = true;
				break;
			}
		}
		if(toReset)
		{
			humanoid.toResetPath = true;
		}
	}

	public void wallOpening()
	{
		openingChanges(BuildersOnScene);
		openingChanges(EmployeesOnScene);
		openingChanges(AdventurersOnScene);
		openingChanges (CiviliansOnScene);
		openingChanges(Innkeeper);
		openingChanges(DeliveryMenOnScene);
	}

	void openingChanges(List<Humanoid> list)
	{
		for(int i = 0; i < list.Count; i++)
		{
			openingChanges (list[i]);
		}
	}
	
	void openingChanges(Humanoid humanoid)
	{
		humanoid.stuck = false;
	}

	/// <summary>
	/// Returns true if furnishing type is on map. Output is the list of all furnishings of that type.
	/// </summary>
	/// <returns><c>true</c>, if furnishing was hased, <c>false</c> otherwise.</returns>
	/// <param name="ID">I.</param>
	/// <param name="output">Output.</param>
	public bool hasFurnishing(FurnishingLibrary.FurnishingID ID, out List<Furnishing> output)
	{
		if(FurnishingOnMap.TryGetValue(ID, out output) && output.Count > 0)
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// Returns true if furnishing type is on map.
	/// </summary>
	/// <returns><c>true</c>, if furnishing was hased, <c>false</c> otherwise.</returns>
	/// <param name="ID">I.</param>
	public bool hasFurnishing(FurnishingLibrary.FurnishingID ID)
	{
		List<Furnishing> output;
		if(FurnishingOnMap.TryGetValue(ID, out output) && output.Count > 0)
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// Checks to see if a certain holdable is available to be picked up.
	/// </summary>
	/// <returns><c>true</c>, if ID is available, <c>false</c> otherwise.</returns>
	/// <param name="ID">I.</param>
	/// <param name="lookingForLiquid">If set to <c>true</c> looking for liquid.</param>
	public bool holdableIsAvailable(FurnishingLibrary.HoldableID ID)
	{
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(ID, out output) && output.Count > 0)
		{
			return true;
		}
        List<Furnishing> Cellars;
        if(CellarStairs.CellarStorage.TryGetValue(ID, out output) && FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.CellarStairs, out Cellars))
        {
            return true;
        }
		
		return false;
	}
	
	/// <summary>
	/// If LIQUID item ID is available and in a barrel, returns true. foundBarrel is true if an empty barrel is found.
	/// </summary>
	/// <returns><c>true</c>, if LIQUID ID is available, <c>false</c> otherwise.</returns>
	/// <param name="ID">Item ID.</param>
	/// <param name="foundBarrel">Found barrel.</param>
	public bool holdableIsAvailable(FurnishingLibrary.HoldableID ID, out bool foundBarrel)
	{
		foundBarrel = false;
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(FurnishingLibrary.HoldableID.Barrel, out output) && output.Count > 0)
		{
			for(int i = 0; i < output.Count; i++)
			{
				if(output[i].liquidHere != null
				   && output[i].liquidHere.liquidID == ID)
				{
					return true;
				}
				if(foundBarrel == false 
				   && output[i].liquidHere == null)
				{
					foundBarrel = true;
				}
			}
		}

        if(foundBarrel)
        {
            return false;
        }

        if (CellarStairs.CellarStorage.TryGetValue(FurnishingLibrary.HoldableID.Barrel, out output) && output.Count > 0)
        {
            foundBarrel = true;
        }

        return false;
	}

	/// <summary>
	/// If LIQUID item ID is available inside a cup, returns true.
	/// </summary>
	/// <returns><c>true</c>, if LIQUID ID is available, <c>false</c> otherwise.</returns>
	/// <param name="ID">Item ID.</param>
	/// <param name="foundBarrel">Found barrel.</param>
	public bool isLiquidCupAvailable(FurnishingLibrary.HoldableID ID, out bool foundCup)
	{
		foundCup = false;
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(FurnishingLibrary.HoldableID.Cup, out output) && output.Count > 0)
		{
			for(int i = 0; i < output.Count; i++)
			{
				if(output[i].liquidHere != null
				   && output[i].liquidHere.liquidID == ID)
				{
					return true;
				}
				if(foundCup == false 
				   && output[i].liquidHere == null)
				{
					foundCup = true;
				}
			}
		}
		return false;
	}

	/// <summary>
	/// If LIQUID item ID is available inside a cup, returns true.
	/// </summary>
	/// <returns><c>true</c>, if LIQUID ID is available, <c>false</c> otherwise.</returns>
	/// <param name="ID">Item ID.</param>
	/// <param name="foundBarrel">Found barrel.</param>
	public bool isLiquidCupAvailable(FurnishingLibrary.HoldableID ID, out Holdable foundCup)
	{
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(FurnishingLibrary.HoldableID.Cup, out output) && output.Count > 0)
		{
			for(int i = 0; i < output.Count; i++)
			{
				if(output[i].liquidHere != null
				   && output[i].liquidHere.liquidID == ID)
				{
					foundCup = output[i];
					return true;
				}
			}
		}
		foundCup = null;
		return false;
	}

	/// <summary>
	/// Adds a holdable to the HoldableOnGround Dictionary.
	/// </summary>
	/// <param name="holdable">Holdable.</param>
	public void addHoldableToGround(Holdable holdable)
	{
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(holdable.holdableID, out output))
		{
			HoldablesOnGround[holdable.holdableID].Add(holdable);
		}
		else
		{
			output = new List<Holdable>();
			output.Add(holdable);
			HoldablesOnGround.Add(holdable.holdableID, output);
		}
	}

	/// <summary>
	/// Removes a holdable to the HoldableOnGround Dictionary.
	/// </summary>
	/// <param name="holdable">Holdable.</param>
	public void removeHoldableToGround(Holdable holdable)
	{
		List<Holdable> output;
		if(HoldablesOnGround.TryGetValue(holdable.holdableID, out output) && output.Count > 0)
		{
			HoldablesOnGround[holdable.holdableID].Remove(holdable);
            if(output.Count == 0)
            {
                HoldablesOnGround.Remove(holdable.holdableID);
            }
		}
	}



	/// <summary>
	/// If LIQUID item ID is available inside of Furnishing it is normally harvested from, returns true.
	/// </summary>
	/// <returns><c>true</c>, if LIQUID ID is available, <c>false</c> otherwise.</returns>
	/// <param name="ID">Item ID.</param>
	/// <param name="foundBarrel">Found barrel.</param>
	public bool isFurnishingLiquidAvailable(FurnishingLibrary.HoldableID ID, out List<Furnishing> foundFurnishings)
	{
		foundFurnishings = new List<Furnishing>();
		List<Furnishing> output;
		if(FurnishingOnMap.TryGetValue(recipeBook.WhereMake(ID), out output) && output.Count > 0)
		{
			for(int i = 0; i < output.Count; i++)
			{
				if(output[i].liquidHere != null
				   && output[i].liquidHere.liquidID == ID)
				{
					foundFurnishings.Add(output[i]);
				}
			}
		}
		if(foundFurnishings.Count > 0)
		{
			return true;
		}
		return false;
	}

	/// <summary>
	/// Returns true if there are applicable quests available to the party as well as the first found quest and board
	/// </summary>
	/// <returns><c>true</c>, if quest available was applicabled, <c>false</c> otherwise.</returns>
	/// <param name="party">Party.</param>
	public bool applicableQuestAvailable(AdventurerParty party, out QuestBoard board, out int toChoose)
	{
		List<Furnishing> output;
		if(FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.QuestBoard, out output))
		{
			for(int i = 0; i < FurnishingOnMap[FurnishingLibrary.FurnishingID.QuestBoard].Count; i++)
			{
				board = (QuestBoard)FurnishingOnMap[FurnishingLibrary.FurnishingID.QuestBoard][i];
				if(board.applicableQuestAvailable(party, out toChoose))
				{
					return true;
				}
			}
		}
		toChoose = 0;
		board = null;
		return false;
	}

	public bool questToPostAvailable(out QuestBoard board, out int toChoose)
	{
		List<Furnishing> output;
		if(FurnishingOnMap.TryGetValue(FurnishingLibrary.FurnishingID.QuestBoard, out output))
		{
			for(int i = 0; i < FurnishingOnMap[FurnishingLibrary.FurnishingID.QuestBoard].Count; i++)
			{
				board = (QuestBoard)FurnishingOnMap[FurnishingLibrary.FurnishingID.QuestBoard][i];
				for(int q = 0; q < board.questsPosted.Length; q++)
				{
					if(board.questsPosted[q] != null
					   &&
					   !board.questsPosted[q].activeOnBoard && board.questsPosted[q].setUpTime <= 0)
					{
						toChoose = q;
						return true;
					}
				}
			}
		}
		toChoose = 0;
		board = null;
		return false;
	}

	public void deleteFurnishing(Door toDelete)
	{	
		Vector2 pos = toDelete.transform.position;
		DestroyImmediate(toDelete.gameObject);
		setGridPathing(pos-new Vector2(1,1), pos+new Vector2(1,1));

	}

	public void deleteFurnishing(Furnishing toDelete)
	{
		if(toDelete.GetComponent<Table>())
		{
			if(toDelete.GetComponent<Table>().called)
			{
				return;
			}
		}
		else if(toDelete.GetComponent<Bed>())
		{
			Bed bedToDelete = toDelete.GetComponent<Bed>();
			if(bedToDelete.called)
			{
				return;
			}
			else
			{
				bedToDelete.roomInsideOf.bedsHere.Remove(bedToDelete);
			}
		}


		InteractableFurnishing interact = toDelete.GetComponent<InteractableFurnishing>();
		if(interact != null && interact.canAssignWorkers)
		{
            interact.prepareRemoval();
		}

		List<PathNode> nodes = toDelete.reachableNodes;

		FurnishingOnMap[toDelete.FurnishingID].Remove(toDelete);

		DestroyImmediate(toDelete.gameObject);

		for(int i = 0; i < nodes.Count; i++)
		{
			nodes[i].setNeighbors();
			for(int a = 0; a < nodes[i].Neighbors.Count; a++)
			{
				nodes[i].Neighbors[a].setNeighbors();
			}
		}
	}

	public bool questToPostAvailable()
	{
		QuestBoard whoCares;
		int noOne;
		return questToPostAvailable(out whoCares, out noOne);
	}

	public void spawnHumanoid(Humanoid.NPCType type, Vector2 position)
	{

        if (type == Humanoid.NPCType.Player)
		{
			Instantiate(innkeeperPrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.Adventurer)
		{
			Instantiate(adventurerPrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.Builder)
		{
			Instantiate(builderPrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.Civilian)
		{
			Instantiate(civilianPrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.DeliveryMan)
		{
			Instantiate(deliveryManPrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.Employee)
		{
			Instantiate(employeePrefab, position, new Quaternion());
		}
		else if(type == Humanoid.NPCType.QuestMaster)
		{
			Instantiate(questMasterPrefab, position, new Quaternion());
        }
    }

    /// <summary>
    /// Spawns Humanoid at random starting position
    /// </summary>
    /// <param name="type"></param>
    public void spawnHumanoid(Humanoid.NPCType type)
    {
        int spawnY = Random.Range(0, 5);
        int left = Random.Range(0, 2);
        Vector2 position = new Vector2(left * (GRID_X - 1), spawnY);

        spawnHumanoid(type, position);
    }

    /// <summary>
    /// Returns true if ID is wanted by any Kitchen Windows
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
	public bool foodOrderWanted (FurnishingLibrary.HoldableID ID)
	{
		List<Furnishing> list;
		if(hasFurnishing(FurnishingLibrary.FurnishingID.KitchenWindow, out list))
		{

			for(int i = 0; i < list.Count; i++)
			{
				KitchenWindow toLookAt = (KitchenWindow) list[i];
                int ammountNeeded = 0;
				int ammoutHave = 0;
                toLookAt.holdableAmmountHere.TryGetValue(ID, out ammoutHave);
                for (int a = 0; a < toLookAt.orders.Count; a++)
				{
                    int tempNeeded = 0;
                    if(toLookAt.orders[a].ammountNeeded.TryGetValue(ID, out tempNeeded))
                    {
                        ammountNeeded += tempNeeded;
                    }
                }
                if (ammountNeeded > ammoutHave)
                {
                    return true;
                }
            }
		}
		return false;
	}


    void UpdateHUDButtons()
    {
        if(attackButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.ChoosingCombatTarget)
            {
                currentScreenState = screenState.ChoosingCombatTarget;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if(talkButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.ChoosingTalkTarget)
            {
                currentScreenState = screenState.ChoosingTalkTarget;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if (pickUpItemButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.PickUpMenu)
            {
                currentScreenState = screenState.PickUpMenu;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }

        else if (decorationFurnishingButton.Update(UIMousPos))
        {

        }
        else if (workFurnishingButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.BuildWorkScreenOpen 
                && currentScreenState != screenState.ChoosingFurnishingSpace 
                && currentScreenState != screenState.DeletingFurnishing
                && currentScreenState != screenState.BuildingDoor)
            {
                currentScreenState = screenState.BuildWorkScreenOpen;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }
        else if (roomButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.BuildingRoom && currentScreenState != screenState.ChoosingRoomOriginPoint)
            {
                currentScreenState = screenState.ChoosingRoomOriginPoint;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }

        else if (illuminatiButton.Update(UIMousPos))
        {

        }
        else if (shiftsButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.EmployeeShiftScreen)
            {
                currentScreenState = screenState.EmployeeShiftScreen;
            }
        }
        else if (hireButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.HireEmployeeMenu)
            {
                currentScreenState = screenState.HireEmployeeMenu;
            }
        }
        else if (merchantDropPointButton.Update(UIMousPos))
        {
            if (currentScreenState != screenState.ChooseMerchantDropPoint)
            {
                currentScreenState = screenState.ChooseMerchantDropPoint;
            }
            else
            {
                currentScreenState = screenState.Normal;
            }
        }
    }

    void UpdateWorkButtons()
    {
        for (int i = 0; i < WorkMenuButton.Count; i++)
        {
            FurnishingLibrary.FurnishingID choice = WorkMenuButton[i].FurnishingUpdate(UIMousPos);
            if(choice != FurnishingLibrary.FurnishingID.Error)
            {
                currentScreenState = screenState.ChoosingFurnishingSpace;
                FurnishPrefabToBuild = FurnishingLibrary.Library.getFurnishingPrefab(choice);
            }
        }


        if(deleteButton.Update(UIMousPos))
        {
            currentScreenState = screenState.DeletingFurnishing;
        }
        if (moveFurnishingButton.Update(UIMousPos))
        {
            currentScreenState = screenState.MoveFurnishingMenu;
        }
        if (doorButton.Update(UIMousPos))
        {
            currentScreenState = screenState.BuildingDoor;
        }
    }

    public void activateHumanoid(Humanoid toActivate)
    {

        toActivate.gameObject.SetActive(true);
        switch (toActivate.thisNPCType)
        {
            case Humanoid.NPCType.Builder:
                BuildersOnScene.Add(toActivate);
                BuildersInReserve.Remove(toActivate);
                return;

            case Humanoid.NPCType.Adventurer:
                AdventurersOnScene.Add(toActivate);
                AdventurersInReserve.Remove(toActivate);
                return;

            case Humanoid.NPCType.Civilian:
                if (CiviliansInReserve.Contains(toActivate))
                {
                    CiviliansInReserve.Remove(toActivate);
                    if (!needToCull(Humanoid.NPCType.Civilian))
                    {
                        CiviliansOnScene.Add(toActivate);
                        toActivate.lookingForParty = true;
                    }
                    else
                    {
                        TownshipOnScene.Add(toActivate);
                        toActivate.lookingForParty = false;
                    }
                }
                else
                {
                    TownshipInReserve.Remove(toActivate);
                    if (hasSpace(Humanoid.NPCType.Civilian))
                    {
                        CiviliansOnScene.Add(toActivate);
                        toActivate.lookingForParty = true;
                    }
                    else
                    {
                        TownshipOnScene.Add(toActivate);
                        toActivate.lookingForParty = false;
                    }
                }
                return;

            case Humanoid.NPCType.Employee:
                if (Time.timeScale == 0)
                {
                    return;
                }
                EmployeesOnScene.Add(toActivate);
                EmployeesInReserve.Remove(toActivate);
                return;

            case Humanoid.NPCType.DeliveryMan:
                DeliveryMenOnScene.Add(toActivate);
                DeliveryMenInReserve.Remove(toActivate);
                return;

            case Humanoid.NPCType.QuestMaster:
                QuestMasterOnScene.Add(toActivate);
                QuestMasterInReserve.Remove(toActivate);
                return;
        }
    }

    public bool hasSpace(Humanoid.NPCType Type)
    {
        if(Type == Humanoid.NPCType.Civilian)
        {
            if (CiviliansInReserve.Count + CiviliansOnScene.Count < civCap*civTrafficMod)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if(Type == Humanoid.NPCType.Adventurer)
        {
            if (AdventurerParties.Count < advCap)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    public bool needToCull(Humanoid.NPCType Type)
    {
        if (Type == Humanoid.NPCType.Civilian)
        {
            if (CiviliansInReserve.Count + CiviliansOnScene.Count > civCap * civTrafficMod)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if (Type == Humanoid.NPCType.Adventurer)
        {
            if (AdventurerParties.Count > advCap)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        return true;
    }

    public void changeMoney(int amount, Vector2 position)
    {
        NumberEffectDisplay downer = new NumberEffectDisplay();
        downer.Set(amount, moneyDisplayPos, CurrencySymbol, true, true, false);
        displayUIEffects.Add(downer);

        NumberEffectDisplay upper = new NumberEffectDisplay();
        upper.Set(amount, position, CurrencySymbol, false, false, false);
        displayWorldEffects.Add(upper);

        currency += amount;
    }

    public void changeMoney(int amount)
    {
        NumberEffectDisplay downer = new NumberEffectDisplay();
        downer.Set(amount, moneyDisplayPos, CurrencySymbol, true, true, false);
        displayUIEffects.Add(downer);

        currency += amount;
    }

    public void changeFame(int amount)
    {
        fame += amount;

        NumberEffectDisplay downer = new NumberEffectDisplay();
        downer.Set(amount, fameDisplayPos, CurrencySymbol, true, true, false);
        displayUIEffects.Add(downer);

        calculateCustomerCap();
    }

    public void changeFame(int amount, Vector2 position)
    {
        fame += amount;
        NumberEffectDisplay fameGain = new NumberEffectDisplay();
        fameGain.Set(amount, position, FameSymbol, false, false, true);
        displayWorldEffects.Add(fameGain);

        NumberEffectDisplay downer = new NumberEffectDisplay();
        downer.Set(amount, fameDisplayPos, CurrencySymbol, true, true, false);
        displayUIEffects.Add(downer);

        calculateCustomerCap();
    }


    void calculateCustomerCap()
    {
        if(fame < 0)
        {
            fame = 0;
        }
        civCap = (int)Mathf.Sqrt(fame / 10) + civBonus;
        advCap = (int)Mathf.Sqrt(fame / 100) + advBonus;
    }

    public void calculateMerchantCost()
    {
        merchantCostMultiplier = 1 + boarderingArea[Area.DungeonSetting.Roads].monstersInArea / 5;
        if (boarderingArea[Area.DungeonSetting.Roads].monstersInArea >= 75)
        {
            tradeAvailable = false;
        }
        else
        {
            tradeAvailable = true;
        }
    }

    public void StockHireableEmployees()
    {
        HireHumanoidLookingAt = -1;

        for (int i = 0; i < HireSlots; i++)
        {
            if (toHire[i] != null)
            {
                TownshipInReserve.Add(toHire[i]);
                toHire[i] = null;
            }
        }

        for(int i = 0; i < HireSlots; i++)
        {
            if (TownshipInReserve.Count > 0)
            {
                int rando = Random.Range(0, TownshipInReserve.Count);
                toHire[i] = TownshipInReserve[rando];
                TownshipInReserve.RemoveAt(rando);
            }
            else
            {
                Debug.Log("No more Civies");
            }
        }
    }

    public void HireEmployee(int choice)
    {
        if (toHire[choice] != null)
        {
            toHire[choice].thisNPCType = Humanoid.NPCType.Employee;
            toHire[choice] = toHire[choice];
            EmployeesInReserve.Add(toHire[choice]);
            toHire[choice] = null;
        }
    }

    void EndOfDayUpdates()
    {
        StockHireableEmployees();
        if (tradeAvailable)
        {
            SetBuyables();
        }
    }
}
