﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoomPiece : MonoBehaviour 
{
	public Transform mainTransform;

	public Room parentRoom;
	public Vector2 gridCoord;
	public float percentBuilt = 0;
	public bool buildComplete = false;

	public List<Sprite> buildSprites = new List<Sprite>();

	public Humanoid calledBuilder;

	SpriteRenderer spriteRenderer;

	public void buildObject(float buildPercentIncrease)
	{
		percentBuilt += buildPercentIncrease;
		if(percentBuilt >= 1)
		{
			MainMechanics.mechanics.roomsToBuild.Remove (this);
		}
	}

	void Awake()
	{
		mainTransform = transform;
	}

	void Start ()
	{

		MainMechanics.mechanics.addToBuildList(this);
		spriteRenderer = GetComponent<SpriteRenderer>();
		transform.parent = MainMechanics.mechanics.roomFolder.transform;
	}

	void Update () 
	{

		if(!buildComplete)
		{
			if(calledBuilder != null && (calledBuilder.calledRoomPiece == null || calledBuilder.calledRoomPiece != this))
			{
				calledBuilder = null;
			}
			if(percentBuilt >= 1)
			{
				spriteRenderer.sprite = buildSprites[4];
			}
			else if(percentBuilt >= 0.75)
			{
				spriteRenderer.sprite = buildSprites[3];
			}
			else if(percentBuilt >= 0.5)
			{
				spriteRenderer.sprite = buildSprites[2];
			}
			else if(percentBuilt >= 0.25)
			{
				spriteRenderer.sprite = buildSprites[1];
			}
			else
			{
				spriteRenderer.sprite = buildSprites[0];
			}
		}
		if(!buildComplete && percentBuilt >= 1)
		{
			MainMechanics.mechanics.mainGrid[(int)gridCoord.x, (int)gridCoord.y].speedMultiplier = 1.5f;
			buildComplete = true;
			calledBuilder = null;
            MainMechanics.mechanics.changeMoney(-MainMechanics.mechanics.floorCost, transform.position);
			//MainMechanics.mechanics.setGridPathing((Vector2) transform.position-new Vector2(1,1), (Vector2) transform.position+new Vector2(1,1));
		}
	}

	public void SetWalls (GameObject wall, Vector2 gridCoord) 
	{
		checkForWalls(wall, new Vector2(1,0), gridCoord);
		checkForWalls(wall, new Vector2(-1,0), gridCoord);
		checkForWalls(wall, new Vector2(0,1), gridCoord);
		checkForWalls(wall, new Vector2(0,-1), gridCoord);
	}

	void checkForWalls(GameObject wall, Vector2 dir, Vector2 gridCoord)
	{
		RaycastHit2D castHit;
		
		Physics2D.queriesStartInColliders = false;
		castHit = Physics2D.Raycast(transform.position, dir, 1f);
		
		if(castHit)
		{
			if(castHit.collider.GetComponent<PathNode>())
			{
				if(castHit.collider.GetComponent<PathNode>().gridHere.roomPieceHere == null
				   || castHit.collider.GetComponent<PathNode>().gridHere.roomPieceHere.parentRoom != parentRoom)
				{
					if(dir.x != 0)
					{
						GameObject instanedWall = Instantiate(wall, (Vector2)transform.position,Quaternion.Euler (0,0,90.0f)) as GameObject;

						instanedWall.transform.parent = transform;

						instanedWall.transform.localPosition = dir/2f;
						
						MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
					}
					else
					{
						GameObject instanedWall = Instantiate(wall, (Vector2)transform.position,Quaternion.Euler (0,0,0)) as GameObject;

						instanedWall.transform.parent = transform;
						
						instanedWall.transform.localPosition = dir/2f;
						
						MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
					}
				}
			}
			else if(castHit.collider.GetComponentInParent<Wall>())
			{
				Wall wallFound = castHit.collider.GetComponentInParent<Wall>();

				if(gridCoord.x+dir.x >= 0 && gridCoord.x+dir.x < MainMechanics.GRID_X 
				   && gridCoord.y+dir.y >= 0 && gridCoord.y+dir.y < MainMechanics.GRID_Y
				   && 
				   MainMechanics.mechanics.mainGrid[(int)(gridCoord.x+dir.x), (int)(gridCoord.y+dir.y)].roomPieceHere != null
				   && MainMechanics.mechanics.mainGrid[(int)(gridCoord.x+dir.x), (int)(gridCoord.y+dir.y)].roomPieceHere.parentRoom == parentRoom)
				{
					Destroy (wallFound.gameObject);
					MainMechanics.mechanics.wallOpening();
				}
			}
			else if(castHit.collider.GetComponentInParent<Furnishing>())
			{
				if(MainMechanics.mechanics.mainGrid[(int)(transform.position.x+dir.x), (int)(transform.position.y+dir.y)].roomPieceHere == null
				   || MainMechanics.mechanics.mainGrid[(int)(transform.position.x+dir.x), (int)(transform.position.y+dir.y)].roomPieceHere.parentRoom != parentRoom)
				{
					if(dir.x != 0)
					{
						GameObject instanedWall = Instantiate(wall, gridCoord,Quaternion.Euler (0,0,90.0f)) as GameObject;
						instanedWall.transform.parent = transform;
						
						instanedWall.transform.localPosition = dir/2f;
						
						MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
					}
					else
					{
						GameObject instanedWall = Instantiate(wall, gridCoord,Quaternion.Euler (0,0,0)) as GameObject;
						instanedWall.transform.parent = transform;
						
						instanedWall.transform.localPosition = dir/2f;
						
						MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
					}
				}
			}
		}
		else
		{
			if(dir.x != 0)
			{
				GameObject instanedWall = Instantiate(wall, gridCoord,Quaternion.Euler (0,0,90.0f)) as GameObject;
				instanedWall.transform.parent = transform;
				
				instanedWall.transform.localPosition = dir/2f;

				MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
			}
			else
			{
				GameObject instanedWall = Instantiate(wall, gridCoord,Quaternion.Euler (0,0,0)) as GameObject;
				instanedWall.transform.parent = transform;
				
				instanedWall.transform.localPosition = dir/2f;
				
				MainMechanics.mechanics.addToBuildList(instanedWall.GetComponent<Wall>());
			}
		}
	}
}
