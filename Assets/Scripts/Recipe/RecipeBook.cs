﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RecipeBook
{
	public List<FurnishingLibrary.HoldableID> knownPrepableIDs = new List<FurnishingLibrary.HoldableID>();
	public List<Recipe> KnownPrepables = new List<Recipe>();

    public List<FurnishingLibrary.HoldableID> knownBrewableIDs = new List<FurnishingLibrary.HoldableID>();
    public List<Recipe> KnownBrewables = new List<Recipe>();

    public void makeRecipeKnown(FurnishingLibrary.HoldableID ID)
	{
		if(!knownPrepableIDs.Contains(ID))
		{
			knownPrepableIDs.Add(ID);
			SetRecipes();
		}
	}


	void SetRecipes()
	{
		KnownPrepables.Clear();

		for(int i = 0; i < knownPrepableIDs.Count; i++)
		{
			Recipe reciToAdd = new Recipe();
			reciToAdd.Set(knownPrepableIDs[i]);
			if(reciToAdd.Ingredients.Count > 0)
			{
				KnownPrepables.Add(reciToAdd);
			}
		}
	}


    public void makeDrinkRecipeKnown(FurnishingLibrary.HoldableID ID)
    {
        if (!knownBrewableIDs.Contains(ID))
        {
            knownBrewableIDs.Add(ID);
            SetDrinkRecipes();
        }
    }


    void SetDrinkRecipes()
    {
        KnownBrewables.Clear();

        for (int i = 0; i < knownBrewableIDs.Count; i++)
        {
            Recipe reciToAdd = new Recipe();
            reciToAdd.Set(knownBrewableIDs[i]);
            if (reciToAdd.Ingredients.Count > 0)
            {
                KnownBrewables.Add(reciToAdd);
            }
        }
    }


    /// <summary>
    /// Returns what ingredients an item is made of.
    /// </summary>
    /// <returns>Ingredients.</returns>
    /// <param name="ID">I.</param>
    public Dictionary<FurnishingLibrary.HoldableID, int> WhatMake (FurnishingLibrary.HoldableID ID)
	{
		//IF WHERE MAKE IS NOT A PREP TABLE, ONLY 1 INGREDIENT SHOULD BE REQUIRED
		Dictionary<FurnishingLibrary.HoldableID, int> returnList = new Dictionary<FurnishingLibrary.HoldableID, int>();
		if(ID == FurnishingLibrary.HoldableID.BreadDough)
		{
			returnList.Add(FurnishingLibrary.HoldableID.LiqWater, 3);
			returnList.Add(FurnishingLibrary.HoldableID.Flour, 1);
		}
		else if(ID == FurnishingLibrary.HoldableID.Bread)
		{
			returnList.Add(FurnishingLibrary.HoldableID.BreadDough, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeat || ID == FurnishingLibrary.HoldableID.RawGroundMeat)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawMeat, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.RawMeatPatty || ID == FurnishingLibrary.HoldableID.RawMeatBall)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawGroundMeat, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeatBall)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawMeatBall, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeatPatty)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawMeatPatty, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.Burger)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Bread, 1);
            returnList.Add(FurnishingLibrary.HoldableID.CookedMeatPatty, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.CheeseBurger)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Bread, 1);
            returnList.Add(FurnishingLibrary.HoldableID.CookedMeatPatty, 1);
            returnList.Add(FurnishingLibrary.HoldableID.Cheese, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.DeluxBurger)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Bread, 1);
            returnList.Add(FurnishingLibrary.HoldableID.CookedMeatPatty, 1);
            returnList.Add(FurnishingLibrary.HoldableID.Cheese, 1);
            returnList.Add(FurnishingLibrary.HoldableID.Greens, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.Salad)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Greens, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.PastaDough)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Egg, 1);
            returnList.Add(FurnishingLibrary.HoldableID.Flour, 2);
        }
        else if (ID == FurnishingLibrary.HoldableID.RawSpaghetti)
        {
            returnList.Add(FurnishingLibrary.HoldableID.PastaDough, 1);
            returnList.Add(FurnishingLibrary.HoldableID.LiqWater, 5);
        }
        else if (ID == FurnishingLibrary.HoldableID.Spaghetti)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawSpaghetti, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.SpaghettiAndMeatballs)
        {
            returnList.Add(FurnishingLibrary.HoldableID.CookedMeatBall, 1);
            returnList.Add(FurnishingLibrary.HoldableID.Spaghetti, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.RawRavioli)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawGroundMeat, 1);
            returnList.Add(FurnishingLibrary.HoldableID.PastaDough, 1);
            returnList.Add(FurnishingLibrary.HoldableID.LiqWater, 5);
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedRavioli)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawRavioli, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.RawFondue)
        {
            returnList.Add(FurnishingLibrary.HoldableID.Cheese, 1);
            returnList.Add(FurnishingLibrary.HoldableID.LiqYBeer, 1);
        }
        else if (ID == FurnishingLibrary.HoldableID.Fondue)
        {
            returnList.Add(FurnishingLibrary.HoldableID.RawFondue, 1);
        }
        return returnList;
	}

	/// <summary>
	/// Returns a FurnishingID for the Furnishing required to make the Holdable
	/// </summary>
	/// <returns>The make.</returns>
	/// <param name="ID">I.</param>
	public FurnishingLibrary.FurnishingID WhereMake(FurnishingLibrary.HoldableID ID)
	{
		if(ID == FurnishingLibrary.HoldableID.BreadDough)
		{
			return FurnishingLibrary.FurnishingID.PrepTable;
		}
		if(ID == FurnishingLibrary.HoldableID.Bread)
		{
			return FurnishingLibrary.FurnishingID.Oven;
        }
        if (ID == FurnishingLibrary.HoldableID.CookedMeat)
        {
            return FurnishingLibrary.FurnishingID.Stove;
        }
        if(ID == FurnishingLibrary.HoldableID.RawGroundMeat)
        {
            return FurnishingLibrary.FurnishingID.MeatGrinder;
        }
        if(ID == FurnishingLibrary.HoldableID.RawMeatBall)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.RawMeatPatty)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.CookedMeatPatty)
        {
            return FurnishingLibrary.FurnishingID.Stove;
        }
        if (ID == FurnishingLibrary.HoldableID.CookedMeatBall)
        {
            return FurnishingLibrary.FurnishingID.Oven;
        }
        if (ID == FurnishingLibrary.HoldableID.Burger || ID == FurnishingLibrary.HoldableID.DeluxBurger || ID == FurnishingLibrary.HoldableID.CheeseBurger)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.Salad)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.PastaDough)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.RawSpaghetti)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.Spaghetti)
        {
            return FurnishingLibrary.FurnishingID.Stove;
        }
        if (ID == FurnishingLibrary.HoldableID.SpaghettiAndMeatballs)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.RawRavioli)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.CookedRavioli)
        {
            return FurnishingLibrary.FurnishingID.Stove;
        }
        if (ID == FurnishingLibrary.HoldableID.RawFondue)
        {
            return FurnishingLibrary.FurnishingID.PrepTable;
        }
        if (ID == FurnishingLibrary.HoldableID.Fondue)
        {
            return FurnishingLibrary.FurnishingID.Stove;
        }


        if (ID == FurnishingLibrary.HoldableID.LiqWater)
		{
			return FurnishingLibrary.FurnishingID.Well;
		}
		if(ID == FurnishingLibrary.HoldableID.LiqYBeer)
		{
			return FurnishingLibrary.FurnishingID.Still;
        }
        if (ID == FurnishingLibrary.HoldableID.LiqBlAle)
        {
            return FurnishingLibrary.FurnishingID.Still;
        }
        if (ID == FurnishingLibrary.HoldableID.LiqGrapeJuice)
        {
            return FurnishingLibrary.FurnishingID.GrapeVat;
        }
        if (ID == FurnishingLibrary.HoldableID.LiqWine)
        {
            return FurnishingLibrary.FurnishingID.Still;
        }

        return FurnishingLibrary.FurnishingID.Error;
	}

	/// <summary>
	/// Returns an int that is the required action to make the object
	/// </summary>
	/// <returns>The make.</returns>
	/// <param name="ID">I.</param>
	public int HowMake(FurnishingLibrary.HoldableID ID)
	{
        FurnishingLibrary.FurnishingID whereMake = WhereMake(ID);


        if (whereMake == FurnishingLibrary.FurnishingID.PrepTable)
        {
            return (int)ID;
        }
        if(whereMake == FurnishingLibrary.FurnishingID.Oven)
        {
            return 1;
        }
        if (whereMake == FurnishingLibrary.FurnishingID.Stove)
        {
            return 0;
        }
        if (whereMake == FurnishingLibrary.FurnishingID.MeatGrinder)
        {
            return 0;
        }
        if(whereMake == FurnishingLibrary.FurnishingID.GrapeVat)
        {
            return 0;
        }
        Debug.Log("HowMake Error "+ID);
		return 0;
	}

	/// <summary>
	/// Returns an int that is the corresponding action to retrieve the completed Holdable from the non-oven Furnishing
	/// </summary>
	/// <returns>The retrieve.</returns>
	/// <param name="ID">I.</param>
	public int HowRetrieve(FurnishingLibrary.HoldableID ID)
	{
        if (ID == FurnishingLibrary.HoldableID.LiqWater)
		{
			return 0;
		}
		if(WhereMake(ID) == FurnishingLibrary.FurnishingID.Still)
		{
			return 2;
		}
        if (ID == FurnishingLibrary.HoldableID.LiqGrapeJuice)
        {
            return 1;
        }
        //Debug.Log("HowRetrieve Error "+ID);

        return -(int)ID;
	}

	public float HowLongToMake(FurnishingLibrary.HoldableID ID)
	{
		return 4;
	}

    public int GetXPGain(FurnishingLibrary.HoldableID ID)
    {
        return 4;
    }

    public List<FurnishingLibrary.HoldableID> GetKnownDrinkRecipes(Dictionary<EmployeeSkill.WorkSkill, int> dictionary)
    {
        List<FurnishingLibrary.HoldableID> toReturn = new List<FurnishingLibrary.HoldableID>();
        toReturn.Add(FurnishingLibrary.HoldableID.LiqYBeer);
        toReturn.Add(FurnishingLibrary.HoldableID.LiqBlAle);
        toReturn.Add(FurnishingLibrary.HoldableID.LiqWine);
        return toReturn;
    }

    public List<FurnishingLibrary.HoldableID> GetKnownFoodRecipes(Dictionary<EmployeeSkill.WorkSkill, int> dictionary, out List<FurnishingLibrary.HoldableID> orderables)
    {
        List<FurnishingLibrary.HoldableID> toReturn = new List<FurnishingLibrary.HoldableID>();
        orderables = new List<FurnishingLibrary.HoldableID>();
        int ammount = 0;
        toReturn.Add(FurnishingLibrary.HoldableID.BreadDough);//HUMANE
        toReturn.Add(FurnishingLibrary.HoldableID.Bread);
        orderables.Add(FurnishingLibrary.HoldableID.Bread);
        
        toReturn.Add(FurnishingLibrary.HoldableID.CookedMeat);//ORCISH
        orderables.Add(FurnishingLibrary.HoldableID.CookedMeat);
        
        toReturn.Add(FurnishingLibrary.HoldableID.Salad);//ELVEN
        orderables.Add(FurnishingLibrary.HoldableID.Salad);

        toReturn.Add(FurnishingLibrary.HoldableID.RawFondue);//DWARVEN
        toReturn.Add(FurnishingLibrary.HoldableID.Fondue);
        orderables.Add(FurnishingLibrary.HoldableID.Fondue);

        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKHumane, out ammount) && ammount > 0)
        {
            toReturn.Add(FurnishingLibrary.HoldableID.PastaDough);
            toReturn.Add(FurnishingLibrary.HoldableID.RawSpaghetti);
            toReturn.Add(FurnishingLibrary.HoldableID.Spaghetti);
            orderables.Add(FurnishingLibrary.HoldableID.Spaghetti);
        }
        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKOrcish, out ammount) && ammount > 0)
        {
            toReturn.Add(FurnishingLibrary.HoldableID.RawMeatBall);
            toReturn.Add(FurnishingLibrary.HoldableID.CookedMeatBall);
            orderables.Add(FurnishingLibrary.HoldableID.CookedMeatBall);
        }
        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKElven, out ammount) && ammount > 0)
        {
            
        }
        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKDwarven, out ammount) && ammount > 0)
        {

        }

        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKHumane, out ammount) && ammount > 0
            && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKOrcish, out ammount) && ammount > 0)
        {
            toReturn.Add(FurnishingLibrary.HoldableID.SpaghettiAndMeatballs);
            orderables.Add(FurnishingLibrary.HoldableID.SpaghettiAndMeatballs);
            toReturn.Add(FurnishingLibrary.HoldableID.RawRavioli);
            toReturn.Add(FurnishingLibrary.HoldableID.CookedRavioli);
            orderables.Add(FurnishingLibrary.HoldableID.CookedRavioli);
            toReturn.Add(FurnishingLibrary.HoldableID.RawMeatPatty);
            toReturn.Add(FurnishingLibrary.HoldableID.Burger);
            orderables.Add(FurnishingLibrary.HoldableID.Burger);
        }
        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKHumane, out ammount) && ammount > 0
           && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKOrcish, out ammount) && ammount > 0
           && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKDwarven, out ammount) && ammount > 0)
        {
            toReturn.Add(FurnishingLibrary.HoldableID.CheeseBurger);
            orderables.Add(FurnishingLibrary.HoldableID.CheeseBurger);
        }
        if (dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKHumane, out ammount) && ammount > 0
           && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKOrcish, out ammount) && ammount > 0
           && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKDwarven, out ammount) && ammount > 0
           && dictionary.TryGetValue(EmployeeSkill.WorkSkill.CKElven, out ammount) && ammount > 0)
        {
            toReturn.Add(FurnishingLibrary.HoldableID.DeluxBurger);
            orderables.Add(FurnishingLibrary.HoldableID.DeluxBurger);
        }

        return toReturn;
    }

    public void GiveRacialCookingXP(FurnishingLibrary.HoldableID ID, Humanoid chef)
    {
        if (ID == FurnishingLibrary.HoldableID.BreadDough)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 6);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.Bread)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 4);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeat)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 10);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.RawGroundMeat)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.RawMeatPatty || ID == FurnishingLibrary.HoldableID.RawMeatBall)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeatBall)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 8);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedMeatPatty)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 8);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.Burger)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 4);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 6);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.CheeseBurger)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKDwarven, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 6);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.DeluxBurger)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKDwarven, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKElven, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 4);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.Salad)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKElven, 10);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.PastaDough)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 6);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.RawSpaghetti)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.Spaghetti)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.SpaghettiAndMeatballs)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.RawRavioli)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 1);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 1);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.CookedRavioli)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKHumane, 2);
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKOrcish, 6);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.RawFondue)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKDwarven, 2);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.Fondue)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.CKDwarven, 8);
            chef.addWorkXP(EmployeeSkill.WorkSkill.BRDwarven, 1);
            return;
        }
        else if(ID == FurnishingLibrary.HoldableID.LiqYBeer)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.BRHumane, 10);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.LiqBlAle)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.BRDwarven, 10);
            return;
        }
        else if (ID == FurnishingLibrary.HoldableID.LiqWine)
        {
            chef.addWorkXP(EmployeeSkill.WorkSkill.BRElven, 8);
            return;
        }
    }

    public Dictionary<FurnishingLibrary.HoldableID, int> WhatMakeLiq(FurnishingLibrary.HoldableID ID)
    {
        //IF WHERE MAKE IS NOT A STILL, ONLY 1 INGREDIENT SHOULD BE REQUIRED
        Dictionary<FurnishingLibrary.HoldableID, int> returnList = new Dictionary<FurnishingLibrary.HoldableID, int>();

        switch(ID)
        {
            case FurnishingLibrary.HoldableID.LiqYBeer:
                returnList.Add(FurnishingLibrary.HoldableID.LiqWater, 10);
                returnList.Add(FurnishingLibrary.HoldableID.Hops, 1);
                return returnList;

            case FurnishingLibrary.HoldableID.LiqBlAle:
                returnList.Add(FurnishingLibrary.HoldableID.LiqWater, 10);
                returnList.Add(FurnishingLibrary.HoldableID.Gruit, 1);
                return returnList;

            case FurnishingLibrary.HoldableID.LiqGrapeJuice:
                returnList.Add(FurnishingLibrary.HoldableID.Grapes, 1);
                return returnList;

            case FurnishingLibrary.HoldableID.LiqWine:
                returnList.Add(FurnishingLibrary.HoldableID.LiqGrapeJuice, 10);
                return returnList;
        }

        return returnList;
    }
}
