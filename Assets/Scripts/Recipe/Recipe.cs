﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Recipe
{
	public Texture2D holdableTexture;
	public FurnishingLibrary.HoldableID holdableID;
	public Dictionary<FurnishingLibrary.HoldableID, int> Ingredients;
	public List<Texture2D> ingredientImage = new List<Texture2D>();

	public void Set(FurnishingLibrary.HoldableID ID)
	{
        char[] letters = ID.ToString().ToCharArray();
        if (letters[0] == 'L' && letters[1] == 'i' && letters[2] == 'q')
        {
            Ingredients = MainMechanics.recipeBook.WhatMakeLiq(ID);
        }
		else
        {
            Ingredients = MainMechanics.recipeBook.WhatMake(ID);
        }
		holdableID = ID;
		holdableTexture = FurnishingLibrary.Library.getHoldablePrefab(ID).holdableTexture;

		List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(Ingredients.Keys);

		for(int i = 0; i < Ingredients.Count; i++)
		{
			ingredientImage.Add(FurnishingLibrary.Library.getHoldablePrefab(keys[i]).holdableTexture);
		}
	}
}
