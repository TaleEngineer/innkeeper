﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Room
{
	public List<RoomPiece> childPieces = new List<RoomPiece>();
	public List<Bed> bedsHere = new List<Bed>();
	public List<PrepTable> prepTablesHere = new List<PrepTable>();
    public List<Bar> barsHere = new List<Bar>();
    public List<FurnishingLibrary.HoldableID> drinksAvailable = new List<FurnishingLibrary.HoldableID>();

	Color roomColor;

    public void updateDrinksAvailable()
    {
        drinksAvailable.Clear();
        for(int i = 0; i < barsHere.Count; i++)
        {
            for(int d = 0; d < barsHere[i].liquidsOnTap.Length; d++)
            {
                if(barsHere[i].liquidsOnTap[d] != null && !drinksAvailable.Contains(barsHere[i].liquidsOnTap[d].liquidID))
                {
                    drinksAvailable.Add(barsHere[i].liquidsOnTap[d].liquidID);
                }
            }
        }
    }

	public bool bedsAvailable(int numberOfPeople)
	{
		if(bedsHere.Count >= numberOfPeople)
		{
			for(int k = 0; k < bedsHere.Count; k++)
			{
				if(bedsHere[k].called || bedsHere[k].isDirty)
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public bool bedsAvailable(Humanoid potentialOccupant)
	{
		if(bedsHere.Count > 0)
		{
			for(int k = 0; k < bedsHere.Count; k++)
			{
				if(bedsHere[k].called || bedsHere[k].isDirty || (bedsHere[k].occupant != null && potentialOccupant.partyApartOf != bedsHere[k].occupant.partyApartOf))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public void setRoomColor(Color color)
	{
		roomColor = color;
		for(int i = 0; i < childPieces.Count; i++)
		{
			if(childPieces[i] != null)
			{
				childPieces[i].GetComponent<SpriteRenderer>().color = color;
			}
			else
			{
				childPieces.RemoveAt(i);
				i--;
			}
		}
	}

	public void setRoomColor()
	{
		for(int i = 0; i < childPieces.Count; i++)
		{
			if(childPieces[i] != null)
			{
				childPieces[i].GetComponent<SpriteRenderer>().color = roomColor;
			}
			else
			{
				childPieces.RemoveAt(i);
				i--;
			}
		}
	}

	public void AddPiece(RoomPiece pieceToAdd)
	{
		if(!childPieces.Contains(pieceToAdd))
		{

			childPieces.Add(pieceToAdd);
		}
	}
}
