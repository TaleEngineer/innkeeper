﻿using UnityEngine;
using System.Collections;

public class EmployeeSkill
{
    public enum WorkSkill
    {
        Generic,
        Bartending,
        Brewing,
        Cooking,
        Cleaning,
        Waitering,
        CKOrcish,
        CKDwarven,
        CKElven,
        CKHumane,
        BROrcish,
        BRDwarven,
        BRElven,
        BRHumane
    }
    public WorkSkill skillID;
    public int currentXP;
    public int maxXP;
    public int level;
    Humanoid toWhom;

    /// <summary>
    /// Sets the class
    /// </summary>
    /// <param name="skill"></param>
    /// <param name="startingXP"></param>
    public void Set(WorkSkill skill, int startingXP, Humanoid whom)
    {
        toWhom = whom;
        skillID = skill;
        maxXP = 100;
        addXP(startingXP);
    }

    public void addXP(int toAdd)
    {
        bool hasLeveled = false;
        currentXP += toAdd;
        while(currentXP >= maxXP)
        {
            char[] array = skillID.ToString().ToCharArray();
            EmployeeSkill skill;
            if (array[1] == 'K' && (!toWhom.workSkills.TryGetValue(WorkSkill.Cooking, out skill) || skill.level <= level))
            {
                currentXP = maxXP - 1;
            }
            else if (array[1] == 'R' && (!toWhom.workSkills.TryGetValue(WorkSkill.Brewing, out skill) || skill.level <= level))
            {
                currentXP = maxXP - 1;
            }
            else
            {
                currentXP -= maxXP;
                level++;
                maxXP *= 5;
                hasLeveled = true;
                if (toWhom.thisNPCType == Humanoid.NPCType.Employee || toWhom.thisNPCType == Humanoid.NPCType.Player)
                {
                    Debug.Log(toWhom.name + " has leveled up his " + skillID);
                }
            }
        }
        if(hasLeveled)
        {
            if(skillID.ToString().ToCharArray()[1] == 'K')
            {
                MainMechanics.mechanics.SetKnownRecipes();
            }
        }
    }
}
