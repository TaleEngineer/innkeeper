﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Party
{
	public List<Humanoid> Members = new List<Humanoid>();
	public string partyName;

    public Humanoid inNeedOfHealing(int ammountToHeal)
    {
        List<Humanoid> toHeal = new List<Humanoid>();
        for(int i = 0; i < Members.Count; i++)
        {
            if(Members[i].currentHP <= ammountToHeal || (float)Members[i].currentHP / (float)Members[i].currentMaxHP <= 0.5f)
            {
                toHeal.Add(Members[i]);
            }
        }

        if(toHeal.Count > 0)
        {
            toHeal = toHeal.OrderBy(h => (float)h.currentHP / (float)h.currentMaxHP).ToList();
            return toHeal[0];
        }
        return null;
    }

    public virtual void setHitTable()
    {

    }

	public bool isOffScreen()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			if(Members[i].gameObject.activeInHierarchy)
			{
				return false;
			}
		}
		return true;
	}

	public virtual void offScreenUpdate()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			Members[i].offScreenUpdate();
		}

	}

	public void addMember (Humanoid toAdd)
	{
		if(Members.Count < 4)
		{
			Members.Add(toAdd);
            setHitTable();
		}
	}

	public void removeMember(Humanoid toRemove)
	{
		Members.Remove (toRemove);
        setHitTable();
    }

	public bool readyToLeaveTable()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			if(Members[i].wantedFood != FurnishingLibrary.HoldableID.Error 
                || 
                (Members[i].wantedDrink != FurnishingLibrary.HoldableID.Error && Members[i].wantedDrink != FurnishingLibrary.HoldableID.HasWantOfDrink))
			{
				return false;
                Debug.Log("Not Ready to Leave");
			}
		}

		return true;
	}

	public bool readyToGo()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			if(Members[i].wantedFood != FurnishingLibrary.HoldableID.Error 
			   || Members[i].wantedDrink != FurnishingLibrary.HoldableID.Error
			   || (Members[i].tiredness >= 50f && Members[i].bedRestingIn == null)
			   || Members[i].currentHP < Members[i].currentMaxHP)
			{
				return false;
			}
		}
		
		return true;
	}

	public void rekindleWants()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			Members[i].rekindleWants();
		}
	}

	public bool hasWants()
	{
		for(int i = 0; i < Members.Count; i++)
		{
			if(Members[i].wantedFood != FurnishingLibrary.HoldableID.Error || Members[i].wantedDrink != FurnishingLibrary.HoldableID.Error
			   ||
			   Members[i].hunger >= Members[i].hungerTrigger || Members[i].thirst >= Members[i].thirstTrigger || Members[i].tiredness >= Members[i].tirednessTrigger)
			{
				return true;
			}
		}
		
		return false;
	}

	public bool rentRooms()
	{
		List<Furnishing> output;
		Room roomToCheck = null;
		int numberOfBeds = 0;

		if(MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.Bed, out output))
		{
			Bed closestFurnishing = null;
			float distance = 0f;
			for(int i = 0; i < output.Count; i++)
			{
				
				Bed bedToLookAt = (Bed)output[i];
				if(!bedToLookAt.called && !bedToLookAt.isDirty)
				{
					numberOfBeds++;
					if(closestFurnishing == null || distance > Vector2.Distance(output[i].transform.position, Members[0].transform.position))
					{
						if(bedToLookAt.roomInsideOf != null && bedToLookAt.roomInsideOf.bedsAvailable(Members.Count))
						{
							closestFurnishing = bedToLookAt;
							distance = Vector2.Distance(output[i].transform.position, Members[0].transform.position);
						}
					}
				}
			}


			if(closestFurnishing != null)
			{
				roomToCheck = closestFurnishing.roomInsideOf;
				for(int i = 0; i < Members.Count; i++)
				{
					if(Members[i].gameObject.activeInHierarchy)
					{
						Members[i].gameObject.SetActive(true);
					}
					if(Members[i].pathToGo.Count > 0)
					{
                        Members[i].clearPath();
					}
					WorkOrder order = new WorkOrder();
					order.Set(roomToCheck.bedsHere[i], 1, Members[i], roomToCheck.bedsHere[i]);
					Members[i].currentWorkOrder.Add(order);
					roomToCheck.bedsHere[i].called = true;
				}
				return true;
			}
			/*else if(numberOfBeds >= Members.Count)
			{
				Debug.Log("Enough found? "+numberOfBeds+" "+Members.Count);
				for(int i = 0; i < Members.Count; i++)
				{
					WorkOrder order = new WorkOrder();
					order.Set(Members[i]);
					Members[i].currentWorkOrder.Add(order);
				}
			}*/
		}

		return false;
	}
}
