﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IFeelable
{
    AdvancedFeeling getFeeling(Humanoid target);
    string getName();
}

public class Kingdom : IFeelable
{
    public string kingdomName = "KINGDOM NAME ERROR";

    List<Kingdom> neighborDistance = new List<Kingdom>();
    List<Army> stationedMilitary;
    List<Army> attackingMilitary;
    List<Settlement> settlements = new List<Settlement>();
    Vector2 worldKingdomPos;
    Vector2 Size;

    List<EventEffect> KingdomEffects = new List<EventEffect>();

    public AdvancedFeeling getFeeling(Humanoid target)
    {
        return target.feelings.getFeeling(this);
    }

    public string getName()
    {
        return kingdomName;
    }
}