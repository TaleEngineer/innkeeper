﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Settlement : IFeelable
{
    public string settlementName = "SETTLEMENT NAME ERROR";

    public List<Settlement> neighbors = new List<Settlement>();
    public Vector2 kingdomPos;
    public List<HeroicFigure> figuresHere = new List<HeroicFigure>();
    public List<HeroicFigure> figuresTraveling = new List<HeroicFigure>();

    public enum SettlementType
    {
        Error,
        Village,
        Town,
        City,
        Capital
    }

    public SettlementType thisSettlementType;

    public int population;
    public int foodStuff;
    public int economy;

    int farmerPopulation;
    int nonFarmerPopulation;

    List<EventEffect> SettlementEffects = new List<EventEffect>();

    public AdvancedFeeling getFeeling(Humanoid target)
    {
        return target.feelings.getFeeling(this);
    }

    public string getName()
    {
        return settlementName;
    }
}