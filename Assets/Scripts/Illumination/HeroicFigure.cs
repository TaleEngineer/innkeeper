﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HeroicFigure : IFeelable
{
    public string Name = "HEROIC FIGURE NAME ERROR";

    Vector2 worldPos;
    Vector2 kingdomPos;

    Kingdom kingdomDestination;
    Settlement settlementDestination;

    Dictionary<EmployeeSkill.WorkSkill, EmployeeSkill> skills = new Dictionary<EmployeeSkill.WorkSkill, EmployeeSkill>();
    Dictionary<Humanoid.CharacterStatistics, int> stats = new Dictionary<Humanoid.CharacterStatistics, int>();
    Humanoid.CharacterClass combatClass;

    List<EventEffect> PersonalEffects = new List<EventEffect>();

    #region IFeelable
    public AdvancedFeeling getFeeling(Humanoid target)
    {
        return target.feelings.getFeeling(this);
    }

    public string getName()
    {
        return Name;
    }
    #endregion
}
