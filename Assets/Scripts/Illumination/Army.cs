﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Army
{
    Kingdom alignedKingdom;
    int numberOfSoldiers;
    HeroicFigure Leader;
    List<HeroicFigure> ImportantSoldiers = new List<HeroicFigure>();
    int food;

    Vector2 kingdomPos;
    Vector2 worldPos;

    List<EventEffect> ArmyEffects = new List<EventEffect>();
}
