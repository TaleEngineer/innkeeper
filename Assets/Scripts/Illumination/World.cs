﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World
{
    List<Kingdom> Kingdoms = new List<Kingdom>();
    List<Battlefield> Battles = new List<Battlefield>();
    List<Army> WanderingArmies = new List<Army>();
    List<HeroicFigure> WanderingFigures = new List<HeroicFigure>();

    List<EventEffect> GlobalEffects = new List<EventEffect>();
}
