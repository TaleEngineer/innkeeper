﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FurnishingLibrary : MonoBehaviour
{
	public static FurnishingLibrary Library;

	public GameObject wellPrefab;
	public GameObject cookingStationPrefab;
	public GameObject StillPrefab;
	public GameObject OvenPrefab;
	public GameObject KitchenWindowPrefab;
	public GameObject TablePrefab;
	public GameObject BarPrefab;
	public GameObject QuestBoardPrefab;
	public GameObject BedPrefab;
    public GameObject StovePrefab;
    public GameObject MeatGrinderPrefab;
    public GameObject CellarStairsPrefab;
    public GameObject GrapeVatPrefab;

    public Holdable barrelPrefab;
	public Holdable cupPrefab;
	public Holdable flourPrefab;
	public Holdable eggPrefab;
	public Holdable doughPrefab;
	public Holdable breadPrefab;
	public Holdable sackPrefab;
    public Holdable bottlePrefab;
    public Holdable rawMeatPrefab;
    public Holdable cookedMeatPrefab;
    public Holdable rawGroundMeatPrefab;
    public Holdable rawMeatballPrefab;
    public Holdable rawMeatPattyPrefab;
    public Holdable cookedMeatballPrefab;
    public Holdable cookedMeatPattyPrefab;
    public Holdable burgerPrefab;
    public Holdable cheesePrefab;
    public Holdable greensPrefab;
    public Holdable cheeseBurgerPrefab;
    public Holdable deluxBurgerPrefab;
    public Holdable saladPrefab;
    public Holdable pastaDoughPrefab;
    public Holdable rawSpaghettiPrefab;
    public Holdable spaghettiPrefab;
    public Holdable spaghettiMeatballsPrefab;
    public Holdable rawRavioliPrefab;
    public Holdable cookedRavioliPrefab;
    public Holdable rawFonduePrefab;
    public Holdable fonduePrefab;
    public Holdable hopsPrefab;
    public Holdable gruitPrefab;
    public Holdable grapesPrefab;


    public Holdable WaterPrefab;
	public Holdable YBeerPrefab;
    public Holdable BlAlePrefab;
    public Holdable GrapeJuicePrefab;
    public Holdable WinePrefab;
    public Holdable BPotionPrefab;
    public Holdable GPotionPrefab;
    public Holdable RPotionPrefab;


    public enum FurnishingID
	{
		Error,
		Well,
		PrepTable,
		Still,
		Oven,
		KitchenWindow,
		Table,
		Bar,
		QuestBoard,
		Bed,
        Stove,
        MeatGrinder,
        CellarStairs,
        GrapeVat
	}

	public enum HoldableID
	{
		Error,
		Flour,
		Egg,
        Cheese,
        RawMeat,
        Greens,
        Barrel,
        Cup,
        BreadDough,
		Bread,
		Sack,
        Bottle,
        CookedMeat,
        RawGroundMeat,
        RawMeatPatty,
        RawMeatBall,
        CookedGroundMeat,
        CookedMeatPatty,
        CookedMeatBall,
        Burger,
        CheeseBurger,
        DeluxBurger,
        Salad,
        PastaDough,
        RawSpaghetti,
        Spaghetti,
        SpaghettiAndMeatballs,
        RawRavioli,
        CookedRavioli,
        RawFondue,
        Fondue,
        Hops,
        Gruit,
        Grapes,

        HasWantOfDrink,
		LiqWater,
		LiqYBeer,
        LiqBlAle,
        LiqGrapeJuice,
        LiqWine,
        LiqRPotion,
        LiqBPotion,
        LiqGPotion
    }

	public enum CookingState
	{
		Error,
		Undercooked,
		Perfect,
		Burnt
	}

    public enum ItemFamilyTag
    {
        Food,
        Drink,
        Poition,
        Ingredient,

        Wood,
        Cloth,
        Glass,
        Metal,

        Orcish,
        Humane,
        Elven,
        Dwarven,
        Multiculture,

        Meat,
        Cheese,
        Flour,
        Greens,
        Fruit,
        Vegetables,

        Raw,
        Cooked,

        Hot,
        Cold,

        Fried,
        Baked,
        Seared,
        Boiled,
        Pickled,

        Sweet,
        Sour,
        Salty,
        Bitter,

        Alchohol,

        Wet,
        Dry
    }

	void Awake()
	{
		Library = this;
		wellPrefab.GetComponent<InteractableFurnishing>().liquidHere = new Liquid();
		wellPrefab.GetComponent<InteractableFurnishing>().liquidHere.Set(HoldableID.LiqWater);
	}

    public HoldableID getJuicedID(HoldableID ID)
    {
        if(ID == HoldableID.Grapes)
        {
            return HoldableID.LiqGrapeJuice;
        }

        return HoldableID.Error;
    }

    public int getJuiceAmount(HoldableID ID)
    {
        if (ID == HoldableID.Grapes)
        {
            return 15;
        }

        return 0;
    }

    public GameObject getBakedObject(HoldableID baseID)
	{
		if(baseID == HoldableID.BreadDough)
		{
			return getHoldable(HoldableID.Bread);
		}
        if (baseID == HoldableID.RawMeat)
        {
            return getHoldable(HoldableID.CookedMeat);
        }
        if (baseID == HoldableID.RawMeatBall)
        {
            return getHoldable(HoldableID.CookedMeatBall);
        }
        if (baseID == HoldableID.RawMeatPatty)
        {
            return getHoldable(HoldableID.CookedMeatPatty);
        }
        if (baseID == HoldableID.RawSpaghetti)
        {
            return getHoldable(HoldableID.Spaghetti);
        }
        if (baseID == HoldableID.RawRavioli)
        {
            return getHoldable(HoldableID.CookedRavioli);
        }
        if (baseID == HoldableID.RawFondue)
        {
            return getHoldable(HoldableID.Fondue);
        }

        return null;
	}

    public HoldableID getBakedID(HoldableID baseID)
    {
        if (baseID == HoldableID.BreadDough)
        {
            return HoldableID.Bread;
        }
        if (baseID == HoldableID.RawMeat)
        {
            return HoldableID.CookedMeat;
        }
        if (baseID == HoldableID.RawMeatBall)
        {
            return HoldableID.CookedMeatBall;
        }
        if (baseID == HoldableID.RawMeatPatty)
        {
            return HoldableID.CookedMeatPatty;
        }
        if (baseID == HoldableID.RawSpaghetti)
        {
            return HoldableID.Spaghetti;
        }
        if (baseID == HoldableID.RawRavioli)
        {
            return HoldableID.CookedRavioli;
        }
        if (baseID == HoldableID.RawFondue)
        {
            return HoldableID.Fondue;
        }

        Debug.Log("getBakeID Error " + baseID);
        return HoldableID.Error;
    }

    public GameObject getGrindedObject(HoldableID ID)
    {
        if(ID == HoldableID.RawMeat)
        {
            return getHoldable(HoldableID.RawGroundMeat);
        }

        Debug.Log(ID + " grind error");
        return null;
    }

	/// <summary>
	/// Gets the name of the item. If plural, returns plural form. Otherwise returns singular form
	/// </summary>
	/// <returns>The item name.</returns>
	/// <param name="ID">I.</param>
	public string getItemName(HoldableID ID, bool plural)
	{
        if (ID >= HoldableID.LiqWater)
        {
            switch (ID)
            {
                case HoldableID.LiqWater: return "Water";

                case HoldableID.LiqYBeer: return "Beer";

                case HoldableID.LiqBlAle: return "Gruit Ale";

                case HoldableID.LiqGrapeJuice: return "Grape Juice";

                case HoldableID.LiqWine: return "Wine";

                case HoldableID.LiqRPotion: return "Red Potion";

                case HoldableID.LiqBPotion: return "Blue Potion";

                case HoldableID.LiqGPotion: return "Green Potion";
            }
        }
        else
        {
            if(plural)
            {
                switch (ID)
                {
                    case HoldableID.Flour: return "Flour";
                    case HoldableID.Egg: return "Eggs";
                    case HoldableID.Cheese: return "Cheese";
                    case HoldableID.RawMeat: return "Raw steaks";
                    case HoldableID.Greens: return "Greens";
                    case HoldableID.Barrel: return "Barrels";
                    case HoldableID.Cup: return "Cups";
                    case HoldableID.BreadDough: return "Bread dough";
                    case HoldableID.Bread: return "Bread";
                    case HoldableID.Sack: return "Sacks";
                    case HoldableID.Bottle: return "Bottles";
                    case HoldableID.CookedMeat: return "Steaks";
                    case HoldableID.RawGroundMeat: return "Raw ground meat";
                    case HoldableID.RawMeatPatty: return "Raw meat patties";
                    case HoldableID.RawMeatBall: return "Raw meat balls";
                    case HoldableID.CookedGroundMeat: return "Ground meat";
                    case HoldableID.CookedMeatPatty: return "Meat patties";
                    case HoldableID.CookedMeatBall: return "Meat balls";
                    case HoldableID.Burger: return "Burgers";
                    case HoldableID.CheeseBurger: return "Cheese burgers";
                    case HoldableID.DeluxBurger: return "Delux burgers";
                    case HoldableID.Salad: return "Salads";
                    case HoldableID.PastaDough: return "Pasta dough";
                    case HoldableID.RawSpaghetti: return "Raw spaghetti";
                    case HoldableID.Spaghetti: return "Spaghetti";
                    case HoldableID.SpaghettiAndMeatballs: return "Spaghetti and meatballs";
                    case HoldableID.RawRavioli: return "Raw ravioli";
                    case HoldableID.CookedRavioli: return "Ravioli";
                    case HoldableID.RawFondue: return "Raw fondue";
                    case HoldableID.Fondue: return "Fondue";
                    case HoldableID.Hops: return "Hops";
                    case HoldableID.Gruit: return "Gruit";
                    case HoldableID.Grapes: return "Grapes";
                }
            }
            else
            {
                switch (ID)
                {
                    case HoldableID.Flour: return "A cup of flour";
                    case HoldableID.Egg: return "An egg";
                    case HoldableID.Cheese: return "A block of cheese";
                    case HoldableID.RawMeat: return "A raw steak";
                    case HoldableID.Greens: return "A pile of greens";
                    case HoldableID.Barrel: return "A barrel";
                    case HoldableID.Cup: return "A cup";
                    case HoldableID.BreadDough: return "A ball of bread dough";
                    case HoldableID.Bread: return "A loaf of bread";
                    case HoldableID.Sack: return "A sack";
                    case HoldableID.Bottle: return "A bottle";
                    case HoldableID.CookedMeat: return "A steak";
                    case HoldableID.RawGroundMeat: return "A pile of raw ground meat";
                    case HoldableID.RawMeatPatty: return "A raw meat patty";
                    case HoldableID.RawMeatBall: return "A raw meat ball";
                    case HoldableID.CookedGroundMeat: return "A pile of ground meat";
                    case HoldableID.CookedMeatPatty: return "A meat patty";
                    case HoldableID.CookedMeatBall: return "A meat ball";
                    case HoldableID.Burger: return "A burger";
                    case HoldableID.CheeseBurger: return "A cheese burger";
                    case HoldableID.DeluxBurger: return "A delux burger";
                    case HoldableID.Salad: return "A salad";
                    case HoldableID.PastaDough: return "A ball of pasta dough";
                    case HoldableID.RawSpaghetti: return "A pile of raw spaghetti";
                    case HoldableID.Spaghetti: return "A plate of spaghetti";
                    case HoldableID.SpaghettiAndMeatballs: return "A plate of spaghetti and meatballs";
                    case HoldableID.RawRavioli: return "A pot of raw ravioli";
                    case HoldableID.CookedRavioli: return "A plate of ravioli";
                    case HoldableID.RawFondue: return "A pot of uncooked fondue";
                    case HoldableID.Fondue: return "A pot of fondue";
                    case HoldableID.Hops: return "A pile of hops";
                    case HoldableID.Gruit: return "A pile of gruit";
                    case HoldableID.Grapes: return "A bunch of grapes";
                }
            }
        }
        return "ItemNameError";
	}

	/// <summary>
	/// Gets the color of the liquid.
	/// </summary>
	/// <returns>The liquid color.</returns>
	/// <param name="ID">I.</param>
	public Color getLiquidColor(HoldableID ID)
	{

        switch (ID)
        {
            case HoldableID.LiqWater: return Color.blue;

            case HoldableID.LiqYBeer: return Color.yellow;

            case HoldableID.LiqBlAle: return new Color(0.124f, 0.078f, 0.078f);

            case HoldableID.LiqGrapeJuice: return new Color(0.301f, 0, 0.203f);

            case HoldableID.LiqWine: return new Color(0.87f, 0, 0.141f);

            case HoldableID.LiqRPotion: return Color.red;

            case HoldableID.LiqBPotion: return Color.blue;

            case HoldableID.LiqGPotion: return Color.green;
        }
        return Color.white;
	}

	/// <summary>
	/// Returns an instanciated Holdable.
	/// </summary>
	/// <returns>The holdable.</returns>
	/// <param name="ID">I.</param>
	public GameObject getHoldable(HoldableID ID)
	{
        switch(ID)
        {
            case HoldableID.Flour: return Instantiate(flourPrefab.gameObject);

            case HoldableID.Egg: return Instantiate(eggPrefab.gameObject);

            case HoldableID.Cheese: return Instantiate(cheesePrefab.gameObject);

            case HoldableID.RawMeat: return Instantiate(rawMeatPrefab.gameObject);

            case HoldableID.Greens: return Instantiate(greensPrefab.gameObject);

            case HoldableID.Barrel: return Instantiate(barrelPrefab.gameObject);

            case HoldableID.Cup: return Instantiate(cupPrefab.gameObject);

            case HoldableID.BreadDough: return Instantiate(doughPrefab.gameObject);

            case HoldableID.Bread: return Instantiate(breadPrefab.gameObject);

            case HoldableID.Sack: return Instantiate(sackPrefab.gameObject);

            case HoldableID.Bottle: return Instantiate(bottlePrefab.gameObject);

            case HoldableID.CookedMeat: return Instantiate(cookedMeatPrefab.gameObject);

            case HoldableID.RawGroundMeat: return Instantiate(rawGroundMeatPrefab.gameObject);

            case HoldableID.RawMeatPatty: return Instantiate(rawMeatPattyPrefab.gameObject);

            case HoldableID.RawMeatBall: return Instantiate(rawMeatballPrefab.gameObject);

            case HoldableID.CookedGroundMeat: return Instantiate(rawGroundMeatPrefab.gameObject);//NEED TO REPLACE!!!!!!!!!!

            case HoldableID.CookedMeatPatty: return Instantiate(cookedMeatPattyPrefab.gameObject);

            case HoldableID.CookedMeatBall: return Instantiate(cookedMeatballPrefab.gameObject);

            case HoldableID.Burger: return Instantiate(burgerPrefab.gameObject);

            case HoldableID.CheeseBurger: return Instantiate(cheeseBurgerPrefab.gameObject);

            case HoldableID.DeluxBurger: return Instantiate(deluxBurgerPrefab.gameObject);

            case HoldableID.Salad: return Instantiate(saladPrefab.gameObject);

            case HoldableID.PastaDough: return Instantiate(pastaDoughPrefab.gameObject);

            case HoldableID.RawSpaghetti: return Instantiate(rawSpaghettiPrefab.gameObject);

            case HoldableID.Spaghetti: return Instantiate(spaghettiPrefab.gameObject);

            case HoldableID.SpaghettiAndMeatballs: return Instantiate(spaghettiMeatballsPrefab.gameObject);

            case HoldableID.RawRavioli: return Instantiate(rawRavioliPrefab.gameObject);

            case HoldableID.CookedRavioli: return Instantiate(cookedRavioliPrefab.gameObject);

            case HoldableID.RawFondue: return Instantiate(rawFonduePrefab.gameObject);

            case HoldableID.Fondue: return Instantiate(fonduePrefab.gameObject);

            case HoldableID.Hops: return Instantiate(hopsPrefab.gameObject);

            case HoldableID.Gruit: return Instantiate(gruitPrefab.gameObject);

            case HoldableID.Grapes: return Instantiate(grapesPrefab.gameObject);
        }

        return null;
	}

	/// <summary>
	/// Gets a Holdable Prefab.
	/// </summary>
	/// <returns>The holdable prefab.</returns>
	/// <param name="ID">I.</param>
	public Holdable getHoldablePrefab(HoldableID ID)
	{

        switch (ID)
        {

            case HoldableID.Flour: return flourPrefab;

            case HoldableID.Egg: return eggPrefab;

            case HoldableID.Cheese: return cheesePrefab;

            case HoldableID.RawMeat: return rawMeatPrefab;

            case HoldableID.Greens: return greensPrefab;

            case HoldableID.Barrel: return barrelPrefab;

            case HoldableID.Cup: return cupPrefab;

            case HoldableID.BreadDough: return doughPrefab;

            case HoldableID.Bread: return breadPrefab;

            case HoldableID.Sack: return sackPrefab;

            case HoldableID.Bottle: return bottlePrefab;

            case HoldableID.CookedMeat: return cookedMeatPrefab;

            case HoldableID.RawGroundMeat: return rawGroundMeatPrefab;

            case HoldableID.RawMeatPatty: return rawMeatPattyPrefab;

            case HoldableID.RawMeatBall: return rawMeatballPrefab;

            case HoldableID.CookedGroundMeat: return null;

            case HoldableID.CookedMeatPatty: return cookedMeatPattyPrefab;

            case HoldableID.CookedMeatBall: return cookedMeatballPrefab;

            case HoldableID.Burger: return burgerPrefab;

            case HoldableID.CheeseBurger: return cheeseBurgerPrefab;

            case HoldableID.DeluxBurger: return deluxBurgerPrefab;

            case HoldableID.Salad: return saladPrefab;

            case HoldableID.PastaDough: return pastaDoughPrefab;

            case HoldableID.RawSpaghetti: return rawSpaghettiPrefab;

            case HoldableID.Spaghetti: return spaghettiPrefab;

            case HoldableID.SpaghettiAndMeatballs: return spaghettiMeatballsPrefab;

            case HoldableID.RawRavioli: return rawRavioliPrefab;

            case HoldableID.CookedRavioli: return cookedRavioliPrefab;

            case HoldableID.RawFondue: return rawFonduePrefab;

            case HoldableID.Fondue: return fonduePrefab;

            case HoldableID.Hops: return hopsPrefab;

            case HoldableID.Gruit: return gruitPrefab;

            case HoldableID.Grapes: return grapesPrefab;


            case HoldableID.LiqWater: return WaterPrefab;

            case HoldableID.LiqYBeer: return YBeerPrefab;

            case HoldableID.LiqBlAle: return BlAlePrefab;

            case HoldableID.LiqGrapeJuice: return GrapeJuicePrefab;

            case HoldableID.LiqWine: return WinePrefab;

            case HoldableID.LiqRPotion: return RPotionPrefab;

            case HoldableID.LiqBPotion: return BPotionPrefab;

            case HoldableID.LiqGPotion: return GPotionPrefab;
        }

        return null;
	}

	/// <summary>
	/// Returns an instantiated Furnishing
	/// </summary>
	/// <returns>The furnishing.</returns>
	/// <param name="ID">I.</param>
	public Furnishing getFurnishing(FurnishingID ID)
	{
        switch(ID)
        {
            case FurnishingID.Well:
                var toReturn = Instantiate(wellPrefab).GetComponent<InteractableFurnishing>();
                toReturn.liquidHere = new Liquid();
                toReturn.liquidHere.Set(FurnishingLibrary.HoldableID.LiqWater);
                return toReturn;

            case FurnishingID.PrepTable:
                return Instantiate(cookingStationPrefab).GetComponent<Furnishing>();

            case FurnishingID.Still:
                return Instantiate(StillPrefab).GetComponent<Furnishing>();

            case FurnishingID.Oven:
                return Instantiate(OvenPrefab).GetComponent<Furnishing>();

            case FurnishingID.KitchenWindow:
                return Instantiate(KitchenWindowPrefab).GetComponent<Furnishing>();

            case FurnishingID.Table:
                return Instantiate(TablePrefab).GetComponent<Furnishing>();

            case FurnishingID.Bar:
                return Instantiate(BarPrefab).GetComponent<Furnishing>();

            case FurnishingID.QuestBoard:
                return Instantiate(QuestBoardPrefab).GetComponent<Furnishing>();

            case FurnishingID.Bed:
                return Instantiate(BedPrefab).GetComponent<Furnishing>();

            case FurnishingID.Stove:
                return Instantiate(StovePrefab).GetComponent<Furnishing>();

            case FurnishingID.MeatGrinder:
                return Instantiate(MeatGrinderPrefab).GetComponent<Furnishing>();

            case FurnishingID.CellarStairs:
                return Instantiate(CellarStairsPrefab).GetComponent<Furnishing>();

            case FurnishingID.GrapeVat:
                return Instantiate(GrapeVatPrefab).GetComponent<Furnishing>();
        }

        return null;
	}

	/// <summary>
	/// Returns a pointer of a Furnishing prefab.
	/// </summary>
	/// <returns>The furnishing prefab.</returns>
	/// <param name="ID">I.</param>
	public GameObject getFurnishingPrefab(FurnishingID ID)
	{

        switch (ID)
        {
            case FurnishingID.Well:
                return wellPrefab;

            case FurnishingID.PrepTable:
                return cookingStationPrefab;

            case FurnishingID.Still:
                return StillPrefab;

            case FurnishingID.Oven:
                return OvenPrefab;

            case FurnishingID.KitchenWindow:
                return KitchenWindowPrefab;

            case FurnishingID.Table:
                return TablePrefab;

            case FurnishingID.Bar:
                return BarPrefab;

            case FurnishingID.QuestBoard:
                return QuestBoardPrefab;

            case FurnishingID.Bed:
                return BedPrefab;

            case FurnishingID.Stove:
                return StovePrefab;

            case FurnishingID.MeatGrinder:
                return MeatGrinderPrefab;

            case FurnishingID.CellarStairs:
                return CellarStairsPrefab;

            case FurnishingID.GrapeVat:
                return GrapeVatPrefab;
        }

        return null;
	}

    public List<ItemFamilyTag> getItemTags(HoldableID ID)
    {
        List<ItemFamilyTag> toReturn = new List<ItemFamilyTag>();

        switch (ID)
        {
            case HoldableID.Flour:
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Dry);
                return toReturn;

            case HoldableID.Egg:
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Ingredient);
                return toReturn;

            case HoldableID.Cheese:
                toReturn.Add(ItemFamilyTag.Dry);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Dwarven);
                return toReturn;

            case HoldableID.RawMeat:
                toReturn.Add(ItemFamilyTag.Dry);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Orcish);
                return toReturn;

            case HoldableID.Greens:
                toReturn.Add(ItemFamilyTag.Dry);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Elven);
                return toReturn;

            case HoldableID.Barrel:
                toReturn.Add(ItemFamilyTag.Wood);
                return toReturn;

            case HoldableID.Cup:
                toReturn.Add(ItemFamilyTag.Metal);
                return toReturn;

            case HoldableID.BreadDough:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Dry);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Humane);
                return toReturn;

            case HoldableID.Bread:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Dry);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Baked);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.Sack:
                toReturn.Add(ItemFamilyTag.Cloth);
                return toReturn;

            case HoldableID.Bottle:
                toReturn.Add(ItemFamilyTag.Glass);
                return toReturn;

            case HoldableID.CookedMeat:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.RawGroundMeat:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Orcish);
                return toReturn;

            case HoldableID.RawMeatPatty:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Orcish);
                return toReturn;

            case HoldableID.RawMeatBall:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Orcish);
                return toReturn;

            case HoldableID.CookedGroundMeat:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.CookedMeatPatty:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.CookedMeatBall:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.Burger:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Multiculture);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.CheeseBurger:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Cheese);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Dwarven);
                toReturn.Add(ItemFamilyTag.Multiculture);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.DeluxBurger:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Cheese);
                toReturn.Add(ItemFamilyTag.Greens);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Dwarven);
                toReturn.Add(ItemFamilyTag.Elven);
                toReturn.Add(ItemFamilyTag.Multiculture);
                toReturn.Add(ItemFamilyTag.Hot);
                return toReturn;

            case HoldableID.Salad:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Greens);
                toReturn.Add(ItemFamilyTag.Elven);
                return toReturn;

            case HoldableID.PastaDough:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Humane);
                return toReturn;

            case HoldableID.RawSpaghetti:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Humane);
                return toReturn;

            case HoldableID.Spaghetti:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Boiled);
                toReturn.Add(ItemFamilyTag.Hot);
                toReturn.Add(ItemFamilyTag.Humane);
                return toReturn;

            case HoldableID.SpaghettiAndMeatballs:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Hot);
                toReturn.Add(ItemFamilyTag.Boiled);
                toReturn.Add(ItemFamilyTag.Seared);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Multiculture);
                return toReturn;

            case HoldableID.RawRavioli:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Multiculture);
                return toReturn;

            case HoldableID.CookedRavioli:
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Flour);
                toReturn.Add(ItemFamilyTag.Meat);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Hot);
                toReturn.Add(ItemFamilyTag.Boiled);
                toReturn.Add(ItemFamilyTag.Orcish);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Multiculture);
                return toReturn;

            case HoldableID.RawFondue:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Cheese);
                toReturn.Add(ItemFamilyTag.Raw);
                toReturn.Add(ItemFamilyTag.Dwarven);
                return toReturn;

            case HoldableID.Fondue:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Cheese);
                toReturn.Add(ItemFamilyTag.Cooked);
                toReturn.Add(ItemFamilyTag.Hot);
                toReturn.Add(ItemFamilyTag.Boiled);
                toReturn.Add(ItemFamilyTag.Dwarven);
                return toReturn;

            case HoldableID.Hops:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Humane);
                return toReturn;

            case HoldableID.Gruit:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Dwarven);
                return toReturn;

            case HoldableID.Grapes:
                toReturn.Add(ItemFamilyTag.Ingredient);
                toReturn.Add(ItemFamilyTag.Food);
                toReturn.Add(ItemFamilyTag.Fruit);
                toReturn.Add(ItemFamilyTag.Elven);
                return toReturn;




            case HoldableID.HasWantOfDrink: return toReturn;

            case HoldableID.LiqWater:
                toReturn.Add(ItemFamilyTag.Drink);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Cold);
                return toReturn;

            case HoldableID.LiqYBeer:
                toReturn.Add(ItemFamilyTag.Drink);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Cold);
                toReturn.Add(ItemFamilyTag.Humane);
                toReturn.Add(ItemFamilyTag.Bitter);
                return toReturn;

            case HoldableID.LiqBlAle:
                toReturn.Add(ItemFamilyTag.Drink);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Cold);
                toReturn.Add(ItemFamilyTag.Dwarven);
                toReturn.Add(ItemFamilyTag.Bitter);
                return toReturn;

            case HoldableID.LiqGrapeJuice:
                toReturn.Add(ItemFamilyTag.Drink);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Cold);
                toReturn.Add(ItemFamilyTag.Fruit);
                toReturn.Add(ItemFamilyTag.Elven);
                toReturn.Add(ItemFamilyTag.Sweet);
                return toReturn;

            case HoldableID.LiqWine:
                toReturn.Add(ItemFamilyTag.Drink);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Alchohol);
                toReturn.Add(ItemFamilyTag.Cold);
                toReturn.Add(ItemFamilyTag.Fruit);
                toReturn.Add(ItemFamilyTag.Elven);
                toReturn.Add(ItemFamilyTag.Sour);
                return toReturn;

            case HoldableID.LiqRPotion:
                toReturn.Add(ItemFamilyTag.Poition);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Sour);
                return toReturn;

            case HoldableID.LiqBPotion:
                toReturn.Add(ItemFamilyTag.Poition);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Sour);
                return toReturn;

            case HoldableID.LiqGPotion:
                toReturn.Add(ItemFamilyTag.Poition);
                toReturn.Add(ItemFamilyTag.Wet);
                toReturn.Add(ItemFamilyTag.Sour);
                return toReturn;
        }

        return toReturn;
    }
}
