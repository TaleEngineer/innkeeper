﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HoldableDropdown : DropdownMenu
{
    public List<FurnishingLibrary.HoldableID> optionIDs;

    public void Set(List<FurnishingLibrary.HoldableID> _options)
    {
        optionIDs = _options;

        List<string> options = new List<string>();
        for(int i = 0; i < optionIDs.Count; i++)
        {
            options.Add(MainMechanics.dialogEngine.getPlural(optionIDs[i]));
        }

        Set(options);
    }

    public override FurnishingLibrary.HoldableID getID()
    {
        return optionIDs[selectedOption];
    }

}
