﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class TalkMenu : Menu
{
    Humanoid targetSpeaker;
    string displayText;
    bool getGreeting = true;
    List<DropdownMenu> dropDownMenus = new List<DropdownMenu>();
    bool hasStatedSomething = false;

    public override void Start()
    {
        base.Start();
        DropdownMenu Action = new DropdownMenu();
        List<string> actionList = new List<string>(Enum.GetNames(typeof(DialogEngine.ActionType)));
        Action.Set(actionList);
        dropDownMenus.Add(Action);

        DropdownMenu Type = new DropdownMenu();
        List<string> typeList = new List<string>(Enum.GetNames(typeof(DialogEngine.ConceptType)));
        Type.Set(typeList);
        dropDownMenus.Add(Type);

        DropdownMenu People = new DropdownMenu();
        List<string> peopleList = new List<string>();
        for (int i = 0; i < 0+1; i++)
        {
            peopleList.Add(MainMechanics.mechanics.Innkeeper.Name+" (You)");
        }
        People.Set(peopleList);
        dropDownMenus.Add(People);

        DropdownMenu Places = new DropdownMenu();
        List<string> placesList = new List<string>(Enum.GetNames(typeof(Area.DungeonSetting)));
        placesList[0] += " (Here)";
        for (int i = 0; i < 0; i++)
        {
            
        }
        Places.Set(placesList);
        dropDownMenus.Add(Places);

        DropdownMenu Monsters = new DropdownMenu();
        List<string> monsterList = new List<string>(Enum.GetNames(typeof(Monster.MonsterType)));
        Monsters.Set(monsterList);
        dropDownMenus.Add(Monsters);

        HoldableDropdown Things = new HoldableDropdown();
        List<FurnishingLibrary.HoldableID> thingsList = new List<FurnishingLibrary.HoldableID>();
        for (int i = 0; i < Enum.GetValues(typeof(FurnishingLibrary.HoldableID)).Length; i++)
        {
            if ((FurnishingLibrary.HoldableID)i != FurnishingLibrary.HoldableID.Error && (FurnishingLibrary.HoldableID)i != FurnishingLibrary.HoldableID.HasWantOfDrink)
            {
                thingsList.Add((FurnishingLibrary.HoldableID)i);
            }
        }
        Things.Set(thingsList);
        dropDownMenus.Add(Things);
    }

    public override void Update()
    {
        targetSpeaker = MainMechanics.mechanics.targetHumanoid;
        if (getGreeting)
        {
            displayText = MainMechanics.dialogEngine.getDialogString(targetSpeaker);
            getGreeting = false;
        }
        base.Update();
    }

    public override void ExitMenu()
    {
        getGreeting = true;
        hasStatedSomething = false;
        MainMechanics.mechanics.targetHumanoid = null;
        base.ExitMenu();
    }

    public override void Draw(Vector2 mousPos)
    {
        var labelStyle = GUI.skin.GetStyle("Label");
        labelStyle.fontSize = Screen.height / 40;
        labelStyle.alignment = TextAnchor.MiddleCenter;

        GUI.color = Color.black;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Texture2D.whiteTexture);
        GUI.color = Color.white;

        Rect TopBox = new Rect(2,0, Screen.width*2/3f-2, Screen.height/2);
        Rect BotBox = new Rect(TopBox.x, TopBox.height+2, TopBox.width, TopBox.height);
        Rect RightBox = new Rect(TopBox.x+TopBox.width+2, 0, Screen.width/3-2, Screen.height);

        GUI.DrawTexture(TopBox, Texture2D.whiteTexture);
        GUI.DrawTexture(BotBox, Texture2D.whiteTexture);
        GUI.DrawTexture(RightBox, Texture2D.whiteTexture);
        #region RightBox
        Rect TargetImageRect = new Rect(RightBox.x+RightBox.width/4, RightBox.y+RightBox.height/40f, RightBox.width/2, RightBox.width/2);
        GUI.DrawTexture(TargetImageRect, targetSpeaker.spriteRenderer.sprite.texture);

        GUI.color = Color.black;
        Rect NameRect = new Rect(TargetImageRect.x, TargetImageRect.y+TargetImageRect.height, TargetImageRect.width, TargetImageRect.height/5);
        GUI.Label(NameRect, targetSpeaker.Name);

        if(targetSpeaker.thisNPCType == Humanoid.NPCType.Adventurer)
        {
            Rect NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (2), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Party:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            if (targetSpeaker.partyApartOf != null)
            {
                GUI.Label(NewTextRect, targetSpeaker.partyApartOf.partyName);
            }
            else
            {
                GUI.Label(NewTextRect, "None");
            }

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (3), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Class:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, targetSpeaker.chosenClass.ToString());

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (4), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Str:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Str]);

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (5), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Dex:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Dex]);

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (6), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Int:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Int]);

            List<EmployeeSkill.WorkSkill> keys = new List<EmployeeSkill.WorkSkill>(targetSpeaker.workSkills.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                Rect textRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (i + 8), NameRect.width, NameRect.height);
                labelStyle.alignment = TextAnchor.MiddleLeft;
                GUI.Label(textRect, keys[i].ToString() + ":");
                labelStyle.alignment = TextAnchor.MiddleRight;
                GUI.Label(textRect, "" + targetSpeaker.workSkills[keys[i]].level);
            }
        }
        else
        {
            List<EmployeeSkill.WorkSkill> keys = new List<EmployeeSkill.WorkSkill>(targetSpeaker.workSkills.Keys);
            for(int i = 0; i < keys.Count; i++)
            {
                Rect textRect = new Rect(TargetImageRect.x, NameRect.y+NameRect.height*(i+2), NameRect.width, NameRect.height);
                labelStyle.alignment = TextAnchor.MiddleLeft;
                GUI.Label(textRect, keys[i].ToString() + ":");
                labelStyle.alignment = TextAnchor.MiddleRight;
                GUI.Label(textRect, ""+targetSpeaker.workSkills[keys[i]].level);
            }
            Rect NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (keys.Count + 3), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Class:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, targetSpeaker.chosenClass.ToString());

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (keys.Count + 4), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Str:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Str]);

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (keys.Count + 5), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Dex:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Dex]);

            NewTextRect = new Rect(TargetImageRect.x, NameRect.y + NameRect.height * (keys.Count + 6), NameRect.width, NameRect.height);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            GUI.Label(NewTextRect, "Int:");
            labelStyle.alignment = TextAnchor.MiddleRight;
            GUI.Label(NewTextRect, "" + targetSpeaker.currentCharaStats[Humanoid.CharacterStatistics.Int]);
        }
        #endregion
        #region TopBox
        labelStyle.alignment = TextAnchor.LowerLeft;
        labelStyle.fontSize = Screen.height / 40;
        Rect TopTextBox = new Rect(TopBox.width*0.05f, TopBox.height*0.05f, TopBox.width*0.9f, TopBox.height*0.9f);
        GUI.Label(TopTextBox, displayText);
        #endregion
        #region BotBox
        labelStyle.fontSize = Screen.height / 40;
        labelStyle.alignment = TextAnchor.MiddleCenter;
        if (hasStatedSomething)
        {
            Rect exitButton = new Rect(BotBox.x, BotBox.y, BotBox.width, BotBox.height / 5f);
            GUI.color = Color.black;
            GUI.DrawTexture(exitButton, Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(new Rect(exitButton.x + 2, exitButton.y + 2, exitButton.width - 4, exitButton.height - 4), Texture2D.whiteTexture);
            GUI.color = Color.black;
            GUI.Label(exitButton, "Goodbye.");

            if (Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && exitButton.Contains(mousPos))
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                ExitMenu();
            }
        }
        else
        {
            Rect actionMenuRect = new Rect(BotBox.x, BotBox.y, BotBox.width / 5, BotBox.height / 5);
            Rect typeMenuRect = new Rect(actionMenuRect.x + actionMenuRect.width, actionMenuRect.y, actionMenuRect.width, actionMenuRect.height);
            Rect conceptMenuRect = new Rect(typeMenuRect.x + typeMenuRect.width, typeMenuRect.y, typeMenuRect.width, typeMenuRect.height);
            Rect confirmButton = new Rect(conceptMenuRect.x + conceptMenuRect.width, conceptMenuRect.y, conceptMenuRect.width, conceptMenuRect.height);
            dropDownMenus[0].Update(mousPos, actionMenuRect);
            dropDownMenus[1].Update(mousPos, typeMenuRect);
            dropDownMenus[dropDownMenus[1].selectedOption + 2].Update(mousPos, conceptMenuRect);

            GUI.color = Color.black;
            GUI.DrawTexture(new Rect(confirmButton.x - 2, confirmButton.y - 2, confirmButton.width + 4, confirmButton.height + 4), Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(confirmButton, Texture2D.whiteTexture);
            GUI.color = Color.black;
            GUI.Label(confirmButton, "Confirm");

            dropDownMenus[dropDownMenus[1].selectedOption + 2].Draw(mousPos, conceptMenuRect, labelStyle.fontSize);
            dropDownMenus[1].Draw(mousPos, typeMenuRect, labelStyle.fontSize);
            dropDownMenus[0].Draw(mousPos, actionMenuRect, labelStyle.fontSize);

            if (Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && confirmButton.Contains(mousPos))
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                StateThoughtToTarget();
            }
        }
        #endregion
    }

    void StateThoughtToTarget()
    {
        displayText += "\n\n" + thoughtString();
        hasStatedSomething = true;
        switch((DialogEngine.ConceptType)dropDownMenus[1].selectedOption)
        {
            case DialogEngine.ConceptType.Person:
                if (dropDownMenus[2].selectedOption == 0)
                {
                    displayText += "\n\n"+targetSpeaker.Name + ": " + MainMechanics.dialogEngine.getDialogString(targetSpeaker, 
                        (DialogEngine.ActionType)dropDownMenus[0].selectedOption, 
                        MainMechanics.mechanics.Innkeeper);
                }
                else
                {
                    displayText += "\n\n" + targetSpeaker.Name + ": " + "ERROR NULL PERSON";
                }
                return;
            case DialogEngine.ConceptType.Place:
                if ((Area.DungeonSetting)dropDownMenus[3].selectedOption > Area.DungeonSetting.Crypt)
                {
                    displayText += "\n\n" + targetSpeaker.Name + ": " + "ERROR NULL PLACE";
                }
                else
                {
                    displayText += "\n\n" + targetSpeaker.Name + ": " + MainMechanics.dialogEngine.getDialogString(targetSpeaker,
                        (DialogEngine.ActionType)dropDownMenus[0].selectedOption,
                        (Area.DungeonSetting)dropDownMenus[3].selectedOption);
                }
                return;

            case DialogEngine.ConceptType.Monster:
                displayText += "\n\n" + targetSpeaker.Name + ": " + MainMechanics.dialogEngine.getDialogString(targetSpeaker,
                        (DialogEngine.ActionType)dropDownMenus[0].selectedOption,
                        (Monster.MonsterType)dropDownMenus[4].selectedOption);
                return;

            case DialogEngine.ConceptType.Thing:
                displayText += "\n\n" + targetSpeaker.Name + ": " + MainMechanics.dialogEngine.getDialogString(targetSpeaker,
                        (DialogEngine.ActionType)dropDownMenus[0].selectedOption,
                        dropDownMenus[5].getID());
                return;
        }
        displayText += "\n\n" + targetSpeaker.Name + ": " + "State thought FULL ERROR";
    }

    public void UpdateHeroicFigures()
    {

    }

    string thoughtString()
    {
        string toSay = MainMechanics.mechanics.Innkeeper.Name + " (You): ";
        switch((DialogEngine.ActionType)dropDownMenus[0].selectedOption)
        {
            case DialogEngine.ActionType.Avoid:
                toSay += "Avoid ";
                break;

            case DialogEngine.ActionType.Seek:
                toSay += "Seek out ";
                break;

            case DialogEngine.ActionType.Fight:
                toSay += "Fight ";
                break;

            case DialogEngine.ActionType.Trust:
                toSay += "Trust in ";
                break;

            case DialogEngine.ActionType.Beware:
                toSay += "Beware ";
                break;
        }
        switch ((DialogEngine.ConceptType)dropDownMenus[1].selectedOption)
        {
            case DialogEngine.ConceptType.Person:
                if (dropDownMenus[2].selectedOption == 0)
                {
                    toSay += MainMechanics.mechanics.Innkeeper.Name;
                }
                else
                {
                    toSay += "ERROR NULL PERSON";
                }
                break;
            case DialogEngine.ConceptType.Place:
                if ((Area.DungeonSetting)dropDownMenus[3].selectedOption > Area.DungeonSetting.Crypt)
                {
                    toSay += "ERROR NULL PLACE";
                }
                else
                {
                    toSay += ((Area.DungeonSetting)dropDownMenus[3].selectedOption).ToString();
                }
                break;

            case DialogEngine.ConceptType.Monster:
                toSay += ((Monster.MonsterType)dropDownMenus[4].selectedOption).ToString();
                break;

            case DialogEngine.ConceptType.Thing:
                toSay += MainMechanics.dialogEngine.getPlural(dropDownMenus[5].getID());
                break;
        }
        return toSay;
    }
}
