﻿using UnityEngine;
using System.Collections;

public class NumberEffectDisplay
{
    Vector2 position;
    Texture2D image;
    int amount;
    float transparency;
    bool goingDown;
    bool onUI;
    bool mirror;

    public void Set(int _amount, Vector2 _position, Texture2D _image, bool _goingDown, bool _onUI, bool _mirror)
    {
        amount = _amount;
        position = _position;
        image = _image;
        goingDown = _goingDown;
        onUI = _onUI;
        transparency = 2;
        mirror = _mirror;
    }

    public bool Update()
    {
        if (goingDown)
        {
            if (onUI)
            {
                position += new Vector2(0, Time.deltaTime*50);
            }
            else
            {
                position -= new Vector2(0, Time.deltaTime*2f);
            }
        }
        else
        {
            if (onUI)
            {
                position -= new Vector2(0, Time.deltaTime*50);
            }
            else
            {
                position += new Vector2(0, Time.deltaTime*2f);
            }
        }
        transparency -= Time.deltaTime*4f;
        if(transparency <= 0)
        {
            return true;
        }
        return false;
    }

    public void Draw()
    {
        Vector2 displayPos;
        if(onUI)
        {
            displayPos = position;
        }
        else
        {
            displayPos = Camera.main.WorldToScreenPoint(position);
        }

        var labelStyle = GUI.skin.GetStyle("Label");
        labelStyle.fontSize = Screen.height / 30;
        Vector2 Size = labelStyle.CalcSize(new GUIContent(""+amount));
        GUI.color = new Color(1,1,1,transparency);
        Rect imageRect;
        if(onUI)
        {
            if (mirror)
            {
                imageRect = new Rect(displayPos.x - Size.y, displayPos.y, Size.y, Size.y);
            }
            else
            {
                imageRect = new Rect(displayPos.x, displayPos.y, Size.y, Size.y);
            }
        }
        else
        {
            if (mirror)
            {
                imageRect = new Rect(displayPos.x - Size.y, Screen.height - displayPos.y, Size.y, Size.y);
            }
            else
            {
                imageRect = new Rect(displayPos.x , Screen.height - displayPos.y, Size.y, Size.y);
            }
        }
        GUI.DrawTexture(imageRect, image);
        Rect textRect;
        if(mirror)
        {
            textRect = new Rect(imageRect.x - Size.x, imageRect.y, Size.x, Size.y);
        }
        else
        {
            textRect = new Rect(imageRect.x + imageRect.width, imageRect.y, Size.x, Size.y);
        }
        if(amount < 0)
        {
            GUI.color = new Color(1, 0, 0, transparency);
        }
        else
        {
            GUI.color = new Color(0, 1, 0, transparency);
        }
        GUI.Label(textRect, ""+amount);
    }
}
