﻿using UnityEngine;
using System.Collections;

public class Menu
{

	// Use this for initialization
	public virtual void Start ()
    {
	
	}

    // Update is called once per frame
    public virtual void Update ()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ExitMenu();
        }
    }

    public virtual void Draw(Vector2 mousPos)
    {

    }

    public virtual void ExitMenu()
    {
        MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
    }
}
