﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropdownMenu
{
	List<string> options;
	bool isOpen = false;
	public int selectedOption;

	float scroll = 0;

	public void Set(List<string> _options)
	{
		options = _options;
	}

	public void Update(Vector2 MousPos, Rect baseRect)
	{
		if(isOpen)
		{
			if(baseRect.y+2+(options.Count+1)*baseRect.height <= Screen.height)//Needs no Scroll
			{
				for(int i = 0; i < options.Count; i++)
				{
					Rect optionRect = new Rect(baseRect.x, baseRect.y+baseRect.height*(i+1)+2, baseRect.width, baseRect.height);
					
					if(Input.GetMouseButtonDown(0) && optionRect.Contains(MousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
					{
						MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
						selectedOption = i;
						isOpen = false;
					}
				}
			}
			else//Needs Scroll
			{
				//Debug.Log(Screen.height - baseRect.y);
				float yLength = baseRect.height*options.Count;
				Rect viewRect = new Rect(baseRect.x-1, baseRect.y-1+baseRect.height, baseRect.width+2, Screen.height - baseRect.y+2);
				GUI.BeginGroup(viewRect);
				for(int i = 0; i < options.Count; i++)
				{
					Rect optionRect = new Rect(1, baseRect.height*(i)+2 - scroll, baseRect.width, baseRect.height);
					
					if(optionRect.Contains(MousPos-new Vector2(viewRect.x, viewRect.y)))
					{
						if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
						{
							MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
							selectedOption = i;
							isOpen = false;
						}
						
						if(Input.GetAxis("Mouse ScrollWheel") != 0 && MainMechanics.mechanics.currentButtonWait <= 0)
						{
							MainMechanics.mechanics.currentButtonWait = 0.0001f;
							scroll -= Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
							if(scroll < 0)
							{
								scroll = 0;
							}
							else if(scroll > yLength - (Screen.height - viewRect.y))
							{
								scroll = yLength - (Screen.height - viewRect.y);
							}
							
						}
					}
					
				}
				GUI.EndGroup();
			}
			
			if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				isOpen = false;
			}
		}
		else
		{
			if(Input.GetMouseButtonDown(0) && baseRect.Contains(MousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				isOpen = true;
			}
		}
	}

	public void Draw(Vector2 MousPos, Rect baseRect, int fontSize)
	{
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = fontSize;
		labelStyle.alignment = TextAnchor.MiddleCenter;

		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(baseRect.x-2, baseRect.y-2, baseRect.width+4, baseRect.height+4), Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(baseRect, Texture2D.whiteTexture);

		GUI.color = Color.black;
		GUI.Label(baseRect, options[selectedOption]);

		if(isOpen)
		{
			if(baseRect.y+2+(options.Count+1)*baseRect.height <= Screen.height)//Needs no Scroll
			{
				for(int i = 0; i < options.Count; i++)
				{
					Rect optionRect = new Rect(baseRect.x, baseRect.y+baseRect.height*(i+1)+2, baseRect.width, baseRect.height);
					GUI.color = Color.black;
					GUI.DrawTexture(new Rect(optionRect.x-1, optionRect.y-1, optionRect.width+2, optionRect.height+2), Texture2D.whiteTexture); 
					GUI.color = Color.white;
					GUI.DrawTexture(optionRect, Texture2D.whiteTexture);
					GUI.color = Color.black;
					GUI.Label(optionRect, options[i]);

					/*if(Input.GetMouseButtonDown(0) && optionRect.Contains(MousPos) && MainMechanics.mechanics.buttonWait <= 0)
					{
						MainMechanics.mechanics.buttonWait = MainMechanics.mechanics.buttonWaitAmmount;
						selectedOption = i;
						isOpen = false;
					}*/
				}
			}
			else//Needs Scroll
			{
				//Debug.Log(Screen.height - baseRect.y);
				float yLength = baseRect.height*options.Count;
				Rect viewRect = new Rect(baseRect.x-1, baseRect.y-1+baseRect.height, baseRect.width+2, Screen.height - baseRect.y+2);
				GUI.BeginGroup(viewRect);
				for(int i = 0; i < options.Count; i++)
				{
					Rect optionRect = new Rect(1, baseRect.height*(i)+2 - scroll, baseRect.width, baseRect.height);
					GUI.color = Color.black;
					GUI.DrawTexture(new Rect(optionRect.x-1, optionRect.y-1, optionRect.width+2, optionRect.height+2), Texture2D.whiteTexture); 
					GUI.color = Color.white;
					GUI.DrawTexture(optionRect, Texture2D.whiteTexture);
					GUI.color = Color.black;
					GUI.Label(optionRect, options[i]);
					
					/*if(optionRect.Contains(MousPos-new Vector2(viewRect.x, viewRect.y)))
					{
						if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.buttonWait <= 0)
						{
							MainMechanics.mechanics.buttonWait = MainMechanics.mechanics.buttonWaitAmmount;
							selectedOption = i;
							isOpen = false;
						}

						if(Input.GetAxis("Mouse ScrollWheel") != 0 && MainMechanics.mechanics.buttonWait <= 0)
						{
							MainMechanics.mechanics.buttonWait = 0.0001f;
							scroll -= Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
							if(scroll < 0)
							{
								scroll = 0;
							}
							else if(scroll > yLength - (Screen.height - viewRect.y))
							{
								scroll = yLength - (Screen.height - viewRect.y);
							}

						}
					}*/

				}
				GUI.EndGroup();
			}

			/*if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.buttonWait <= 0)
			{
				MainMechanics.mechanics.buttonWait = MainMechanics.mechanics.buttonWaitAmmount;
				isOpen = false;
			}*/
		}
		/*else
		{
			if(Input.GetMouseButtonDown(0) && baseRect.Contains(MousPos) && MainMechanics.mechanics.buttonWait <= 0)
			{
				MainMechanics.mechanics.buttonWait = MainMechanics.mechanics.buttonWaitAmmount;
				isOpen = true;
			}
		}*/
	}

    public virtual FurnishingLibrary.HoldableID getID()
    {
        return 0;
    }
}
