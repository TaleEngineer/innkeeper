﻿using UnityEngine;
using System.Collections;

public class MenuButton
{
    public static GUIStyle labelStyle;
    public Rect containingRect;
    Texture2D buttonDisplay;
    public bool displayTooltip;
    public string tooltip;

    public virtual void Set(Texture2D texture, string _tooltip)
    {
        buttonDisplay = texture;
        tooltip = _tooltip;
    }

    public virtual void Draw(Rect mainRect)
    {

        containingRect = mainRect;
        GUI.color = Color.white;
        GUI.DrawTexture(containingRect, buttonDisplay);
    }

    public virtual void DrawTooltip(Vector2 UIMousPos)
    {
        if (labelStyle == null)
        {
            labelStyle = GUI.skin.GetStyle("Label");
        }

        labelStyle.fontSize = Screen.height / 50;
        labelStyle.alignment = TextAnchor.UpperLeft;


        if (displayTooltip)
        {
            float yPos = UIMousPos.y - Screen.height / 4;
            float xPos = UIMousPos.x - Screen.height / 4;
            if (yPos < 0)
            {
                yPos = UIMousPos.y;
            }
            if(xPos < 0)
            {
                xPos = UIMousPos.x;
            }

            Rect toolRect = new Rect(xPos, yPos, Screen.height / 4, Screen.height / 4);

            GUI.color = new Color(0, 0, 0, 0.75f);
            GUI.DrawTexture(toolRect, Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.Label(toolRect, tooltip);
        }
    }

    public virtual bool Update(Vector2 UIMousPos)
    {
        if(containingRect.Contains(UIMousPos))
        {
            displayTooltip = true;
            if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                return true;
            }
        }
        else
        {
            displayTooltip = false;
        }

        return false;
    }
	
}
