﻿using UnityEngine;
using System.Collections;

public class FurnishingButton : CostingButton
{
    FurnishingLibrary.FurnishingID ID;

    public void Set(Texture2D texture, string _tooltip, FurnishingLibrary.FurnishingID _ID)
    {
        base.Set(texture, _tooltip);
        ID = _ID;

        GameObject Object = FurnishingLibrary.Library.getFurnishingPrefab(ID);
        cost = Object.GetComponent<Furnishing>().cost;
    }

    public override void DrawTooltip(Vector2 UIMousPos)
    {

        if (labelStyle == null)
        {
            labelStyle = GUI.skin.GetStyle("Label");
        }

        labelStyle.fontSize = Screen.height / 50;
        labelStyle.alignment = TextAnchor.UpperLeft;
        Vector2 size = labelStyle.CalcSize(new GUIContent("Xy"));

        if (displayTooltip)
        {
            float yPos = UIMousPos.y - Screen.height / 4;
            float xPos = UIMousPos.x - Screen.height / 4;
            if (yPos < 0)
            {
                yPos = UIMousPos.y;
            }
            if (xPos < 0)
            {
                xPos = UIMousPos.x;
            }

            Rect toolRect = new Rect(xPos, yPos, Screen.height / 4, Screen.height / 4);
            GUI.color = new Color(0, 0, 0, 0.75f);
            GUI.DrawTexture(toolRect, Texture2D.whiteTexture);

            GUI.color = Color.white;

            Rect currencySymbolRect = new Rect(toolRect.x, toolRect.y, size.y, size.y);
            Rect costArea = new Rect(toolRect.x+currencySymbolRect.width, toolRect.y, toolRect.width-currencySymbolRect.width, currencySymbolRect.height);
            Rect textRect = new Rect(toolRect.x, toolRect.y + costArea.height, toolRect.width, toolRect.height - costArea.height);
            GUI.DrawTexture(currencySymbolRect, MainMechanics.mechanics.CurrencySymbol);
            if(cost > MainMechanics.mechanics.currency)
            {
                GUI.color = Color.red;
            }
            GUI.Label(costArea, ""+cost);
            GUI.color = Color.white;
            GUI.Label(textRect, tooltip);
        }
    }

    public FurnishingLibrary.FurnishingID FurnishingUpdate(Vector2 UIMousPos)
    {
        if(base.Update(UIMousPos))
        {
            return ID;
        }
        return FurnishingLibrary.FurnishingID.Error;
    }
}
