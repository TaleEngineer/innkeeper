﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CellarStairs : InteractableFurnishing
{
    public static Dictionary<FurnishingLibrary.HoldableID, List<Holdable>> CellarStorage;
    public List<FurnishingLibrary.HoldableID> toStore = new List<FurnishingLibrary.HoldableID>();
    static DropdownMenu toAddDrop;
    Rect toGatherArea;
    Rect background;

    public static float heightOfThing = 19f / 96f;

    float storageScroll = 0;
    float toStoreScroll = 0;

    int storageY = 0;

    public override void Start()
    {
        if (toAddDrop == null)
        {
            toAddDrop = new DropdownMenu();
            List<string> option = new List<string>();
            for (int i = 0; i < 7; i++)
            {
                if (i == 0)
                {
                    option.Add("");
                }
                else
                {
                    option.Add(((FurnishingLibrary.HoldableID)i).ToString());
                }
            }

            toAddDrop.Set(option);
        }
    }

    public override void getInteraction(Humanoid interacter, int actionNumber)
    {
        if(actionNumber == 0)//OPEN MENU
        {
            //interacter.startWorking(1f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
            MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
            MainMechanics.mechanics.employeeFurnish = this;
        }
        else if(actionNumber == 1)//PLACE HELD 0
        {
            FromHandToStorage(interacter, 0);
        }
        else if (actionNumber == 2)//PLACE HELD 1
        {
            FromHandToStorage(interacter, 1);
        }
        else if(actionNumber < 0)
        {
            FromStorageToHand(interacter, (FurnishingLibrary.HoldableID) (-actionNumber));
        }
    }

    public override void CompleteInteract(Humanoid interacter, int choice)
    {
        if (choice == 0)
        {
            MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
            MainMechanics.mechanics.employeeFurnish = this;
        }
        else if (choice > 0)
        {
            FromHandToStorage(interacter, choice - 1);
        }
        else
        {
            FromStorageToHand(interacter, (FurnishingLibrary.HoldableID)(choice * -1));
        }

    }

    void FromHandToStorage(Humanoid interacter, int handNumber)
    {
        if (handNumber < 0 || handNumber > 1)
        {
            return;
        }

        List<Holdable> output;
        if(CellarStorage.TryGetValue(interacter.heldObjects[handNumber].holdableID, out output) && output != null)
        {
            output.Add(interacter.heldObjects[handNumber]);
            interacter.heldObjects[handNumber].gameObject.SetActive(false);
            interacter.heldObjects[handNumber] = null;
        }
        else
        {
            output = new List<Holdable>();
            output.Add(interacter.heldObjects[handNumber]);
            interacter.heldObjects[handNumber].gameObject.SetActive(false);
            CellarStorage.Add(interacter.heldObjects[handNumber].holdableID, output);
            interacter.heldObjects[handNumber] = null;
        }
    }

    void FromStorageToHand(Humanoid interacter, FurnishingLibrary.HoldableID ID)
    {
        int handNumber = -1;
        if(interacter.heldObjects[0] == null)
        {
            handNumber = 0;
        }
        else if(interacter.heldObjects[1] == null)
        {
            handNumber = 1;
        }

        if(handNumber == -1)
        {
            return;
        }

        List<Holdable> output;
        if(CellarStorage.TryGetValue(ID, out output) && output != null)
        {
            interacter.heldObjects[handNumber] = output[0];
            interacter.heldObjects[handNumber].gameObject.SetActive(true);
            output.RemoveAt(0);
            if(output.Count == 0)
            {
                CellarStorage.Remove(ID);
            }
        }
    }

    public override void DrawMenu(Vector2 UIMousPos, float PickUpScroll)
    {
        var labelStyle = GUI.skin.GetStyle("Label");
        labelStyle.fontSize = Screen.height / 40;
        GUIContent textSizer = new GUIContent("Xy");
        Vector2 textSize = labelStyle.CalcSize(textSizer);


        Rect lowerBG = new Rect(0, Screen.height - Screen.height * heightOfThing, Screen.width / 2 + Screen.height / 8, Screen.height * heightOfThing);
        Rect LeftHandUseRect = new Rect(Screen.width / 2 - Screen.height / 8, lowerBG.y, Screen.height / 8, lowerBG.height);
        Rect RightHandUseRect = new Rect(Screen.width / 2, lowerBG.y, Screen.height / 8, lowerBG.height);


        GUI.color = Color.black;
        GUI.DrawTexture(toGatherArea, Texture2D.whiteTexture);
        GUI.color = Color.white;
        GUI.DrawTexture(background, Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(toGatherArea.x + 1, toGatherArea.y + 1, toGatherArea.width - 2, toGatherArea.height - 2), Texture2D.whiteTexture);

        int x = 0;
        int y = 0;
        float boxHeight = background.width/5f;
        List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(CellarStorage.Keys);
        for (int i = 0; i < keys.Count; i++)
        {
            Rect mainRect = new Rect(x * boxHeight, y * boxHeight + storageScroll, boxHeight, boxHeight);
            GUI.color = Color.black;
            GUI.DrawTexture(mainRect, Texture2D.whiteTexture);
            GUI.color = Color.white;
            GUI.DrawTexture(new Rect(mainRect.x + 1, mainRect.y + 1, mainRect.width - 2, mainRect.height - 2), Texture2D.whiteTexture);

            Rect imageRect = new Rect(mainRect.x + boxHeight / 4, mainRect.y, boxHeight / 2, boxHeight / 2);
            GUI.DrawTexture(imageRect, CellarStorage[keys[i]][0].holdableTexture);

            labelStyle.alignment = TextAnchor.UpperCenter;
            Rect textRect = new Rect(mainRect.x, imageRect.y + imageRect.height, mainRect.width, textSize.y);
            Rect amountRect = new Rect(mainRect.x, textRect.y + textRect.height, mainRect.width, textSize.y);
            GUI.color = Color.black;
            GUI.Label(textRect, keys[i].ToString());
            GUI.Label(amountRect, "x" + CellarStorage[keys[i]].Count);

            if (Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && mainRect.Contains(UIMousPos) && !lowerBG.Contains(UIMousPos))
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                FromStorageToHand(MainMechanics.mechanics.Innkeeper, keys[i]);
            }

            x++;
            if ((x + 1) > 5 && i < keys.Count - 1)
            {
                y++;
                x = 0;
            }
        }
        storageY = y;





        GUI.color = Color.black;
        GUI.DrawTexture(lowerBG, Texture2D.whiteTexture);
        GUI.color = Color.white;
        GUI.DrawTexture(new Rect(lowerBG.x+1, lowerBG.y+1, lowerBG.width-2, lowerBG.height-2), Texture2D.whiteTexture);

        GUI.color = new Color(0, 0.231f, 0.345f);
        GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
        GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
        GUI.color = new Color(0, 0.486f, 0.729f);
        GUI.DrawTexture(new Rect(LeftHandUseRect.x + 1, LeftHandUseRect.y + 1, LeftHandUseRect.width - 2, LeftHandUseRect.height - 2), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(RightHandUseRect.x + 1, RightHandUseRect.y + 1, RightHandUseRect.width - 2, RightHandUseRect.height - 2), Texture2D.whiteTexture);
        GUI.color = Color.white;
        if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
        {
            Rect heldLeft = new Rect(LeftHandUseRect.x, LeftHandUseRect.y, LeftHandUseRect.width, LeftHandUseRect.width);
            GUI.color = Color.white;
            GUI.DrawTexture(heldLeft, MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableTexture);

            if(Input.GetMouseButtonDown(0) && heldLeft.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                FromHandToStorage(MainMechanics.mechanics.Innkeeper, 0);
            }
        }
        if (MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
        {
            Rect heldRight = new Rect(RightHandUseRect.x, RightHandUseRect.y, RightHandUseRect.width, RightHandUseRect.width);
            GUI.color = Color.white;
            GUI.DrawTexture(heldRight, MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableTexture);

            if (Input.GetMouseButtonDown(0) && heldRight.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                FromHandToStorage(MainMechanics.mechanics.Innkeeper, 1);
            }
        }

        labelStyle.alignment = TextAnchor.MiddleCenter;
        for (int i = 0; i <= toStore.Count; i++)
        {
            if (i < toStore.Count)
            {
                Rect toFindRect = new Rect(toGatherArea.x, toGatherArea.y + Screen.height / 20 * i + toStoreScroll, toGatherArea.width, Screen.height/20);
                Rect cancelButton = new Rect(toFindRect.x+toFindRect.width-toFindRect.height, toFindRect.y, toFindRect.height, toFindRect.height);
                GUI.color = Color.black;
                GUI.DrawTexture(toFindRect, Texture2D.whiteTexture);
                GUI.color = Color.white;
                GUI.DrawTexture(new Rect(toFindRect.x + 1, toFindRect.y + 1, toFindRect.width - 2, toFindRect.height - 2), Texture2D.whiteTexture);

                GUI.color = Color.black;
                GUI.DrawTexture(cancelButton, Texture2D.whiteTexture);
                GUI.color = Color.white;
                GUI.DrawTexture(new Rect(cancelButton.x + 1, cancelButton.y + 1, cancelButton.width - 2, cancelButton.height - 2), Texture2D.whiteTexture);

                GUI.color = Color.black;
                GUI.Label(toFindRect, toStore[i].ToString());
                GUI.Label(cancelButton, "X");

                if (Input.GetMouseButtonDown(0) && cancelButton.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
                {
                    MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                    toStore.RemoveAt(i);
                }
            }
            else
            {
                Rect toSetRect = new Rect(toGatherArea.x, toGatherArea.y + Screen.height / 20 * i + toStoreScroll, toGatherArea.width, Screen.height / 20);

                toAddDrop.Draw(UIMousPos, toSetRect, labelStyle.fontSize);

                toAddDrop.Update(UIMousPos, toSetRect);
            }
        }
    }

    public override void UpdateDrawMenu(Vector2 UIMousPos)
    {

        background = new Rect(0, 0, Screen.width / 2 + Screen.height / 8, Screen.height - Screen.height * heightOfThing);
        toGatherArea = new Rect(Screen.width / 2 + Screen.height / 8, 0, Screen.width / 2 - Screen.height / 8, Screen.height);

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            if(background.Contains(UIMousPos))
            {
                storageScroll += Input.GetAxis("Mouse ScrollWheel") * (Screen.height);
            }
            else if(toGatherArea.Contains(UIMousPos))
            {
                toStoreScroll += Input.GetAxis("Mouse ScrollWheel") * (Screen.height);
            }
        }
        if (toStoreScroll > 0 || toStore.Count < 19)
        {
            toStoreScroll = 0;
        }
        else if (toStoreScroll < -(toStore.Count - 19) * Screen.height * 0.05f )
        {
            toStoreScroll = -(toStore.Count - 19) * Screen.height * 0.05f;
        }

        if (storageScroll > 0 || storageY+1 < background.height/(background.width/5f))
        {
            storageScroll = 0;
        }
        else if (storageScroll < -(storageY+1 - background.height / (background.width / 5f)) * background.width * 0.2f)
        {
            storageScroll = -(storageY + 1 - background.height / (background.width / 5f)) * background.width * 0.2f;
        }

        if(toAddDrop.selectedOption != 0)
        {
            if (!toStore.Contains((FurnishingLibrary.HoldableID)toAddDrop.selectedOption))
            {
                toStore.Add((FurnishingLibrary.HoldableID)toAddDrop.selectedOption);
            }
            toAddDrop.selectedOption = 0;
        }
    }

    public override bool getWorkOrder()
    {
        if(assignedEmployee.heldObjects[0] != null && toStore.Contains(assignedEmployee.heldObjects[0].holdableID))
        {
            WorkOrder toAdd = new WorkOrder();
            toAdd.Set(FurnishingID, 1, assignedEmployee, this);
            assignedEmployee.currentWorkOrder.Add(toAdd);
            return false;
        }
        if (assignedEmployee.heldObjects[1] != null && toStore.Contains(assignedEmployee.heldObjects[1].holdableID))
        {
            WorkOrder toAdd = new WorkOrder();
            toAdd.Set(FurnishingID, 2, assignedEmployee, this);
            assignedEmployee.currentWorkOrder.Add(toAdd);
            return false;
        }

        for (int i = 0; i < toStore.Count; i++)
        {
            List<Holdable> whoCares;
            if(MainMechanics.mechanics.HoldablesOnGround.TryGetValue(toStore[i], out whoCares))
            {
                WorkOrder toAdd = new WorkOrder();
                toAdd.Set(toStore[i], assignedEmployee, this);
                assignedEmployee.currentWorkOrder.Add(toAdd);
                return false;
            }
        }

        return true;
    }
}
