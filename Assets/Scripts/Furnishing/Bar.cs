﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bar : InteractableFurnishing 
{
	public List<Order> orders = new List<Order>();
	public List<Order> assignedOrders = new List<Order>();

	public Liquid[] liquidsOnTap = new Liquid[5];
	public int[] ammountOnTap = new int[5];

	public List<Holdable> emptyCups = new List<Holdable>();
	public Dictionary<FurnishingLibrary.HoldableID, List<Holdable>> filledDrinks = new Dictionary<FurnishingLibrary.HoldableID, List<Holdable>>();

	Rect filledDrinksHereRect = new Rect(1,1, Screen.width/4-2, Screen.height/2-2);
	Rect emptyCupsHereRect = new Rect(1, Screen.height/2+1, Screen.width/4-2, Screen.height/2-2);
	Rect mainArea = new Rect(Screen.width/4+1, 1, Screen.width/2-2, Screen.height/2-2);
	Rect orderArea = new Rect(Screen.width*3/4+1, 1, Screen.width/3-2, Screen.height-2);
	Rect botArea = new Rect(Screen.width/4+1, Screen.height/2+1, Screen.width/2-2, Screen.height/2-2);

	Vector2 mousPos = new Vector2();
	float filledCupsScroll;
	float ordersScroll;


    public override void Start()
    {
        roomInsideOf = MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere.parentRoom;

        if (!roomInsideOf.barsHere.Contains(this))
        {
            roomInsideOf.barsHere.Add(this);
        }
    }

    public void UpdateAvailableDrinks()
    {
        if(roomInsideOf != null)
        {
            roomInsideOf.updateDrinksAvailable();
        }
    }

	public override void Update ()
	{
        base.Update();
		for(int i = 0; i < 5; i++)
		{
			if(liquidsOnTap[i] != null && ammountOnTap[i] <= 0)
			{
				liquidsOnTap[i] = null;
                UpdateAvailableDrinks();
			}
		}

		if(MainMechanics.mechanics.employeeFurnish == this && MainMechanics.mechanics.currentScreenState == MainMechanics.screenState.FurnishingMenu)
		{
			if(filledDrinksHereRect.Contains(mousPos) && Input.GetAxis("Mouse ScrollWheel") != 0)
			{
				filledCupsScroll -= Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
				
				checkScroll();
			}
			else if(orderArea.Contains(mousPos) && Input.GetAxis("Mouse ScrollWheel") != 0)
			{
				ordersScroll -= Input.GetAxis("Mouse ScrollWheel")*(Screen.height);
				
				checkScroll();
			}
		}



        List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(filledDrinks.Keys);
        Dictionary<FurnishingLibrary.HoldableID, int> tempList = new Dictionary<FurnishingLibrary.HoldableID, int>();
        for (int i = 0; i < ammountKeys.Count; i++)
        {
            tempList.Add(ammountKeys[i], filledDrinks[ammountKeys[i]].Count);
        }
        for (int i = 0; i < orders.Count; i++)
		{
            bool toRemove = false;
            if (orders[i].hasBeenCalled && !orders[i].checkItemsLeft())
            {
                toRemove = true;
            }
            if (filledDrinks.Count > 0)
            {
                if (!orders[i].hasBeenCalled)
                {
                    if (canCallOrder(i, tempList) && orders[i].orderedFrom.assignedEmployee != null)
                    {
                        bool hasOrder = false;
                        for (int o = 0; o < orders[i].orderedFrom.assignedEmployee.currentWorkOrder.Count; o++)
                        {
                            if (orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate != null
                               && (orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate.FurnishingID == FurnishingLibrary.FurnishingID.Bar
                                || orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingIDToFind == FurnishingLibrary.FurnishingID.Bar))
                            {
                                hasOrder = true;
                                break;
                            }
                        }
                        if (!hasOrder)
                        {
                            WorkOrder work = new WorkOrder();
                            work.Set(this, 4, orders[i].orderedFrom.assignedEmployee, this);
                            orders[i].orderedFrom.assignedEmployee.currentWorkOrder.Add(work);
                        }
                    }
                    //callOrder(i, orders[i].orderedFrom.assignedEmployee);
                }
            }
            if (toRemove)
            {
                assignedOrders.Remove(orders[i]);
                orders.RemoveAt(i);
                i--;
            }
        }



		for(int i = 0; i < assignedOrders.Count; i++)
		{
			if(assignedOrders[i].orderedFrom.assignedEmployee != null
			   && (assignedOrders[i].orderedFrom.assignedEmployee.heldObjects[0] == null || assignedOrders[i].orderedFrom.assignedEmployee.heldObjects[1] == null))
			{
				bool hasPickUpOrder = false;
				for(int o = 0; o < assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder.Count; o++)
				{
					if(assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate != null
					   && assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate.FurnishingID == FurnishingLibrary.FurnishingID.Bar)
					{
						hasPickUpOrder = true;
						break;
					}
				}
				if(!hasPickUpOrder)
				{
					WorkOrder work = new WorkOrder();
					work.Set(this, 5, assignedOrders[i].orderedFrom.assignedEmployee, this);
					assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder.Add(work);
				}
			}
		}



	}

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		int whereFill = getWhereFill(interacter);


		if(actionNumber == 0)//Open Menu
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
		}
		else if(actionNumber == 1)//Fill Tap
		{
			int length = 0;
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				if(interacter.heldObjects[0].liquidHere != null 
				   && (liquidsOnTap[whereFill] == null || interacter.heldObjects[0].liquidHere.liquidID == liquidsOnTap[whereFill].liquidID))
				{
					length += interacter.heldObjects[0].currentLiquidContainment;
				}
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				if(interacter.heldObjects[1].liquidHere != null 
				   && (liquidsOnTap[whereFill] == null || interacter.heldObjects[1].liquidHere.liquidID == liquidsOnTap[whereFill].liquidID))
				{
					length += interacter.heldObjects[1].currentLiquidContainment;
				}
			}
			
			if(length > maxLiquidContainment - ammountOnTap[whereFill])
			{
				interacter.startWorking((maxLiquidContainment - ammountOnTap[whereFill])/8f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
			else
			{
				interacter.startWorking(length/8f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
		}
		else if(actionNumber == 2)//Fill Cup
		{
			if(liquidsOnTap[whereFill] != null)
			{
				int length = 0;
				if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
				{
					if(interacter.heldObjects[0].liquidHere != null 
					   && interacter.heldObjects[0].liquidHere.liquidID == liquidsOnTap[whereFill].liquidID
					   && interacter.heldObjects[0].currentLiquidContainment < interacter.heldObjects[0].maxLiquidContainment)
					{
						length += interacter.heldObjects[0].maxLiquidContainment - interacter.heldObjects[0].currentLiquidContainment;
					}
					else if(interacter.heldObjects[0].liquidHere == null)
					{
						length += interacter.heldObjects[0].maxLiquidContainment;
					}
				}
				
				if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
				{
					if(interacter.heldObjects[1].liquidHere != null 
					   && interacter.heldObjects[1].liquidHere.liquidID == liquidsOnTap[whereFill].liquidID
					   && interacter.heldObjects[1].currentLiquidContainment < interacter.heldObjects[1].maxLiquidContainment)
					{
						length += interacter.heldObjects[1].maxLiquidContainment - interacter.heldObjects[1].currentLiquidContainment;
					}
					else if(interacter.heldObjects[1].liquidHere == null)
					{
						length += interacter.heldObjects[1].maxLiquidContainment;
					}
				}
				
				if(length > ammountOnTap[whereFill])
				{
					interacter.startWorking(ammountOnTap[whereFill]/4f, actionNumber, this, EmployeeSkill.WorkSkill.Bartending, 1);
				}
				else
				{
					interacter.startWorking(length/4f, actionNumber, this, EmployeeSkill.WorkSkill.Bartending, 1);
				}
			}
		}
		else if(actionNumber == 3)//Place Orders
		{
			placeOrders(interacter);
		}
		else if(actionNumber == 4)//Call Order
		{
			callOrder(interacter);
		}
		else if(actionNumber == 5)//Get Called Order
		{
			getCalledOrder(interacter);
		}
		else if(actionNumber == 6)//Get Empty Cup
		{
			getEmptyCup(interacter);
		}
		else if(actionNumber == 1000)
		{
			putInHandDown(interacter, true);
		}
		else if(actionNumber == -1000)
		{
			putInHandDown(interacter, false);
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		int whereFill = getWhereFill(interacter);

		if(choice == 1)//Fill Tap
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				interacter.heldObjects[0].PourLiquidIntoFurnishing(this, whereFill);
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				interacter.heldObjects[1].PourLiquidIntoFurnishing(this, whereFill);
			}
		}
		else if(choice == 2)//Fill Held
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[0]);
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[1]);
			}
		}
	}

	public override void Draw ()
	{
		for(int i = 0; i < 5; i++)
		{
			if(liquidsOnTap[i] != null)
			{
				int placement = i-2;
				Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position+transform.TransformDirection(new Vector2(placement, -0.5f)));
				midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
				
				float halfSize = Screen.height/(Camera.main.orthographicSize*2);
				
				Rect drawRect = new Rect(midPoint-new Vector2(halfSize/7, halfSize/2)/2, new Vector2(halfSize/7,halfSize/2));
				GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidBar);
				
				drawRect = new Rect(new Vector2(drawRect.x+1, drawRect.y+1+(drawRect.height-2)*(maxLiquidContainment-ammountOnTap[i])/maxLiquidContainment), new Vector2(drawRect.width-2, (drawRect.height-2)*ammountOnTap[i]/maxLiquidContainment));
				GUI.color = liquidsOnTap[i].liquidColor;
				GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidMarker);
				
				GUI.color = Color.white;
			}
		}
	}

	public override void DrawMenu (Vector2 UIMousPos, float PickUpScroll)
	{
		mousPos = UIMousPos;
		GUI.color = Color.black;
		Rect blackBack = new Rect(0,0,Screen.width, Screen.height);
		GUI.DrawTexture(blackBack, Texture2D.whiteTexture);

		GUI.color = Color.white;
		filledDrinksHereRect = new Rect(1,1, Screen.width/4-2, Screen.height/2-2);
		emptyCupsHereRect = new Rect(1, Screen.height/2+1, Screen.width/4-2, Screen.height/2-2);
		mainArea = new Rect(Screen.width/4+1, 1, Screen.width/2-2, Screen.height/2-2);
		orderArea = new Rect(Screen.width*3/4+1, 1, Screen.width/4-2, Screen.height-2);
		botArea = new Rect(Screen.width/4+1, Screen.height/2+1, Screen.width/2-2, Screen.height/2-2);
		GUI.DrawTexture(filledDrinksHereRect, Texture2D.whiteTexture);
		GUI.DrawTexture(emptyCupsHereRect, Texture2D.whiteTexture);
		GUI.DrawTexture(mainArea, Texture2D.whiteTexture);
		GUI.DrawTexture(orderArea, Texture2D.whiteTexture);
		GUI.DrawTexture(botArea, Texture2D.whiteTexture);


		Rect LeftHandUseRect = new Rect(Screen.width/2-Screen.height/8, Screen.height-Screen.height/4, Screen.height/8, Screen.height/4);
		Vector2 leftMidPoint = new Vector2(LeftHandUseRect.x, LeftHandUseRect.y)+ new Vector2(LeftHandUseRect.width, LeftHandUseRect.height)/2;
		Rect leftHeldImage = new Rect(leftMidPoint-new Vector2(LeftHandUseRect.width, LeftHandUseRect.width)/2,new Vector2(LeftHandUseRect.width, LeftHandUseRect.width));

		Rect RightHandUseRect = new Rect(Screen.width/2, Screen.height-LeftHandUseRect.height, LeftHandUseRect.width, LeftHandUseRect.height);
		Vector2 rightMidPoint = new Vector2(RightHandUseRect.x, RightHandUseRect.y)+ new Vector2(RightHandUseRect.width, RightHandUseRect.height)/2;
		Rect rightHeldImage = new Rect(rightMidPoint-new Vector2(RightHandUseRect.width, RightHandUseRect.width)/2,new Vector2(RightHandUseRect.width, RightHandUseRect.width));

		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
		GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(new Rect(LeftHandUseRect.x+1, LeftHandUseRect.y+1, LeftHandUseRect.width-2, LeftHandUseRect.height-2), Texture2D.whiteTexture);
		GUI.DrawTexture(new Rect(RightHandUseRect.x+1, RightHandUseRect.y+1, RightHandUseRect.width-2, RightHandUseRect.height-2), Texture2D.whiteTexture);

		List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(filledDrinks.Keys);

		mousPos = UIMousPos;
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleLeft;

		GUI.BeginGroup(filledDrinksHereRect);
		for(int i = 0; i < filledDrinks.Count; i++)
		{
			Rect cupImage = new Rect(filledDrinksHereRect.x, 
			                         filledDrinksHereRect.y+filledDrinksHereRect.height/10*i-filledCupsScroll,
			                         filledDrinksHereRect.height/10, filledDrinksHereRect.height/10);

			Rect filledCup = new Rect(filledDrinksHereRect.x, 
			                          filledDrinksHereRect.y+filledDrinksHereRect.height/10*i-filledCupsScroll, 
			                          filledDrinksHereRect.width, filledDrinksHereRect.height/10);

			Rect textRect = new Rect(filledDrinksHereRect.x+filledDrinksHereRect.height/10, 
			                          filledDrinksHereRect.y+filledDrinksHereRect.height/10*i-filledCupsScroll, 
			                          filledDrinksHereRect.width, filledDrinksHereRect.height/10);


			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(filledCup.x, filledCup.y-1, filledCup.width, filledCup.height+2), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(filledCup, Texture2D.whiteTexture);
			GUI.color = Color.black;
			string liqName = FurnishingLibrary.Library.getItemName(keys[i], true);
			GUI.Label(textRect, "Cup of "+liqName+" x"+filledDrinks[keys[i]].Count);
			GUI.color = FurnishingLibrary.Library.getLiquidColor(keys[i]);
			GUI.DrawTexture(cupImage, FurnishingLibrary.Library.getHoldablePrefab(keys[i]).holdableTexture);

			if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && filledCup.Contains(UIMousPos) && filledDrinksHereRect.Contains(UIMousPos))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				getFilledCup(MainMechanics.mechanics.Innkeeper, keys[i]);
			}
		}
		GUI.EndGroup();

		
		GUI.color = Color.black;
		Rect getCupPicRect = new Rect(emptyCupsHereRect.x+emptyCupsHereRect.width/2-emptyCupsHereRect.height/4, 
		                              emptyCupsHereRect.y, 
		                              emptyCupsHereRect.height/2, emptyCupsHereRect.height/2);
		Rect ammountTextRect = new Rect(getCupPicRect.x+getCupPicRect.width, getCupPicRect.y, emptyCupsHereRect.width/2-getCupPicRect.width/2, getCupPicRect.height);
		Rect getCupTextRect = new Rect(emptyCupsHereRect.x, emptyCupsHereRect.y+getCupPicRect.height, emptyCupsHereRect.width, emptyCupsHereRect.height/2);
		GUI.Label(ammountTextRect, "x"+emptyCups.Count);
		labelStyle.alignment = TextAnchor.UpperCenter;
		GUI.Label(getCupTextRect, "Get Empty Cup");
		GUI.color = Color.white;
		GUI.DrawTexture(getCupPicRect, FurnishingLibrary.Library.getHoldablePrefab(FurnishingLibrary.HoldableID.Cup).holdableTexture);
		
		Rect liqHereRect = new Rect(mainArea.x+mainArea.width/2-mainArea.height/4, mainArea.y+mainArea.height/2-mainArea.height/4, mainArea.height/2, mainArea.height/2);
		Rect fillUpButton = new Rect(liqHereRect.x, liqHereRect.y+liqHereRect.height, liqHereRect.width, liqHereRect.height/5);
		GUI.color = Color.black;
		GUI.DrawTexture(fillUpButton, Texture2D.whiteTexture);

		int whereAt = getWhereFill(MainMechanics.mechanics.Innkeeper);
		if(liquidsOnTap[whereAt] != null)
		{
			GUI.color = Color.black;
			GUI.DrawTexture(liqHereRect, MainMechanics.mechanics.LiquidDrop);
			GUI.color = liquidsOnTap[whereAt].liquidColor;
			float percentHere = (float)ammountOnTap[whereAt]/(float)maxLiquidContainment;
			Rect unmaskedArea = new Rect(liqHereRect.x, liqHereRect.y+((1f-percentHere)*liqHereRect.height), liqHereRect.width, liqHereRect.height*percentHere);
			GUI.BeginGroup(unmaskedArea);
			GUI.DrawTexture(new Rect(0,0-(1f-percentHere)*liqHereRect.height, liqHereRect.width, liqHereRect.height), FurnishingLibrary.Library.getHoldablePrefab(liquidsOnTap[whereAt].liquidID).holdableTexture);
			GUI.EndGroup();
			GUI.color = Color.white;
		}
		else
		{
			GUI.color = Color.gray;
		}
		GUI.DrawTexture(new Rect(fillUpButton.x+1, fillUpButton.y+1, fillUpButton.width-2, fillUpButton.height-2), Texture2D.whiteTexture);
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUI.color = Color.black;
		GUI.Label(fillUpButton, "Fill Up Cup");


		
		
		Rect placeOrdersButton = new Rect(orderArea.x, orderArea.y+orderArea.height-orderArea.height/20-5, orderArea.width, orderArea.height/20);


		//ORDERS
		int x = 0;
		int y = 0;
		
		for(int i = 0; i < orders.Count; i++)
		{
			Rect order = new Rect(orderArea.x+orderArea.width*x/2,orderArea.y+orderArea.width*y/2-ordersScroll, orderArea.width/2, orderArea.width/2);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(order.x-1, order.y-1, order.width+2, order.height+2), Texture2D.whiteTexture);
			if(orders[i].hasBeenCalled)
			{
				GUI.color = Color.gray;
			}
			else
			{
				GUI.color = Color.white;
			}
			GUI.DrawTexture(order, Texture2D.whiteTexture);
			Rect tableNameRect = new Rect(order.x+order.width/20, order.y+order.height/20, order.width*9/10, order.height/10);
			GUI.color = Color.black;
			//GUI.DrawTexture(tableNameRect, Texture2D.whiteTexture);
			GUI.Label(tableNameRect, "Table Name");
			//GUI.Label(tableNameRect, orders[i].orderedFrom.name);
			Rect criteriaArea = new Rect(tableNameRect.x, tableNameRect.y+tableNameRect. height,tableNameRect.width,  order.height*14/20);
			GUI.DrawTexture(criteriaArea, Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(criteriaArea.x+1, criteriaArea.y+1, criteriaArea.width-2, criteriaArea.height-2), Texture2D.whiteTexture);
			int inX = 0;
			int inY = 0;
			bool toRemove = false;
			for(int four = 0; four < 4; four++)
			{
				Rect inBox = new Rect(criteriaArea.x+(criteriaArea.width/2)*inX, criteriaArea.y+(criteriaArea.height/2)*inY, criteriaArea.width/2,criteriaArea.height/2);
				GUI.color = Color.black;
				GUI.DrawTexture(inBox, Texture2D.whiteTexture);
				if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null && orders[i].holdableToRetrieve[four] != null)
				{
					GUI.color = Color.green;
				}
				else if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null)
				{
					GUI.color = Color.red;
				}
				else
				{
					GUI.color = Color.white;
				}
				GUI.DrawTexture(new Rect(inBox.x+1, inBox.y+1, inBox.width-2, inBox.height-2), Texture2D.whiteTexture);
				GUI.color = Color.white;
				if(orders[i].orderedHoldables[four] != FurnishingLibrary.HoldableID.Error)
				{
					GUI.DrawTexture(inBox, FurnishingLibrary.Library.getHoldablePrefab(orders[i].orderedHoldables[four]).holdableTexture);
				}
				
				if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null && orders[i].holdableToRetrieve[four] != null
				   && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && inBox.Contains(UIMousPos)
				   && !placeOrdersButton.Contains(UIMousPos))
				{
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null)
					{
						MainMechanics.mechanics.Innkeeper.heldObjects[0] = orders[i].holdableToRetrieve[four];
						MainMechanics.mechanics.Innkeeper.heldObjects[0].gameObject.SetActive(true);
						orders[i].holdableToRetrieve[four] = null;
						if(!orders[i].checkItemsLeft())
						{
							toRemove = true;
						}
					}
					else if(MainMechanics.mechanics.Innkeeper.heldObjects[1] == null)
					{
						MainMechanics.mechanics.Innkeeper.heldObjects[1] = orders[i].holdableToRetrieve[four];
						MainMechanics.mechanics.Innkeeper.heldObjects[1].gameObject.SetActive(true);
						orders[i].holdableToRetrieve[four] = null;
						if(!orders[i].checkItemsLeft())
						{
							toRemove = true;
						}
					}
				}
				
				if(inX == 1)
				{
					inY++;
					inX = 0;
				}
				else
				{
					inX++;
				}
			}
			bool canCall = canCallOrder(i);
			
			Rect callButton = new Rect(criteriaArea.x, criteriaArea.y+criteriaArea.height, tableNameRect.width, tableNameRect.height);
			GUI.color = Color.black;
			GUI.DrawTexture(callButton, Texture2D.whiteTexture);

			if(canCall)
			{
				GUI.color = Color.white;
			}
			else
			{
				GUI.color = Color.gray;
			}
			GUI.DrawTexture(new Rect(callButton.x+1, callButton.y+1, callButton.width-2, callButton.height-2), Texture2D.whiteTexture);
			GUI.color = Color.black;
			GUI.Label(callButton, "CALL");
			
			if(canCall && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 
			   && callButton.Contains(UIMousPos)
			   && !placeOrdersButton.Contains(UIMousPos))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				callOrder(i, MainMechanics.mechanics.Innkeeper);
			}
			
			if(toRemove)
			{
				orders.RemoveAt(i);
				i--;
			}
			else if(x >= 1)
			{
				x = 0;
				y++;
			}
			else
			{
				x++;
			}
		}
		//END ORDERS



		
		GUI.color = Color.black;
		GUI.DrawTexture(placeOrdersButton, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(placeOrdersButton.x+1, placeOrdersButton.y+1, placeOrdersButton.width-2, placeOrdersButton.height-2), Texture2D.whiteTexture);
		GUI.color = Color.black;
		GUI.Label (placeOrdersButton, "Place Orders");





		GUI.color = Color.white;
		if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			GUI.DrawTexture(leftHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableTexture);
		}
		if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			GUI.DrawTexture(rightHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableTexture);
		}

		if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && LeftHandUseRect.Contains(UIMousPos) && MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			if(MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup)
			{
				putInHandDown(MainMechanics.mechanics.Innkeeper, true);
			}
			else if(MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel 
			        && MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere != null)
			{
				MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
				getInteraction(MainMechanics.mechanics.Innkeeper, 1);
			}
		}
		else if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && RightHandUseRect.Contains(UIMousPos) && MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			if(MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup)
			{
				putInHandDown(MainMechanics.mechanics.Innkeeper, false);
			}
			else if(MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel 
			        && MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere != null)
			{
				MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
				getInteraction(MainMechanics.mechanics.Innkeeper, 1);
			}
		}
		else if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && emptyCupsHereRect.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			getEmptyCup(MainMechanics.mechanics.Innkeeper);
		}
		else if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && fillUpButton.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
			getInteraction(MainMechanics.mechanics.Innkeeper, 2);
		}
		else if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && placeOrdersButton.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			getInteraction(MainMechanics.mechanics.Innkeeper, 3);
		}
	}


	public override void PourLiquidIntoContainer(Humanoid interacter, Holdable container)
	{
		int whereFill = getWhereFill(interacter);
		if(liquidsOnTap[whereFill] != null && ammountOnTap[whereFill] > 0)
		{
			if(container.currentLiquidContainment == 0)
			{
				if(container.maxLiquidContainment <= ammountOnTap[whereFill])
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					ammountOnTap[whereFill] -= container.maxLiquidContainment;
					container.liquidHere = liquidsOnTap[whereFill];
				}
				else
				{
					container.currentLiquidContainment = ammountOnTap[whereFill];
					container.liquidHere = liquidsOnTap[whereFill];
					ammountOnTap[whereFill] = 0;
					liquidsOnTap[whereFill] = null;
                    UpdateAvailableDrinks();
                }
			}
			else if(container.liquidHere.liquidID == liquidsOnTap[whereFill].liquidID)
			{
				int ammountEmpty = container.maxLiquidContainment - container.currentLiquidContainment;
				
				if(ammountEmpty <= ammountOnTap[whereFill])
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					ammountOnTap[whereFill] -= ammountEmpty;
					container.liquidHere = liquidsOnTap[whereFill];
				}
				else
				{
					container.currentLiquidContainment +=ammountOnTap[whereFill];
					container.liquidHere = liquidsOnTap[whereFill];
					ammountOnTap[whereFill] = 0;
					liquidsOnTap[whereFill] = null;
                    UpdateAvailableDrinks();
                }
			}
		}
	}

	int getWhereFill(Humanoid interacter)
	{
		int whereFill = (int)Mathf.Round(transform.InverseTransformPoint(interacter.transform.position).x+2);
		if(whereFill > 4)
		{
			whereFill = 4;
		}
		else if(whereFill < 0)
		{
			whereFill = 0;
		}

		return whereFill;
	}

	void getEmptyCup(Humanoid interacter)
	{
		if(emptyCups.Count > 0)
		{
			if(interacter.heldObjects[0] == null)
			{
				emptyCups[0].gameObject.SetActive(true);
				interacter.heldObjects[0] = emptyCups[0];
				emptyCups.RemoveAt(0);
			}
			else if(interacter.heldObjects[1] == null)
			{
				emptyCups[0].gameObject.SetActive(true);
				interacter.heldObjects[1] = emptyCups[0];
				emptyCups.RemoveAt(0);
			}
            else if(interacter.thisNPCType != Humanoid.NPCType.Player)
            {
                interacter.dropItemOnFloor(true);
                emptyCups[0].gameObject.SetActive(true);
                interacter.heldObjects[0] = emptyCups[0];
                emptyCups.RemoveAt(0);
            }
		}
	}

	void getFilledCup(Humanoid interacter, FurnishingLibrary.HoldableID key)
	{
		List<Holdable> list;
		if(filledDrinks.TryGetValue(key, out list) && filledDrinks[key].Count > 0)
		{
			if(interacter.heldObjects[0] == null)
			{
				filledDrinks[key][0].gameObject.SetActive(true);
				interacter.heldObjects[0] = filledDrinks[key][0];
				filledDrinks[key].RemoveAt(0);
                if(filledDrinks[key].Count <= 0)
                {
                    filledDrinks.Remove(key);
                }
			}
			else if(interacter.heldObjects[1] == null)
			{
				filledDrinks[key][0].gameObject.SetActive(true);
				interacter.heldObjects[1] = filledDrinks[key][0];
				filledDrinks[key].RemoveAt(0);
                if (filledDrinks[key].Count <= 0)
                {
                    filledDrinks.Remove(key);
                }
            }
		}
	}

	void checkScroll()
	{
		float yLength = Screen.height/20*filledDrinks.Count;
		if(filledCupsScroll < 0)
		{
			filledCupsScroll = 0;
		}
		else if(filledDrinks.Count < 10)
		{
			filledCupsScroll = 0;
		}
		else if(filledCupsScroll > yLength - filledDrinksHereRect.height)
		{
			filledCupsScroll = yLength - filledDrinksHereRect.height;
		}


		int orderLength;
		
		if(orders.Count%2 == 0)
		{
			orderLength = ((int)(orders.Count/2))*(int)(orderArea.width/2);
		}
		else
		{
			orderLength = ((int)(orders.Count/2)+1)*(int)(orderArea.width/2);
		}


		if(ordersScroll < 0)
		{
			ordersScroll = 0;
		}
		if(orderArea.height >= orderLength)
		{
			ordersScroll = 0;
		}
		else if(ordersScroll > orderLength - orderArea.height)
		{
			ordersScroll = orderLength - orderArea.height;
		}
	}

	void placeOrders(Humanoid interacter)
	{
		List<FurnishingLibrary.HoldableID> ordered = new List<FurnishingLibrary.HoldableID>();
		for(int i = 0; i < interacter.takenOrders.Count; i++)
		{
			string String = interacter.takenOrders[i].orderedHoldables[0].ToString();
			var charArray = String.ToCharArray(0,3);
			if(interacter.takenOrders[i].isDrink
			   && !interacter.takenOrders[i].hasBeenDelievered)
			{
				orders.Add(interacter.takenOrders[i]);
				interacter.takenOrders[i].hasBeenDelievered = true;
				for(int o = 0; o < 4; o++)
				{
					ordered.Add(interacter.takenOrders[i].orderedHoldables[o]);
				}
			}
		}
		
	}

	void callOrder(Humanoid interacter)
	{
		if(interacter.thisNPCType != Humanoid.NPCType.Player)
		{
			for(int i = 0; i < orders.Count; i++)
			{
				if(orders[i].orderedFrom.assignedEmployee == interacter && canCallOrder(i))
				{
					callOrder(i, interacter);
				}
			}
		}
	}

	void callOrder(int orderNumber, Humanoid interacter)
	{
		if(orders[orderNumber].hasBeenCalled || !canCallOrder(orderNumber))
		{
			Debug.LogError("Has Already been Called or Not Enough Stuff");
			return;
		}


		for(int i = 0; i < 4; i++)
		{
            if (orders[orderNumber].orderedHoldables[i] != FurnishingLibrary.HoldableID.Error)
            {
                orders[orderNumber].addRetrievable(filledDrinks[orders[orderNumber].orderedHoldables[i]][0], i);
                filledDrinks[orders[orderNumber].orderedHoldables[i]].RemoveAt(0);
            }
		}
		orders[orderNumber].hasBeenCalled = true;
		assignedOrders.Add(orders[orderNumber]);
	}


	void putInHandDown(Humanoid interacter, bool isLeft)
	{
		int handNumber;
		if(isLeft)
		{
			handNumber = 0;
		}
		else
		{
			handNumber = 1;
		}
		if(interacter.heldObjects[handNumber] != null && interacter.heldObjects[handNumber].holdableID == FurnishingLibrary.HoldableID.Cup)
		{
			if(interacter.heldObjects[handNumber].liquidHere != null)
			{
				List<Holdable> output;
				if(filledDrinks.TryGetValue(interacter.heldObjects[handNumber].liquidHere.liquidID, out output))
				{
					filledDrinks[interacter.heldObjects[handNumber].liquidHere.liquidID].Add(interacter.heldObjects[handNumber]);
					interacter.heldObjects[handNumber].gameObject.SetActive(false);
					interacter.heldObjects[handNumber] = null;
				}
				else
				{
					List<Holdable> newList = new List<Holdable>();
					newList.Add(interacter.heldObjects[handNumber]);
					filledDrinks.Add(interacter.heldObjects[handNumber].liquidHere.liquidID, newList);
					interacter.heldObjects[handNumber].gameObject.SetActive(false);
					interacter.heldObjects[handNumber] = null;
				}
			}
			else if(interacter.heldObjects[handNumber].liquidHere == null)
			{
				emptyCups.Add(interacter.heldObjects[handNumber]);
				interacter.heldObjects[handNumber].gameObject.SetActive(false);
				interacter.heldObjects[handNumber] = null;
			}
		}
	}


    /// <summary>
    /// Returns if call order is possible while keeping track of a temperary list that ammounts are removed from
    /// </summary>
    /// <param name="orderNumber"></param>
    /// <param name="tempList"></param>
    /// <returns></returns>
    bool canCallOrder(int orderNumber, Dictionary<FurnishingLibrary.HoldableID, int> tempList)
    {
        List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(orders[orderNumber].ammountNeeded.Keys);
        int[] counter = new int[ammountKeys.Count];
        bool isKill = false;
        for (int k = 0; k < ammountKeys.Count; k++)
        {
            int ammount;
            if ((!tempList.TryGetValue(ammountKeys[k], out ammount)
               || orders[orderNumber].ammountNeeded[ammountKeys[k]] > ammount) && ammountKeys[k] != FurnishingLibrary.HoldableID.Error)
            {
                isKill = true;
            }
            counter[k] = orders[orderNumber].ammountNeeded[ammountKeys[k]];
        }
        for (int i = 0; i < ammountKeys.Count; i++)
        {
            if (tempList.ContainsKey(ammountKeys[i]))
            {
                tempList[ammountKeys[i]] -= counter[i];
            }
        }
        if (isKill)
        {
            return false;
        }
        return true;
    }

    bool canCallOrder(int orderNumber)
	{
		List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(orders[orderNumber].ammountNeeded.Keys);
		for(int k = 0; k < ammountKeys.Count; k++)
		{
            if (ammountKeys[k] != FurnishingLibrary.HoldableID.Error)
            {
                List<Holdable> ammount;
                if (!filledDrinks.TryGetValue(ammountKeys[k], out ammount)
                   || orders[orderNumber].ammountNeeded[ammountKeys[k]] > ammount.Count)
                {
                    return false;
                }
            }
		}
		return true;
	}
	
	void getCalledOrder(Humanoid interacter)
	{
		for(int i = 0; i < assignedOrders.Count; i++)
		{
			if(assignedOrders[i].orderedFrom.assignedEmployee == interacter
			   && assignedOrders[i].hasBeenCalled)
			{
				bool nothingToPickUp = true;
				for(int h = 0; h < 4; h++)
				{
					if(assignedOrders[i].holdableToRetrieve[h] != null)
					{
						nothingToPickUp = false;
						if(interacter.heldObjects[0] == null)
						{
							interacter.heldObjects[0] = assignedOrders[i].holdableToRetrieve[h];
							interacter.heldObjects[0].gameObject.SetActive(true);
							assignedOrders[i].holdableToRetrieve[h] = null;
							
							WorkOrder work = new WorkOrder();
							work.Set (assignedOrders[i].orderedFrom, 1000, interacter, this);
							interacter.currentWorkOrder.Insert(1, work);
							return;
							
						}
						else if(interacter.heldObjects[1] == null)
						{
							interacter.heldObjects[1] = assignedOrders[i].holdableToRetrieve[h];
							interacter.heldObjects[1].gameObject.SetActive(true);
							assignedOrders[i].holdableToRetrieve[h] = null;
							
							WorkOrder work = new WorkOrder();
							work.Set (assignedOrders[i].orderedFrom, -1000, interacter, this);
							interacter.currentWorkOrder.Insert(1, work);
							return;
						}
					}
				}
				if(nothingToPickUp)
				{
					assignedOrders.RemoveAt(i);
					i--;
				}
			}
		}
	}

	Vector2 getAdjacentTile(int tile)
	{
		int placement = tile-2;
		Vector2 midPoint = transform.position+transform.TransformDirection(new Vector2(placement, 0.5f));
		midPoint = new Vector2(Mathf.RoundToInt(midPoint.x), Mathf.RoundToInt(midPoint.y));
		return midPoint;
	}


	public override bool getWorkOrder ()
	{
		WorkOrder work = new WorkOrder();

		if(orders.Count > 0)
		{
            Dictionary<FurnishingLibrary.HoldableID, int> totalAmount = new Dictionary<FurnishingLibrary.HoldableID, int>();
            for (int i = 0; i < orders.Count; i++)
            {
                if (!orders[i].hasBeenCalled)
                {
                    List<FurnishingLibrary.HoldableID> ammKeys = new List<FurnishingLibrary.HoldableID>(orders[i].ammountNeeded.Keys);
                    for (int k = 0; k < ammKeys.Count; k++)
                    {
                        int amount = orders[i].ammountNeeded[ammKeys[k]];
                        int whoCares;
                        if (totalAmount.TryGetValue(ammKeys[k], out whoCares))
                        {
                            totalAmount[ammKeys[k]] += amount;
                        }
                        else
                        {
                            totalAmount.Add(ammKeys[k], amount);
                        }
                    }
                }
            }
            List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(totalAmount.Keys);
            for (int k = 0; k < keys.Count; k++)
			{
                List<Holdable> ammoutHave;
				if(!filledDrinks.TryGetValue(keys[k], out ammoutHave) || totalAmount[keys[k]] > ammoutHave.Count)
				{
					if((assignedEmployee.heldObjects[0] != null 
						&& assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup
						&& assignedEmployee.heldObjects[0].liquidHere != null
						&& assignedEmployee.heldObjects[0].liquidHere.liquidID == keys[k]))
					{
						work.Set(this, 1000, assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(work);
						return false;
					}
					if(assignedEmployee.heldObjects[1] != null 
						&& assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup
						&& assignedEmployee.heldObjects[1].liquidHere != null
						&& assignedEmployee.heldObjects[1].liquidHere.liquidID == keys[k])
					{
						work.Set(this, -1000, assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(work);
						return false;
					}

					if(hasLiquid(keys[k]) 
						&& 
						(assignedEmployee.heldObjects[0] != null 
						&& assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup
						&& assignedEmployee.heldObjects[0].liquidHere == null)
						||
						(assignedEmployee.heldObjects[1] != null 
							&& assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup
							&& assignedEmployee.heldObjects[1].liquidHere == null))
					{
						int tapToChoose = whereLiquid(keys[k]);
						if(tapToChoose > -1)
						{
							Vector2 currentPos = assignedEmployee.WhereAmIOnGrid();
							Vector2 destination = getAdjacentTile(tapToChoose);
							Dictionary<PathNode, bool> dic = new Dictionary<PathNode, bool>();
							dic.Add(MainMechanics.mechanics.mainGrid[(int)destination.x, (int)destination.y].pathNode, false);
							List<PathNode> potentialPath = assignedEmployee.pathFinder.pathfind(MainMechanics.mechanics.mainGrid[(int)currentPos.x, (int) currentPos.y].pathNode,
								                                                                dic, assignedEmployee);
							if(potentialPath != null && potentialPath.Count > 0)
							{
								assignedEmployee.pathToGo = potentialPath;
							}

							if(assignedEmployee.heldObjects[0] != null 
								&& assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel)
							{
								assignedEmployee.dropItemOnFloor(true);
							}
							if(assignedEmployee.heldObjects[1] != null 
								&& assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel)
							{
								assignedEmployee.dropItemOnFloor(false);
							}

							work.Set(this, 2, assignedEmployee, this);
							assignedEmployee.currentWorkOrder.Add(work);
							return false;
						}
					}


					if(hasLiquid(keys[k]) 
						&& 
						(assignedEmployee.heldObjects[0] == null 
						|| assignedEmployee.heldObjects[0].holdableID != FurnishingLibrary.HoldableID.Cup)
						&&
						(assignedEmployee.heldObjects[1] == null 
						|| assignedEmployee.heldObjects[1].holdableID != FurnishingLibrary.HoldableID.Cup))
					{
						Holdable output;
						if(MainMechanics.mechanics.isLiquidCupAvailable(keys[k], out output))
						{
							work.Set (output, assignedEmployee, this);
							assignedEmployee.currentWorkOrder.Add(work);
							return false;
						}
						if(emptyCups.Count > 0)
						{

							work.Set(this, 6, assignedEmployee, this);
							assignedEmployee.currentWorkOrder.Add(work);
							return false;
						}
					}

				}
			}
		}

		if((assignedEmployee.heldObjects[0] != null 
		    && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel
		    && assignedEmployee.heldObjects[0].liquidHere != null
		    && !hasLiquid(assignedEmployee.heldObjects[0].liquidHere.liquidID))
		   ||
		   (assignedEmployee.heldObjects[1] != null 
		   && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel 
		   && assignedEmployee.heldObjects[1].liquidHere != null
		   && !hasLiquid(assignedEmployee.heldObjects[1].liquidHere.liquidID)))
		{
			int tapToChoose = -1;
			for(int i = 0; i < liquidsOnTap.Length; i++)
			{
				if(liquidsOnTap[i] == null)
				{
					tapToChoose = i;
					break;
				}
			}
			if(tapToChoose != -1)
			{
				Vector2 currentPos = assignedEmployee.WhereAmIOnGrid();
				Vector2 destination = getAdjacentTile(tapToChoose);
				Dictionary<PathNode, bool> dic = new Dictionary<PathNode, bool>();
				dic.Add(MainMechanics.mechanics.mainGrid[(int)destination.x, (int)destination.y].pathNode, false);
				List<PathNode> potentialPath = assignedEmployee.pathFinder.pathfind(MainMechanics.mechanics.mainGrid[(int)currentPos.x, (int) currentPos.y].pathNode,
				                                     dic, assignedEmployee);
				if(potentialPath != null && potentialPath.Count > 0)
				{
					assignedEmployee.pathToGo = potentialPath;
				}

				work.Set(this, 1, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(work);
				return false;
			}
		}

		FurnishingLibrary.HoldableID liquidToLookFor = FurnishingLibrary.HoldableID.Error;
        if (!hasLiquid(FurnishingLibrary.HoldableID.LiqWater))
        {
            liquidToLookFor = FurnishingLibrary.HoldableID.LiqWater;
        }
        else
        {
            for (int i = 0; i < MainMechanics.recipeBook.knownBrewableIDs.Count; i++)
            {
                if (!hasLiquid(MainMechanics.recipeBook.knownBrewableIDs[i]))
                {
                    liquidToLookFor = MainMechanics.recipeBook.knownBrewableIDs[i];
                    break;
                }
            }
        }
		/*if(!hasLiquid(FurnishingLibrary.HoldableID.LiqWater))
		{
			liquidToLookFor = FurnishingLibrary.HoldableID.LiqWater;
		}
		else if(!hasLiquid(FurnishingLibrary.HoldableID.LiqYBeer))
		{
			liquidToLookFor = FurnishingLibrary.HoldableID.LiqYBeer;
		}*/

		
		bool foundBarrel;
		if(MainMechanics.mechanics.holdableIsAvailable(liquidToLookFor, out foundBarrel))
		{
			work.Set(liquidToLookFor, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(work);
			return false;
		}
		
		List<Furnishing> furnishingFound;
		if(liquidToLookFor != FurnishingLibrary.HoldableID.Error && MainMechanics.mechanics.isFurnishingLiquidAvailable(liquidToLookFor, out furnishingFound))
		{
			if((assignedEmployee.heldObjects[0] != null 
			   && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel
			   && assignedEmployee.heldObjects[0].liquidHere == null)
			   ||
			   (assignedEmployee.heldObjects[1] != null 
			   && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel
			   && assignedEmployee.heldObjects[1].liquidHere == null))
			{
				work.Set(furnishingFound, MainMechanics.recipeBook.HowRetrieve(liquidToLookFor), assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(work);
				if(assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID != FurnishingLibrary.HoldableID.Barrel)
				{
					assignedEmployee.dropItemOnFloor(true);
				}
				if(assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID != FurnishingLibrary.HoldableID.Barrel)
				{
					assignedEmployee.dropItemOnFloor(false);
				}
				return false;
			}
			if(foundBarrel)
			{
				work.Set(FurnishingLibrary.HoldableID.Barrel, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(work);
				return false;
			}
		}


		if(assignedEmployee.heldObjects[0] != null 
		   && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup 
		   && assignedEmployee.heldObjects[0].liquidHere == null)
		{
			work.Set(this, 1000, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(work);
			return false;
		}
		if(assignedEmployee.heldObjects[1] != null 
		   && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup 
		   && assignedEmployee.heldObjects[1].liquidHere == null)
		{
			work.Set(this, -1000, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(work);
			return false;
		}

		if(MainMechanics.mechanics.holdableIsAvailable(FurnishingLibrary.HoldableID.Cup))
		{
			work.Set(FurnishingLibrary.HoldableID.Cup, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(work);
			return false;
		}
		return true;
	}

	/// <summary>
	/// returns what slot the wanted liquid is in.
	/// </summary>
	/// <returns>The liquid.</returns>
	/// <param name="ID">I.</param>
	int whereLiquid(FurnishingLibrary.HoldableID ID)
	{
		for(int i = 0; i < liquidsOnTap.Length; i++)
		{
			if(liquidsOnTap[i] != null && liquidsOnTap[i].liquidID == ID)
			{
				return i;
			}
		}
		return -1;
	}

	/// <summary>
	/// Checks liquid IDs at the bar to see if they match the input.
	/// </summary>
	/// <returns><c>true</c>, if liquid is found, <c>false</c> otherwise.</returns>
	/// <param name="ID">I.</param>
	bool hasLiquid(FurnishingLibrary.HoldableID ID)
	{
		for(int i = 0; i < liquidsOnTap.Length; i++)
		{
			if(liquidsOnTap[i] != null && liquidsOnTap[i].liquidID == ID)
			{
				return true;
			}
		}
		return false;
	}

	/*/// <summary>
	/// Checks liquid IDs that are ordered to see if they match the input.
	/// </summary>
	/// <returns><c>true</c>, if liquid is found, <c>false</c> otherwise.</returns>
	/// <param name="ID">I.</param>
	bool isLiquidOrdered(FurnishingLibrary.HoldableID ID)
	{
		for(int i = 0; i < orders.Count; i++)
		{
			int outputA;
			List<Holdable> outputH;
			if(!orders[i].hasBeenCalled && orders[i].ammountNeeded.TryGetValue(ID, out outputA) && (!filledDrinks.TryGetValue(ID, out outputH) || outputA > outputH.Count))
			{
				return true;
			}
		}
		return false;
	}*/

}
