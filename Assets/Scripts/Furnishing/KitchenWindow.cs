﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KitchenWindow : InteractableFurnishing
{
	public List<Holdable> uncalledHoldablesHere = new List<Holdable>();
	public List<Order> orders = new List<Order>();

	public List<Order> assignedOrders = new List<Order>();

	public Dictionary<FurnishingLibrary.HoldableID, int> holdableAmmountHere = new Dictionary<FurnishingLibrary.HoldableID, int>();

	int xMax = 5;

	Vector2 mousPos;

	Rect mainRect = new Rect(0,0, Screen.width*2/3, Screen.height/2);
	float mainScroll = 0f;
	
	Rect holdablesRect = new Rect(Screen.width*2/3, 0, Screen.width/3, Screen.height);
	//float rightScroll = 0f;
	
	Rect botArea = new Rect(0, Screen.height/2, Screen.width*2/3, Screen.height/2);

	public override void Update ()
	{
		base.Update ();
		if(mainRect.Contains(mousPos) && Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			mainScroll += Input.GetAxis("Mouse ScrollWheel")*(Screen.height/10);
			checkScroll();
		}

		mainRect = new Rect(0,0, Screen.width*2/3, Screen.height/2);
		
		holdablesRect = new Rect(Screen.width*2/3, 0, Screen.width/3, Screen.height);
		
		botArea = new Rect(0, Screen.height/2, Screen.width*2/3, Screen.height/2);

        List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(holdableAmmountHere.Keys);
        Dictionary<FurnishingLibrary.HoldableID, int> tempList = new Dictionary<FurnishingLibrary.HoldableID, int>();
        for (int i = 0; i < ammountKeys.Count; i++)
        {
            tempList.Add(ammountKeys[i], holdableAmmountHere[ammountKeys[i]]);
        }

		for(int i = 0; i < orders.Count; i++)
		{
			bool toRemove = false;
			if(orders[i].hasBeenCalled && !orders[i].checkItemsLeft())
			{
				toRemove = true;
			}
			if(holdableAmmountHere.Count > 0)
			{
				if(!orders[i].hasBeenCalled)
				{
					if(canCallOrder(i, tempList) && orders[i].orderedFrom.assignedEmployee != null)
                    {
                        bool hasOrder = false;
						for(int o = 0; o < orders[i].orderedFrom.assignedEmployee.currentWorkOrder.Count; o++)
						{
							if(orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate != null
							   && (orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate.FurnishingID == FurnishingLibrary.FurnishingID.KitchenWindow
							    || orders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingIDToFind == FurnishingLibrary.FurnishingID.KitchenWindow))
							{
								hasOrder = true;
								break;
							}
						}
						if(!hasOrder)
                        {
                            WorkOrder work = new WorkOrder();
							work.Set(this, 3, orders[i].orderedFrom.assignedEmployee, this);
							orders[i].orderedFrom.assignedEmployee.currentWorkOrder.Add(work);
						}
					}
					//callOrder(i, orders[i].orderedFrom.assignedEmployee);
				}
			}
			if(toRemove)
			{
				assignedOrders.Remove(orders[i]);
				orders.RemoveAt(i);
				i--;
			}
		}
		for(int i = 0; i < assignedOrders.Count; i++)
		{
			if(assignedOrders[i].orderedFrom.assignedEmployee != null
			   && (assignedOrders[i].orderedFrom.assignedEmployee.heldObjects[0] == null || assignedOrders[i].orderedFrom.assignedEmployee.heldObjects[1] == null))
			{
				bool hasPickUpOrder = false;
				for(int o = 0; o < assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder.Count; o++)
				{
					if(assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate != null
					   && assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder[o].furnishingToActivate.FurnishingID == FurnishingLibrary.FurnishingID.KitchenWindow)
					{
						hasPickUpOrder = true;
						break;
					}
				}
				if(!hasPickUpOrder)
				{
					WorkOrder work = new WorkOrder();
					work.Set(this, 2, assignedOrders[i].orderedFrom.assignedEmployee, this);
					assignedOrders[i].orderedFrom.assignedEmployee.currentWorkOrder.Add(work);
				}
			}
		}
	}

	public override void DrawMenu (Vector2 UIMousPos, float PickUpScroll)
	{
		mousPos = UIMousPos;
		GUI.color = Color.white;
		GUI.DrawTexture(mainRect, Texture2D.whiteTexture);

		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUIContent textSizer = new GUIContent("Xy");
		Vector2 textSize = labelStyle.CalcSize(textSizer);
		
		List<FurnishingLibrary.HoldableID> keys = null;

		//MAIN RECT
		int x = 0;
		int y = 0;
		
		for(int i = 0; i < orders.Count; i++)
		{
			Rect order = new Rect(mainRect.x+mainRect.width*x/5,mainRect.y+mainRect.width*y/5+mainScroll, mainRect.width/5, mainRect.width/5);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(order.x-1, order.y-1, order.width+2, order.height+2), Texture2D.whiteTexture);
			if(orders[i].hasBeenCalled)
			{
				GUI.color = Color.gray;
			}
			else
			{
				GUI.color = Color.white;
			}
			GUI.DrawTexture(order, Texture2D.whiteTexture);
			Rect tableNameRect = new Rect(order.x+order.width/20, order.y+order.height/20, order.width*9/10, order.height/10);
			GUI.color = Color.black;
			//GUI.DrawTexture(tableNameRect, Texture2D.whiteTexture);
			GUI.Label(tableNameRect, "Table Name");
			//GUI.Label(tableNameRect, orders[i].orderedFrom.name);
			Rect criteriaArea = new Rect(tableNameRect.x, tableNameRect.y+tableNameRect. height,tableNameRect.width,  order.height*14/20);
			GUI.DrawTexture(criteriaArea, Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(criteriaArea.x+1, criteriaArea.y+1, criteriaArea.width-2, criteriaArea.height-2), Texture2D.whiteTexture);
			int inX = 0;
			int inY = 0;
			bool toRemove = false;
			for(int four = 0; four < 4; four++)
			{
				Rect inBox = new Rect(criteriaArea.x+(criteriaArea.width/2)*inX, criteriaArea.y+(criteriaArea.height/2)*inY, criteriaArea.width/2,criteriaArea.height/2);
				GUI.color = Color.black;
				GUI.DrawTexture(inBox, Texture2D.whiteTexture);
				if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null && orders[i].holdableToRetrieve[four] != null)
				{
					GUI.color = Color.green;
				}
				else if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null)
				{
					GUI.color = Color.red;
				}
				else
				{
					GUI.color = Color.white;
				}
				GUI.DrawTexture(new Rect(inBox.x+1, inBox.y+1, inBox.width-2, inBox.height-2), Texture2D.whiteTexture);
				GUI.color = Color.white;
				if(orders[i].orderedHoldables[four] != FurnishingLibrary.HoldableID.Error)
				{
					GUI.DrawTexture(inBox, FurnishingLibrary.Library.getHoldablePrefab(orders[i].orderedHoldables[four]).holdableTexture);
				}

				if(orders[i].hasBeenCalled && orders[i].orderedFrom.assignedEmployee == null && orders[i].holdableToRetrieve[four] != null
				   && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && inBox.Contains(UIMousPos))
				{
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null)
					{
						MainMechanics.mechanics.Innkeeper.heldObjects[0] = orders[i].holdableToRetrieve[four];
						MainMechanics.mechanics.Innkeeper.heldObjects[0].gameObject.SetActive(true);
						orders[i].holdableToRetrieve[four] = null;
						if(!orders[i].checkItemsLeft())
						{
							toRemove = true;
						}
					}
					else if(MainMechanics.mechanics.Innkeeper.heldObjects[1] == null)
					{
						MainMechanics.mechanics.Innkeeper.heldObjects[1] = orders[i].holdableToRetrieve[four];
						MainMechanics.mechanics.Innkeeper.heldObjects[1].gameObject.SetActive(true);
						orders[i].holdableToRetrieve[four] = null;
						if(!orders[i].checkItemsLeft())
						{
							toRemove = true;
						}
					}
				}

				if(inX == 1)
				{
					inY++;
					inX = 0;
				}
				else
				{
					inX++;
				}
			}
			bool canCall = canCallOrder(i);

			Rect callButton = new Rect(criteriaArea.x, criteriaArea.y+criteriaArea.height, tableNameRect.width, tableNameRect.height);
			GUI.color = Color.black;
			GUI.DrawTexture(callButton, Texture2D.whiteTexture);

			if(canCall)
			{
				GUI.color = Color.white;
			}
			else
			{
				GUI.color = Color.gray;
			}
			GUI.DrawTexture(new Rect(callButton.x+1, callButton.y+1, callButton.width-2, callButton.height-2), Texture2D.whiteTexture);
			GUI.color = Color.black;
			GUI.Label(callButton, "CALL");

			if(canCall && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && callButton.Contains(UIMousPos))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				callOrder(i, MainMechanics.mechanics.Innkeeper);
			}

			if(toRemove)
			{
				assignedOrders.Remove(orders[i]);
				orders.RemoveAt(i);
				i--;
			}
			else if(x >= xMax-1)
			{
				x = 0;
				y++;
			}
			else
			{
				x++;
			}
		}
		//END MAIN RECT


		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(holdablesRect.x-1, holdablesRect.y-1, holdablesRect.width+2, holdablesRect.height+2), Texture2D.whiteTexture);
		GUI.color = new Color(0.5f,0.5f,0.5f);
		GUI.DrawTexture(holdablesRect, Texture2D.whiteTexture);
		
		Rect exitButton = new Rect(holdablesRect.x, holdablesRect.height*19/20, holdablesRect.width, holdablesRect.height/20);
		

		
		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(exitButton.x-1, exitButton.y-1, exitButton.width+2, exitButton.height+2), Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(exitButton, Texture2D.whiteTexture);
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUI.color = Color.black;
		GUI.Label(exitButton, "EXIT");
		
		if(exitButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
		}

		GUI.color = Color.black;
		GUI.DrawTexture(botArea, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(botArea.x+1, botArea.y+1, botArea.width-2, botArea.width-2), Texture2D.whiteTexture);
		
		Rect RightHandUseRect = new Rect(botArea.width-botArea.width/3, Screen.height/2, botArea.width/3, Screen.height/2);
		Rect RightHandDarkRect = new Rect(RightHandUseRect.x+1, RightHandUseRect.y+1, RightHandUseRect.width-2, RightHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(RightHandDarkRect, Texture2D.whiteTexture);
		
		if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			Vector2 RightMidPoint = new Vector2(RightHandUseRect.x, RightHandUseRect.y)+ new Vector2(RightHandUseRect.width, RightHandUseRect.height)/2;
			Rect RightHeldImage = new Rect(RightMidPoint-new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2)/2,new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(RightHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[1].GetComponent<SpriteRenderer>().sprite.texture);
			
			if(RightHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				getInteraction(MainMechanics.mechanics.Innkeeper, -1000);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}
		
		
		
		Rect LeftHandUseRect = new Rect(RightHandUseRect.x-RightHandUseRect.width, RightHandUseRect.y, RightHandUseRect.width, RightHandUseRect.height);
		Rect LeftHandDarkRect = new Rect(LeftHandUseRect.x+1, LeftHandUseRect.y+1, LeftHandUseRect.width-2, LeftHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(LeftHandDarkRect, Texture2D.whiteTexture);
		
		if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			Vector2 LeftMidPoint = new Vector2(LeftHandUseRect.x, LeftHandUseRect.y)+ new Vector2(LeftHandUseRect.width, LeftHandUseRect.height)/2;
			Rect LeftHeldImage = new Rect(LeftMidPoint-new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2)/2,new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(LeftHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[0].GetComponent<SpriteRenderer>().sprite.texture);
			
			if(LeftHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				getInteraction(MainMechanics.mechanics.Innkeeper, 1000);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}
		
		Rect bottomRightArea = new Rect(botArea.x, botArea.y, botArea.width/3, botArea.height);

		GUI.color = Color.black;
		GUI.DrawTexture(bottomRightArea, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(bottomRightArea.x+1, bottomRightArea.y+1, bottomRightArea.width-2, bottomRightArea.height-2), Texture2D.whiteTexture);
		GUI.color = Color.black;
		GUI.Label(bottomRightArea, "Place Orders");

		if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && bottomRightArea.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			placeOrders(MainMechanics.mechanics.Innkeeper);
		}

		keys = new List<FurnishingLibrary.HoldableID>(holdableAmmountHere.Keys);
		
		for(int i = 0; i < keys.Count; i++)
		{
			Rect holdableAmmout = new Rect(holdablesRect.x+holdablesRect.width/2, exitButton.y-exitButton.height*(i+1), holdablesRect.width/2, exitButton.height);
			Rect holdableName = new Rect(holdableAmmout.x-exitButton.height, holdableAmmout.y, exitButton.height, exitButton.height);
			Rect ingredTrueRect = new Rect(holdableName.x, holdableName.y, holdableName.width+holdableAmmout.width, holdableName.height);
			
			Texture2D objectTexture = null;
			objectTexture = FurnishingLibrary.Library.getHoldablePrefab(keys[i]).holdableTexture;
			
			
			GUI.color = Color.white;
			GUI.DrawTexture(holdableName, objectTexture);
			GUI.color = Color.black;
			labelStyle.alignment = TextAnchor.MiddleLeft;
			GUI.Label(holdableAmmout, " x"+holdableAmmountHere[keys[i]]);
			
			
			if(ingredTrueRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				bool handleLiquid = false;
				
				string String = keys[i].ToString();
				var charArray = String.ToCharArray(0,3);
				if(charArray[0].Equals('L') && charArray[1].Equals('i') && charArray[2].Equals('q'))
				{
					handleLiquid = true;
				}
				
				if(handleLiquid)
				{
					Debug.LogError("Liquid at KitchenWindow. Please help!");
				}
				else
				{
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null || MainMechanics.mechanics.Innkeeper.heldObjects[1] == null)
					{
						MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;

						int whereHoldable = -1;

						for(int h = 0; h < uncalledHoldablesHere.Count; h++)
						{
							if(uncalledHoldablesHere[h].holdableID == keys[i])
							{
								whereHoldable = h;
								break;
							}
						}

						if(whereHoldable != -1)
						{
							if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null && holdableAmmountHere[keys[i]] > 0)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[0] = uncalledHoldablesHere[whereHoldable];
								uncalledHoldablesHere.RemoveAt(whereHoldable);
								MainMechanics.mechanics.Innkeeper.heldObjects[0].gameObject.SetActive(true);
								calculateHoldablesHere();
							}
							else if(MainMechanics.mechanics.Innkeeper.heldObjects[1] == null && holdableAmmountHere[keys[i]] > 0)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[1] = uncalledHoldablesHere[whereHoldable];
								uncalledHoldablesHere.RemoveAt(whereHoldable);
								MainMechanics.mechanics.Innkeeper.heldObjects[1].gameObject.SetActive(true);
								calculateHoldablesHere();
							}
						}
					}
				}
			}
		}
	}

	void placeOrders(Humanoid interacter)
	{
		List<FurnishingLibrary.HoldableID> ordered = new List<FurnishingLibrary.HoldableID>();
		for(int i = 0; i < interacter.takenOrders.Count; i++)
		{
			string String = interacter.takenOrders[i].orderedHoldables[0].ToString();
			var charArray = String.ToCharArray(0,3);
			if(!interacter.takenOrders[i].isDrink
			   && !interacter.takenOrders[i].hasBeenDelievered)
			{
				orders.Add(interacter.takenOrders[i]);
				//unassignedOrders.Add(interacter.takenOrders[i]);
				interacter.takenOrders[i].hasBeenDelievered = true;
				for(int o = 0; o < 4; o++)
				{
					ordered.Add(interacter.takenOrders[i].orderedHoldables[o]);
				}
			}
		}

		List<PrepTable> applicableTables = findApplicablePrepTable();

		if(applicableTables.Count > 0)
		{
			for(int i = 0; i < ordered.Count; i++)
			{
                if (ordered[i] != FurnishingLibrary.HoldableID.Error)
                {
                    PrepTable toAddTo = applicableTables[0];
                    for (int a = 1; a < applicableTables.Count; a++)
                    {
                        PrepTable lookingAt = applicableTables[a];
                        if (lookingAt.orders.Count < toAddTo.orders.Count)
                        {
                            toAddTo = lookingAt;
                        }
                    }
                    PrepTableOrder toAdd = new PrepTableOrder();
                    toAdd.Set(ordered[i]);
                    toAddTo.orders.Add(toAdd);
                }
			}
		}

	}

    /// <summary>
    /// Returns if call order is possible while keeping track of a temperary list that ammounts are removed from
    /// </summary>
    /// <param name="orderNumber"></param>
    /// <param name="tempList"></param>
    /// <returns></returns>
    bool canCallOrder(int orderNumber, Dictionary<FurnishingLibrary.HoldableID, int> tempList)
    {
        List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(orders[orderNumber].ammountNeeded.Keys);
        int[] counter = new int[ammountKeys.Count];
        bool isKill = false;
        for (int k = 0; k < ammountKeys.Count; k++)
        {
            int ammount;
            if ((!tempList.TryGetValue(ammountKeys[k], out ammount)
               || orders[orderNumber].ammountNeeded[ammountKeys[k]] > ammount) && ammountKeys[k] != FurnishingLibrary.HoldableID.Error)
            {
                isKill = true;
            }
            counter[k] = orders[orderNumber].ammountNeeded[ammountKeys[k]];
        }
        for (int i = 0; i < ammountKeys.Count; i++)
        {
            if (tempList.ContainsKey(ammountKeys[i]))
            {
                tempList[ammountKeys[i]] -= counter[i];
            }
        }
        if (isKill)
        {
            return false;
        }
        return true;
    }

    bool canCallOrder(int orderNumber)
	{
		List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(orders[orderNumber].ammountNeeded.Keys);
		for(int k = 0; k < ammountKeys.Count; k++)
		{
            if (ammountKeys[k] != FurnishingLibrary.HoldableID.Error)
            {
                int ammount;
                if (!holdableAmmountHere.TryGetValue(ammountKeys[k], out ammount)
                   || orders[orderNumber].ammountNeeded[ammountKeys[k]] > ammount)
                {
                    return false;
                }
            }
		}
		return true;
	}

	List<PrepTable> findApplicablePrepTable()
	{
		List<PrepTable> toReturn = new List<PrepTable>();
		List<Room> checkedRooms = new List<Room>();
		for(int i = 0; i < reachableNodes.Count; i++)
		{
			if(reachableNodes[i].gridHere.roomPieceHere != null 
                && !checkedRooms.Contains(reachableNodes[i].gridHere.roomPieceHere.parentRoom))
			{
				for(int t = 0; t < reachableNodes[i].gridHere.roomPieceHere.parentRoom.prepTablesHere.Count; t++)
				{
					toReturn.Add(reachableNodes[i].gridHere.roomPieceHere.parentRoom.prepTablesHere[t]);
				}
				checkedRooms.Add(reachableNodes[i].gridHere.roomPieceHere.parentRoom);
			}
		}

		return toReturn;
	}

    void callOrder(int orderNumber, Humanoid interacter)
    {
        List<FurnishingLibrary.HoldableID> ammountKeys = new List<FurnishingLibrary.HoldableID>(orders[orderNumber].ammountNeeded.Keys);
        if (!canCallOrder(orderNumber))
        {
            return;
        }

        for(int i = 0; i < 4; i++)
        {
            for (int h = 0; h < uncalledHoldablesHere.Count; h++)
            {
                if (uncalledHoldablesHere[h].holdableID == orders[orderNumber].orderedHoldables[i])
                {
                    orders[orderNumber].addRetrievable(uncalledHoldablesHere[h], i);
                    uncalledHoldablesHere.RemoveAt(h);
                    break;
                }
            }
        }

        /*for (int i = 0; i < ammountKeys.Count; i++)
        {
            int a = orders[orderNumber].ammountNeeded[ammountKeys[i]];
            for (int h = 0; h < uncalledHoldablesHere.Count; h++)
            {
                if (uncalledHoldablesHere[h].holdableID == ammountKeys[i])
                {
                    orders[orderNumber].addRetrievable(uncalledHoldablesHere[h]);
                    uncalledHoldablesHere.RemoveAt(h);
                    h--;
                    a--;
                    if (a <= 0)
                    {
                        break;
                    }
                }
            }
        }*/
        orders[orderNumber].hasBeenCalled = true;
		assignedOrders.Add(orders[orderNumber]);
		calculateHoldablesHere();
	}

	void getCalledOrder(Humanoid interacter)
	{
		for(int i = 0; i < assignedOrders.Count; i++)
		{
			if(assignedOrders[i].orderedFrom.assignedEmployee == interacter
			   && assignedOrders[i].hasBeenCalled)
			{
				for(int h = 0; h < 4; h++)
				{
					if(assignedOrders[i].holdableToRetrieve[h] != null)
					{
						if(interacter.heldObjects[0] == null)
						{
							interacter.heldObjects[0] = assignedOrders[i].holdableToRetrieve[h];
							interacter.heldObjects[0].gameObject.SetActive(true);
							assignedOrders[i].holdableToRetrieve[h] = null;

							WorkOrder work = new WorkOrder();
							work.Set (assignedOrders[i].orderedFrom, 1000, interacter, this);
							interacter.currentWorkOrder.Insert(1, work);
							return;

						}
						else if(interacter.heldObjects[1] == null)
						{
							interacter.heldObjects[1] = assignedOrders[i].holdableToRetrieve[h];
							interacter.heldObjects[1].gameObject.SetActive(true);
							assignedOrders[i].holdableToRetrieve[h] = null;

							WorkOrder work = new WorkOrder();
							work.Set (assignedOrders[i].orderedFrom, -1000, interacter, this);
							interacter.currentWorkOrder.Insert(1, work);
							return;
						}
					}
				}
			}
		}
	}

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
		}
		else if(actionNumber == 1)//PLACE ORDERS
		{
			placeOrders(interacter);
		}
		else if(actionNumber == 2)
		{
			getCalledOrder(interacter);
		}
		else if(actionNumber == 3)
		{
			callOrder(interacter);
		}
		else if(actionNumber == 1000)
		{
			if(interacter.heldObjects[0] != null)
			{
				uncalledHoldablesHere.Add(interacter.heldObjects[0]);
				interacter.heldObjects[0].gameObject.SetActive(false);
				interacter.heldObjects[0] = null;
				calculateHoldablesHere();
			}
		}
		else if(actionNumber == -1000)
		{
			if(interacter.heldObjects[1] != null)
			{
				uncalledHoldablesHere.Add(interacter.heldObjects[1]);
				interacter.heldObjects[1].gameObject.SetActive(false);
				interacter.heldObjects[1] = null;
				calculateHoldablesHere();
			}
		}
	}

	void callOrder(Humanoid interacter)
	{
		if(interacter.thisNPCType != Humanoid.NPCType.Player)
		{
			for(int i = 0; i < orders.Count; i++)
			{
				if(orders[i].orderedFrom.assignedEmployee == interacter && canCallOrder(i))
				{
					callOrder(i, interacter);
				}
			}
		}
	}

	void checkScroll()
	{
		int orderLength;
		
		if(orders.Count%5 == 0)
		{
			orderLength = ((int)(orders.Count/5))*(int)(mainRect.width/5);
		}
		else
		{
			orderLength = ((int)(orders.Count/5)+1)*(int)(mainRect.width/5);
		}
		
		
		if(mainScroll < 0)
		{
			mainScroll = 0;
		}
		if(mainRect.height >= orderLength)
		{
			mainScroll = 0;
		}
		else if(mainScroll > orderLength - mainRect.height)
		{
			mainScroll = orderLength - mainRect.height;
		}
	}


    /// <summary>
    /// Returns true if a holdable with the ID is not needed by any held orders.
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
	public bool doesHaveEnough(FurnishingLibrary.HoldableID ID)
	{
		int ammountNeeded = 0;
		calculateHoldablesHere();
		for(int i = 0; i < orders.Count; i++)
		{
			int toAdd;
			if(!orders[i].hasBeenCalled && orders[i].ammountNeeded.TryGetValue(ID, out toAdd))
			{
				ammountNeeded += toAdd;
			}
		}

		int whoCares;

		if(holdableAmmountHere.TryGetValue(ID, out whoCares))
		{
			if(ammountNeeded > holdableAmmountHere[ID])
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(ammountNeeded > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	void calculateHoldablesHere()
	{
		holdableAmmountHere.Clear();
		for(int i = 0; i < uncalledHoldablesHere.Count; i++)
		{
			if(holdableAmmountHere.ContainsKey(uncalledHoldablesHere[i].holdableID))
			{
				holdableAmmountHere[uncalledHoldablesHere[i].holdableID]++;
			}
			else
			{
				holdableAmmountHere.Add(uncalledHoldablesHere[i].holdableID, 1);
			}
		}
	}
}
