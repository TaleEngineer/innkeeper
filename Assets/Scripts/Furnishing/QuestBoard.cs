﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class QuestBoard : InteractableFurnishing
{
	public Quest[] questsPosted = new Quest[6];
	int questExamining;

	int tempQuestLevel = 1;

	DropdownMenu dungeonSettingDropdown = new DropdownMenu();
	DropdownMenu dungeonMonstersDropdown = new DropdownMenu();

	enum MenuSubstatus
	{
		MainWindow,
		ConfirmRemove,
		Order
	}
	MenuSubstatus currentMenuState = MenuSubstatus.MainWindow;

	public override void Start ()
	{
		base.Start ();
		List<string> toSet = new List<string>(Enum.GetNames(typeof(Area.DungeonSetting)));
		if(MainMechanics.boarderingArea[Area.DungeonSetting.Roads].lostMonsters.Count <= 0)
		{
			toSet.RemoveRange(0,2);
		}
		else
		{
			toSet.RemoveAt(0);
		}
		dungeonSettingDropdown.Set(toSet);
		
		toSet = new List<string>(Enum.GetNames(typeof(Monster.MonsterType)));
		dungeonMonstersDropdown.Set(toSet);
	}

	public override void Update ()
	{
		base.Update ();

		List<string> toSet = new List<string>(Enum.GetNames(typeof(Area.DungeonSetting)));
		if(MainMechanics.boarderingArea[Area.DungeonSetting.Roads].lostMonsters.Count <= 0)
		{
			toSet.RemoveRange(0,2);
		}
		else
		{
			toSet.RemoveAt(0);
		}
		dungeonSettingDropdown.Set(toSet);

		for(int i = 0; i < questsPosted.Length; i++)
		{
			if(questsPosted[i] != null && questsPosted[i].setUpTime > 0)
			{
				questsPosted[i].setUpTime -= Time.unscaledDeltaTime;
				if(questsPosted[i].setUpTime < 0)
				{
					questsPosted[i].setUpTime = 0;
				}
			}
			/*else if(questsPosted[i] != null && questsPosted[i].setUpTime <= 0 && questsPosted[i].activeOnBoard == false)
			{
				questsPosted[i].activeOnBoard = true;
			}*/
		}
	}

	public override void DrawMenu (Vector2 UIMousPos, float PickUpScroll)
	{
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(0,0,Screen.width, Screen.height), Texture2D.whiteTexture);

		for(int i = 0; i < questsPosted.Length; i++)
		{
			Rect posterSpace = new Rect((Screen.width/(questsPosted.Length+0.25f))*(i+0.125f), Screen.height/4, Screen.height/5, Screen.height/2);
			Rect button = new Rect(posterSpace.x, posterSpace.y+posterSpace.height+Screen.height/20, posterSpace.width, Screen.height/20);

			if(questsPosted[i] != null)
			{
				GUI.color = Color.black;
				GUI.DrawTexture(posterSpace, Texture2D.whiteTexture);
				GUI.color = Color.white;
				GUI.DrawTexture(new Rect(posterSpace.x+1, posterSpace.y+1, posterSpace.width-2, posterSpace.height-2), Texture2D.whiteTexture);
				GUI.color = Color.black;
				if(questsPosted[i].activeOnBoard)
				{
                    if (questsPosted[i].dungeon.setting != Area.DungeonSetting.Roads)
                    {
                        GUI.Label(posterSpace, "Adventurers needed to slay " + questsPosted[i].monster.ToString() + " at the " + questsPosted[i].dungeon.setting);
                    }
                    else
                    {
                        GUI.Label(posterSpace, "Adventurers needed to slay monsters blocking the roads");
                    }
				}
				else
				{
					int minutes = (int)questsPosted[i].setUpTime/60;
					int seconds = (int)questsPosted[i].setUpTime%60;
					if(seconds >= 10)
					{
						GUI.Label(posterSpace, "Quest ready in\n"+minutes+":"+seconds);
					}
					else
					{
						GUI.Label(posterSpace, "Quest ready in\n"+minutes+":0"+seconds);
					}
				}
			}

			GUI.color = Color.black;
			GUI.DrawTexture(button, Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(button.x+1, button.y+1, button.width-2, button.height-2), Texture2D.whiteTexture);
			GUI.color = Color.black;
			if(questsPosted[i] != null)
			{
				GUI.Label(button, "Remove Quest");
				if(currentMenuState == MenuSubstatus.MainWindow && button.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
				{
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
					questExamining = i;
					currentMenuState = MenuSubstatus.ConfirmRemove;
				}
			}
			else
			{
				GUI.Label(button, "New Quest");
				if(currentMenuState == MenuSubstatus.MainWindow && button.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
				{
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
					questExamining = i;
					currentMenuState = MenuSubstatus.Order;
				}
			}
		}

		if(currentMenuState == MenuSubstatus.ConfirmRemove)
		{
			Rect popUpScreen = new Rect(Screen.width/2 - Screen.height/4, Screen.height*0.25f, Screen.height/2, Screen.height/2);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(popUpScreen.x-3, popUpScreen.y-3, popUpScreen.width+6, popUpScreen.height+6), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(popUpScreen, Texture2D.whiteTexture);
			GUI.color = Color.black;
			Rect confirmText = new Rect(popUpScreen.x, popUpScreen.y, popUpScreen.width, popUpScreen.height/2);
			GUI.Label(confirmText, "Remove Quest? \n No resources will be refunded.");

			Rect confirm = new Rect(popUpScreen.x+popUpScreen.width/20, popUpScreen.y+popUpScreen.height-popUpScreen.height/5-popUpScreen.height/20, 
			                        popUpScreen.width/3, popUpScreen.height/5);

			Rect cancel = new Rect(popUpScreen.x+popUpScreen.width-popUpScreen.width/20-popUpScreen.width/3, popUpScreen.y+popUpScreen.height-popUpScreen.height/5-popUpScreen.height/20, 
			                        popUpScreen.width/3, popUpScreen.height/5);

			GUI.DrawTexture(new Rect(cancel.x-2, cancel.y-2, cancel.width+4, cancel.height+4), Texture2D.whiteTexture);
			GUI.DrawTexture(new Rect(confirm.x-2, confirm.y-2, confirm.width+4, confirm.height+4), Texture2D.whiteTexture);

			GUI.color = Color.white;
			
			GUI.DrawTexture(cancel, Texture2D.whiteTexture);
			GUI.DrawTexture(confirm, Texture2D.whiteTexture);

			GUI.color = Color.black;

			GUI.Label(confirm, "Yes");
			GUI.Label(cancel, "No");

			if(confirm.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				questsPosted[questExamining].Offload();
				questsPosted[questExamining] = null;
				currentMenuState = MenuSubstatus.MainWindow;
			}
			else if(cancel.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				currentMenuState = MenuSubstatus.MainWindow;
			}
		}
		else if(currentMenuState == MenuSubstatus.Order)
        {
            bool showMonsters = true;
            Area.DungeonSetting dunList;
            if (MainMechanics.boarderingArea[Area.DungeonSetting.Roads].lostMonsters.Count <= 0)
            {
                dunList = (Area.DungeonSetting)(dungeonSettingDropdown.selectedOption + 2);
            }
            else
            {
                dunList = (Area.DungeonSetting)(dungeonSettingDropdown.selectedOption + 1);
                if((Area.DungeonSetting)(dungeonSettingDropdown.selectedOption + 1) == Area.DungeonSetting.Roads)
                {
                    showMonsters = false;
                }
            }

            Rect popUpScreen = new Rect(Screen.width/2 - Screen.height/4, 0, Screen.height/2, Screen.height);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(popUpScreen.x-3, popUpScreen.y-3, popUpScreen.width+6, popUpScreen.height+6), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(popUpScreen, Texture2D.whiteTexture);

			Rect levelTitle = new Rect(popUpScreen.x, popUpScreen.y+popUpScreen.height/20, popUpScreen.width, popUpScreen.height/20);
			Rect levelNeg = new Rect(popUpScreen.x+popUpScreen.width/20, levelTitle.y+levelTitle.height, popUpScreen.width/10, popUpScreen.width/10);
			Rect levelDisplay = new Rect(levelNeg.x+levelNeg.width, levelNeg.y, popUpScreen.width*7/10, levelNeg.height);
			Rect levelPos = new Rect(levelDisplay.x+levelDisplay.width, levelDisplay.y, levelNeg.width, levelNeg.height);

            if (showMonsters)
            {
                GUI.DrawTexture(levelNeg, MainMechanics.mechanics.GUINegButton);
                GUI.DrawTexture(levelPos, MainMechanics.mechanics.GUIPosButton);
                GUI.color = Color.black;
                GUI.Label(levelTitle, "Level");
                GUI.Label(levelDisplay, "" + tempQuestLevel);
            }

            GUI.color = Color.black;
            Rect dunLocTitle = new Rect(levelDisplay.x, levelDisplay.y+levelDisplay.height*2, levelDisplay.width, levelDisplay.height);
			GUI.Label(dunLocTitle, "Dungeon Location");
			Rect dunDropRect = new Rect(levelDisplay.x, dunLocTitle.y+dunLocTitle.height, levelDisplay.width, levelDisplay.height);

            Rect dunMonTitle = new Rect(dunDropRect.x, dunDropRect.y + dunDropRect.height * 2, dunDropRect.width, dunDropRect.height);
            if (showMonsters)
            {
                GUI.Label(dunMonTitle, "Monsters");
            }
            Rect dunMonDropRect = new Rect(levelDisplay.x, dunMonTitle.y + dunMonTitle.height, levelDisplay.width, levelDisplay.height);

            Rect wildMonTitleRect = new Rect(dunMonDropRect.x, dunMonDropRect.y + dunMonDropRect.height*2, dunMonDropRect.width, dunMonDropRect.height);
            Rect wildMonLevelRect = new Rect(dunMonDropRect.x, wildMonTitleRect.y + wildMonTitleRect.height, dunMonDropRect.width, dunMonDropRect.height);

            Rect wildChanceTitleRect = new Rect(wildMonLevelRect.x, wildMonLevelRect.y+wildMonLevelRect.height+wildMonTitleRect.height, wildMonTitleRect.width, wildMonTitleRect.height);
            Rect wildChanceNumberRect = new Rect(wildChanceTitleRect.x, wildChanceTitleRect.y+wildChanceTitleRect.height, wildChanceTitleRect.width, wildChanceTitleRect.height);


            GUI.Label(wildMonTitleRect, "Average Wild Monster Level");
            if (showMonsters)
            {
                GUI.Label(wildChanceTitleRect, "Wild Monster Invasion Chance");
            }
            labelStyle.fontSize = Screen.height / 20;
            GUI.Label(wildMonLevelRect, "" + MainMechanics.boarderingArea[dunList].levelAverage);
            if (showMonsters)
            {
                GUI.Label(wildChanceNumberRect, "" + MainMechanics.boarderingArea[dunList].lostMonsters.Count + "%");
            }
            labelStyle.fontSize = Screen.height / 40;

            Rect confirm = new Rect(popUpScreen.x+popUpScreen.width/20, popUpScreen.y+popUpScreen.height-popUpScreen.width/5-popUpScreen.height/20, 
			                        popUpScreen.width/3, popUpScreen.width/5);
			
			Rect cancel = new Rect(popUpScreen.x+popUpScreen.width-popUpScreen.width/20-popUpScreen.width/3, popUpScreen.y+popUpScreen.height-popUpScreen.width/5-popUpScreen.height/20, 
			                       popUpScreen.width/3, popUpScreen.width/5);

			GUI.DrawTexture(new Rect(cancel.x-2, cancel.y-2, cancel.width+4, cancel.height+4), Texture2D.whiteTexture);
			GUI.DrawTexture(new Rect(confirm.x-2, confirm.y-2, confirm.width+4, confirm.height+4), Texture2D.whiteTexture);
			
			GUI.color = Color.white;
			
			GUI.DrawTexture(cancel, Texture2D.whiteTexture);
			GUI.DrawTexture(confirm, Texture2D.whiteTexture);
			
			GUI.color = Color.black;
			
			GUI.Label(confirm, "Order");
			GUI.Label(cancel, "Cancel");

            if (showMonsters)
            {
                dungeonMonstersDropdown.Draw(UIMousPos, dunMonDropRect, labelStyle.fontSize);
            }
			dungeonSettingDropdown.Draw(UIMousPos, dunDropRect, labelStyle.fontSize);


			dungeonSettingDropdown.Update(UIMousPos, dunDropRect);
            if (showMonsters)
            {
                dungeonMonstersDropdown.Update(UIMousPos, dunMonDropRect);
            }





			if(levelPos.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				tempQuestLevel++;
			}
			else if(levelNeg.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				tempQuestLevel--;
				if(tempQuestLevel <= 0)
				{
					tempQuestLevel = 1;
				}
			}
			else if(confirm.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				currentMenuState = MenuSubstatus.MainWindow;
				questsPosted[questExamining] = new Quest();
                //Dungeon toAdd = new Dungeon();
                if (showMonsters)
                {
                    if (MainMechanics.boarderingArea[Area.DungeonSetting.Roads].lostMonsters.Count <= 0)
                    {
                        dunList = (Area.DungeonSetting)(dungeonSettingDropdown.selectedOption + 2);
                    }
                    else
                    {
                        dunList = (Area.DungeonSetting)(dungeonSettingDropdown.selectedOption + 1);
                    }
                    var monList = (Monster.MonsterType)dungeonMonstersDropdown.selectedOption;

                    //toAdd.Start(tempQuestLevel, dunList, monList);
                    questsPosted[questExamining].Set(tempQuestLevel, dunList, 0f + (20 * tempQuestLevel), 200f + (20 * tempQuestLevel), monList);
                }
                else
                {
                    questsPosted[questExamining].Set(MainMechanics.boarderingArea[Area.DungeonSetting.Roads].levelAverage);
                }
			}
			else if(cancel.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				currentMenuState = MenuSubstatus.MainWindow;
			}
		}
	}

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
			currentMenuState = MenuSubstatus.MainWindow;
		}
		else if(actionNumber < 0)//Put Down Quest
		{
			interacter.startWorking(2, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
		}
		else if(actionNumber > 0)//Pick Up Quest
		{
			interacter.startWorking(10, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		int whichQuest = Mathf.Abs(choice) - 1;
		if(choice < 0)
		{
			questsPosted[whichQuest].activeOnBoard = true;
		}
		else
		{
			acceptQuest(interacter, whichQuest);
		}
	}

	
	
	void acceptQuest(Humanoid interacter, int questToAccept)
	{
		if(questsPosted[questToAccept] != null && questsPosted[questToAccept].activeOnBoard)
		{
			AdventurerParty party = (AdventurerParty)interacter.partyApartOf;
			party.setQuest(questsPosted[questToAccept]);
			questsPosted[questToAccept] = null;
		}
		else if(questsPosted[questToAccept] == null)
		{
			if(applicableQuestAvailable((AdventurerParty)interacter.partyApartOf, out questToAccept))
			{
				AdventurerParty party = (AdventurerParty)interacter.partyApartOf;
				party.setQuest(questsPosted[questToAccept]);
				questsPosted[questToAccept] = null;
			}
		}
	}

	public bool applicableQuestAvailable(AdventurerParty party, out int toChoose)
	{
		int total = 0;
		for(int m = 0; m < party.Members.Count; m++)
		{
			total += party.Members[m].combatLevel;
		}
		float average = total / 4;

		for(int i = 0; i < questsPosted.Length; i++)
		{
			if(questsPosted[i] != null 
			   && questsPosted[i].activeOnBoard 
			   && Mathf.Abs(questsPosted[i].dungeon.dungeonLevel - average) <= 2)
			{
				toChoose = i;
				return true;
			}
		}
		toChoose = 0;
		return false;
	}
}
