﻿using UnityEngine;
using System.Collections;

public class PastaMachine : InteractableFurnishing
{
    public Holdable pastaFilter;

    public override void getInteraction(Humanoid interacter, int actionNumber)
    {
        if (actionNumber == 0)
        {
            if ((interacter.heldObjects[0] != null && interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.PastaDough)
                ||
                (interacter.heldObjects[1] != null && interacter.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.PastaDough))
            {
                interacter.startWorking(2, actionNumber, this, EmployeeSkill.WorkSkill.CKHumane, 0);
            }
        }
    }

    public override void CompleteInteract(Humanoid interacter, int choice)
    {
        if (choice == 0)
        {
            int handToUse = -1;
            if (interacter.heldObjects[0] != null && interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.PastaDough)
            {
                handToUse = 0;
            }
            else if (interacter.heldObjects[1] != null && interacter.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.PastaDough)
            {
                handToUse = 1;
            }

            if (handToUse != -1)
            {
                GameObject getGrind = FurnishingLibrary.Library.getGrindedObject(interacter.heldObjects[handToUse].holdableID);
                if (getGrind != null)
                {
                    Destroy(interacter.heldObjects[handToUse].gameObject);
                    interacter.heldObjects[handToUse] = getGrind.GetComponent<Holdable>();
                }
            }
        }
    }
}
