﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Furnishing : MonoBehaviour 
{
	public Transform mainTransform;

    public int cost;

	public FurnishingLibrary.FurnishingID FurnishingID;
	public List<PathNode> reachableNodes = new List<PathNode>();
	public bool canAssignWorkers;
	public Room roomInsideOf;
	public Texture2D furnishingTexture;

	public Liquid liquidHere;
	public int maxLiquidContainment;
	public int currentLiquidContainment;

	public bool wallMounted;

	public enum Placability
	{
		Anywhere,
		InsideOnly,
		OutsideOnly
	}
	public Placability currentPlacability;

	void Awake()
	{
		mainTransform = transform;
        furnishingTexture = GetComponent<SpriteRenderer>().sprite.texture;
	}

	// Use this for initialization
	public virtual void Start () 
	{
        if (MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere != null)
        {
            roomInsideOf = MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere.parentRoom;
        }
    }
	
	// Update is called once per frame
	public virtual void Update () 
	{
	
	}

	public virtual void Draw()
	{

	}

	public virtual void CompleteInteract(Humanoid interacter, int choice)
	{

	}

	public virtual void DisplayOptions(Vector2 mousePos)
	{

	}

	public virtual void getInteraction(Humanoid interacter, int actionNumber)
	{

	}
}
