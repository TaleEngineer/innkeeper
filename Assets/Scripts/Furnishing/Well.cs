﻿using UnityEngine;
using System.Collections;

public class Well : InteractableFurnishing
{
	float waterFillTimer = 0;

	public override void Update ()
	{
		base.Update ();
		if(currentLiquidContainment < maxLiquidContainment)
		{
			waterFillTimer+= Time.deltaTime;
			if(waterFillTimer >= 5)
			{
				currentLiquidContainment++;
				waterFillTimer -= 5;
				if(liquidHere == null)
				{
					liquidHere = new Liquid();
					liquidHere.Set (FurnishingLibrary.HoldableID.LiqWater);
				}
			}
		}
	}


	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			int length = 0;
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				if(interacter.heldObjects[0].liquidHere != null 
				   && interacter.heldObjects[0].liquidHere.liquidID == liquidHere.liquidID
				   && interacter.heldObjects[0].currentLiquidContainment < interacter.heldObjects[0].maxLiquidContainment)
				{
					length += interacter.heldObjects[0].maxLiquidContainment - interacter.heldObjects[0].currentLiquidContainment;
				}
				else if(interacter.heldObjects[0].liquidHere == null)
				{
					length += interacter.heldObjects[0].maxLiquidContainment;
				}
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				if(interacter.heldObjects[1].liquidHere != null 
				   && interacter.heldObjects[1].liquidHere.liquidID == liquidHere.liquidID
				   && interacter.heldObjects[1].currentLiquidContainment < interacter.heldObjects[1].maxLiquidContainment)
				{
					length += interacter.heldObjects[1].maxLiquidContainment - interacter.heldObjects[1].currentLiquidContainment;
				}
				else if(interacter.heldObjects[1].liquidHere == null)
				{
					length += interacter.heldObjects[1].maxLiquidContainment;
				}
			}

			if(length > currentLiquidContainment)
			{
				interacter.startWorking(currentLiquidContainment/4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
			else
			{
				interacter.startWorking(length/4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		if(choice == 0)
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[0]);
			}

			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[1]);
			}
		}
	}
}
