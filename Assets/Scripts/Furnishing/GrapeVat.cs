﻿using UnityEngine;
using System.Collections;

public class GrapeVat : InteractableFurnishing
{

    /*public override bool getWorkOrder()
    {
        if(maxLiquidContainment - currentLiquidContainment > 15)
        {
            return true;
        }

        if((assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Grapes)
            ||
            (assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Grapes))
        {
            WorkOrder toAdd = new WorkOrder();
            toAdd.Set(this, 0, assignedEmployee, this);
            assignedEmployee.currentWorkOrder.Add(toAdd);
            return false;
        }

        if(MainMechanics.mechanics.holdableIsAvailable(FurnishingLibrary.HoldableID.Grapes))
        {
            WorkOrder toAdd = new WorkOrder();
            toAdd.Set(FurnishingLibrary.HoldableID.Grapes, assignedEmployee, this);
            return false;
        }

        return true;
    }*/

    public override void getInteraction(Humanoid interacter, int actionNumber)
    {
        if(actionNumber == 0)
        {
            int toPlace = 0;
            int willFill = 0;
            FurnishingLibrary.HoldableID JuiceToHave = FurnishingLibrary.HoldableID.Error;

            if(interacter.heldObjects[0] != null)
            {
                FurnishingLibrary.HoldableID willGet = FurnishingLibrary.Library.getJuicedID(interacter.heldObjects[0].holdableID);
                if (willGet != FurnishingLibrary.HoldableID.Error)
                {
                    if (CanFillWithLiquid(willGet))
                    {
                        JuiceToHave = willGet;
                        willFill += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[0].holdableID);
                        toPlace++;
                    }
                }
            }
            if (interacter.heldObjects[1] != null)
            {
                FurnishingLibrary.HoldableID willGet = FurnishingLibrary.Library.getJuicedID(interacter.heldObjects[1].holdableID);
                if (willGet != FurnishingLibrary.HoldableID.Error)
                {
                    if (CanFillWithLiquid(willGet) 
                        && 
                        (JuiceToHave == FurnishingLibrary.HoldableID.Error || JuiceToHave == willGet))
                    {
                        willFill += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[1].holdableID);
                        toPlace++;
                    }
                }
            }

            if (toPlace > 0)
            {
                interacter.startWorking(toPlace*5, actionNumber, this, EmployeeSkill.WorkSkill.BRElven, 2);
            }
        }
        else if(actionNumber == 1)
        {
            if (liquidHere != null)
            {
                int length = 0;
                if (interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
                {
                    if (interacter.heldObjects[0].liquidHere != null
                       && interacter.heldObjects[0].liquidHere.liquidID == liquidHere.liquidID
                       && interacter.heldObjects[0].currentLiquidContainment < interacter.heldObjects[0].maxLiquidContainment)
                    {
                        length += interacter.heldObjects[0].maxLiquidContainment - interacter.heldObjects[0].currentLiquidContainment;
                    }
                    else if (interacter.heldObjects[0].liquidHere == null)
                    {
                        length += interacter.heldObjects[0].maxLiquidContainment;
                    }
                }

                if (interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
                {
                    if (interacter.heldObjects[1].liquidHere != null
                       && interacter.heldObjects[1].liquidHere.liquidID == liquidHere.liquidID
                       && interacter.heldObjects[1].currentLiquidContainment < interacter.heldObjects[1].maxLiquidContainment)
                    {
                        length += interacter.heldObjects[1].maxLiquidContainment - interacter.heldObjects[1].currentLiquidContainment;
                    }
                    else if (interacter.heldObjects[1].liquidHere == null)
                    {
                        length += interacter.heldObjects[1].maxLiquidContainment;
                    }
                }

                if (length > currentLiquidContainment)
                {
                    interacter.startWorking(currentLiquidContainment / 4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
                }
                else
                {
                    interacter.startWorking(length / 4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
                }
            }
        }
    }

    public override void CompleteInteract(Humanoid interacter, int choice)
    {
        if(choice == 0)
        {
            FurnishingLibrary.HoldableID willGetO = FurnishingLibrary.HoldableID.Error;
            FurnishingLibrary.HoldableID willGetI = FurnishingLibrary.HoldableID.Error;

            if (interacter.heldObjects[0] != null)
            {
                willGetO = FurnishingLibrary.Library.getJuicedID(interacter.heldObjects[0].holdableID);
            }
            if (interacter.heldObjects[1] != null)
            {
                willGetI = FurnishingLibrary.Library.getJuicedID(interacter.heldObjects[1].holdableID);
            }



            if (CanFillWithLiquid(willGetO))
            {
                if (liquidHere != null)
                {
                    currentLiquidContainment += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[0].holdableID);
                    Destroy(interacter.heldObjects[0].gameObject);
                    interacter.heldObjects[0] = null;
                }
                else
                {
                    liquidHere = new Liquid();
                    liquidHere.Set(willGetO);
                    currentLiquidContainment += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[0].holdableID);
                    Destroy(interacter.heldObjects[0].gameObject);
                    interacter.heldObjects[0] = null;
                }
            }
            if (CanFillWithLiquid(willGetI))
            {
                if (liquidHere != null)
                {
                    currentLiquidContainment += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[1].holdableID);
                    Destroy(interacter.heldObjects[1].gameObject);
                    interacter.heldObjects[1] = null;
                }
                else
                {
                    liquidHere = new Liquid();
                    liquidHere.Set(willGetI);
                    currentLiquidContainment += FurnishingLibrary.Library.getJuiceAmount(interacter.heldObjects[1].holdableID);
                    Destroy(interacter.heldObjects[1].gameObject);
                    interacter.heldObjects[1] = null;
                }
            }

            if(currentLiquidContainment > maxLiquidContainment)
            {
                currentLiquidContainment = maxLiquidContainment;
            }
        }
        else if(choice == 1)
        {
            if (interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
            {
                PourLiquidIntoContainer(interacter, interacter.heldObjects[0]);
            }

            if (interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
            {
                PourLiquidIntoContainer(interacter, interacter.heldObjects[1]);
            }
        }
    }
}
