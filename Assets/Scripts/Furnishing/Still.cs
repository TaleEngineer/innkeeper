﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Still : InteractableFurnishing
{
	float cookTimer = 0;
	float timeToCook;
	bool cooking;

    int liqBase = 0;

    Dictionary<FurnishingLibrary.HoldableID, List<Holdable>> holdablesHere = new Dictionary<FurnishingLibrary.HoldableID, List<Holdable>>();

    Dictionary<FurnishingLibrary.HoldableID, int> displayAmounts = new Dictionary<FurnishingLibrary.HoldableID, int>();

    Dictionary<FurnishingLibrary.HoldableID, int> recipe = new Dictionary<FurnishingLibrary.HoldableID, int>();

    FurnishingLibrary.HoldableID requiredLiquid;

    FurnishingLibrary.HoldableID brew;

    float brewScroll = 0f;
    float ingredScroll = 0f;

    FurnishingLibrary.HoldableID toBrew
    {
        get
        {
            return brew;
        }
        set
        {
            brew = value;
            recipe = MainMechanics.recipeBook.WhatMakeLiq(value);
            UpdateDisplayAmounts();
        }
    }

    void UpdateDisplayAmounts()
    {
        displayAmounts.Clear();

        if (toBrew != FurnishingLibrary.HoldableID.Error)
        {
            List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(recipe.Keys);
            for (int i = 0; i < keys.Count; i++)
            {
                List<Holdable> haveHere;
                char[] words = keys[i].ToString().ToCharArray();

                if (words[0] == 'L' && words[1] == 'i' && words[2] == 'q')
                {
                    displayAmounts.Add(keys[i], 0);
                    requiredLiquid = keys[i];
                    if(liquidHere == null || liquidHere.liquidID != keys[i])
                    {
                        liqBase = 0;
                    }
                    else
                    {
                        liqBase = currentLiquidContainment / recipe[keys[i]];
                        if(currentLiquidContainment % recipe[keys[i]] != 0)
                        {
                            liqBase++;
                        }
                    }
                }
                else
                {
                    if (holdablesHere.TryGetValue(keys[i], out haveHere))
                    {
                        displayAmounts.Add(keys[i], haveHere.Count);
                    }
                    else
                    {
                        displayAmounts.Add(keys[i], 0);
                    }
                }
            }
        }


        List<FurnishingLibrary.HoldableID> hKeys = new List<FurnishingLibrary.HoldableID>(holdablesHere.Keys);
        for (int i = 0; i < hKeys.Count; i++)
        {
            int whoCares;
            if(!displayAmounts.TryGetValue(hKeys[i], out whoCares) && holdablesHere[hKeys[i]].Count > 0)
            {
                displayAmounts.Add(hKeys[i], holdablesHere[hKeys[i]].Count);
            }
        }
    }

    public override void DrawMenu(Vector2 UIMousPos, float PickUpScroll)
    {
        var labelStyle = GUI.skin.GetStyle("Label");
        labelStyle.fontSize = Screen.height / 40;

        Rect background = new Rect(0, 0, Screen.width, Screen.height);
        GUI.DrawTexture(background, Texture2D.whiteTexture);

        float heightOfThing = CellarStairs.heightOfThing;

        Rect LeftHandUseRect = new Rect(Screen.width / 2 - Screen.height / 8, Screen.height - Screen.height * heightOfThing,
            Screen.height / 8, Screen.height * heightOfThing);
        Rect RightHandUseRect = new Rect(Screen.width / 2, LeftHandUseRect.y, Screen.height / 8, LeftHandUseRect.height);

        Rect dryIngredRect = new Rect(0,0,Screen.width/3, Screen.height);
        Rect tryMakeRect = new Rect(Screen.width*2/3, 0, Screen.width / 3, Screen.height);

        GUI.color = Color.black;
        GUI.DrawTexture(new Rect(0, 0, dryIngredRect.width + 1, dryIngredRect.height), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(tryMakeRect.x - 1, tryMakeRect.y, 1, tryMakeRect.height), Texture2D.whiteTexture);

        GUI.color = Color.white;
        GUI.DrawTexture(dryIngredRect, Texture2D.whiteTexture);
        GUI.DrawTexture(tryMakeRect, Texture2D.whiteTexture);

        GUI.color = new Color(0, 0.231f, 0.345f);
        GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
        GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
        GUI.color = new Color(0, 0.486f, 0.729f);
        GUI.DrawTexture(new Rect(LeftHandUseRect.x + 1, LeftHandUseRect.y + 1, LeftHandUseRect.width - 2, LeftHandUseRect.height - 2), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(RightHandUseRect.x + 1, RightHandUseRect.y + 1, RightHandUseRect.width - 2, RightHandUseRect.height - 2), Texture2D.whiteTexture);
        GUI.color = Color.white;
        if (MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
        {
            Rect heldLeft = new Rect(LeftHandUseRect.x, LeftHandUseRect.y, LeftHandUseRect.width, LeftHandUseRect.width);
            GUI.color = Color.white;
            GUI.DrawTexture(heldLeft, MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableTexture);

            if (Input.GetMouseButtonDown(0) && heldLeft.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                if (MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere != null)
                {
                    getInteraction(MainMechanics.mechanics.Innkeeper, 1);
                    MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
                }
                else if (!MainMechanics.mechanics.Innkeeper.heldObjects[0].canHoldLiquid)
                {
                    PutItemFromHand(MainMechanics.mechanics.Innkeeper, 0);
                }
            }
        }
        if (MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
        {
            Rect heldRight = new Rect(RightHandUseRect.x, RightHandUseRect.y, RightHandUseRect.width, RightHandUseRect.width);
            GUI.color = Color.white;
            GUI.DrawTexture(heldRight, MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableTexture);

            if (Input.GetMouseButtonDown(0) && heldRight.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                if (MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere != null)
                {
                    getInteraction(MainMechanics.mechanics.Innkeeper, 1);
                    MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
                }
                else if (!MainMechanics.mechanics.Innkeeper.heldObjects[1].canHoldLiquid)
                {
                    PutItemFromHand(MainMechanics.mechanics.Innkeeper, 1);
                }
            }
        }

        Rect fillButtonRect = new Rect(LeftHandUseRect.x-2, LeftHandUseRect.y- LeftHandUseRect.height, LeftHandUseRect.width, Screen.height/20);
        Rect brewButtonRect = new Rect(RightHandUseRect.x + 2, fillButtonRect.y, fillButtonRect.width, fillButtonRect.height);
        GUI.color = Color.black;
        GUI.DrawTexture(new Rect(fillButtonRect.x - 1, fillButtonRect.y - 1, fillButtonRect.width + 2, fillButtonRect.height + 2), Texture2D.whiteTexture);
        GUI.DrawTexture(new Rect(brewButtonRect.x - 1, brewButtonRect.y - 1, brewButtonRect.width + 2, brewButtonRect.height + 2), Texture2D.whiteTexture);
        GUI.color = Color.white;
        GUI.DrawTexture(fillButtonRect, Texture2D.whiteTexture);
        GUI.DrawTexture(brewButtonRect, Texture2D.whiteTexture);
        GUI.color = Color.black;
        labelStyle.alignment = TextAnchor.MiddleCenter;
        GUI.Label(fillButtonRect, "Fill Held");
        GUI.Label(brewButtonRect, "Brew");
        labelStyle.alignment = TextAnchor.MiddleLeft;

        if(Input.GetMouseButtonDown(0) && fillButtonRect.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
        {
            getInteraction(MainMechanics.mechanics.Innkeeper, 2);
            MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
        }
        if (Input.GetMouseButtonDown(0) && brewButtonRect.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
        {
            getInteraction(MainMechanics.mechanics.Innkeeper, 3);
            MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
        }
        Rect liqHereRect = new Rect(Screen.width / 2 - Screen.height / 8, fillButtonRect.y - Screen.height / 4, Screen.height / 4, Screen.height / 4);
        GUI.color = Color.gray;
        GUI.DrawTexture(liqHereRect, MainMechanics.mechanics.LiquidDrop);
        if (liquidHere != null)
        {
            GUI.color = liquidHere.liquidColor;
            float percentHere = (float)currentLiquidContainment / (float)maxLiquidContainment;
            Rect unmaskedArea = new Rect(liqHereRect.x, liqHereRect.y + ((1f - percentHere) * liqHereRect.height), liqHereRect.width, liqHereRect.height * percentHere);
            GUI.BeginGroup(unmaskedArea);
            GUI.DrawTexture(new Rect(0, 0 - (1f - percentHere) * liqHereRect.height, liqHereRect.width, liqHereRect.height), FurnishingLibrary.Library.getHoldablePrefab(liquidHere.liquidID).holdableTexture);
            GUI.EndGroup();
        }
        GUI.color = Color.white;

        List<FurnishingLibrary.HoldableID> displayKeys = new List<FurnishingLibrary.HoldableID>(displayAmounts.Keys);
        for(int i = 0; i < displayKeys.Count; i++)
        {
            var idName = displayKeys[i].ToString().ToCharArray();
            bool dealingWithLiq = false;
            if (idName[0] == 'L' && idName[1] == 'i' && idName[2] == 'q')
            {
                dealingWithLiq = true;
            }
            bool failure = false;
            Rect bodyRect = new Rect(dryIngredRect.x, dryIngredRect.y+dryIngredRect.height/20*i, dryIngredRect.width, dryIngredRect.height/20);
            if (!dealingWithLiq)
            {
                int amountNeeded = 0;
                if (recipe.TryGetValue(displayKeys[i], out amountNeeded) && amountNeeded*liqBase > displayAmounts[displayKeys[i]])
                {
                    failure = true;
                }
            }
            GUI.color = Color.black;
            GUI.DrawTexture(new Rect(bodyRect.x, bodyRect.y - 1, bodyRect.width, bodyRect.height + 2), Texture2D.whiteTexture);
            if ((dealingWithLiq && (liquidHere == null || displayKeys[i] != liquidHere.liquidID))
                ||
                (failure))
            {
                GUI.color = Color.red;
                failure = true;
            }
            else
            {
                GUI.color = Color.white;
            }
            GUI.DrawTexture(bodyRect, Texture2D.whiteTexture);
            Rect imageRect = new Rect(bodyRect.x, bodyRect.y, bodyRect.height, bodyRect.height);
            Rect textRect = new Rect(imageRect.x + imageRect.width, imageRect.y, bodyRect.width - imageRect.width, bodyRect.height);
            if(failure)
            {
                GUI.color = Color.white;
            }
            GUI.DrawTexture(imageRect, FurnishingLibrary.Library.getHoldablePrefab(displayKeys[i]).holdableTexture);
            GUI.color = Color.black;
            if (dealingWithLiq)
            {
                GUI.Label(textRect, FurnishingLibrary.Library.getItemName(displayKeys[i], true));
            }
            else
            {
                GUI.Label(textRect, displayKeys[i].ToString() + " x"+displayAmounts[displayKeys[i]]);
            }

            if(displayAmounts[displayKeys[i]] > 0 && Input.GetMouseButtonDown(0) && bodyRect.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                PickUpItemToHand(MainMechanics.mechanics.Innkeeper, displayKeys[i]);
            }
        }

        for (int i = 0; i < MainMechanics.recipeBook.knownBrewableIDs.Count; i++)
        {
            bool chosen = false;
            if(MainMechanics.recipeBook.knownBrewableIDs[i] == toBrew)
            {
                chosen = true;
            }
            Rect bodyRect = new Rect(tryMakeRect.x, tryMakeRect.y + tryMakeRect.height / 20 * i, tryMakeRect.width, tryMakeRect.height / 20);

            GUI.color = Color.black;
            GUI.DrawTexture(new Rect(bodyRect.x, bodyRect.y - 1, bodyRect.width, bodyRect.height + 2), Texture2D.whiteTexture);
            if (!chosen)
            {
                GUI.color = Color.white;
            }
            GUI.DrawTexture(bodyRect, Texture2D.whiteTexture);
            Rect imageRect = new Rect(bodyRect.x, bodyRect.y, bodyRect.height, bodyRect.height);
            Rect textRect = new Rect(imageRect.x + imageRect.width, imageRect.y, bodyRect.width - imageRect.width, bodyRect.height);

            GUI.color = Color.white;
            GUI.DrawTexture(imageRect, FurnishingLibrary.Library.getHoldablePrefab(MainMechanics.recipeBook.knownBrewableIDs[i]).holdableTexture);

            if (!chosen)
            {
                GUI.color = Color.black;
            }
            GUI.Label(textRect, FurnishingLibrary.Library.getItemName(MainMechanics.recipeBook.knownBrewableIDs[i], true));

            if(Input.GetMouseButtonDown(0) && bodyRect.Contains(UIMousPos) && MainMechanics.mechanics.currentButtonWait <= 0)
            {
                MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
                toBrew = MainMechanics.recipeBook.knownBrewableIDs[i];
            }
        }


    }

    public override bool getWorkOrder ()
	{
		if((liquidHere != null && liquidHere.liquidID != requiredLiquid) || cooking || toBrew == FurnishingLibrary.HoldableID.Error)
		{
			return true;
		}

		if(liquidHere != null && liquidHere.liquidID == requiredLiquid && currentLiquidContainment == maxLiquidContainment && CanBrew())
		{
			WorkOrder toAssign = new WorkOrder();
			toAssign.Set (this, 3, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(toAssign);
			return false;
		}


        if (currentLiquidContainment < maxLiquidContainment)
        {
            if ((assignedEmployee.heldObjects[0] != null
               && assignedEmployee.heldObjects[0].liquidHere != null
                && assignedEmployee.heldObjects[0].liquidHere.liquidID == requiredLiquid)
               ||
               (assignedEmployee.heldObjects[1] != null
                && assignedEmployee.heldObjects[1].liquidHere != null
             && assignedEmployee.heldObjects[1].liquidHere.liquidID == requiredLiquid))
            {
                WorkOrder toAssign = new WorkOrder();
                toAssign.Set(this, 1, assignedEmployee, this);
                assignedEmployee.currentWorkOrder.Add(toAssign);
                return false;
            }

            bool hasBarrel;
            if (MainMechanics.mechanics.holdableIsAvailable(requiredLiquid, out hasBarrel))
            {
                WorkOrder toAssign = new WorkOrder();
                toAssign.Set(requiredLiquid, assignedEmployee, this);
                assignedEmployee.currentWorkOrder.Add(toAssign);
                return false;
            }

            List<Furnishing> whoCares;
            if (MainMechanics.mechanics.isFurnishingLiquidAvailable(requiredLiquid, out whoCares))
            {
                if ((assignedEmployee.heldObjects[0] != null
                && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel
                && assignedEmployee.heldObjects[0].liquidHere == null)
               ||
               (assignedEmployee.heldObjects[1] != null
             && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel
             && assignedEmployee.heldObjects[1].liquidHere == null))
                {
                    WorkOrder toAssign = new WorkOrder();
                    FurnishingLibrary.FurnishingID whatGetFrom = MainMechanics.recipeBook.WhereMake(requiredLiquid);
                    int howGet = MainMechanics.recipeBook.HowRetrieve(requiredLiquid);

                    toAssign.Set(whatGetFrom, requiredLiquid, howGet, assignedEmployee, this);
                    assignedEmployee.currentWorkOrder.Add(toAssign);
                    return false;
                }
            }
            else
            {
                if(MainMechanics.mechanics.hasFurnishing(MainMechanics.recipeBook.WhereMake(requiredLiquid)))
                {
                    Dictionary<FurnishingLibrary.HoldableID, int> recipeBits = MainMechanics.recipeBook.WhatMakeLiq(requiredLiquid);
                    if (recipeBits.Count > 0)
                    {
                        List<FurnishingLibrary.HoldableID> whatTake = new List<FurnishingLibrary.HoldableID>(recipeBits.Keys);
                        if ((assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == whatTake[0])
                            ||
                            (assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == whatTake[0]))
                        {
                            WorkOrder toAdd = new WorkOrder();
                            toAdd.Set(MainMechanics.recipeBook.WhereMake(requiredLiquid), MainMechanics.recipeBook.HowMake(requiredLiquid), assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAdd);
                            return false;
                        }
                        if (MainMechanics.mechanics.holdableIsAvailable(whatTake[0]))
                        {
                            WorkOrder toAdd = new WorkOrder();
                            toAdd.Set(whatTake[0], assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAdd);
                            return false;
                        }
                    }
                }
            }

            if ((assignedEmployee.heldObjects[0] != null
                        && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel
                        && assignedEmployee.heldObjects[0].liquidHere == null)
                    ||
                    (assignedEmployee.heldObjects[1] != null
                        && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel
                        && assignedEmployee.heldObjects[1].liquidHere == null))
            {
                //No idea
            }
            else if(hasBarrel)
            {
                WorkOrder toAssign = new WorkOrder();
                toAssign.Set(FurnishingLibrary.HoldableID.Barrel, assignedEmployee, this);
                assignedEmployee.currentWorkOrder.Add(toAssign);
                return false;
            }
        }

        List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(recipe.Keys);
        for (int i = 0; i < keys.Count; i++)
        {
            if (keys[i] != requiredLiquid)
            {
                int haveHere;
                if (displayAmounts.TryGetValue(keys[i], out haveHere) && haveHere < recipe[keys[i]] * liqBase)
                {
                    if (assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == keys[i])
                    {
                        WorkOrder toAssign = new WorkOrder();
                        toAssign.Set(this, 1000, assignedEmployee, this);
                        assignedEmployee.currentWorkOrder.Add(toAssign);
                        return false;
                    }
                    else if (assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == keys[i])
                    {
                        WorkOrder toAssign = new WorkOrder();
                        toAssign.Set(this, -1000, assignedEmployee, this);
                        assignedEmployee.currentWorkOrder.Add(toAssign);
                        return false;
                    }
                    else if (MainMechanics.mechanics.holdableIsAvailable(keys[i]))
                    {
                        WorkOrder toAssign = new WorkOrder();
                        toAssign.Set(keys[i], assignedEmployee, this);
                        assignedEmployee.currentWorkOrder.Add(toAssign);
                        return false;
                    }
                }
            }
        }
		return true;
	}

	public override void Update ()
	{
		base.Update ();
		if(cooking)
		{
			cookTimer += Time.deltaTime;
			if(cookTimer >= timeToCook)
			{
                float quality = liquidHere.tasteQuality;
				liquidHere = new Liquid();
				liquidHere.Set (toBrew);
                liquidHere.tasteQuality *= (1 + quality);
                cooking = false;
                UpdateDisplayAmounts();
            }
		}

	}

	public override void Draw ()
	{
		base.Draw ();
		if(cooking)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);
			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize, halfSize*0.3f)/2, new Vector2(halfSize,halfSize*0.3f));
			GUI.color = new Color(0,0,0,0.75f);
			GUI.DrawTexture(drawRect, Texture2D.whiteTexture);
			
			Rect progressRect = new Rect(drawRect.x+1, drawRect.y+1, cookTimer/timeToCook*(drawRect.width-2), drawRect.height-2);
			
			GUI.color = Color.white;
			
			GUI.DrawTexture(progressRect, MainMechanics.mechanics.progressMarker);
		}
	}

	public override void DisplayOptions (Vector2 MousPos)
	{
		if(!cooking)
		{
			base.DisplayOptions (MousPos);
		}
	}
	
	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
        if(actionNumber == 0)//OPEN MENU
        {
            MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
            MainMechanics.mechanics.employeeFurnish = this;
        }
        else if(actionNumber == 1)//Pour Water into Still
		{
			int length = 0;
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				if(interacter.heldObjects[0].liquidHere != null 
				   && (liquidHere == null || interacter.heldObjects[0].liquidHere.liquidID == liquidHere.liquidID))
				{
					length += interacter.heldObjects[0].currentLiquidContainment;
				}
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				if(interacter.heldObjects[1].liquidHere != null 
				   && (liquidHere == null || interacter.heldObjects[1].liquidHere.liquidID == liquidHere.liquidID))
				{
					length += interacter.heldObjects[1].currentLiquidContainment;
				}
			}
			
			if(length > maxLiquidContainment - currentLiquidContainment)
			{
				interacter.startWorking((maxLiquidContainment - currentLiquidContainment)/8f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
			else
			{
				interacter.startWorking(length/8f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
			}
		}
		else if(actionNumber == 2)//Fill barrel with Beer
		{
			if(liquidHere != null)
			{
				int length = 0;
				if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
				{
					if(interacter.heldObjects[0].liquidHere != null 
					   && interacter.heldObjects[0].liquidHere.liquidID == liquidHere.liquidID
					   && interacter.heldObjects[0].currentLiquidContainment < interacter.heldObjects[0].maxLiquidContainment)
					{
						length += interacter.heldObjects[0].maxLiquidContainment - interacter.heldObjects[0].currentLiquidContainment;
					}
					else if(interacter.heldObjects[0].liquidHere == null)
					{
						length += interacter.heldObjects[0].maxLiquidContainment;
					}
				}
				
				if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
				{
					if(interacter.heldObjects[1].liquidHere != null 
					   && interacter.heldObjects[1].liquidHere.liquidID == liquidHere.liquidID
					   && interacter.heldObjects[1].currentLiquidContainment < interacter.heldObjects[1].maxLiquidContainment)
					{
						length += interacter.heldObjects[1].maxLiquidContainment - interacter.heldObjects[1].currentLiquidContainment;
					}
					else if(interacter.heldObjects[1].liquidHere == null)
					{
						length += interacter.heldObjects[1].maxLiquidContainment;
					}
				}
				
				if(length > currentLiquidContainment)
				{
					interacter.startWorking(currentLiquidContainment/4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
				}
				else
				{
					interacter.startWorking(length/4f, actionNumber, this, EmployeeSkill.WorkSkill.Generic, 0);
				}
			}
		}
		else if (actionNumber == 3)//Brew Beer
		{
			if(CanBrew())
			{
				interacter.startWorking(1f, actionNumber, this, EmployeeSkill.WorkSkill.Brewing, 20);
                MainMechanics.recipeBook.GiveRacialCookingXP(toBrew, interacter);
            }
		}
        else if(actionNumber == 1000)
        {
            PutItemFromHand(interacter, 0);
        }
        else if(actionNumber == -1000)
        {
            PutItemFromHand(interacter, 1);
        }
        else if(actionNumber < 0)
        {
            PickUpItemToHand(interacter, (FurnishingLibrary.HoldableID) (-actionNumber));
        }

	}
	
	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		if(choice == 1)
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				interacter.heldObjects[0].PourLiquidIntoFurnishing(this);
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				interacter.heldObjects[1].PourLiquidIntoFurnishing(this);
			}
            UpdateDisplayAmounts();
		}
		else if(choice == 2)
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[0]);
			}
			
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].canHoldLiquid)
			{
				PourLiquidIntoContainer(interacter, interacter.heldObjects[1]);
            }
            UpdateDisplayAmounts();
        }
		else if(choice == 3)
		{
			cooking = true;
			cookTimer = 0;
			timeToCook = currentLiquidContainment/10;
            EmployeeSkill skill;
            if (interacter.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Brewing, out skill))
            {
                liquidHere.tasteQuality += skill.level / 2f;
            }
            ConsumeIngred();
            UpdateDisplayAmounts();
        }
	}

    void PutItemFromHand(Humanoid interacter, int handNumber)
    {
        if(handNumber > 1 || handNumber < 0)
        {
            return;
        }

        if(interacter.heldObjects[handNumber] != null && !interacter.heldObjects[handNumber].canHoldLiquid)
        {
            List<Holdable> whoCares;
            if(holdablesHere.TryGetValue(interacter.heldObjects[handNumber].holdableID, out whoCares))
            {
                holdablesHere[interacter.heldObjects[handNumber].holdableID].Add(interacter.heldObjects[handNumber]);
            }
            else
            {
                List<Holdable> toAdd = new List<Holdable>();
                toAdd.Add(interacter.heldObjects[handNumber]);
                holdablesHere.Add(interacter.heldObjects[handNumber].holdableID, toAdd);
            }

            interacter.heldObjects[handNumber].gameObject.SetActive(false);
            interacter.heldObjects[handNumber] = null;
            UpdateDisplayAmounts();
        }
    }

    void PickUpItemToHand(Humanoid interacter, FurnishingLibrary.HoldableID ID)
    {
        int handNumber = -1;

        if (interacter.heldObjects[0] == null)
        {
            handNumber = 0;
        }
        else if(interacter.heldObjects[1] == null)
        {
            handNumber = 1;
        }

        if(handNumber == -1)
        {
            return;
        }

        List<Holdable> whoCares;
        if (holdablesHere.TryGetValue(ID, out whoCares) && whoCares.Count > 0)
        {
            interacter.heldObjects[handNumber] = holdablesHere[ID][0];
            interacter.heldObjects[handNumber].gameObject.SetActive(true);
            holdablesHere[ID].RemoveAt(0);
            UpdateDisplayAmounts();
        }
        
    }

    bool CanBrew()
    {
        if(toBrew == FurnishingLibrary.HoldableID.Error)
        {
            return false;
        }

        List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(recipe.Keys);
        for(int i = 0; i < keys.Count; i++)
        {
            char[] word = keys[i].ToString().ToCharArray();
            if (word[0] == 'L' && word[1] == 'i' && word[2] == 'q')
            {
                if(liquidHere == null || liquidHere.liquidID != keys[i])
                {
                    return false;
                }
            }
            else
            {
                int amountHave;
                if (!displayAmounts.TryGetValue(keys[i], out amountHave) || recipe[keys[i]] * liqBase > amountHave)
                {
                    return false;
                }
            }
        }

        return true;
    }

    void ConsumeIngred()
    {
        List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(recipe.Keys);
        for (int i = 0; i < keys.Count; i++)
        {
            char[] word = keys[i].ToString().ToCharArray();
            if (word[0] != 'L' || word[1] != 'i' || word[2] != 'q')
            {
                int toRemove = recipe[keys[i]] * liqBase;
                for (int r = 0; r < toRemove; r++)
                {
                    holdablesHere[keys[i]].RemoveAt(0);
                }
            }
        }
    }
}
