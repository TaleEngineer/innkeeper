﻿using UnityEngine;
using System.Collections;

public class MeatGrinder : InteractableFurnishing
{
    public override void getInteraction(Humanoid interacter, int actionNumber)
    {
        if(actionNumber == 0)
        {
            if((interacter.heldObjects[0] != null && interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.RawMeat)
                ||
                (interacter.heldObjects[1] != null && interacter.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.RawMeat))
            {
                interacter.startWorking(2, actionNumber, this, EmployeeSkill.WorkSkill.Cooking, 1);
            }
        }
    }

    public override void CompleteInteract(Humanoid interacter, int choice)
    {
        if (choice == 0)
        {
            int handToUse = -1;
            if (interacter.heldObjects[0] != null && interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.RawMeat)
            {
                handToUse = 0;
            }
            else if(interacter.heldObjects[1] != null && interacter.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.RawMeat)
            {
                handToUse = 1;
            }

            if(handToUse != -1)
            {
                GameObject getGrind = FurnishingLibrary.Library.getGrindedObject(interacter.heldObjects[handToUse].holdableID);
                if (getGrind != null)
                {
                    Destroy(interacter.heldObjects[handToUse].gameObject);
                    interacter.heldObjects[handToUse] = getGrind.GetComponent<Holdable>();
                    MainMechanics.recipeBook.GiveRacialCookingXP(interacter.heldObjects[handToUse].holdableID, interacter);
                }
            }
        }
    }

}
