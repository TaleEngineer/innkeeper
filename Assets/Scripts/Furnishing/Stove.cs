﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Stove : InteractableFurnishing
{
    public Bakeable[] stoveTop = new Bakeable[2];
    Humanoid[] bakerSlots = new Humanoid[2];

    public List<Humanoid> chefsCooking = new List<Humanoid>();

    public override void Draw()
    {
        float halfSize = Screen.height / (Camera.main.orthographicSize * 2);

        for (int i = 0; i < 2; i++)
        {
            if (stoveTop[i] != null)
            {
                float placement = i - 0.5f;
                Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position + transform.TransformDirection(new Vector2(placement, 0)));
                midPoint = new Vector2(midPoint.x, Screen.height - midPoint.y);

                Rect slot = new Rect(midPoint - new Vector2(halfSize / 2, halfSize / 2) / 2, new Vector2(halfSize / 2, halfSize / 2));
                GUI.DrawTexture(slot, stoveTop[i].holdableTexture);

                Rect redBar = new Rect(slot.x, slot.y + slot.height - slot.height / 10, slot.width, slot.height / 10);
                Rect greenBar = new Rect(redBar.x, redBar.y, redBar.width * stoveTop[i].burnCookTime / stoveTop[i].ashCookTime, redBar.height);
                Rect yellowBar = new Rect(redBar.x, redBar.y, redBar.width * stoveTop[i].perfectCookTime / stoveTop[i].ashCookTime, redBar.height);

                GUI.color = Color.red;
                GUI.DrawTexture(redBar, Texture2D.whiteTexture);
                GUI.color = Color.green;
                GUI.DrawTexture(greenBar, Texture2D.whiteTexture);
                GUI.color = Color.yellow;
                GUI.DrawTexture(yellowBar, Texture2D.whiteTexture);

                GUI.color = Color.black;
                Rect marker = new Rect(redBar.x + redBar.width * stoveTop[i].timeCooked / stoveTop[i].ashCookTime - 1, redBar.y, 2, redBar.height);
                GUI.DrawTexture(marker, Texture2D.whiteTexture);
                GUI.color = Color.white;
            }
        }
    }

    public override void Update()
    {
        base.Update();
        for (int i = 0; i < 2; i++)
        {
            if (stoveTop[i] != null)
            {
                stoveTop[i].updateBakeTimer();
                if (stoveTop[i].timeCooked >= stoveTop[i].ashCookTime)
                {
                    if (bakerSlots[i].thisNPCType != Humanoid.NPCType.Player)
                    {
                        PrepTable prepToChange = null;
                        for (int f = 0; i < bakerSlots[i].assignedFurnishings.Count; i++)
                        {
                            if (bakerSlots[i].assignedFurnishings[f].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
                            {
                                prepToChange = (PrepTable)bakerSlots[i].assignedFurnishings[f];
                                break;
                            }
                        }

                        if (prepToChange != null && prepToChange.orders.Count > 0
                            && prepToChange.orders[0].recipeToMake.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(stoveTop[i].holdableID))
                            && !prepToChange.orders[0].recipeClone.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(stoveTop[i].holdableID)))
                        {
                            prepToChange.orders[0].recipeClone.Ingredients.Add(FurnishingLibrary.Library.getBakedID(stoveTop[i].holdableID), 1);
                        }
                        else
                        {
                            PrepTableOrder toAdd = new PrepTableOrder();
                            toAdd.Set(FurnishingLibrary.Library.getBakedID(stoveTop[i].holdableID));
                            prepToChange.orders.Add(toAdd);
                        }
                    }

                    Destroy(stoveTop[i].gameObject);
                    stoveTop[i] = null;
                    bakerSlots[i] = null;
                }
                else if (stoveTop[i].timeCooked >= stoveTop[i].perfectCookTime)
                {
                    if (bakerSlots[i] == null)
                    {
                        Debug.LogError("Stove BakerSlotError: " + i);
                    }
                    else if (bakerSlots[i].thisNPCType != Humanoid.NPCType.Player && !stoveTop[i].hasCalledChef)
                    {
                        WorkOrder order = new WorkOrder();
                        order.Set(this, 1, bakerSlots[i], this);
                        bakerSlots[i].currentWorkOrder.Add(order);
                        stoveTop[i].hasCalledChef = true;
                    }
                }
            }
        }
    }

    public override void getInteraction(Humanoid interacter, int actionNumber)
    {
        if (actionNumber == 0)//Add item
        {
            int hand = -1;
            if (interacter.heldObjects[0] != null && interacter.heldObjects[0].isCookedIn == FurnishingID)
            {
                hand = 0;
            }
            else if (interacter.heldObjects[1] != null && interacter.heldObjects[1].isCookedIn == FurnishingID)
            {
                hand = 1;
            }

            int wherePlace = findOpenSlot();
            if (hand != -1 && wherePlace != -1)
            {
                addItemFromHand(interacter, hand);
            }
        }
        else if (actionNumber == 1)
        {
            int hand = -1;
            int top = -1;
            if(interacter.heldObjects[0] == null)
            {
                hand = 0;
            }
            else if (interacter.heldObjects[1] == null)
            {
                hand = 1;
            }
            if (interacter.thisNPCType != Humanoid.NPCType.Player)
            {
                if(hand == -1)
                {
                    if (interacter.heldObjects[0] != null && interacter.heldObjects[1] != null)
                    {
                        if (interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel)
                        {
                            interacter.dropItemOnFloor(true);
                            hand = 0;
                        }
                        else if (interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel)
                        {
                            interacter.dropItemOnFloor(false);
                            hand = 1;
                        }
                    }
                }

                if (bakerSlots[0] == interacter && stoveTop[0].timeCooked >= stoveTop[0].perfectCookTime)
                {
                    top = 0;
                }
                else if (bakerSlots[1] == interacter && stoveTop[1].timeCooked >= stoveTop[1].perfectCookTime)
                {
                    top = 1;
                }
            }
            else
            {
                float placement = -0.5f;
                Vector2 midPoint = transform.position + transform.TransformDirection(new Vector2(placement, 0));
                placement = 0.5f;
                Vector2 midPoint2 = transform.position + transform.TransformDirection(new Vector2(placement, 0));

                if (stoveTop[0] != null && stoveTop[1] != null)
                {
                    if (Vector2.Distance(midPoint, interacter.transform.position) <= Vector2.Distance(midPoint2, interacter.transform.position))
                    {
                        top = 0;
                    }
                    else
                    {
                        top = 1;
                    }
                }
                else if (stoveTop[0] != null)
                {
                    top = 0;
                }
                else if(stoveTop [1] != null)
                {
                    top = 1;
                }
            }
            if(hand != -1 && top != -1)
            {
                addItemToHand(interacter, hand == 0, top);
            }
        }
    }


    void addItemToHand(Humanoid interacter, bool isLeft, int x)
    {
        int handNumber = 0;

        if (!isLeft)
        {
            handNumber = 1;
        }


        bool didAThing = false;
        if (interacter.heldObjects[handNumber] == null)
        {
            interacter.heldObjects[handNumber] = stoveTop[x].pullFromOven(bakerSlots[x]).GetComponent<Holdable>();
            bakerSlots[x] = null;
            Destroy(stoveTop[x].gameObject);
            stoveTop[x] = null;
            didAThing = true;
            Consumable toTaste = interacter.heldObjects[handNumber].GetComponent<Consumable>();
            if (toTaste != null)
            {
                EmployeeSkill skill;
                if (interacter.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Cooking, out skill))
                {
                    toTaste.TasteQuality *= (1 + skill.level / 2f);
                }
            }

            MainMechanics.recipeBook.GiveRacialCookingXP(interacter.heldObjects[handNumber].holdableID, interacter);
            updateCookingChefs();
        }

        if (interacter.thisNPCType != Humanoid.NPCType.Player && didAThing)
        {
            for (int i = 0; i < interacter.assignedFurnishings.Count; i++)
            {
                if (interacter.assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
                {
                    PrepTable table = (PrepTable)interacter.assignedFurnishings[i];
                    table.getWorkOrderFromOven(interacter, isLeft);
                    break;
                }
            }
        }
    }

    void addItemFromHand(Humanoid interacter, int handNumber)
    {
        if (interacter.heldObjects[handNumber] != null && interacter.heldObjects[handNumber].isCookedIn == FurnishingID)
        {
            int wherePlace = findOpenSlot();
            if (wherePlace != -1)
            {
                if (interacter.thisNPCType != Humanoid.NPCType.Player
                    && interacter.currentWorkOrder[0].orderer.FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
                {
                    PrepTable station = interacter.currentWorkOrder[0].orderer as PrepTable;
                    if (station.orders.Count > 0
                        && station.orders[0].recipeToMake.Ingredients.Count == 1
                        && station.orders[0].recipeToMake.Ingredients.ContainsKey(interacter.heldObjects[handNumber].holdableID))
                    {
                        station.orders.RemoveAt(0);
                    }
                    else if (station.orders.Count > 0
                        && station.orders[0].recipeClone.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(interacter.heldObjects[handNumber].holdableID)))
                    {
                        station.orders[0].recipeClone.Ingredients.Remove(FurnishingLibrary.Library.getBakedID(interacter.heldObjects[handNumber].holdableID));
                    }
                }

                stoveTop[wherePlace] = interacter.heldObjects[handNumber] as Bakeable;
                bakerSlots[wherePlace] = interacter;
                interacter.heldObjects[handNumber].gameObject.SetActive(false);
                interacter.heldObjects[handNumber] = null;

                updateCookingChefs();
            }
        }
    }

    int findOpenSlot()
    {
        if(stoveTop[0] == null)
        {
            return 0;
        }
        else if(stoveTop[1] == null)
        {
            return 1;
        }
        return -1;
    }

    void updateCookingChefs()
    {
        chefsCooking.Clear();
        for(int i = 0; i < bakerSlots.Length; i++)
        {
            if(bakerSlots[i] != null && !chefsCooking.Contains(bakerSlots[i]))
            {
                chefsCooking.Add(bakerSlots[i]);
            }
        }
    }
}
