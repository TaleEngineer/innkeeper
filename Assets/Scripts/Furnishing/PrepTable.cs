﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class PrepTable : InteractableFurnishing
{
	public List<PrepTableOrder> orders = new List<PrepTableOrder>();

	public Dictionary<FurnishingLibrary.HoldableID, List<Holdable>> ingredHere = new Dictionary<FurnishingLibrary.HoldableID, List<Holdable>>();



	public override bool getWorkOrder ()
	{
		WorkOrder toAssign = new WorkOrder();
		if(orders.Count <= 0)
		{
			return true;
		}
		else
		{
			List<Holdable> ammo;
			if(MainMechanics.mechanics.foodOrderWanted(orders[0].recipeToMake.holdableID))
			{

				if(assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == orders[0].recipeClone.holdableID)
				{
					toAssign.Set(assignedEmployee.heldObjects[0].holdableID, 1000, assignedEmployee, this);
					assignedEmployee.currentWorkOrder.Add(toAssign);
					orders.RemoveAt(0);
					return false;
				}
				else if(assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == orders[0].recipeClone.holdableID)
				{
					toAssign.Set(assignedEmployee.heldObjects[1].holdableID, -1000, assignedEmployee, this);
					assignedEmployee.currentWorkOrder.Add(toAssign);
					orders.RemoveAt(0);
					return false;
				}
				else if(ingredHere.TryGetValue(orders[0].recipeToMake.holdableID, out ammo) && ammo.Count > 0)
				{
					toAssign.Set(this, MainMechanics.recipeBook.HowRetrieve(orders[0].recipeToMake.holdableID), assignedEmployee, this);
					assignedEmployee.currentWorkOrder.Add(toAssign);
					return false;
				}
			}

            List<FurnishingLibrary.HoldableID> toFind = new List<FurnishingLibrary.HoldableID>();
            List<FurnishingLibrary.HoldableID> toFindFor = new List<FurnishingLibrary.HoldableID>();
			bool canMake = true;
			List<FurnishingLibrary.HoldableID> ingredients = new List<FurnishingLibrary.HoldableID>(orders[0].recipeClone.Ingredients.Keys);
			for(int o = 0; o < orders[0].recipeClone.Ingredients.Count; o++)
			{
				bool found = false;
				if(MainMechanics.recipeBook.WhereMake(orders[0].recipeClone.holdableID) != FurnishingID
				   &&
				   ((assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == ingredients[o])
				   ||
				   (assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == ingredients[o])))
				{
					found = true;
					canMake = true;
				}
				if(!found)
				{
					if(ingredHere.ContainsKey(ingredients[o]))
					{
						found = true;
						if(ingredHere[ingredients[o]].Count < orders[0].recipeClone.Ingredients[ingredients[o]])
						{
							canMake = false;
							if(!toFind.Contains(ingredients[o]))
							{
								toFind.Add(ingredients[o]);
                                toFindFor.Add(orders[0].recipeClone.holdableID);
							}
						}
					}
				}
				if(!found)
				{
					if(!toFind.Contains(ingredients[o]))
					{
						toFind.Add(ingredients[o]);
                        toFindFor.Add(orders[0].recipeClone.holdableID);
                    }
					canMake = false;
				}
			}
			if(canMake)
			{
				if(MainMechanics.recipeBook.WhereMake(orders[0].recipeClone.holdableID) != FurnishingID)
				{
					var ID = MainMechanics.recipeBook.WhereMake(orders[0].recipeClone.holdableID);
					bool furnishingFound = false;
					if(MainMechanics.mechanics.hasFurnishing(ID))
					{
							furnishingFound = true;
					}
					if(furnishingFound)
					{
						if( (assignedEmployee.heldObjects[0] != null && assignedEmployee.heldObjects[0].holdableID == ingredients[0])
							||
							(assignedEmployee.heldObjects[1] != null && assignedEmployee.heldObjects[1].holdableID == ingredients[0]))
						{
							toAssign.Set(MainMechanics.recipeBook.WhereMake(orders[0].recipeClone.holdableID), MainMechanics.recipeBook.HowMake(orders[0].recipeClone.holdableID), assignedEmployee, this);
							assignedEmployee.currentWorkOrder.Add(toAssign);
							return false;
						}
						else
						{
							toAssign.Set(this, MainMechanics.recipeBook.HowRetrieve(ingredients[0]), assignedEmployee, this);
							assignedEmployee.currentWorkOrder.Add(toAssign);
							return false;
						}
					}
				}
				else
				{
					toAssign.Set(this, MainMechanics.recipeBook.HowMake(orders[0].recipeClone.holdableID), assignedEmployee, this);
					assignedEmployee.currentWorkOrder.Add(toAssign);
					return false;
				}
			}
			for(int count = 0; count < toFind.Count; count++)
			{
				bool handleLiquid = false;
				
				string String = toFind[count].ToString();
				var charArray = String.ToCharArray(0,3);
				if(charArray[0].Equals('L') && charArray[1].Equals('i') && charArray[2].Equals('q'))
				{
					handleLiquid = true;
				}

				if(handleLiquid)
				{
					if(assignedEmployee.heldObjects[0] != null 
					   && assignedEmployee.heldObjects[0].liquidHere != null
					   && assignedEmployee.heldObjects[0].liquidHere.liquidID == toFind[count])
					{
						toAssign.Set(this, 1000, assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
					if(assignedEmployee.heldObjects[1] != null 
					   && assignedEmployee.heldObjects[1].liquidHere != null
					   && assignedEmployee.heldObjects[1].liquidHere.liquidID == toFind[count])
					{
						toAssign.Set(this, -1000, assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
					//IF empty barrel
					if(assignedEmployee.heldObjects[0] != null
					   && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Barrel
					   && assignedEmployee.heldObjects[0].liquidHere == null)
					{
						toAssign.Set(MainMechanics.recipeBook.WhereMake(toFind[count]), toFind[count], MainMechanics.recipeBook.HowRetrieve(toFind[count]), assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
					if(assignedEmployee.heldObjects[1] != null 
					   && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Barrel
					   && assignedEmployee.heldObjects[1].liquidHere == null)
					{
						toAssign.Set(MainMechanics.recipeBook.WhereMake(toFind[count]), toFind[count], MainMechanics.recipeBook.HowRetrieve(toFind[count]), assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}

					bool foundBarrel = false;
					if(MainMechanics.mechanics.holdableIsAvailable (toFind[count], out foundBarrel))
					{
						toAssign.Set(toFind[count], assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
					if(foundBarrel)
					{
						toAssign.Set(FurnishingLibrary.HoldableID.Barrel, assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
				}
				else
				{
					if(assignedEmployee.heldObjects[0] != null 
					   && assignedEmployee.heldObjects[0].holdableID == toFind[count])
                    {
                        FurnishingLibrary.FurnishingID whereMake = MainMechanics.recipeBook.WhereMake(toFindFor[count]);
                        if (whereMake == FurnishingLibrary.FurnishingID.PrepTable)
                        {
                            toAssign.Set(this, 1000, assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAssign);
                            return false;
                        }
                        else if(whereMake != FurnishingLibrary.FurnishingID.Error)
                        {
                            toAssign.Set(whereMake, MainMechanics.recipeBook.HowMake(toFindFor[count]), assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAssign);
                            return false;
                        }
					}
                    if (assignedEmployee.heldObjects[1] != null
                       && assignedEmployee.heldObjects[1].holdableID == toFind[count])
                    {
                        FurnishingLibrary.FurnishingID whereMake = MainMechanics.recipeBook.WhereMake(toFindFor[count]);
                        if (whereMake == FurnishingLibrary.FurnishingID.PrepTable)
                        {
                            toAssign.Set(this, -1000, assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAssign);
                            return false;
                        }
                        else if (whereMake != FurnishingLibrary.FurnishingID.Error)
                        {
                            toAssign.Set(whereMake, MainMechanics.recipeBook.HowMake(toFindFor[count]), assignedEmployee, this);
                            assignedEmployee.currentWorkOrder.Add(toAssign);
                            return false;
                        }
                    }
					if(MainMechanics.mechanics.holdableIsAvailable(toFind[count]))
					{
						toAssign.Set(toFind[count], assignedEmployee, this);
						assignedEmployee.currentWorkOrder.Add(toAssign);
						return false;
					}
					else
					{
						var ingred = MainMechanics.recipeBook.WhatMake(toFind[count]);
						List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(ingred.Keys);
						canMake = true;
						for(int i = 0; i < ingred.Count; i++)
						{
							if(!ingredHere.ContainsKey(keys[i]) || ingredHere[keys[i]].Count < ingred[keys[i]])
							{
								toFind.Add(keys[i]);
                                toFindFor.Add(toFind[count]);
								canMake = false;
							}
						}
						if(canMake)
						{
							int howMake = MainMechanics.recipeBook.HowMake(toFind[count]);
                            var whereMake = MainMechanics.recipeBook.WhereMake(toFind[count]);
                            var whatMake = new List<FurnishingLibrary.HoldableID>(MainMechanics.recipeBook.WhatMake(toFind[count]).Keys);

                            if (whereMake == FurnishingID && howMake != 0)
							{
								toAssign.Set(this, howMake, assignedEmployee, this);
								assignedEmployee.currentWorkOrder.Add(toAssign);
								return false;
							}
                            else if(whereMake != FurnishingID && whatMake.Count == 1)
                            {
                                toAssign.Set(this, -(int)whatMake[0], assignedEmployee, this);
                                assignedEmployee.currentWorkOrder.Add(toAssign);
                                return false;
                            }
						}
					}
				}
			}
		}
		return true;
	}

	public override void DrawMenu(Vector2 UIMousPos, float PickUpScroll)
	{
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), Texture2D.whiteTexture);

		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleLeft;
		GUIContent textSizer = new GUIContent("Xy");
		Vector2 textSize = labelStyle.CalcSize(textSizer);

		List<FurnishingLibrary.HoldableID> keys = null;

		for(int i = 0; i < MainMechanics.recipeBook.KnownPrepables.Count; i++)
		{
			if(PickUpScroll + Screen.height/20*i < Screen.height/2)
			{
				labelStyle.alignment = TextAnchor.MiddleLeft;
				bool CanMake = true;

				Rect textRect = new Rect(new Vector2(Screen.height/20, PickUpScroll) + new Vector2(0, Screen.height/20)*i, new Vector2(Screen.width*2/3 - Screen.height/20, Screen.height/20));
				Rect productDrawRect = new Rect(textRect.x - Screen.height/20, textRect.y, Screen.height/20, Screen.height/20);
				GUI.DrawTexture(textRect, Texture2D.whiteTexture);

				Rect trueRect = new Rect(productDrawRect.x, productDrawRect.y, productDrawRect.width+textRect.width, textRect.height);

				GUI.color = Color.black;
				GUI.DrawTexture(new Rect(trueRect.x-1, trueRect.y-1, trueRect.width+2, trueRect.height+2), Texture2D.whiteTexture);
				GUI.color = new Color(0.75f,0.75f,0.75f);
				GUI.DrawTexture(trueRect, Texture2D.whiteTexture);
				GUI.color = Color.black;
				GUI.Label(textRect, MainMechanics.recipeBook.KnownPrepables[i].holdableID.ToString());
				GUI.color = Color.white;
				GUI.DrawTexture(productDrawRect, MainMechanics.recipeBook.KnownPrepables[i].holdableTexture);

				keys = new List<FurnishingLibrary.HoldableID>(MainMechanics.recipeBook.KnownPrepables[i].Ingredients.Keys);

				for(int r = 0; r < MainMechanics.recipeBook.KnownPrepables[i].Ingredients.Count; r++)
				{
					int required = MainMechanics.recipeBook.KnownPrepables[i].Ingredients[keys[r]];

					Rect ingredDrawRect = new Rect(textRect.x+textRect.width/4+(textRect.height/2)+(textRect.height*2*r), textRect.y, textRect.height, textRect.height);
					Rect ingredTextRect = new Rect(ingredDrawRect.x+ingredDrawRect.width, ingredDrawRect.y, textRect.height,textRect.height);
					GUI.DrawTexture(ingredDrawRect, MainMechanics.recipeBook.KnownPrepables[i].ingredientImage[r]);
					if(ingredHere.ContainsKey(keys[r])
					   && ingredHere[keys[r]].Count >= required)
					{
						GUI.color = Color.black;
					}
					else
					{
						GUI.color = Color.red;
						CanMake = false;
					}
					GUI.Label(ingredTextRect, "x"+MainMechanics.recipeBook.KnownPrepables[i].Ingredients[keys[r]]);
					GUI.color = Color.white;
					//RETURN
				}

				GUI.color = Color.black;
				Rect MakeButton = new Rect(trueRect.x+trueRect.width*4/5+1, trueRect.y+1, trueRect.width/10-2, trueRect.height-2);
				Rect OrderButton = new Rect(MakeButton.x+MakeButton.width+1, MakeButton.y, MakeButton.width, MakeButton.height);
				//GUI.DrawTexture(MakeButton, Texture2D.whiteTexture);
				GUI.DrawTexture(OrderButton, Texture2D.whiteTexture);
				GUI.color = Color.white;
				//GUI.DrawTexture(new Rect(MakeButton.x+1, MakeButton.y+1, MakeButton.width-2, MakeButton.height-2), Texture2D.whiteTexture);
				GUI.DrawTexture(new Rect(OrderButton.x+1, OrderButton.y+1, OrderButton.width-2, OrderButton.height-2), Texture2D.whiteTexture);

				GUI.color = Color.black;
				labelStyle.alignment = TextAnchor.MiddleCenter;
				//GUI.Label(MakeButton, "Make");
				GUI.Label(OrderButton, "Order");

				if(MainMechanics.recipeBook.WhereMake(MainMechanics.recipeBook.KnownPrepables[i].holdableID) == FurnishingID)
				{
					GUI.color = Color.black;
					GUI.DrawTexture(MakeButton, Texture2D.whiteTexture);
					GUI.color = Color.white;
					GUI.DrawTexture(new Rect(MakeButton.x+1, MakeButton.y+1, MakeButton.width-2, MakeButton.height-2), Texture2D.whiteTexture);
					
					GUI.color = Color.black;
					GUI.Label(MakeButton, "Make");

					if(CanMake)
					{

						if(MakeButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0)&& MainMechanics.mechanics.currentButtonWait <= 0)
						{
							getInteraction(MainMechanics.mechanics.Innkeeper, MainMechanics.recipeBook.HowMake(MainMechanics.recipeBook.KnownPrepables[i].holdableID));
							MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
							MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
						}
					}
					else if(!CanMake)
					{
						GUI.color = new Color(0.75f,0.75f,0.75f,0.75f);
						GUI.DrawTexture(MakeButton, Texture2D.whiteTexture);
					}
				}
				
				if(OrderButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0)&& MainMechanics.mechanics.currentButtonWait <= 0)
				{
                    PrepTableOrder toAdd = new PrepTableOrder();
                    toAdd.Set(MainMechanics.recipeBook.KnownPrepables[i]);
					orders.Add(toAdd);
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				}
			}
		}

		Rect holdablesRect = new Rect(Screen.width*2/3, 0, Screen.width/3, Screen.height);
		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(holdablesRect.x-1, holdablesRect.y-1, holdablesRect.width+2, holdablesRect.height+2), Texture2D.whiteTexture);
		GUI.color = new Color(0.5f,0.5f,0.5f);
		GUI.DrawTexture(holdablesRect, Texture2D.whiteTexture);
		
		Rect exitButton = new Rect(holdablesRect.x, holdablesRect.height*19/20, holdablesRect.width, holdablesRect.height/20);

		keys = new List<FurnishingLibrary.HoldableID>(ingredHere.Keys);

		for(int i = 0; i < keys.Count; i++)
		{
			Rect holdableAmmout = new Rect(holdablesRect.x+holdablesRect.width/2, exitButton.y-exitButton.height*(i+1), holdablesRect.width/2, exitButton.height);
			Rect holdableName = new Rect(holdableAmmout.x-exitButton.height, holdableAmmout.y, exitButton.height, exitButton.height);
			Rect ingredTrueRect = new Rect(holdableName.x, holdableName.y, holdableName.width+holdableAmmout.width, holdableName.height);

			Texture2D objectTexture = null;
			objectTexture = FurnishingLibrary.Library.getHoldablePrefab(keys[i]).holdableTexture;


			GUI.color = Color.white;
			GUI.DrawTexture(holdableName, objectTexture);
			GUI.color = Color.black;
			labelStyle.alignment = TextAnchor.MiddleLeft;
			GUI.Label(holdableAmmout, " x"+ingredHere[keys[i]].Count);

			
			if(ingredTrueRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				bool handleLiquid = false;

				string String = keys[i].ToString();
				var charArray = String.ToCharArray(0,3);
				if(charArray[0].Equals('L') && charArray[1].Equals('i') && charArray[2].Equals('q'))
				{
					handleLiquid = true;
				}

				if(handleLiquid)
				{
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null 
					   && MainMechanics.mechanics.Innkeeper.heldObjects[0].canHoldLiquid
					   && (MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere == null || MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere.liquidID == keys[i])
					   && MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment < MainMechanics.mechanics.Innkeeper.heldObjects[0].maxLiquidContainment)
					{
						if(MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere == null)
						{
							MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere = new Liquid();
							MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere.Set(keys[i]);
							if(MainMechanics.mechanics.Innkeeper.heldObjects[0].maxLiquidContainment > ingredHere[keys[i]].Count)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment = ingredHere[keys[i]].Count;
								ingredHere[keys[i]].Clear();
							}
							else
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment = MainMechanics.mechanics.Innkeeper.heldObjects[0].maxLiquidContainment;
								for(int li = 0; li < MainMechanics.mechanics.Innkeeper.heldObjects[0].maxLiquidContainment; li++)
								{
									ingredHere[keys[i]].RemoveAt(0);
                                    if (ingredHere[keys[i]].Count <= 0)
                                    {
                                        ingredHere.Remove(keys[i]);
                                    }
                                }
							}
						}
						else if(MainMechanics.mechanics.Innkeeper.heldObjects[0].liquidHere.liquidID == keys[i])
						{
							int toAdd = MainMechanics.mechanics.Innkeeper.heldObjects[0].maxLiquidContainment - MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment;
							if(ingredHere[keys[i]].Count < toAdd)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment += ingredHere[keys[i]].Count;
								ingredHere[keys[i]].Clear();
							}
							else
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[0].currentLiquidContainment += toAdd;
								for(int li = 0; li < toAdd; li++)
								{
									ingredHere[keys[i]].RemoveAt(0);
                                    if (ingredHere[keys[i]].Count <= 0)
                                    {
                                        ingredHere.Remove(keys[i]);
                                    }
                                }
							}
						}
					}


					else if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null 
					        && MainMechanics.mechanics.Innkeeper.heldObjects[1].canHoldLiquid
					        && (MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere == null || MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere.liquidID == keys[i])
					        && MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment < MainMechanics.mechanics.Innkeeper.heldObjects[1].maxLiquidContainment)
					{
						if(MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere == null)
						{
							MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere = new Liquid();
							MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere.Set(keys[i]);
							if(MainMechanics.mechanics.Innkeeper.heldObjects[1].maxLiquidContainment > ingredHere[keys[i]].Count)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment = ingredHere[keys[i]].Count;
								ingredHere[keys[i]].Clear();
							}
							else
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment = MainMechanics.mechanics.Innkeeper.heldObjects[1].maxLiquidContainment;
								for(int li = 0; li < MainMechanics.mechanics.Innkeeper.heldObjects[1].maxLiquidContainment; li++)
								{
									ingredHere[keys[i]].RemoveAt(0);
                                    if (ingredHere[keys[i]].Count <= 0)
                                    {
                                        ingredHere.Remove(keys[i]);
                                    }
                                }
							}
						}
						else if(MainMechanics.mechanics.Innkeeper.heldObjects[1].liquidHere.liquidID == keys[i])
						{
							int toAdd = MainMechanics.mechanics.Innkeeper.heldObjects[1].maxLiquidContainment - MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment;
							if(ingredHere[keys[i]].Count < toAdd)
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment += ingredHere[keys[i]].Count;
								ingredHere[keys[i]].Clear();
							}
							else
							{
								MainMechanics.mechanics.Innkeeper.heldObjects[1].currentLiquidContainment += toAdd;
								for(int li = 0; li < toAdd; li++)
								{
									ingredHere[keys[i]].RemoveAt(0);
                                    if (ingredHere[keys[i]].Count <= 0)
                                    {
                                        ingredHere.Remove(keys[i]);
                                    }
                                }
							}
						}
					}


				}
				else
				{
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null || MainMechanics.mechanics.Innkeeper.heldObjects[1] == null)
					{
						MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
						if((MainMechanics.mechanics.Innkeeper.heldObjects[0] == null && ingredHere[keys[i]].Count > 0)
                            ||
                            (MainMechanics.mechanics.Innkeeper.heldObjects[1] == null && ingredHere[keys[i]].Count > 0))

                        {
                            pickUpHoldable(keys[i], MainMechanics.mechanics.Innkeeper);
						}
					}
				}

			}
		}

		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(exitButton.x-1, exitButton.y-1, exitButton.width+2, exitButton.height+2), Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(exitButton, Texture2D.whiteTexture);
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUI.color = Color.black;
		GUI.Label(exitButton, "EXIT");

		if(exitButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
		}

		Rect botArea = new Rect(0, Screen.height/2, Screen.width*2/3, Screen.height/2);
		GUI.color = Color.black;
		GUI.DrawTexture(botArea, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(botArea.x+1, botArea.y+1, botArea.width-2, botArea.width-2), Texture2D.whiteTexture);

		Rect RightHandUseRect = new Rect(botArea.width-botArea.width/3, Screen.height/2, botArea.width/3, Screen.height/2);
		Rect RightHandDarkRect = new Rect(RightHandUseRect.x+1, RightHandUseRect.y+1, RightHandUseRect.width-2, RightHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(RightHandDarkRect, Texture2D.whiteTexture);

		if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			Vector2 RightMidPoint = new Vector2(RightHandUseRect.x, RightHandUseRect.y)+ new Vector2(RightHandUseRect.width, RightHandUseRect.height)/2;
			Rect RightHeldImage = new Rect(RightMidPoint-new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2)/2,new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(RightHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[1].GetComponent<SpriteRenderer>().sprite.texture);

			if(RightHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				getInteraction(MainMechanics.mechanics.Innkeeper, -1000);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}



		Rect LeftHandUseRect = new Rect(RightHandUseRect.x-RightHandUseRect.width, RightHandUseRect.y, RightHandUseRect.width, RightHandUseRect.height);
		Rect LeftHandDarkRect = new Rect(LeftHandUseRect.x+1, LeftHandUseRect.y+1, LeftHandUseRect.width-2, LeftHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(LeftHandDarkRect, Texture2D.whiteTexture);

		if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			Vector2 LeftMidPoint = new Vector2(LeftHandUseRect.x, LeftHandUseRect.y)+ new Vector2(LeftHandUseRect.width, LeftHandUseRect.height)/2;
			Rect LeftHeldImage = new Rect(LeftMidPoint-new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2)/2,new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(LeftHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[0].GetComponent<SpriteRenderer>().sprite.texture);

			if(LeftHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				getInteraction(MainMechanics.mechanics.Innkeeper, 1000);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}

		Rect orderArea = new Rect(botArea.x, botArea.y, botArea.width/3, botArea.height);
		int x = 0;
		int y = 0;

		for(int i = 0; i < orders.Count; i++)
		{
			if(y < 10)
			{
				Rect tileArea = new Rect(orderArea.x + x*orderArea.width/2, orderArea.y+ y*orderArea.height/10, orderArea.width/2, orderArea.height/10);
				GUI.color = Color.black;
				GUI.DrawTexture(tileArea, Texture2D.whiteTexture);
				GUI.color = Color.white;
				GUI.DrawTexture(new Rect(tileArea.x+1, tileArea.y+1, tileArea.width-2, tileArea.height-2), Texture2D.whiteTexture);
				Rect textRect = new Rect(tileArea.x+tileArea.width/2, tileArea.y, tileArea.width/2, tileArea.height);
				Rect imageRect = new Rect(textRect.x-tileArea.height, textRect.y, textRect.height, textRect.height);
				GUI.DrawTexture(imageRect, orders[i].recipeToMake.holdableTexture);
				GUI.color = Color.black;
				GUI.Label(textRect, orders[i].recipeToMake.holdableID.ToString());

				if(tileArea.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
				{
					orders.RemoveAt (i);
					i--;
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				}

				if(x <= 0)
				{
					x++;
				}
				else
				{
					y++;
					x = 0;
				}
			}
		}
	}


	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		/*Dictionary<FurnishingLibrary.HoldableID, int> toRemove = null;
		List<FurnishingLibrary.HoldableID> keys = null;

		bool everythingInOrder = false;*/

		if(actionNumber == 1000)
		{
			putDownHoldable(interacter, true);
		}
		else if(actionNumber == -1000)
		{
			putDownHoldable(interacter, false);
		}
		else if(actionNumber == 0)
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
		}
		else if(actionNumber > 0)//CRAFT HOLDABLE
		{
			startCraftHoldable((FurnishingLibrary.HoldableID) actionNumber, interacter);
        }
        else if(actionNumber < 0)
		{
			CompleteInteract (interacter, actionNumber);
		}
	}
	
	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		if(choice > 0)//MAKE DOUGH
		{
			completeCraftHoldable((FurnishingLibrary.HoldableID) choice, interacter);
		}
        else if(choice < 0)
        {
            pickUpHoldable((FurnishingLibrary.HoldableID) (-choice), interacter);
        }
        /*else if(choice == 2)//MAKE MEATBALLS
        {
            completeCraftHoldable(FurnishingLibrary.HoldableID.RawMeatBall, interacter);
        }
        else if(choice == 3)//MAKE MEAT PATTY
        {
            completeCraftHoldable(FurnishingLibrary.HoldableID.RawMeatPatty, interacter);
        }
		else if(choice == -1)//PICK UP DOUGH
		{
			pickUpHoldable(FurnishingLibrary.HoldableID.Dough, interacter);
		}
		else if(choice == -2)//PICK UP BREAD
		{
			pickUpHoldable(FurnishingLibrary.HoldableID.Bread, interacter);
        }
        else if (choice == -3)//PICK UP COOKED MEAT
        {
            pickUpHoldable(FurnishingLibrary.HoldableID.CookedMeat, interacter);
        }*/
    }

	void startCraftHoldable(FurnishingLibrary.HoldableID ID, Humanoid interacter)
	{
        //Debug.Log("Trying to make " + ID);
		var toRemove = MainMechanics.recipeBook.WhatMake(ID);
		List<FurnishingLibrary.HoldableID> keys = new List<FurnishingLibrary.HoldableID>(toRemove.Keys);
		bool everythingInOrder = false;
		int removeCount = 0;
		for(int i = 0; i < toRemove.Count; i++)
		{
			List<Holdable> whoCares;
			if(ingredHere.TryGetValue(keys[i], out whoCares)
			   && ingredHere[keys[i]].Count >= toRemove[keys[i]])
			{
				removeCount ++;
			}
		}
		
		if(removeCount == toRemove.Count)
		{
			everythingInOrder = true;
		}

		if(toRemove != null && everythingInOrder)
		{
			for(int i = 0; i < keys.Count; i++)
			{
				for(int rov = 0; rov < toRemove[keys[i]]; rov++)
				{
					GameObject toDestroy = ingredHere[keys[i]][0].gameObject;
					
					string String = keys[i].ToString();
					var charArray = String.ToCharArray(0,3);
					if(!charArray[0].Equals('L') && !charArray[1].Equals('i') && !charArray[2].Equals('q'))
					{
						Destroy(toDestroy);
					}
					ingredHere[keys[i]].RemoveAt(0);
                    if(ingredHere[keys[i]].Count <= 0)
                    {
                        ingredHere.Remove(keys[i]);
                    }
				}
			}
			interacter.startWorking(MainMechanics.recipeBook.HowLongToMake(ID), MainMechanics.recipeBook.HowMake(ID), this, EmployeeSkill.WorkSkill.Cooking, MainMechanics.recipeBook.GetXPGain(ID));
            MainMechanics.recipeBook.GiveRacialCookingXP(ID, interacter);
		}
	}

	void completeCraftHoldable(FurnishingLibrary.HoldableID ID, Humanoid interacter)
	{
		List<Holdable> whoCares;
		if(ingredHere.TryGetValue(ID, out whoCares))
		{
			Holdable toAdd = FurnishingLibrary.Library.getHoldable(ID).GetComponent<Holdable>();
			ingredHere[ID].Add(toAdd);
            Consumable toTaste = toAdd.GetComponent<Consumable>();
            if(toTaste != null)
            {
                EmployeeSkill skill;
                if (interacter.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Cooking, out skill))
                {
                    toTaste.TasteQuality *= (1 + skill.level / 2f);
                }
            }
			toAdd.gameObject.SetActive(false);
		}
		else
		{
			Holdable toAdd = FurnishingLibrary.Library.getHoldable(ID).GetComponent<Holdable>();
			ingredHere.Add(ID, new List<Holdable>());
			ingredHere[ID].Add(toAdd);
            Consumable toTaste = toAdd.GetComponent<Consumable>();
            if (toTaste != null)
            {
                EmployeeSkill skill;
                if (interacter.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Cooking, out skill))
                {
                    toTaste.TasteQuality *= (1 + skill.level / 2f);
                }
            }
            toAdd.gameObject.SetActive(false);
		}
        if (!MainMechanics.mechanics.foodOrderWanted(ID))
        {
            for (int i = 0; i < orders.Count; i++)
            {
                if (orders[i].recipeToMake.holdableID == ID)
                {
                    //Debug.LogError("Unwanted Food Made");
                    orders.RemoveAt(i);
                    return;
                }
            }
        }
    }
	
	void pickUpHoldable(FurnishingLibrary.HoldableID ID, Humanoid interacter)
	{
		List<Holdable> theList;
		if(ingredHere.TryGetValue(ID, out theList) && theList.Count > 0)
		{
			if(interacter.heldObjects[0] == null)
			{
				interacter.heldObjects[0] = theList[0];
				ingredHere[ID].RemoveAt(0);
				interacter.heldObjects[0].gameObject.SetActive(true);
                if (ingredHere[ID].Count <= 0)
                {
                    ingredHere.Remove(ID);
                }
            }
			else if(interacter.heldObjects[1] == null)
			{
				interacter.heldObjects[1] = theList[0];
				ingredHere[ID].RemoveAt(0);
				interacter.heldObjects[1].gameObject.SetActive(true);
                if (ingredHere[ID].Count <= 0)
                {
                    ingredHere.Remove(ID);
                }
            }
		}
	}

	void putDownHoldable(Humanoid interacter, bool left)
	{
		int handNumer = 0;

		if(!left)
		{
			handNumer = 1;
		}

		if(interacter.heldObjects[handNumer] != null)
		{
			bool handleLiquid = false;
			if(interacter.heldObjects[handNumer].canHoldLiquid)
			{
				handleLiquid = true;
			}
			
			
			List<Holdable> whoCares; //An int is required to check for the value in a dictionary
			if(handleLiquid)
			{
				if(interacter.heldObjects[handNumer].liquidHere != null)
				{
					if(!ingredHere.TryGetValue(interacter.heldObjects[handNumer].liquidHere.liquidID, out whoCares))
					{
						ingredHere.Add(interacter.heldObjects[handNumer].liquidHere.liquidID, new List<Holdable>());
						for(int li = 0; li < interacter.heldObjects[handNumer].currentLiquidContainment; li++)
						{
							ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID].Add(FurnishingLibrary.Library.getHoldablePrefab(interacter.heldObjects[handNumer].liquidHere.liquidID));
							ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID][ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID].Count-1].gameObject.SetActive(false);
						}
					}
					else
					{
						for(int li = 0; li < interacter.heldObjects[handNumer].currentLiquidContainment; li++)
						{
							ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID].Add(FurnishingLibrary.Library.getHoldablePrefab(interacter.heldObjects[handNumer].liquidHere.liquidID));
							ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID][ingredHere[interacter.heldObjects[handNumer].liquidHere.liquidID].Count-1].gameObject.SetActive(false);
						}
					}
					interacter.heldObjects[handNumer].currentLiquidContainment = 0;
					interacter.heldObjects[handNumer].liquidHere = null;
				}
			}
			else
			{
				if(!ingredHere.TryGetValue(interacter.heldObjects[handNumer].holdableID, out whoCares))
				{
					ingredHere.Add(interacter.heldObjects[handNumer].holdableID, new List<Holdable>());
					ingredHere[interacter.heldObjects[handNumer].holdableID].Add(interacter.heldObjects[handNumer]);
				}
				else
				{
					ingredHere[interacter.heldObjects[handNumer].holdableID].Add(interacter.heldObjects[handNumer]);
				}
				interacter.heldObjects[handNumer].gameObject.SetActive(false);
				interacter.heldObjects[handNumer] = null;
			}
		}
	}
	
	
	
	public override void prepareRemoval()
	{
        base.prepareRemoval();
        if (roomInsideOf != null)
        {
            roomInsideOf.prepTablesHere.Remove(this);
        }
	}
	
	public override void Start()
	{
		if(MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere != null)
		{
			roomInsideOf = MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere.parentRoom;
			roomInsideOf.prepTablesHere.Add(this);
		}
	}

	public void getWorkOrderFromOven(Humanoid interacter, bool isLeft)
	{
		int handNumber = 0;

		if(!isLeft)
		{
			handNumber = 1;
		}
		WorkOrder wherePlace = new WorkOrder();

        if(orders.Count > 0 
            && orders[0].recipeToMake.Ingredients.ContainsKey(interacter.heldObjects[handNumber].holdableID) 
            && !orders[0].recipeClone.Ingredients.ContainsKey(interacter.heldObjects[handNumber].holdableID)
            && !orders[0].completedBakeables.Contains(interacter.heldObjects[handNumber].holdableID))
        {
            orders[0].completedBakeables.Add(interacter.heldObjects[handNumber].holdableID);
            if (isLeft)
            {
                wherePlace.Set(this, 1000, interacter, this);
            }
            else
            {
                wherePlace.Set(this, -1000, interacter, this);
            }

            if (interacter.currentWorkOrder.Count > 0)
            {
                interacter.currentWorkOrder.Insert(1, wherePlace);
            }
            else
            {
                interacter.currentWorkOrder.Add(wherePlace);
            }
            return;
        }

		bool hasKW = false;
		KitchenWindow kwToGoTo = null;
		List<Furnishing> list;
		if(MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.KitchenWindow, out list))
		{
			for(int i = 0; i < list.Count; i++)
			{
				KitchenWindow toLookAt = list[i] as KitchenWindow;
				for(int o = 0; o < toLookAt.orders.Count; o++)
				{
					int whoCares;
					if(toLookAt.orders[o].ammountNeeded.TryGetValue(interacter.heldObjects[handNumber].holdableID, out whoCares)
					   && !toLookAt.orders[o].hasBeenCalled)
					{
						hasKW = true;
						kwToGoTo = toLookAt;
						break;
					}
				}
			}
		}
		
		
		if(hasKW)
		{
			if(isLeft)
			{
				wherePlace.Set (interacter.heldObjects[handNumber].holdableID, 1000, interacter, this);
			}
			else
			{
				wherePlace.Set (interacter.heldObjects[handNumber].holdableID, -1000, interacter, this);
			}
		}
		else
		{
			if(isLeft)
			{
				wherePlace.Set (this, 1000, interacter, this);
			}
			else
			{
				wherePlace.Set (this, -1000, interacter, this);
			}
		}
        if (interacter.currentWorkOrder.Count > 0)
        {
            interacter.currentWorkOrder.Insert(1, wherePlace);
        }
        else
        {
            interacter.currentWorkOrder.Add(wherePlace);
        }
	}
}
