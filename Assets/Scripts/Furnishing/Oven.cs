﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Oven : InteractableFurnishing
{
	static int maxX = 5;
	static int maxY = 5;
	public static int maxSlots = maxX * maxY;
	float temperature;
	Bakeable[,] bakingSlots = new Bakeable[maxX,maxY];
	Humanoid[,] bakerSlots = new Humanoid[maxX,maxY];
	public int count = 0;

    public List<Humanoid> chefsCooking = new List<Humanoid>();

    public override void Update ()
	{
		base.Update ();
		for(int y = 0; y < maxY; y++)
		{
			for(int x = 0; x < maxX; x++)
			{
				if(bakingSlots[x,y] != null)
				{
					bakingSlots[x,y].updateBakeTimer();
					if(bakingSlots[x,y].timeCooked >= bakingSlots[x,y].ashCookTime)
					{
                        if(bakerSlots[x, y].thisNPCType != Humanoid.NPCType.Player)
                        {
                            PrepTable prepToChange = null;
                            for (int i = 0; i < bakerSlots[x,y].assignedFurnishings.Count; i++)
                            {
                                if(bakerSlots[x,y].assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
                                {
                                    prepToChange = (PrepTable)bakerSlots[x, y].assignedFurnishings[i];
                                    break;
                                }
                            }

                            if (prepToChange != null && prepToChange.orders.Count > 0
                                && prepToChange.orders[0].recipeToMake.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(bakingSlots[x, y].holdableID))
                                && !prepToChange.orders[0].recipeClone.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(bakingSlots[x, y].holdableID)))
                            {
                                prepToChange.orders[0].recipeClone.Ingredients.Add(FurnishingLibrary.Library.getBakedID(bakingSlots[x, y].holdableID), 1);
                            }
                            else
                            {
                                PrepTableOrder toAdd = new PrepTableOrder();
                                toAdd.Set(FurnishingLibrary.Library.getBakedID(bakingSlots[x, y].holdableID));
                                prepToChange.orders.Add(toAdd);
                            }
                        }
						Destroy(bakingSlots[x,y].gameObject);
						bakerSlots[x,y] = null;
						count--;
					}
					else if(bakingSlots[x,y].timeCooked >= bakingSlots[x,y].perfectCookTime)
					{
						if(bakerSlots[x,y] == null)
						{
							Debug.LogError("X:"+x+"| Y:"+y);
						}
						else if(bakerSlots[x,y].thisNPCType != Humanoid.NPCType.Player && !bakingSlots[x,y].hasCalledChef)
						{
							WorkOrder order = new WorkOrder();
							order.Set(this, -1, bakerSlots[x,y], this);
							bakerSlots[x,y].currentWorkOrder.Add(order);
							bakingSlots[x,y].hasCalledChef = true;
						}
					}
				}
			}
		}
	}

	public override void DrawMenu (Vector2 UIMousPos, float PickUpScroll)
	{
		Rect gridScreen = new Rect(0,0, Screen.width*2/3, Screen.height);
		Rect rightScreen = new Rect(gridScreen.width, 0, Screen.width/3, Screen.height);
		GUI.color = Color.black;
		GUI.DrawTexture(gridScreen, Texture2D.whiteTexture);
		GUI.DrawTexture(rightScreen, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(gridScreen.x+1, gridScreen.y+1, gridScreen.width-2, gridScreen.height-2), Texture2D.whiteTexture);
		GUI.DrawTexture(new Rect(rightScreen.x+1, rightScreen.y+1, rightScreen.width-2, rightScreen.height-2), Texture2D.whiteTexture);

		Rect centerPlacement = new Rect(gridScreen.x+gridScreen.width/10, gridScreen.y+1, gridScreen.width*8/10, gridScreen.height*8/10);
		//GUI.color = Color.black;
		//GUI.DrawTexture(centerPlacement, Texture2D.whiteTexture);
		//GUI.color = Color.white;
		//GUI.DrawTexture(new Rect(centerPlacement.x+1, centerPlacement.y+1, centerPlacement.width-2, centerPlacement.height-2), Texture2D.whiteTexture);

		int x = 0;
		int y = 0;
		for(int i = 0; i < maxSlots; i++)
		{
			Rect slot = new Rect(centerPlacement.x+centerPlacement.width*x/maxX,
			                     centerPlacement.y+centerPlacement.height*y/maxY,
			                     centerPlacement.width/maxX,centerPlacement.height/maxY);

			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(slot.x-1, slot.y-1, slot.width+2, slot.height+2), Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(slot, Texture2D.whiteTexture);

			if(bakingSlots[x,y] != null)
			{
				Rect imageRect = new Rect(slot.x+slot.width/2-slot.height/2, slot.y, slot.height, slot.height);
				GUI.DrawTexture(imageRect, bakingSlots[x,y].holdableTexture);

				Rect redBar = new Rect(slot.x, slot.y+slot.height-slot.height/10, slot.width, slot.height/10);
				Rect greenBar = new Rect(redBar.x, redBar.y, redBar.width*bakingSlots[x,y].burnCookTime/bakingSlots[x,y].ashCookTime, redBar.height);
				Rect yellowBar = new Rect(redBar.x, redBar.y, redBar.width*bakingSlots[x,y].perfectCookTime/bakingSlots[x,y].ashCookTime, redBar.height);

				GUI.color = Color.red;
				GUI.DrawTexture(redBar, Texture2D.whiteTexture);
				GUI.color = Color.green;
				GUI.DrawTexture(greenBar, Texture2D.whiteTexture);
				GUI.color = Color.yellow;
				GUI.DrawTexture(yellowBar, Texture2D.whiteTexture);

				GUI.color = Color.black;
				Rect marker = new Rect(redBar.x+redBar.width*bakingSlots[x,y].timeCooked/bakingSlots[x,y].ashCookTime-1, redBar.y, 2,redBar.height);
				GUI.DrawTexture(marker, Texture2D.whiteTexture);

				if(MainMechanics.mechanics.currentButtonWait <= 0 && slot.Contains (UIMousPos) && Input.GetMouseButtonDown(0))
				{
					MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
					if(MainMechanics.mechanics.Innkeeper.heldObjects[0] == null)
					{
						addItemToHand(MainMechanics.mechanics.Innkeeper, true, x, y);
					}
					else if(MainMechanics.mechanics.Innkeeper.heldObjects[1] == null)
					{
						addItemToHand(MainMechanics.mechanics.Innkeeper, false, x, y);
					}
				}
			}



			if(x < maxX-1)
			{
				x++;
			}
			else
			{
				y++;
				x = 0;
			}
		}

		var labelStyle = GUI.skin.GetStyle ("Label");

		Rect exitButton = new Rect(rightScreen.x, rightScreen.height*19/20, rightScreen.width, rightScreen.height/20);

		GUI.color = Color.black;
		GUI.DrawTexture(new Rect(exitButton.x-1, exitButton.y-1, exitButton.width+2, exitButton.height+2), Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(exitButton, Texture2D.whiteTexture);
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUI.color = Color.black;
		GUI.Label(exitButton, "EXIT");
		
		if(exitButton.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
		}
		
		Rect RightHandUseRect = new Rect(gridScreen.x+gridScreen.width/2, centerPlacement.y+centerPlacement.height,
		                                 (Screen.height - (centerPlacement.y+centerPlacement.height)), 
		                                 (Screen.height - (centerPlacement.y+centerPlacement.height)));
		Rect RightHandDarkRect = new Rect(RightHandUseRect.x+1, RightHandUseRect.y+1, RightHandUseRect.width-2, RightHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(RightHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(RightHandDarkRect, Texture2D.whiteTexture);
		
		if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			Vector2 RightMidPoint = new Vector2(RightHandUseRect.x, RightHandUseRect.y)+ new Vector2(RightHandUseRect.width, RightHandUseRect.height)/2;
			Rect RightHeldImage = new Rect(RightMidPoint-new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2)/2,new Vector2(RightHandUseRect.height/2, RightHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(RightHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[1].GetComponent<SpriteRenderer>().sprite.texture);
			
			if(RightHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				addItemFromHand(MainMechanics.mechanics.Innkeeper, false);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}
		
		
		
		Rect LeftHandUseRect = new Rect(RightHandUseRect.x-RightHandUseRect.width, RightHandUseRect.y, RightHandUseRect.width, RightHandUseRect.height);
		Rect LeftHandDarkRect = new Rect(LeftHandUseRect.x+1, LeftHandUseRect.y+1, LeftHandUseRect.width-2, LeftHandUseRect.height-2);
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(LeftHandUseRect, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(LeftHandDarkRect, Texture2D.whiteTexture);
		
		if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			Vector2 LeftMidPoint = new Vector2(LeftHandUseRect.x, LeftHandUseRect.y)+ new Vector2(LeftHandUseRect.width, LeftHandUseRect.height)/2;
			Rect LeftHeldImage = new Rect(LeftMidPoint-new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2)/2,new Vector2(LeftHandUseRect.height/2, LeftHandUseRect.height/2));
			GUI.color = Color.white;
			GUI.DrawTexture(LeftHeldImage, MainMechanics.mechanics.Innkeeper.heldObjects[0].GetComponent<SpriteRenderer>().sprite.texture);
			
			if(LeftHandUseRect.Contains(UIMousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				addItemFromHand(MainMechanics.mechanics.Innkeeper, true);
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			}
		}

	}

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
		}
		else if(actionNumber == 1)
		{
			if(interacter.heldObjects[0] != null && interacter.heldObjects[0].isCookedIn == FurnishingID)
			{
				addItemFromHand(interacter, true);
			}
			if(interacter.heldObjects[1] != null && interacter.heldObjects[1].isCookedIn == FurnishingID)
			{
				addItemFromHand(interacter, false);
			}
		}
		else if(actionNumber == -1)//Get completed Bakable
		{
			for(int x = 0; x < maxX; x++)
			{
				for(int y = 0; y < maxY; y++)
				{
					if(bakingSlots[x,y]!= null 
					   && bakingSlots[x,y].timeCooked >= bakingSlots[x,y].perfectCookTime 
					   && bakerSlots[x,y] == interacter)
					{
						if(interacter.heldObjects[0] == null)
						{
							addItemToHand(interacter, true, x, y);
                            return;
						}
						else if(interacter.heldObjects[1] == null)
						{
							addItemToHand(interacter, false, x, y);
                            return;
						}
						else
						{
							interacter.dropItemOnFloor (true);
							addItemToHand(interacter, true, x, y);
                            return;
						}
					}
				}
			}
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{

	}

	void addItemToHand(Humanoid interacter, bool isLeft, int x, int y)
	{
		int handNumber = 0;

		if(!isLeft)
		{
			handNumber = 1;
		}


		bool didAThing = false;
		if(interacter.heldObjects[handNumber] == null)
		{
			interacter.heldObjects[handNumber] =  bakingSlots[x,y].pullFromOven(bakerSlots[x,y]).GetComponent<Holdable>();
			bakerSlots[x,y] = null;
			Destroy(bakingSlots[x,y].gameObject);
			bakingSlots[x,y] = null;
			count--;
			didAThing = true;
            Consumable toTaste = interacter.heldObjects[handNumber].GetComponent<Consumable>();
            if (toTaste != null)
            {
                EmployeeSkill skill;
                if (interacter.workSkills.TryGetValue(EmployeeSkill.WorkSkill.Cooking, out skill))
                {
                    toTaste.TasteQuality *= (1 + skill.level / 2f);
                }
            }
            MainMechanics.recipeBook.GiveRacialCookingXP(interacter.heldObjects[handNumber].holdableID, interacter);
            updateCookingChefs();
        }
		
		if(interacter.thisNPCType != Humanoid.NPCType.Player && didAThing)
		{
			for(int i = 0; i < interacter.assignedFurnishings.Count; i++)
			{
				if(interacter.assignedFurnishings[i].FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
				{
					PrepTable table = (PrepTable)interacter.assignedFurnishings[i];
					table.getWorkOrderFromOven(interacter, isLeft);
					break;
				}
			}
		}
	}

	void addItemFromHand(Humanoid interacter, bool isLeft)
	{
		if(count < maxSlots)
		{
			int handNumber = 0;
			
			if(!isLeft)
			{
				handNumber = 1;
			}

			if(interacter.heldObjects[handNumber] != null && interacter.heldObjects[handNumber].isCookedIn == FurnishingID)
			{
				Vector2 wherePlace = findOpenSlot();
				if(wherePlace.x != -1)
				{
					if(interacter.thisNPCType != Humanoid.NPCType.Player 
					   && interacter.currentWorkOrder[0].orderer.FurnishingID == FurnishingLibrary.FurnishingID.PrepTable)
					{
						PrepTable station = interacter.currentWorkOrder[0].orderer as PrepTable;
						if(station.orders.Count > 0
						   && station.orders[0].recipeToMake.Ingredients.Count == 1
						   && station.orders[0].recipeToMake.Ingredients.ContainsKey(interacter.heldObjects[handNumber].holdableID))
						{
							station.orders.RemoveAt(0);
						}
                        else if (station.orders.Count > 0
                           && station.orders[0].recipeClone.Ingredients.ContainsKey(FurnishingLibrary.Library.getBakedID(interacter.heldObjects[handNumber].holdableID)))
                        {
                            station.orders[0].recipeClone.Ingredients.Remove(FurnishingLibrary.Library.getBakedID(interacter.heldObjects[handNumber].holdableID));
                        }
                    }


                    bakingSlots[(int)wherePlace.x,(int)wherePlace.y] = interacter.heldObjects[handNumber] as Bakeable;
					bakerSlots[(int)wherePlace.x,(int)wherePlace.y] = interacter;
					interacter.heldObjects[handNumber].gameObject.SetActive(false);
					interacter.heldObjects[handNumber] = null;
					count++;

                    updateCookingChefs();
				}
			}
		}
	}

	Vector2 findOpenSlot()
	{
		for(int y = 0; y < maxY; y++)
		{
			for(int x = 0; x < maxX; x++)
			{
				if(bakingSlots[x,y] == null)
				{
					return new Vector2(x,y);
				}
			}
		}

		return new Vector2(-1,-1);
	}

    void updateCookingChefs()
    {
        chefsCooking.Clear();
        for (int x = 0; x < maxX; x++)
        {
            for(int y = 0; y < maxY; y++)
            if (bakerSlots[x, y] != null && !chefsCooking.Contains(bakerSlots[x,y]))
            {
                chefsCooking.Add(bakerSlots[x,y]);
            }
        }
    }
}
