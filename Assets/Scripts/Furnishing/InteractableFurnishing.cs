﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InteractableFurnishing : Furnishing 
{
	public List<string> interactions = new List<string>();

    public Humanoid nightEmployee;
    public Humanoid dayEmployee;
    Humanoid tempEmployee;

    public Humanoid assignedEmployee
    {
        get
        {
            if(MainMechanics.currentTime != MainMechanics.TimeType.Day && nightEmployee != null)
            {
                return nightEmployee;
            }
            if (MainMechanics.currentTime == MainMechanics.TimeType.Day && dayEmployee != null)
            {
                return dayEmployee;
            }
            return tempEmployee;
        }
        set
        {
            if (value == null)
            {
                if (MainMechanics.currentTime != MainMechanics.TimeType.Day)
                {
                    if(nightEmployee != null)
                    {
                        nightEmployee.removeWorkStation(this);
                    }
                    nightEmployee = value;
                }
                else if (MainMechanics.currentTime == MainMechanics.TimeType.Day)
                {
                    if (dayEmployee != null)
                    {
                        dayEmployee.removeWorkStation(this);
                    }
                    dayEmployee = value;
                }
            }
            else
            {
                if (MainMechanics.currentTime != MainMechanics.TimeType.Day && value.hasNightShift)
                {
                    if (nightEmployee != null)
                    {
                        nightEmployee.removeWorkStation(this);
                    }
                    nightEmployee = value;
                }
                else if (MainMechanics.currentTime == MainMechanics.TimeType.Day && !value.hasNightShift)
                {
                    if (dayEmployee != null)
                    {
                        dayEmployee.removeWorkStation(this);
                    }
                    dayEmployee = value;
                }
                else
                {
                    tempEmployee = value;
                }
            }
        }
    }

    public virtual void DrawMenu(Vector2 UIMousPos, float PickUpScroll)
	{

	}

    public virtual void UpdateDrawMenu(Vector2 UIMousPos)
    {

    }

    /// <summary>
    /// Gets the work order. Returns true if work is complete.
    /// </summary>
    /// <returns><c>true</c>, if work order was gotten, <c>false</c> otherwise.</returns>
    public virtual bool getWorkOrder()
	{
		return true;
	}


	public override void Draw()
	{
		if(liquidHere != null && currentLiquidContainment > 0)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);
			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize/7, halfSize)/2, new Vector2(halfSize/7,halfSize));
			GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidBar);

			drawRect = new Rect(new Vector2(drawRect.x+1, drawRect.y+1+(drawRect.height-2)*(maxLiquidContainment-currentLiquidContainment)/maxLiquidContainment), new Vector2(drawRect.width-2, (drawRect.height-2)*currentLiquidContainment/maxLiquidContainment));
			GUI.color = liquidHere.liquidColor;
			GUI.DrawTexture(drawRect, MainMechanics.mechanics.LiquidMarker);

			GUI.color = Color.white;
		}
	}

	public override void Start ()
	{
		base.Start ();
	}

	public override void Update ()
	{
		base.Update ();
        if(tempEmployee != null && !tempEmployee.gameObject.activeInHierarchy)
        {
            tempEmployee = null;
        }
		if(currentLiquidContainment <= 0 && liquidHere != null)
		{
			liquidHere = null;
			currentLiquidContainment = 0;
		}
	}

	public override void getInteraction(Humanoid interacter, int actionNumber)
	{

	}

	public override void DisplayOptions(Vector2 MousPos)
	{
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUIContent textSizer = new GUIContent("Xy");
		Vector2 textSize = labelStyle.CalcSize(textSizer);

		Vector2 midPoint = Camera.main.WorldToScreenPoint(transform.position);
		midPoint.y = Screen.height-midPoint.y;

		if(canAssignWorkers)
		{
			Rect employButton = new Rect(midPoint - new Vector2(Screen.height/20, Screen.height/5)/2-new Vector2(0, textSize.y*3), new Vector2(Screen.height/20, Screen.height/20));
			GUI.DrawTexture(employButton, MainMechanics.mechanics.GUIAssignEmployeeButton);
			if(employButton.Contains (MousPos) && Input.GetMouseButtonDown(0))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.AssignWorkerMenu;
				MainMechanics.mechanics.employeeFurnish = this;
			}
		}


		for(int i = 0; i < interactions.Count; i++)
		{
			Rect textRect = new Rect(midPoint-new Vector2(Screen.height/5, textSize.y)/2- new Vector2(0, textSize.y*3)+new Vector2(0, textSize.y)*i, new Vector2(Screen.height/5, textSize.y));
			GUI.DrawTexture(textRect, Texture2D.whiteTexture);
			GUI.color = Color.black;
			GUI.Label(textRect, interactions[i]);
			GUI.color = Color.white;

			if(textRect.Contains(MousPos) && Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0)
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				getInteraction(MainMechanics.mechanics.Innkeeper, i);
			}
		}
	}

	public virtual void PourLiquidIntoContainer(Humanoid interacter, Holdable container)
	{
		if(liquidHere != null && currentLiquidContainment > 0)
		{
			if(container.currentLiquidContainment == 0)
			{
				if(container.maxLiquidContainment <= currentLiquidContainment)
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					currentLiquidContainment -= container.maxLiquidContainment;
					container.liquidHere = liquidHere;
				}
				else
				{
					container.currentLiquidContainment = currentLiquidContainment;
					container.liquidHere = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
				}
			}
			else if(container.liquidHere.liquidID == liquidHere.liquidID)
			{
				int ammountEmpty = container.maxLiquidContainment - container.currentLiquidContainment;

				if(ammountEmpty <= currentLiquidContainment)
				{
					container.currentLiquidContainment = container.maxLiquidContainment;
					currentLiquidContainment -= ammountEmpty;
					container.liquidHere = liquidHere;
				}
				else
				{
					container.currentLiquidContainment += currentLiquidContainment;
					container.liquidHere = liquidHere;
					currentLiquidContainment = 0;
					liquidHere = null;
				}
			}
		}
	}

    public virtual void prepareRemoval()
    {
        if (nightEmployee != null)
        {
            nightEmployee.removeWorkStation(this);
        }
        if (dayEmployee != null)
        {
            dayEmployee.removeWorkStation(this);
        }
        if (tempEmployee != null)
        {
            tempEmployee.removeWorkStation(this);
        }
    }

    public virtual void UnasignWorker(Humanoid worker)
    {
        if(tempEmployee == worker)
        {
            tempEmployee = null;
        }
        if(nightEmployee == worker)
        {
            nightEmployee = null;
        }
        if(dayEmployee == worker)
        {
            dayEmployee = null;
        }
    }

    public virtual bool CanFillWithLiquid(FurnishingLibrary.HoldableID ID)
    {
        if(ID == FurnishingLibrary.HoldableID.Error)
        {
            return false;
        }
        if(liquidHere == null || liquidHere.liquidID == ID)
        {
            return true;
        }
        return false;
    }
}
