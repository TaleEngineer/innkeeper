﻿using UnityEngine;
using System.Collections;

public class Bed : InteractableFurnishing
{
	SpriteRenderer spriteRenderer;
	public Sprite clean;
	public Sprite dirty;
	public bool isDirty;

	public float sleepTimer = 0;

	public Humanoid occupant;
	public bool called = false;

	public override void Start ()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		roomInsideOf = MainMechanics.mechanics.mainGrid[(int)transform.position.x, (int)transform.position.y].roomPieceHere.parentRoom;

		if(!roomInsideOf.bedsHere.Contains(this))
		{
			roomInsideOf.bedsHere.Add(this);
		}
	}

	public override bool getWorkOrder ()
	{
		if(isDirty && occupant == null)
		{
			WorkOrder workOrder = new WorkOrder();
			workOrder.Set(this, 0, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(workOrder);
			return false;
		}
		return true;
	}

	public override void Update ()
	{
        base.Update();
		if(sleepTimer > 0)
		{
			sleepTimer -= Time.deltaTime;
		}
		else if(occupant != null && reachableNodes.Count > 0)
		{
			occupant.hunger += 30;
			occupant.thirst += 30;
			occupant.tiredness = 0f;

			occupant.currentHP = occupant.currentMaxHP;

            if(occupant.thisNPCType == Humanoid.NPCType.Adventurer)
            {
                MainMechanics.mechanics.changeMoney(10, mainTransform.position);
            }

			occupant.rekindleWants();
			occupant.bedRestingIn = null;
			isDirty = true;
			occupant.transform.position = reachableNodes[0].transform.position;
			occupant.transform.rotation = new Quaternion(0,0,0,0);
			occupant = null;
			called = false;
		}

		if(isDirty && spriteRenderer.sprite != dirty)
		{
			spriteRenderer.sprite = dirty;
		}
		else if(!isDirty && spriteRenderer != clean)
		{
			spriteRenderer.sprite = clean;
		}
	}

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			interacter.startWorking(5f, actionNumber, this, EmployeeSkill.WorkSkill.Cleaning, 4);
		}
		if(actionNumber == 1 && occupant == null)
		{
			called = true;
			interacter.bedRestingIn = this;
			occupant = interacter;
            interacter.clearPath();
			interacter.transform.position = transform.position;
			interacter.transform.rotation = transform.rotation;
			sleepTimer = 480f*MainMechanics.mechanics.secondsToMinute;
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		if(choice == 0)
		{
			isDirty = false;
		}

	}
}
