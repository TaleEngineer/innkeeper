﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Table : InteractableFurnishing
{
	public BoxCollider2D northBox;
	public BoxCollider2D southBox;
	public BoxCollider2D westBox;
	public BoxCollider2D eastBox;

	Vector2 mousPos;
	
	static Rect tableAreaRect = new Rect(0,0,Screen.width*2/3,Screen.height);
	static Rect ordersArea = new Rect(tableAreaRect.width, 0, Screen.width-tableAreaRect.width, Screen.height);

	float mainScroll = 0;

	public Humanoid[] customersHere = new Humanoid[4];

	public int peopleHere = 0;

	public List<Holdable> thingsThatNeedToBeCleaned = new List<Holdable>();
	public List<Order> ordersHere = new List<Order>();
	public bool called = false;

    public float fameToGive = 0;
    public int moneyToGive = 0;

	public override void getInteraction (Humanoid interacter, int actionNumber)
	{
		if(actionNumber == 0)
		{
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.FurnishingMenu;
			MainMechanics.mechanics.employeeFurnish = this;
		}
		else if(actionNumber == 1)//SIT DOWN
		{
			sitInChair (interacter);
		}
		else if(actionNumber == -1)//STAND UP
		{
			standUpFromChair(interacter);
		}
		else if(actionNumber == 2)//GET DRINK ORDERS
		{
			interacter.startWorking(peopleHere*0.5f, actionNumber, this, EmployeeSkill.WorkSkill.Waitering, 2);
		}
		else if(actionNumber == 3)//GET FOOD ORDERS
		{
			interacter.startWorking(peopleHere*0.5f, actionNumber, this, EmployeeSkill.WorkSkill.Waitering, 2);
		}
		else if(actionNumber == 4)
		{
			CleanTable(interacter);
		}
		else if(actionNumber == 1000)
		{
			placeHoldableOnTable(true, interacter);
		}
		else if(actionNumber == -1000)
		{
			placeHoldableOnTable(false, interacter);
		}
	}

	public override void CompleteInteract (Humanoid interacter, int choice)
	{
		if(choice == 2)//GET DRINK ORDER
		{
			getDrinkOrders(interacter);
		}
		else if(choice == 3)//GET FOOR ORDER
		{
			getFoodOrders(interacter);
		}
	}



	void placeHoldableOnTable (bool left, Humanoid interacter)
	{
		Holdable toGive;
		if (left) 
		{
			toGive = interacter.heldObjects[0];
		} 
		else 
		{
			toGive = interacter.heldObjects[1];
		}

		if (toGive != null) 
		{
			for (int i = 0; i < 4; i++) 
			{
				if (customersHere [i] != null) 
				{
					if (toGive.liquidHere != null) 
					{
						if (customersHere [i].wantedDrink == toGive.liquidHere.liquidID) 
						{
							if (customersHere [i].heldObjects[0] == null
								&& (customersHere [i].heldObjects[1] == null 
								|| (customersHere [i].heldObjects[1].liquidHere != null && customersHere [i].wantedDrink != customersHere [i].heldObjects[1].liquidHere.liquidID))) 
							{
								customersHere [i].heldObjects[0] = toGive;
								if (left) 
								{
									interacter.heldObjects[0] = null;
								} 
								else 
								{
									interacter.heldObjects[1] = null;
								}
								break;
							} 
							else if (customersHere [i].heldObjects[1] == null
								&& (customersHere [i].heldObjects[0] == null 
								|| (customersHere [i].heldObjects[0].liquidHere != null && customersHere [i].wantedDrink != customersHere [i].heldObjects[0].liquidHere.liquidID))) 
							{
								customersHere [i].heldObjects[1] = toGive;
								if (left) 
								{
									interacter.heldObjects[0] = null;
								} 
								else 
								{
									interacter.heldObjects[1] = null;
								}
								break;
							}
						}
					} 
					else 
					{
						if (customersHere [i].wantedFood == toGive.holdableID) 
						{
							if (customersHere [i].heldObjects[0] == null
								&& (customersHere [i].heldObjects[1] == null 
								|| customersHere [i].wantedFood != customersHere [i].heldObjects[1].holdableID)) 
							{
								customersHere [i].heldObjects[0] = toGive;
								if (left) 
								{
									interacter.heldObjects[0] = null;
								} 
								else 
								{
									interacter.heldObjects[1] = null;
								}
								break;
							} 
							else if (customersHere [i].heldObjects[1] == null
								&& (customersHere [i].heldObjects[0] == null 
								|| customersHere [i].wantedFood != customersHere [i].heldObjects[0].holdableID)) 
							{
								customersHere [i].heldObjects[1] = toGive;
								if (left) 
								{
									interacter.heldObjects[0] = null;
								} 
								else 
								{
									interacter.heldObjects[1] = null;
								}
								break;
							}
						}
					}
				}
			}
		}
	}

	public void PutHoldablesOnTable(Humanoid interacter)
	{
		if(interacter.heldObjects[0] != null 
		   && interacter.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup
		   && interacter.heldObjects[0].currentLiquidContainment <= 0)
		{
			thingsThatNeedToBeCleaned.Add(interacter.heldObjects[0]);
			interacter.heldObjects[0].gameObject.SetActive(false);
			interacter.heldObjects[0] = null;
		}
		if(interacter.heldObjects[1] != null 
		   && interacter.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup 
		   && interacter.heldObjects[1].currentLiquidContainment <= 0)
		{
			thingsThatNeedToBeCleaned.Add(interacter.heldObjects[1]);
			interacter.heldObjects[1].gameObject.SetActive(false);
			interacter.heldObjects[1] = null;
		}
	}

	/// <summary>
	/// Puts a holdable that needs to be cleared from the table into an empty hand.
	/// </summary>
	/// <param name="interacter">Interacter.</param>
	public void CleanTable(Humanoid interacter)
	{
		if(thingsThatNeedToBeCleaned.Count > 0)
		{
			if(interacter.heldObjects[0] == null)
			{
				interacter.heldObjects[0] = thingsThatNeedToBeCleaned[0];
				interacter.heldObjects[0].gameObject.SetActive(true);
				thingsThatNeedToBeCleaned.RemoveAt(0);
			}
			else if(interacter.heldObjects[1] == null)
			{
				interacter.heldObjects[1] = thingsThatNeedToBeCleaned[0];
				interacter.heldObjects[1].gameObject.SetActive(true);
				thingsThatNeedToBeCleaned.RemoveAt(0);
			}
		}
	}


	void checkScroll()
	{
		int yLength;
		
		if(MainMechanics.mechanics.Innkeeper.takenOrders.Count%2 == 0)
		{
			yLength = ((int)(MainMechanics.mechanics.Innkeeper.takenOrders.Count/2))*(int)(ordersArea.width/2);
		}
		else
		{
			yLength = ((int)(MainMechanics.mechanics.Innkeeper.takenOrders.Count/2)+1)*(int)(ordersArea.width/2);
		}
		
		if(mainScroll < 0)
		{
			mainScroll = 0;
		}
		if(ordersArea.height >= yLength)
		{
			mainScroll = 0;
		}
		else if(mainScroll > yLength - ordersArea.height)
		{
			mainScroll = yLength - ordersArea.height;
		}
	}

	void scrollToBot()
	{
		int yLength = ((int)(MainMechanics.mechanics.Innkeeper.takenOrders.Count/2)+1)*(int)(ordersArea.width/2);
		mainScroll = yLength - ordersArea.height;
		checkScroll();
	}

	public override void Update ()
	{
		base.Update ();

		
		tableAreaRect = new Rect(0,0,Screen.width*2/3,Screen.height);
		ordersArea = new Rect(tableAreaRect.width, 0, Screen.width-tableAreaRect.width, Screen.height);

		if(ordersArea.Contains(mousPos) && Input.GetAxis("Mouse ScrollWheel") != 0)
		{
			mainScroll -= Input.GetAxis("Mouse ScrollWheel")*(Screen.height);

			checkScroll();
		}
	}

	/// <summary>
	/// Gets the drink orders and adds them to the interacter's takenOrders list and table's ordersHere list.
	/// </summary>
	/// <param name="interacter">Interacter.</param>
	void getDrinkOrders(Humanoid interacter)
	{
		Order toAdd = new Order();
        FurnishingLibrary.HoldableID[] orders = new FurnishingLibrary.HoldableID[4];
        bool addedSomething = false;
        if (customersHere[0] != null)
        {
            for (int i = 0; i < customersHere[0].partyApartOf.Members.Count; i++)
            {
                addedSomething = true;
                orders[i] = customersHere[0].partyApartOf.Members[i].wantedDrink;
                //orders.Add(customersHere[i].wantedDrink);//REMOVE THIS?
                if (customersHere[0].partyApartOf.Members[i].wantedDrink != FurnishingLibrary.HoldableID.Error)
                {
                    customersHere[0].partyApartOf.Members[i].SetDrinkOrderTimeMultiplier(1);
                }
                customersHere[0].partyApartOf.Members[i].timeSinceOrderedDrink = 0.000001f;
            }
        }
		if(addedSomething)
		{
			toAdd.Set(interacter, orders, this, true, customersHere[0].partyApartOf);
			interacter.takenOrders.Add(toAdd);
			ordersHere.Add(toAdd);
			if(interacter.thisNPCType == Humanoid.NPCType.Player)
			{
				scrollToBot();
			}
		}

        if(interacter.thisNPCType != Humanoid.NPCType.Player)
        {
            WorkOrder toAssign = new WorkOrder();
            toAssign.Set(FurnishingLibrary.FurnishingID.Bar, 3, interacter, this);
            interacter.currentWorkOrder.Add(toAssign);
        }
	}

	/// <summary>
	/// Gets the food orders and adds them to the interacter's takenOrders list and table's ordersHere list.
	/// </summary>
	/// <param name="interacter">Interacter.</param>
	void getFoodOrders(Humanoid interacter)
	{
		Order toAdd = new Order();
        bool addedSomething = false;
        FurnishingLibrary.HoldableID[] orders = new FurnishingLibrary.HoldableID[4];
        if (customersHere[0] != null)
        {
            for (int i = 0; i < customersHere[0].partyApartOf.Members.Count; i++)
            {
                addedSomething = true;
                orders[i] = customersHere[0].partyApartOf.Members[i].wantedFood;
                //orders.Add(customersHere[i].wantedFood);
                if (customersHere[0].partyApartOf.Members[i].wantedFood != FurnishingLibrary.HoldableID.Error)
                {
                    customersHere[0].partyApartOf.Members[i].SetFoodOrderTimeMultiplier(1);
                }
                customersHere[0].partyApartOf.Members[i].timeSinceOrderedFood = 0.000001f;
            }
        }
		if(addedSomething)
		{
			toAdd.Set(interacter, orders, this, false, customersHere[0].partyApartOf);
			interacter.takenOrders.Add(toAdd);
			ordersHere.Add(toAdd);
			if(interacter.thisNPCType == Humanoid.NPCType.Player)
			{
				scrollToBot();
			}
		}

        if (interacter.thisNPCType != Humanoid.NPCType.Player)
        {
            WorkOrder toAssign = new WorkOrder();
            toAssign.Set(FurnishingLibrary.FurnishingID.KitchenWindow, 1, interacter, this);
            interacter.currentWorkOrder.Add(toAssign);
        }
    }

	void sitInChair(Humanoid interacter)
	{
		for(int i = 0; i < 4; i++)
		{
			if(customersHere[i] == null)
			{
				customersHere[i] = interacter;
				Vector2 dir;

				if(i == 0)
				{
					dir = new Vector2(0,1);
					northBox.enabled = true;
				}
				else if(i == 1)
				{
					dir = new Vector2(1,0);
					eastBox.enabled = true;
				}
				else if(i == 2)
				{
					dir = new Vector2(0,-1);
					southBox.enabled = true;
				}
				else if(i == 3)
				{
					dir = new Vector2(-1,0);
					westBox.enabled = true;
				}
				else
				{
					dir = new Vector2(0,0);
				}

				interacter.truePosition = mainTransform.TransformPoint(dir);
				//interacter.transform.position = (Vector2)transform.position+dir;
				MainMechanics.mechanics.setGridPathing((Vector2)mainTransform.position+new Vector2(-2,-2), (Vector2)mainTransform.position+new Vector2(2,2));
				interacter.tableSittingAt = this;
                MainMechanics.mechanics.MoveQueue.Remove(interacter);
                if(interacter.wantedDrink == FurnishingLibrary.HoldableID.HasWantOfDrink && roomInsideOf.drinksAvailable.Count > 0)
                {
                    interacter.setDrinkWant(roomInsideOf.drinksAvailable);
                }
				peopleHere++;
				break;
			}
		}
	}

	void standUpFromChair(Humanoid interacter)
	{
		for(int i = 0; i < 4; i++)
		{
			if(customersHere[i] == interacter)
            {
                if (i == 0)
				{
					northBox.enabled = false;
				}
				else if(i == 1)
				{
					eastBox.enabled = false;
				}
				else if(i == 2)
				{
					southBox.enabled = false;
				}
				else if(i == 3)
				{
					westBox.enabled = false;
				}
				//interacter.transform.position = (Vector2)transform.position+dir;
				MainMechanics.mechanics.setGridPathing((Vector2)mainTransform.position+new Vector2(-2,-2), (Vector2)mainTransform.position+new Vector2(2,2));
				interacter.tableSittingAt = null;
				peopleHere--;

                customersHere[i] = null;

                break;
			}
		}
		if(peopleHere == 0)
		{
			called = false;

            if (moneyToGive > 0)
            {
                MainMechanics.mechanics.changeMoney(moneyToGive, mainTransform.position);
                moneyToGive = 0;
            }
            if (fameToGive != 0)
            {
                MainMechanics.mechanics.changeFame((int)fameToGive, mainTransform.position);
                fameToGive = 0;
            }
        }
    }


	public override bool getWorkOrder ()
	{
		bool wantDrink = false;
		bool wantFood = false;
		for(int i = 0; i < peopleHere; i++)
		{
			if(customersHere[i] != null)
			{
				if(customersHere[i].wantedFood != FurnishingLibrary.HoldableID.Error)
				{
					wantFood = true;
				}
				if(customersHere[i].wantedDrink != FurnishingLibrary.HoldableID.Error && customersHere[i].wantedDrink != FurnishingLibrary.HoldableID.HasWantOfDrink)
				{
					wantDrink = true;
				}
			}
			if(wantFood && wantDrink)
			{
				break;
			}
		}



		WorkOrder toAssign = new WorkOrder();
		if(assignedEmployee.heldObjects[0] != null
		   && assignedEmployee.heldObjects[0].holdableID == FurnishingLibrary.HoldableID.Cup 
		   && assignedEmployee.heldObjects[0].liquidHere == null)
		{
			toAssign.Set(FurnishingLibrary.FurnishingID.Bar, 1000, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(toAssign);
			return false; 
		}
		if(assignedEmployee.heldObjects[1] != null
		   && assignedEmployee.heldObjects[1].holdableID == FurnishingLibrary.HoldableID.Cup 
		   && assignedEmployee.heldObjects[1].liquidHere == null)
		{
			toAssign.Set(FurnishingLibrary.FurnishingID.Bar, -1000, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(toAssign);
			return false; 
		}
		if(peopleHere <= 0 && thingsThatNeedToBeCleaned.Count > 0)
		{
			toAssign.Set(this, 4, assignedEmployee, this);
			assignedEmployee.currentWorkOrder.Add(toAssign);
			return false; 
		}
		if(customersHere[0] != null && customersHere[0].partyApartOf.Members.Count == peopleHere
		        &&((customersHere[0].timeSinceOrderedDrink <= 0 && wantDrink )|| (customersHere[0].timeSinceOrderedFood <= 0 && wantFood)))//Get Orders
		{
			if(customersHere[0].timeSinceOrderedDrink <= 0 && wantDrink)
			{
				toAssign.Set(this, 2, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(toAssign);
				return false;
			}
			else if(customersHere[0].timeSinceOrderedFood <= 0 && wantFood)
			{
				toAssign.Set(this, 3, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(toAssign);
				return false;
			}
		}
		for(int i = 0; i < ordersHere.Count; i++)
		{
			if(!assignedEmployee.takenOrders.Contains(ordersHere[i]))
			{
				assignedEmployee.takenOrders.Add(ordersHere[i]);
			}
			//RETURN HERE
			if(!ordersHere[i].hasBeenDelievered && !ordersHere[i].isDrink && MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.KitchenWindow))//Turn in orders
			{
				toAssign.Set(FurnishingLibrary.FurnishingID.KitchenWindow, 1, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(toAssign);
				return false;
			}
			else if(!ordersHere[i].hasBeenDelievered && ordersHere[i].isDrink && MainMechanics.mechanics.hasFurnishing(FurnishingLibrary.FurnishingID.Bar))//Turn in orders
			{
				toAssign.Set(FurnishingLibrary.FurnishingID.Bar, 3, assignedEmployee, this);
				assignedEmployee.currentWorkOrder.Add(toAssign);
				return false;
			}
		}
		return true;
	}
	
	public override void Draw ()
	{
		base.Draw ();
		
		for(int i = 0; i < thingsThatNeedToBeCleaned.Count; i++)
		{
			Vector2 midPoint = Camera.main.WorldToScreenPoint(mainTransform.position);
			midPoint = new Vector2(midPoint.x, Screen.height-midPoint.y);
			
			float halfSize = Screen.height/(Camera.main.orthographicSize*2);
			
			Rect drawRect = new Rect(midPoint-new Vector2(halfSize/7, halfSize)/2, new Vector2(halfSize/7,halfSize));
			
			//float percentOfCircle = (float)i/(float)thingsThatNeedToBeCleaned.Count;
			
			float rotation = mainTransform.rotation.eulerAngles.z;
			
			if((int)rotation == 90 || (int)rotation == 270)
			{
				rotation = -mainTransform.rotation.eulerAngles.z;
			}

			float percentOfCircle;
			if(thingsThatNeedToBeCleaned.Count <= 4)
			{
				percentOfCircle = (float)i/4f;
			}
			else
			{
				percentOfCircle = (float)i/thingsThatNeedToBeCleaned.Count;
			}
			Vector2 center = new Vector2(midPoint.x + halfSize/2*(float)Math.Cos((percentOfCircle+(rotation-90)/360)*2*Math.PI), 
			                             midPoint.y + halfSize/2*(float)Math.Sin((percentOfCircle+(rotation-90)/360)*2*Math.PI));
			
			Rect itemDrawRect = new Rect(center.x-halfSize/2, center.y-halfSize/2, halfSize, halfSize);
			GUI.DrawTexture(itemDrawRect, thingsThatNeedToBeCleaned[i].holdableTexture);
		}
	}
	
	public override void DrawMenu (Vector2 UIMousPos, float PickUpScroll)
	{
		bool wantDrink = false;
		bool wantFood = false;
		for(int i = 0; i < peopleHere; i++)
		{
			if(customersHere[i].wantedFood != FurnishingLibrary.HoldableID.Error)
			{
				wantFood = true;
			}
			if(customersHere[i].wantedDrink != FurnishingLibrary.HoldableID.Error && customersHere[i].wantedDrink != FurnishingLibrary.HoldableID.HasWantOfDrink)
			{
				wantDrink = true;
			}

            if(wantDrink && wantFood)
            {
                break;
            }
		}


		mousPos = UIMousPos;
		var labelStyle = GUI.skin.GetStyle ("Label");
		labelStyle.fontSize = Screen.height/40;
		labelStyle.alignment = TextAnchor.MiddleCenter;
		GUIContent textSizer = new GUIContent("Xy");
		Vector2 textSize = labelStyle.CalcSize(textSizer);
		
		GUI.color = Color.black;
		GUI.DrawTexture(tableAreaRect, Texture2D.whiteTexture);
		GUI.DrawTexture(ordersArea, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(tableAreaRect.x+1, tableAreaRect.y+1, tableAreaRect.width-2, tableAreaRect.height-2), Texture2D.whiteTexture);
		GUI.DrawTexture(new Rect(ordersArea.x+1, tableAreaRect.y+1, tableAreaRect.width-2, tableAreaRect.height-2), Texture2D.whiteTexture);
		
		Rect table = new Rect(tableAreaRect.x+tableAreaRect.width/2-tableAreaRect.height/4, tableAreaRect.y+tableAreaRect.height/2-tableAreaRect.height/4, tableAreaRect.height/2, tableAreaRect.height/2);
		GUI.DrawTexture(table, GetComponent<SpriteRenderer>().sprite.texture);
		
		for(int i = 0; i < thingsThatNeedToBeCleaned.Count; i++)
		{
			float rotation = mainTransform.rotation.eulerAngles.z;
			
			if((int)rotation == 90 || (int)rotation == 270)
			{
				rotation = -mainTransform.rotation.eulerAngles.z;
			}
			
			float percentOfCircle;
			if(thingsThatNeedToBeCleaned.Count <= 4)
			{
				percentOfCircle = (float)i/4f;
			}
			else
			{
				percentOfCircle = (float)i/thingsThatNeedToBeCleaned.Count;
			}
			Vector2 center = new Vector2(table.center.x + table.width/7*(float)Math.Cos((percentOfCircle+(rotation-90)/360)*2*Math.PI), 
			                             table.center.y + table.width/7*(float)Math.Sin((percentOfCircle+(rotation-90)/360)*2*Math.PI));
			
			Rect itemDrawRect = new Rect(center.x-table.height/6, center.y-table.height/6, table.height/3, table.height/3);
			GUI.DrawTexture(itemDrawRect, thingsThatNeedToBeCleaned[i].holdableTexture);
		}
		
		
		Rect cleanTableButton = new Rect(0, table.y, table.height/5, table.height/5);
		GUI.color = Color.black;
		GUI.DrawTexture(cleanTableButton, Texture2D.whiteTexture);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(cleanTableButton.x+1, cleanTableButton.y+1, cleanTableButton.width-2, cleanTableButton.height-2), Texture2D.whiteTexture);
		GUI.color = Color.black;
		GUI.Label(new Rect(cleanTableButton.x+1, cleanTableButton.y+1, cleanTableButton.width-2, cleanTableButton.height-2), "Clear Table");
		
		GUI.color = Color.white;
		for(int i = 0; i < 4; i++)
		{
			if(customersHere[i] != null)
			{
				Texture2D characterToDraw = customersHere[i].GetComponent<SpriteRenderer>().sprite.texture;
				
				if(i == 0)//North
				{
					Rect characterBox = new Rect(table.x+table.width/3, table.y, table.height/3, table.height/3);
					GUI.DrawTexture(characterBox, characterToDraw);
				}
				else if(i == 1)//East
				{
					Rect characterBox = new Rect(table.x+table.width*2/3, table.y+table.height/3, table.height/3, table.height/3);
					GUI.DrawTexture(characterBox, characterToDraw);
				}
				else if(i == 2)//South
				{
					Rect characterBox = new Rect(table.x+table.width/3, table.y+table.height*2/3, table.height/3, table.height/3);
					GUI.DrawTexture(characterBox, characterToDraw);
				}
				else if(i == 3)//West
				{
					Rect characterBox = new Rect(table.x, table.y+table.height/3, table.height/3, table.height/3);
					GUI.DrawTexture(characterBox, characterToDraw);
				}
				else//Error
				{
					
				}
			}
		}
		bool canOrderFood = false;
		bool canOrderDrink = false;
		Rect getDrinkOrdersRect = new Rect(table.x, table.y+table.height, table.width/2, textSize.y+2);
		Rect getFoodOrdersRect = new Rect(getDrinkOrdersRect.x+getDrinkOrdersRect.width, getDrinkOrdersRect.y, getDrinkOrdersRect.width, getDrinkOrdersRect.height);
		GUI.color = Color.black;
		GUI.DrawTexture(getDrinkOrdersRect, Texture2D.whiteTexture);
		GUI.DrawTexture(getFoodOrdersRect, Texture2D.whiteTexture);
		if(customersHere[0] != null && customersHere[0].partyApartOf.Members.Count == peopleHere && customersHere[0].timeSinceOrderedDrink <= 0 && wantDrink)
		{
			GUI.color = Color.white;
			canOrderDrink = true;
		}
		else
		{
			GUI.color = Color.gray;
		}
		GUI.DrawTexture(new Rect(getDrinkOrdersRect.x+1, getDrinkOrdersRect.y+1, getDrinkOrdersRect.width-2, getDrinkOrdersRect.height-2), Texture2D.whiteTexture);
		if(customersHere[0] != null && customersHere[0].partyApartOf.Members.Count == peopleHere && customersHere[0].timeSinceOrderedFood <= 0 && wantFood)
		{
			GUI.color = Color.white;
			canOrderFood = true;
		}
		else
		{
			GUI.color = Color.gray;
		}
		GUI.DrawTexture(new Rect(getFoodOrdersRect.x+1, getFoodOrdersRect.y+1, getFoodOrdersRect.width-2, getFoodOrdersRect.height-2), Texture2D.whiteTexture);
		GUI.color = Color.black;
		GUI.Label(new Rect(getDrinkOrdersRect.x+1, getDrinkOrdersRect.y+1, getDrinkOrdersRect.width-2, getDrinkOrdersRect.height-2), "Get Drink Orders");
		GUI.Label(new Rect(getFoodOrdersRect.x+1, getFoodOrdersRect.y+1, getFoodOrdersRect.width-2, getFoodOrdersRect.height-2), "Get Food Orders");
		
		Rect leftHand = new Rect(Screen.width/2-Screen.height/8, Screen.height-Screen.height/6, Screen.height/8, Screen.height/6);
		Rect rightHand = new Rect(Screen.width/2, leftHand.y, leftHand.width, leftHand.height);
		
		GUI.color = new Color(0, 0.231f, 0.345f);
		GUI.DrawTexture(leftHand, Texture2D.whiteTexture);
		GUI.DrawTexture(rightHand, Texture2D.whiteTexture);
		GUI.color = new Color(0, 0.486f, 0.729f);
		GUI.DrawTexture(new Rect(leftHand.x+1, leftHand.y+1, leftHand.width-2, leftHand.height-2), Texture2D.whiteTexture);
		GUI.DrawTexture(new Rect(rightHand.x+1, rightHand.y+1, rightHand.width-2, rightHand.height-2), Texture2D.whiteTexture);
		
		GUI.color = Color.white;
		if(MainMechanics.mechanics.Innkeeper.heldObjects[0] != null)
		{
			Rect item = new Rect(leftHand.x, leftHand.y+leftHand.height/2-leftHand.width/2, leftHand.width, leftHand.width);
			GUI.DrawTexture(item, MainMechanics.mechanics.Innkeeper.heldObjects[0].holdableTexture);
			if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && leftHand.Contains(UIMousPos))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				getInteraction(MainMechanics.mechanics.Innkeeper, 1000);
			}
		}
		if(MainMechanics.mechanics.Innkeeper.heldObjects[1] != null)
		{
			Rect item = new Rect(rightHand.x, rightHand.y+rightHand.height/2-rightHand.width/2, rightHand.width, rightHand.width);
			GUI.DrawTexture(item, MainMechanics.mechanics.Innkeeper.heldObjects[1].holdableTexture);
			if(Input.GetMouseButtonDown(0) && MainMechanics.mechanics.currentButtonWait <= 0 && rightHand.Contains(UIMousPos))
			{
				MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
				getInteraction(MainMechanics.mechanics.Innkeeper, -1000);
			}
		}





		int x = 0;
		int y = 0;
		for(int i = 0; i < MainMechanics.mechanics.Innkeeper.takenOrders.Count; i++)
		{
			Rect order = new Rect(ordersArea.x+ordersArea.width/2*x,ordersArea.y+ordersArea.width/2*y-mainScroll, ordersArea.width/2, ordersArea.width/2);
			GUI.color = Color.black;
			GUI.DrawTexture(new Rect(order.x-1, order.y-1, order.width+2, order.height+2), Texture2D.whiteTexture);
			if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom == this)
			{
				GUI.color = Color.white;
			}
			else
			{
				GUI.color = Color.gray;
			}
			GUI.DrawTexture(order, Texture2D.whiteTexture);
			Rect tableNameRect = new Rect(order.x+order.width/20, order.y+order.height/20, order.width*9/10, textSize.y);
			GUI.color = Color.black;
			//GUI.DrawTexture(tableNameRect, Texture2D.whiteTexture);
			GUI.Label(tableNameRect, MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom.name);
			//GUI.Label(tableNameRect, orders[i].orderedFrom.name);
			Rect criteriaArea = new Rect(tableNameRect.x, tableNameRect.y+tableNameRect. height,tableNameRect.width,  order.height*14/20);
			GUI.DrawTexture(criteriaArea, Texture2D.whiteTexture);
			GUI.color = Color.white;
			GUI.DrawTexture(new Rect(criteriaArea.x+1, criteriaArea.y+1, criteriaArea.width-2, criteriaArea.height-2), Texture2D.whiteTexture);
			int inX = 0;
			int inY = 0;
			
			for(int four = 0; four < 4; four++)
			{
				Rect inBox = new Rect(criteriaArea.x+(criteriaArea.width/2)*inX, criteriaArea.y+(criteriaArea.height/2)*inY, criteriaArea.width/2,criteriaArea.height/2);
				GUI.color = Color.black;
				GUI.DrawTexture(inBox, Texture2D.whiteTexture);
				if(four < MainMechanics.mechanics.Innkeeper.takenOrders[i].orderers.Members.Count)
				{
					if(MainMechanics.mechanics.Innkeeper.takenOrders[i].isDrink)
					{
						if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderers.Members[four].wantedDrink == FurnishingLibrary.HoldableID.Error)
						{
							GUI.color = Color.green;
						}
						else
						{
							if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom == this)
							{
								GUI.color = Color.white;
							}
							else
							{
								GUI.color = Color.gray;
							}
						}
					}
					else
					{
						if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderers.Members[four].wantedFood == FurnishingLibrary.HoldableID.Error)
						{
							GUI.color = Color.green;
						}
						else
						{
							if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom == this)
							{
								GUI.color = Color.white;
							}
							else
							{
								GUI.color = Color.gray;
							}
						}
					}
				}
				else
				{
					if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom == this)
					{
						GUI.color = Color.white;
					}
					else
					{
						GUI.color = Color.gray;
					}
				}
				GUI.DrawTexture(new Rect(inBox.x+1, inBox.y+1, inBox.width-2, inBox.height-2), Texture2D.whiteTexture);
				if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedFrom == this)
				{
					GUI.color = Color.white;
				}
				else
				{
					GUI.color = Color.gray;
				}
				if(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedHoldables[four] != FurnishingLibrary.HoldableID.Error)
				{
					GUI.DrawTexture(inBox, FurnishingLibrary.Library.getHoldablePrefab(MainMechanics.mechanics.Innkeeper.takenOrders[i].orderedHoldables[four]).holdableTexture);
				}
				
				if(inX == 1)
				{
					inY++;
					inX = 0;
				}
				else
				{
					inX++;
				}
			}
			
			if(x < 1)
			{
				x++;
			}
			else
			{
				x = 0;
				y++;
			}
		}



		if(canOrderDrink && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && getDrinkOrdersRect.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			getInteraction(MainMechanics.mechanics.Innkeeper, 2);
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
		}
		else if(canOrderFood && MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && getFoodOrdersRect.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			getInteraction(MainMechanics.mechanics.Innkeeper, 3);
			MainMechanics.mechanics.currentScreenState = MainMechanics.screenState.Normal;
		}
		else if(MainMechanics.mechanics.currentButtonWait <= 0 && Input.GetMouseButtonDown(0) && cleanTableButton.Contains(UIMousPos))
		{
			MainMechanics.mechanics.currentButtonWait = MainMechanics.mechanics.buttonWaitAmmount;
			CleanTable(MainMechanics.mechanics.Innkeeper);
		}
	}
}
